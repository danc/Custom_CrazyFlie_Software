#ifndef CRAZYFLIE_GROUND_STATION_H_
#define CRAZYFLIE_GROUND_STATION_H_

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <string>

#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"
#include "computations/LocalizationLogger.h"

#include <quat.h>

#include "Bridge.h"

extern uint8_t exitProgram;

extern Bridge *bridge;

// The lowest battery level to fly with
#define LOW_BATTERY_LEVEL	3.2

#define USE_BRIDGE 1

#define USE_LOCALIZATION

#define USE_VRPN 1
#define USE_KEYBOARD 1
#define USE_PRINTOUT 0
#define USE_LOGGING 0
#define USE_BASIC_LOGGING 0
#define USE_PR_YAW_CORRECT 0	//DO NOT NEED (NOW IMPLEMENTED ON FIRMWARE)
#define USE_HAND 0

extern LocalizationLogger *compLogger;

void query_vrpn();

/**
 * This function is registered as the handler for when Ctrl-C is pressed
 */
void ctrlc_handler(int sig);

#endif
