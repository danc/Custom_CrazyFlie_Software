#ifndef BRIDGE_H_
#define BRIDGE_H_

#include "structures.h"
#include <vrpn_Tracker.h>

class Bridge {
	
	private:

	public:

		// Location of the two endpoints
		lateralPosition endpoint1;
		lateralPosition endpoint2;
		
		targetDistance  disToQuad1;
		targetDistance  disToQuad2;
		targetDistance  disToQuad3;
		targetDistance  disToQuad4;

		bool endpoint1Valid;
		bool endpoint2Valid;

		Bridge();

		~Bridge();

		/**
		 * This function will find the point located between the two endpoints.
		 * If the second endpoint is missing, then the 1st endpoint is returned
		 * 
		 * @param pos Pointer to a lateral position struct
		 * @param loc Location between the two endpoints to find (loc must be in [0,1])
		 * @return True if a location is valid, false if no location is able to found
		 */
		bool getInterpolatedPosition(lateralPosition *pos, int loc);

		/**
		 * Calculates the distance between the Bridge and each of the active Crazyflie
		 * 
		 * @param pos Pointer to a lateral position struct
		 * @param dis Distance between Bridge and Quadcopters, up to four
		 * @return True if a location is valid, false if no location is able to found
		 */
		bool getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1);
		bool getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1, targetDistance *disToQuad2);
		bool getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1, targetDistance *disToQuad2, targetDistance *disToQuad3);
		bool getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1, targetDistance *disToQuad2, targetDistance *disToQuad3, targetDistance *disToQuad4);
		/**
		 * Callbacks for the VRPN trackers on either end of the bridge
		 */
		static void VRPN_CALLBACK end1Callback(void* inst, const vrpn_TRACKERCB t);
		static void VRPN_CALLBACK end2Callback(void* inst, const vrpn_TRACKERCB t);
};


#endif
