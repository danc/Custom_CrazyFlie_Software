#ifndef HELPER_FUNCTIONS_H_
#define HELPER_FUNCTIONS_H_

#define HOVER_THRUST(takeoff)({\
	takeoff.profile[ (takeoff.numTakeoffSteps-1)*3 + 0 ]; \
})

/**
 * This code chunk will compute the X, Y, Z coordinate of the bridge at the given convex input
 * @param t The convex input
 * @param x Float for the x position
 * @param y Float for the y position
 * @param z Float for the z position
 */
#define COMPUTE_BRIDGE_COORDINATE(t, xRes, yRes, zRes) ({\
 	xRes = t*bridge1.x + (1 - t)*bridge2.x; \
 	yRes = t*bridge1.y + (1 - t)*bridge2.y; \
 	zRes = t*bridge1.z + (1 - t)*bridge2.z; \
})

/**
 * This code chunk will extract the position from the VRPN data and put
 * it into the controller data array
 * @param i The number of the quadcopter
 * @param t The VRPN data array
 */
#define EXTRACT_POSITION(i, t) ({\
 	crazyflie_info[i].controllerData.xPosition = t.pos[0]; \
	crazyflie_info[i].controllerData.yPosition = t.pos[1]; \
	crazyflie_info[i].controllerData.zPosition = t.pos[2]; \
	q_vec_type euler; \
	q_to_euler(euler, t.quat); \
	crazyflie_info[i].controllerData.yawPosition = Q_RAD_TO_DEG(euler[0]); \
	crazyflie_info[i].controllerData.pitchPosition = Q_RAD_TO_DEG(euler[1]); \
	crazyflie_info[i].controllerData.rollPosition = Q_RAD_TO_DEG(euler[2]); \
})

#define SET_X(quadNumber, setpoint) ( crazyflie_info[quadNumber-1].cflieCopter->setSetpointX( setpoint ) )
#define SET_Y(quadNumber, setpoint) ( crazyflie_info[quadNumber-1].cflieCopter->setSetpointY( setpoint ) )
#define SET_Z(quadNumber, setpoint) ( crazyflie_info[quadNumber-1].cflieCopter->setSetpointZ( setpoint ) )
#define SET_YAW(quadNumber, setpoint) ( crazyflie_info[quadNumber-1].cflieCopter->setSetpointYaw( setpoint ) )

#define SET_FLIGHTMODE(quadNumber, mode) ( crazyflie_info[quadNumber-1].cflieCopter->setFlightMode( mode ) )

#endif