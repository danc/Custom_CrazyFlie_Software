#ifndef NETWORKFORWARDING_H_
#define NETWORKFORWARDING_H_

#include <queue>
#include <stdlib.h>

#include "../CCRTPPacket.h"

#include "LocalizationLogger.h"

typedef struct edge{
	int node1;
	int node2;
} edge_t;

class NetworkForwarding {

private:
	// The edges that the network has
	std::vector<int> **edges;

	// The number of nodes in the network
	int numNodes;

	// Array to hold pointers to the queues storing the packets to send to each quadcopter
	std::queue<CCRTPPacket> **packetQueues;

	LocalizationLogger *networkLogger;

	pthread_mutex_t queueMutex;

public:
	// The constructor
	NetworkForwarding(int numNodes);

	NetworkForwarding(int numNodes, edge_t *edges, int numEdges);

	// The destructor
	~NetworkForwarding();

	void clearAllNetworkQueues();
	void clearNetworkQueue(int node);

	void setNetworkLogger(LocalizationLogger *logger);
	LocalizationLogger* getNetworkLogger();

	/*
	 * Add an edge to the graph
	 *
	 * @param edge The edge to add
	 */
	void addEdge(edge_t edge);

	/**
	 * Remove an edge from the graph
	 * 
	 * @param edge The edge to remove
	 */
	void removeEdge(edge_t edge);

	/**
 	 * List all the edges in the computation graph
 	 */
	void listEdges();

	/**
 	 * Display the graph adjacency matrix
 	 *
 	 */
	void displayAdjacency();

	/**
	 * Retrieve the next packet to send
	 *
	 * @param quadNum The number of the quadcopter to get the next packet for
	 * @param pk Pointer to the packet object to return the next packet in
	 * @return If there was a packet to send
	 */
	bool retrievePacket(int quadNum, CCRTPPacket *pk);

	/*
	 * Callback to be called when a packet is received from a quadcopter
	 *
	 * @param quadNum The number of the quadcopter the packet was received from
	 * @param pk Pointer to the CRTP packet that was received
	 */
	void packetReceived(int quadNum, CCRTPPacket *pk);
	static void packetCallback(NetworkForwarding *network, int quadNum, CCRTPPacket *pk);

	/**
	 * 
	 *
	 */

};


#endif