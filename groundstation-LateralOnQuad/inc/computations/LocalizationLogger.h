#ifndef LOCALIZATIONLOGGER_H_
#define LOCALIZATIONLOGGER_H_

#include <iostream>
#include <fstream>
#include <string.h>

#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"

#include "CCRTPPacket.h"

typedef struct computation_localization_shared_node_data {
	float measuredRadius;
	float estimatedX;
	float estimatedY;
	float estimatedZ;
	float alpha;
	float beta;
	float gamma;
} __attribute__((packed)) computation_localization_node_data;

class LocalizationLogger{

private:
	computation_localization_node_data *nodeData;

	float *agentPositionX;
	float *agentPositionY;
	float *agentPositionZ;

	int numAgents;

	float targetX;
	float targetY;
	float targetZ;

	double startTime;

	double nextLogTime;

	double logInterval;

	std::ofstream file_log;

public:
	LocalizationLogger(int numAgents, char* filename, double logInterval);

	~LocalizationLogger();

	/*
	 * Functions to get different computation values from an agent
	 */
	float getMeasuredRadius(int agent);
	float getEstimatedX(int agent);
	float getEstimatedY(int agent);
	float getEstimatedZ(int agent);
	float getAlpha(int agent);
	float getBeta(int agent);
	float getGamma(int agent);

	static void VRPN_CALLBACK vrpnTrackerCallback(void *logger, const vrpn_TRACKERCB t);

	void setAgentPosition(int agent, float x, float y, float z);

	void setTargetPosition(float x, float y, float z);

	void logData();

	void packetReceived(CCRTPPacket *packet);

};

#endif