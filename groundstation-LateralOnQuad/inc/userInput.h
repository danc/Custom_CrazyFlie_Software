#ifndef USERINPUT_H_
#define USERINPUT_H_

#include <string>

enum commandType {
	CMD_TAKEOFF = 't',
	CMD_LAND = 'l',
	CMD_SET_X = 'x',
	CMD_SET_Y = 'y',
	CMD_SET_Z = 'z',
	CMD_SET_YAW = 'w',
	CMD_BRIDGE_HOVER = 'r',
	CMD_BRIDGE_LAND = 'e',
	CMD_BRIDGE_TAKEOFF = 'b',
	CMD_KILL = 'k',
	CMD_ALL_MOTORS = 'm',
	CMD_GOTO_DEFAULT = 'd',
	CMD_START_MOTORS = 's',
	CMD_TOGGLE_COOR = 'f',
	CMD_TOGGLE_COMP = 'c',
};

extern const std::string CLEAR;    // When sent to cout, this clears the terminal screen

void* UIThread(void *threadID);

#endif