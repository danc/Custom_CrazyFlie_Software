#include <iostream>
#include <fstream>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#include "vrpn.h"

#include "computations/LocalizationLogger.h"
#include "CCRTPPacket.h"

#define NSEC_PER_SEC 1000000000L

LocalizationLogger::LocalizationLogger(int numAgents, char* fileName, double logInterval) {
	this->numAgents = numAgents;

	this->logInterval = logInterval;

	std::cout << "Logging computation at " << this->logInterval << " second intervals" << std::endl;

	// Init the target position to 0
	this->targetX = 0;
	this->targetY = 0;
	this->targetZ = 0;

	// Save the start time
	struct timespec tsTime;
	clock_gettime(CLOCK_MONOTONIC, &tsTime);

	this->startTime = (tsTime.tv_sec + double(tsTime.tv_nsec) / NSEC_PER_SEC);
	this->nextLogTime = this->startTime;

	// Create the arrays to hold the agent position
	this->agentPositionX = new float[numAgents]();
	this->agentPositionY = new float[numAgents]();
	this->agentPositionZ = new float[numAgents]();

	// Create the array to hold the data from the nodes and zero out the data
	this->nodeData = new computation_localization_node_data[numAgents]();
	for (int i=0; i < numAgents; i++) {
		this->nodeData[i].measuredRadius = 0;
		this->nodeData[i].estimatedX = 0;
		this->nodeData[i].estimatedY = 0;
		this->nodeData[i].estimatedZ = 0;
		this->nodeData[i].alpha = 0;
		this->nodeData[i].beta = 0;
		this->nodeData[i].gamma = 0;

		this->agentPositionX[i] = 0;
		this->agentPositionY[i] = 0;
		this->agentPositionZ[i] = 0;
	}

	// Open the file 
	try {
		char logFileName[255];

		char timeString[40];
		time_t rawtime;
		time( &rawtime );
		struct tm *timeinfo = localtime( &rawtime );
		strftime(timeString, 40, "%Y_%m_%d_%H:%M:%S", timeinfo);

		// Create the log file name (including date)
		sprintf(logFileName, "%s_%s.txt", fileName, timeString);

		this->file_log.open(logFileName, std::ios_base::out);

		std::cout << "Opened computation log file: " << logFileName << std::endl;
	} catch (...) {
		std::cout << "Error opening log file for localization logger" << std::endl;
		exit(-1);
	}

	// Create the header for the logfile
	this->file_log << "#Computation:Localization" << std::endl;

	// Create the field names
	this->file_log << "\%Time\t\tTargetX\t\tTargetY\t\tTargetZ";
	for (int i = 0; i < numAgents; i++) {
		this->file_log << "\t\tNode" << i << "_positionX";
		this->file_log << "\t\tNode" << i << "_positionY";
		this->file_log << "\t\tNode" << i << "_positionZ";
		this->file_log << "\t\tNode" << i << "_measuredRadius";
		this->file_log << "\t\tNode" << i << "_estimatedX";
		this->file_log << "\t\tNode" << i << "_estimatedY";
		this->file_log << "\t\tNode" << i << "_estimatedZ";
		this->file_log << "\t\tNode" << i << "_alpha";
		this->file_log << "\t\tNode" << i << "_beta";
		this->file_log << "\t\tNode" << i << "_gamma";
	}
	this->file_log << std::endl;

	// Create the field units
	this->file_log << "&sec\t\tmeters\t\tmeters\t\tmeters";
	for (int i = 0; i < numAgents; i++) {
		this->file_log << "\t\tmeters";
		this->file_log << "\t\tmeters";
		this->file_log << "\t\tmeters";
		this->file_log << "\t\tmeters";
		this->file_log << "\t\tmeters";
		this->file_log << "\t\tmeters";
		this->file_log << "\t\tmeters";
		this->file_log << "\t\traw";
		this->file_log << "\t\traw";
		this->file_log << "\t\traw";
	}
	this->file_log << std::endl;


}

LocalizationLogger::~LocalizationLogger() {
	// Delete the array holding the data
	delete[] this->nodeData;
	delete[] this->agentPositionX;
	delete[] this->agentPositionY;
	delete[] this->agentPositionZ;

	// Close the logger file
	this->file_log.close();
}

void VRPN_CALLBACK LocalizationLogger::vrpnTrackerCallback(void *logger, const vrpn_TRACKERCB t) {
	// Make a new variable so the methods can be called
	LocalizationLogger *log = (LocalizationLogger *) logger;

	// Pass the position into the logger
	float x = t.pos[0];
	float y = t.pos[1];
	float z = t.pos[2];

	log->setTargetPosition(x, y, z);
}

void LocalizationLogger::setAgentPosition(int agent, float x, float y, float z) {
	// Make sure that the agent actually exists before setting the position
	if (agent < this->numAgents) {
		this->agentPositionX[agent] = x;
		this->agentPositionY[agent] = y;
		this->agentPositionZ[agent] = z;
	}
}


void LocalizationLogger::setTargetPosition(float x, float y, float z) {
	this->targetX = x;
	this->targetY = y;
	this->targetZ = z;
}

void LocalizationLogger::logData() {

	// Get the timestamp to log
	struct timespec tsTime;
	clock_gettime(CLOCK_MONOTONIC, &tsTime);

	double currentTime = (tsTime.tv_sec + double(tsTime.tv_nsec) / NSEC_PER_SEC);

	if ( currentTime < this->nextLogTime ) {
		// Not time to log yet, break out
		return;
	}

	double timeSinceStart = currentTime - this->startTime;

	this->nextLogTime = currentTime + this->logInterval;

	// Log the time since start
	this->file_log << timeSinceStart;

	// Log the target data
	this->file_log << "\t\t" << this->targetX;
	this->file_log << "\t\t" << this->targetY;
	this->file_log << "\t\t" << this->targetZ;

	// Log the agent data
	for (int i = 0; i < numAgents; i++) {
		this->file_log << "\t\t" << this->agentPositionX[i];
		this->file_log << "\t\t" << this->agentPositionY[i];
		this->file_log << "\t\t" << this->agentPositionZ[i];

		this->file_log << "\t\t" << this->nodeData[i].measuredRadius;
		this->file_log << "\t\t" << this->nodeData[i].estimatedX;
		this->file_log << "\t\t" << this->nodeData[i].estimatedY;
		this->file_log << "\t\t" << this->nodeData[i].estimatedZ;
		this->file_log << "\t\t" << this->nodeData[i].alpha;
		this->file_log << "\t\t" << this->nodeData[i].beta;
		this->file_log << "\t\t" << this->nodeData[i].gamma;
	}

	// Add the newline
	this->file_log << std::endl;
}

float LocalizationLogger::getMeasuredRadius(int agent) {
	// The agent doesn't exist, return 0
	if (agent >= this->numAgents) {
		return (0.0);
	}

	// Otherwise just return the data
	return (nodeData[agent].measuredRadius);
}

float LocalizationLogger::getEstimatedX(int agent) {
	// The agent doesn't exist, return 0
	if (agent >= this->numAgents) {
		return (0.0);
	}

	// Otherwise just return the data
	return (nodeData[agent].estimatedX);
}

float LocalizationLogger::getEstimatedY(int agent) {
	// The agent doesn't exist, return 0
	if (agent >= this->numAgents) {
		return (0.0);
	}

	// Otherwise just return the data
	return (nodeData[agent].estimatedY);
}

float LocalizationLogger::getEstimatedZ(int agent) {
	// The agent doesn't exist, return 0
	if (agent >= this->numAgents) {
		return (0.0);
	}

	// Otherwise just return the data
	return (nodeData[agent].estimatedZ);
}

float LocalizationLogger::getAlpha(int agent) {
	// The agent doesn't exist, return 0
	if (agent >= this->numAgents) {
		return (0.0);
	}

	// Otherwise just return the data
	return (nodeData[agent].alpha);
}

float LocalizationLogger::getBeta(int agent) {
	// The agent doesn't exist, return 0
	if (agent >= this->numAgents) {
		return (0.0);
	}

	// Otherwise just return the data
	return (nodeData[agent].beta);
}

float LocalizationLogger::getGamma(int agent) {
	// The agent doesn't exist, return 0
	if (agent >= this->numAgents) {
		return (0.0);
	}

	// Otherwise just return the data
	return (nodeData[agent].gamma);
}

void LocalizationLogger::packetReceived(CCRTPPacket *packet) {
	// Retrieve the agent number
	char* packetData = packet->data();
	int agent = packetData[0];

	// Copy the data from the packet into the data struct
	memcpy(&(nodeData[agent]), &(packetData[1]), sizeof(computation_localization_node_data));
}