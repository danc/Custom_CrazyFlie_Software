#include "computations/NetworkForwarding.h"

#include <iostream>
#include <queue>
#include "CCRTPPacket.h"

NetworkForwarding::NetworkForwarding(int numNodes) {
	this->numNodes = numNodes;

	this->networkLogger = NULL;

	// Init the mutex
	pthread_mutex_init( &(this->queueMutex), NULL);

	// Create the queues for the nodes and the vectors for the edges
	this->packetQueues = (std::queue<CCRTPPacket>**) malloc( sizeof(std::queue<CCRTPPacket>*)*numNodes );
	this->edges = (std::vector<int>**) malloc( sizeof(std::vector<int>)*numNodes );
	for (int i=0; i < numNodes; i++) {
		this->packetQueues[i] = new std::queue<CCRTPPacket>();
		this->edges[i] = new std::vector<int>();
	}
}

NetworkForwarding::NetworkForwarding(int numNodes, edge_t *edges, int numEdges) {
	// Copy the variables
	this->numNodes = numNodes;

	// Init the mutex
	pthread_mutex_init( &(this->queueMutex), NULL);

	// Create the queues for the nodes and the vectors for the edges
	this->packetQueues = (std::queue<CCRTPPacket>**) malloc( sizeof(std::queue<CCRTPPacket>*)*numNodes );
	this->edges = (std::vector<int>**) malloc( sizeof(std::vector<int>)*numNodes );
	for (int i=0; i < numNodes; i++) {
		this->packetQueues[i] = new std::queue<CCRTPPacket>();
		this->edges[i] = new std::vector<int>();
	}

	// Add the edges to the computation graph
	for (int i=0; i < numEdges; i++) {
		this->addEdge(edges[i]);
	}
}

NetworkForwarding::~NetworkForwarding() {
	// Delete each queue and then the array holding them
	for (int i=0; i < this->numNodes; i++) {
		delete( packetQueues[i] );
		delete( edges[i] );
	}
	delete[] packetQueues;
	delete[] edges;
}

void NetworkForwarding::clearAllNetworkQueues() {
	for (int i=0; i<numNodes; i++) {
		this->clearNetworkQueue(i);
	}
}

void NetworkForwarding::clearNetworkQueue(int node) {
	pthread_mutex_lock( &(this->queueMutex) );

	// Clear out the queue
	while (!packetQueues[node]->empty()) {
		packetQueues[node]->pop();
	}

	// Release the data mutex
	pthread_mutex_unlock( &(this->queueMutex) );
}

void NetworkForwarding::setNetworkLogger(LocalizationLogger *logger) {
	this->networkLogger = logger;
}

LocalizationLogger* NetworkForwarding::getNetworkLogger() {
	return( this->networkLogger );
}

/*
 * Add an edge to the graph
 *
 * @param edge The edge to add
 */
void NetworkForwarding::addEdge(edge_t edge) {
	int fromNode = edge.node1 - 1;
	int toNode = edge.node2 - 1;

	// Iterate over the edges already existing to make sure it is not a duplicate
	for (std::vector<int>::iterator it = edges[fromNode]->begin(); it != edges[fromNode]->end(); ++it) {

		if (*it == toNode) {
			std::cout << "Edge from " << fromNode+1 << " to " << toNode+1 << " already exists" << std::endl;
			return;
		}
	}

	// Add the edge
	edges[fromNode]->push_back(toNode);
	std::cout << "Edge from " << fromNode+1 << " to " << toNode+1 << " added" << std::endl;
}

/**
 * Remove an edge from the graph
 * 
 * @param edge The edge to remove
 */
void NetworkForwarding::removeEdge(edge_t edge) {
	int fromNode = edge.node1 - 1;
	int toNode = edge.node2 - 1;

	// Iterate over the edges and delete it if it exists
	for (std::vector<int>::iterator it = edges[fromNode]->begin(); it != edges[fromNode]->end(); ++it) {
		if (*it == toNode) {
			edges[fromNode]->erase(it);
			std::cout << "Edge from " << fromNode+1 << " to " << toNode+1 << " removed" << std::endl;
			return;
		}
	}
}

/**
 * List all the edges in the computation graph
 */
void NetworkForwarding::listEdges() {
	std::cout << "Edges in graph" << std::endl;
	for (int i=0; i < numNodes; i++) {
		for (std::vector<int>::iterator it = edges[i]->begin(); it != edges[i]->end(); ++it) {
			std::cout << i+1 << " to " << *it+1 << std::endl;
		}
	}
}

/**
 * Display the graph adjacency matrix
 *
 */
void NetworkForwarding::displayAdjacency() {
	std::cout << "Network adjacency matrix" << std::endl;

	for (int i=0; i < numNodes; i++) {
		int nodeConnection[numNodes];
		memset(nodeConnection, 0, sizeof(int)*numNodes);

		for (std::vector<int>::iterator it = edges[i]->begin(); it != edges[i]->end(); ++it) {
			nodeConnection[*it] = 1;
		}

		for (int j=0; j < numNodes; j++) {
			if (nodeConnection[j] == 1) {
				std::cout << " 1";
			} else {
				std::cout << " 0";
			}
		}
		std::cout << std::endl;
	}
}

/**
 * Retrieve the next packet to send
 *
 * @param quadNum The number of the quadcopter to get the next packet for
 * @param pk Pointer to the packet object to return the next packet in
 * @return If there was a packet to send
 */
bool NetworkForwarding::retrievePacket(int quadNum, CCRTPPacket *pk) {
	if ( packetQueues[quadNum]->size() == 0 ) {
		return(false);
	}

	// Get the data mutex
	pthread_mutex_lock( &(this->queueMutex) );

	// Pull the next packet from the queue
	*pk = packetQueues[quadNum]->front();
	packetQueues[quadNum]->pop();

	// Release the data mutex
	pthread_mutex_unlock( &(this->queueMutex) );

	//std::cout << "Sending comp packet to quad " << quadNum << std::endl;
	return(true);
}

/**
 * Callback to be called when a packet is received from a quadcopter
 *
 * @param quadNum The number of the quadcopter the packet was received from
 * @param pk Pointer to the CRTP packet that was received
 */
void NetworkForwarding::packetReceived(int quadNum, CCRTPPacket *pk) {
	//std::cout << "Received comp packet from quad " << quadNum << std::endl;

	// Get the data mutex
	pthread_mutex_lock( &(this->queueMutex) );

	// Determine the quads to get the packet
	for (std::vector<int>::iterator it = edges[quadNum]->begin(); it != edges[quadNum]->end(); ++it) {
		// Add it to the queue of the adjacent quadcopter
		packetQueues[*it]->push(*pk);
	}

	// Release the data mutex
	pthread_mutex_unlock( &(this->queueMutex) );

	// Pass the packet to the logger
	if (this->networkLogger != NULL) {
		this->networkLogger->packetReceived(pk);
	}
}
