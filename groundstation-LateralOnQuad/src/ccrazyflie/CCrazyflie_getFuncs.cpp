#include "CCrazyflie.h"
#include "CCrazyRadio.h"

int CCrazyflie::getControllerType() {
	return this->controllerType;
}

int CCrazyflie::getComputationType() {
	return this->computationType;
}

std::string CCrazyflie::getComputationTypeString() {
	switch ( this->getComputationType() ) {
	case 0:
		return( "Computation:None" );
		break;
	case 1:
		return( "Computation:Localization" );
		break;
	}
	return( "Computation:Unknown" );
}

std::string CCrazyflie::getControllerTypeString() {
	switch (this->getControllerType() ) {
	case 0:
		return( "Controller:Bypassed" );
		break;
	case 1:
		return( "Controller:PID" );
		break;
	case 2:
		return( "Controller:LQR" );
		break;
	case 3:
		return( "Controller:LQI" );
		break;
	case 4:
		return( "Controller:SGSF" );
	}
	return( "Controller:Unknown" );
}

double CCrazyflie::batteryLevel() {
	return this->sensorDoubleValue("pm.vbat");
}

float CCrazyflie::batteryState() {
	return this->sensorDoubleValue("pm.state");
}

int CCrazyflie::getRadioChannel() {
	return m_nRadioChannel;
}

XferRate CCrazyflie::getRadioDataRate() {
	return m_radioDataRate;
}

int CCrazyflie::getQuadcopterNumber() {
	return this->m_quadNum;
}

/*
 * These functions will return the current quadcopter
 * lateral position as reported by the camera system.
 */
float CCrazyflie::getPositionX(coordinateSystem coor) {
	float retVal = 0;
	pthread_mutex_lock( &(this->cameraDataMutex) );
	
	if (coor == coor_local) {
		retVal = this->m_currentLocalPosition.x;
	} else {
		retVal = this->m_currentCameraPosition.x;
	}

	pthread_mutex_unlock( &(this->cameraDataMutex) );
	return(retVal);
}

float CCrazyflie::getPositionY(coordinateSystem coor) {
	float retVal = 0;
	pthread_mutex_lock( &(this->cameraDataMutex) );
	
	if (coor == coor_local) {
		retVal = this->m_currentLocalPosition.y;
	} else {
		retVal = this->m_currentCameraPosition.y;
	}

	pthread_mutex_unlock( &(this->cameraDataMutex) );
	return(retVal);
}

float CCrazyflie::getPositionZ(coordinateSystem coor) {
	float retVal = 0;
	pthread_mutex_lock( &(this->cameraDataMutex) );
	
	if (coor == coor_local) {
		retVal = this->m_currentLocalPosition.z;
	} else {
		retVal = this->m_currentCameraPosition.z;
	}

	pthread_mutex_unlock( &(this->cameraDataMutex) );
	return(retVal);
}


/*
 * These functions will return the current quadcopter
 * angular position as reported by the quadcopter sensors.
 */
float CCrazyflie::getQuadRoll() {
	return this->sensorDoubleValue("stabilizer.roll");
}

float CCrazyflie::getQuadPitch() {
	return this->sensorDoubleValue("stabilizer.pitch");
}

float CCrazyflie::getQuadYaw() {
	return this->sensorDoubleValue("stabilizer.yaw");
}


/*
 * These functions will return the current quadcopter
 * angular position as reported by the camera system.
 */
float CCrazyflie::getCameraRoll() {
	pthread_mutex_lock( &(this->cameraDataMutex) );
	float retVal = this->m_currentCameraAngle.roll;
	pthread_mutex_unlock( &(this->cameraDataMutex) );
	return(retVal);
}

float CCrazyflie::getCameraPitch() {
	pthread_mutex_lock( &(this->cameraDataMutex) );
	float retVal = this->m_currentCameraAngle.pitch;
	pthread_mutex_unlock( &(this->cameraDataMutex) );
	return(retVal);
}

float CCrazyflie::getCameraYaw() {
	pthread_mutex_lock( &(this->cameraDataMutex) );
	float retVal = this->m_currentCameraAngle.yaw;
	pthread_mutex_unlock( &(this->cameraDataMutex) );
	return(retVal);
}


/*
 * These functions will return the current controller setpoints.
 */
float CCrazyflie::getSetpointX(coordinateSystem coor) {
	float retVal = 0;
	
	if (coor == coor_local) {
		retVal = this->m_desiredPositionLocal.x;
	} else {
		retVal = this->m_desiredPositionGlobal.x;
	}

	return retVal;
}

float CCrazyflie::getSetpointY(coordinateSystem coor) {
	float retVal = 0;
	
	if (coor == coor_local) {
		retVal = this->m_desiredPositionLocal.y;
	} else {
		retVal = this->m_desiredPositionGlobal.y;
	}

	return retVal;
}

float CCrazyflie::getSetpointZ(coordinateSystem coor) {
	float retVal = 0;
	
	if (coor == coor_local) {
		retVal = this->m_desiredPositionLocal.z;
	} else {
		retVal = this->m_desiredPositionGlobal.z;
	}

	return retVal;
}

float CCrazyflie::getSetpointYaw() {
	return this->m_desiredYaw;
}

uint16_t CCrazyflie::getBaseThrust() {
	return this->m_baseThrust;
}

double CCrazyflie::getPositionTimeDelta() {
	return this->positionTimeDelta;
}

coordinateSystem CCrazyflie::getCoordinateSystem() {
	return this->m_systemToUse;
}