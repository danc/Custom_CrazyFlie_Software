/**
 * This file contains the functions associated with the logging and parameter tables
 * of contents for the CCrazyflie class.
 *
 */
#include "CCrazyflie.h"
#include "CTOC.h"

#include <time.h>
#include <iostream>
#include <stdio.h>
#include <errno.h>
#include <string.h>

// Various headers for the files
static const char *variables = "%Time\t\tPositionDelta\t\tBattery\t\t"
							   "xSetpointGlobal\t\txSetpointLocal\t\t"
							   "ySetpointGlobal\t\tySetpointLocal\t\t"
							   "zSetpointGlobal\t\tzSetpointLocal\t\t"
							   "yawSetpoint\t\t"
							   "xPositionGlobal\t\txPositionLocal\t\t"
							   "yPositionGlobal\t\tyPositionLocal\t\t"
							   "zPositionGlobal\t\tzPositionLocal\t\t"
							   "quadPitch\t\tquadRoll\t\tquadYaw\t\t\t\tcamPitch\t\tcamRoll\t\tcamYaw\t\t"
							   "gyroX\t\tgyroY\t\tgyroZ\t\taccelX\t\taccelY\t\taccelZ\t\t"
							   "motor1\t\tmotor2\t\tmotor3\t\tmotor4\t\t"
   							   "xOut\t\tyOut\t\tzOut\t\t"
							   "Pitch_Out\t\tRoll_Out\t\tYaw_Out\t\t"
							   "PitchRate_Out\t\tRollRate_Out\t\tYawRate_Out\t\t"
							   "baseThrust\t\tcontrollerReset\t\t"
							   "Ut\t\tUa\t\tUe\t\tUr\t\t"
							   "controllerDuration\t\testimatorDuration";

static const char *units = "&sec\t\tsec\t\tVolt\t\t"
						   "meters\t\tmeters\t\t"
						   "meters\t\tmeters\t\t"
						   "meters\t\tmeters\t\t"
						   "degrees\t\t"
						   "meters\t\tmeters\t\t"
						   "meters\t\tmeters\t\t"
						   "meters\t\tmeters\t\t"
						   "degrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\t"
						   "deg\\sec\t\tdeg\\sec\t\tdeg\\sec\t\tg\t\tg\t\tg\t\t"
						   "raw\t\traw\t\traw\t\traw\t\t"
						   "raw\t\traw\t\traw\t\t"
						   "raw\t\traw\t\traw\t\t"
						   "raw\t\traw\t\traw\t\t"
						   "raw\t\tboolean\t\t"
						   "raw\t\traw\t\traw\t\traw\t\t"
						   "millisec\t\tmillisec";

/*
 * Open a log file for the quadcopter
 *
 * @param baseFileName The base filename for the logfile
 */
void CCrazyflie::openLogFile(char *baseFileName) {
	char logFileName[40];

	char timeString[40];
	time_t rawtime;
	time( &rawtime );
	struct tm *timeinfo = localtime( &rawtime );
	strftime(timeString, 40, "%Y_%m_%d_%H:%M:%S", timeinfo);

	// Create the log file name (including date)
	sprintf(logFileName, "%s_%s.txt", baseFileName, timeString);

	printf("Opening log file %s  for quadcopter %d...", logFileName, (m_quadNum + 1));

	try {
		file_log.open(logFileName, std::ios_base::out);
	} catch (...) {
		std::cout << "Error opening logfile for quadcopter " << m_quadNum;
		exit(-1);
	}

	// Place the header information into the log file
	file_log << "#Crazyflie" << std::endl;

	// Add the controller type to the logfile
	file_log << "#" << this->getControllerTypeString() << std::endl;
	file_log << variables << std::endl;
	file_log << units << std::endl;

	std::cout << " Complete" << std::endl;
}

/*
 * Write data to the log file
 */
void CCrazyflie::writeLogData() {
	// Make a new line
	file_log << std::endl;

	// Print the time
	file_log << this->currentTime();

	// Print the time delta for the position information
	file_log << "\t\t" << this->getPositionTimeDelta();

	// Print the battery
	file_log << "\t\t" << this->batteryLevel();

	// Print the setpoints
	file_log << "\t\t" << this->getSetpointX(coor_global);
	file_log << "\t\t" << this->getSetpointX(coor_local);
	file_log << "\t\t" << this->getSetpointY(coor_global);
	file_log << "\t\t" << this->getSetpointY(coor_local);
	file_log << "\t\t" << this->getSetpointZ(coor_global);
	file_log << "\t\t" << this->getSetpointY(coor_local);
	file_log << "\t\t" << this->getSetpointYaw();
	
	// Print the current local position
	file_log << "\t\t" << this->getPositionX(coor_global);
	file_log << "\t\t" << this->getPositionX(coor_local);
	file_log << "\t\t" << this->getPositionY(coor_global);
	file_log << "\t\t" << this->getPositionY(coor_local);
	file_log << "\t\t" << this->getPositionZ(coor_global);
	file_log << "\t\t" << this->getPositionZ(coor_local);

	// Print the angles
	file_log << "\t\t" << this->getQuadPitch();
	file_log << "\t\t" << this->getQuadRoll();
	file_log << "\t\t" << this->getQuadYaw();
	file_log << "\t\t" << this->getCameraPitch();
	file_log << "\t\t" << this->getCameraRoll();
	file_log << "\t\t" << this->getCameraYaw();

	// Print the sensor data
	file_log << "\t\t" << this->sensorDoubleValue("gyro.x");
	file_log << "\t\t" << this->sensorDoubleValue("gyro.y");
	file_log << "\t\t" << this->sensorDoubleValue("gyro.z");
	file_log << "\t\t" << this->sensorDoubleValue("accel.x");
	file_log << "\t\t" << this->sensorDoubleValue("accel.y");
	file_log << "\t\t" << this->sensorDoubleValue("accel.z");

	file_log << "\t\t" << this->sensorDoubleValue("motor.m1");
	file_log << "\t\t" << this->sensorDoubleValue("motor.m2");
	file_log << "\t\t" << this->sensorDoubleValue("motor.m3");
	file_log << "\t\t" << this->sensorDoubleValue("motor.m4");


	file_log << "\t\t" << this->sensorDoubleValue("posCtlAlt.outz");
	file_log << "\t\t" << this->sensorDoubleValue("posCtlAlt.outy");
	file_log << "\t\t" << this->sensorDoubleValue("posCtlAlt.outz");

	file_log << "\t\t" << this->sensorDoubleValue("pitchPID.output");
	file_log << "\t\t" << this->sensorDoubleValue("rollPID.output");
	file_log << "\t\t" << this->sensorDoubleValue("yawPID.output");

	file_log << "\t\t" << this->sensorDoubleValue("pitchRatePID.output");
	file_log << "\t\t" << this->sensorDoubleValue("rollRatePID.output");
	file_log << "\t\t" << this->sensorDoubleValue("yawRatePID.output");

	file_log << "\t\t" << this->getBaseThrust();
	file_log << "\t\t" << "0";//quad->controllerData.resetController;

	file_log << "\t\t" << this->sensorDoubleValue("mixer.ctr_thrust");
	file_log << "\t\t" << this->sensorDoubleValue("mixer.ctr_roll");
	file_log << "\t\t" << this->sensorDoubleValue("mixer.ctr_pitch");
	file_log << "\t\t" << this->sensorDoubleValue("mixer.ctr_yaw");

	file_log << "\t\t" << this->sensorDoubleValue("controlStats.contDur");
	file_log << "\t\t" << this->sensorDoubleValue("controlStats.estimDur");
}


bool CCrazyflie::readTOCParameters() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	printf( "      readTocParameters requestMetaData\n" );
	if (m_tocParameters->requestMetaData()) {
		printf( "      requestItems\n" );
		if (m_tocParameters->requestItems()) {
			printf( "      return true\n" );
			return true;
		}
	}

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return false;
}

bool CCrazyflie::readTOCLogs() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	if (m_tocLogs->requestMetaData()) {
		if (m_tocLogs->requestItems()) {
			return true;
		}
	}

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return false;
}

void CCrazyflie::displayLoggingBlocksInitialized() {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	m_tocLogs->printLoggingBlocksInitialized();
	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

double CCrazyflie::sensorDoubleValue(std::string strName) {
	return m_tocLogs->doubleValue(strName);
}

bool CCrazyflie::addLoggingBlock(const char *name, uint16_t frequency) {
	bool retval = m_tocLogs->registerLoggingBlock(name, frequency);
	return(retval);
}

void CCrazyflie::removeLoggingBlock(const char *name) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	m_tocLogs->unregisterLoggingBlock(name);

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

bool CCrazyflie::addLoggingEntry(const char *blockName, const char *entryName) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	bool retval = m_tocLogs->startLogging(entryName, blockName);

	if (!retval) {
		std::cout << "Error starting logging entry " << entryName << " in block " << blockName << std::endl;
	}

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return ( retval );
}