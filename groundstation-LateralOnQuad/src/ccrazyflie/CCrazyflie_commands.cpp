#include "CCrazyflie.h"
#include "structures.h"
#include "userInput.h"

#include "quadcopterData.h"
#include "crazyflieGroundStation.h"

/**
 * Pass a command from the user into this Crazyflie
 *
 * @param command The command to pass in
 */
void CCrazyflie::passUserCommand(userCommands_t command) {
	pthread_mutex_lock( &(this->userCommandsMutex) );

	userCommands.push(command);

	pthread_mutex_unlock( &(this->userCommandsMutex) );
}

/**
 * Parse the commands that are sitting in the queue.
 * Each call will only parse one command.
 *
 * @return True if command parsed, false otherwise
 */
bool CCrazyflie::parseReceivedUserCommand() {
	// Get the mutex
	pthread_mutex_lock( &(userCommandsMutex) );

	// If there are no commands, then return
	if (userCommands.size() == 0) {
		pthread_mutex_unlock( &(userCommandsMutex) );
		return (false);
	}

	// Get the next command from the queue and release the mutex
	userCommands_t command = userCommands.front();
	userCommands.pop();
	pthread_mutex_unlock( &(userCommandsMutex) );

	// Parse the command
	switch ( command.type ) {
	case CMD_KILL:
		this->setFlightMode( GROUNDED_MODE );

		std::cout << "Crazyflie " << (m_quadNum+1) << " Killed" << std::endl;
		break;

	case CMD_TAKEOFF:
		this->setCoordinateSystem( coor_global );

		// Configure the setpoints
		this->setSetpointX( this->getPositionX( coor_global ), coor_global );
		this->setSetpointY( this->getPositionY( coor_global ), coor_global );
		this->setSetpointZ( defaultSetpoints[m_quadNum-1][2], coor_global );

		// Get the quad ready to takeoff
		this->setTakeoffProfile( quad1_takeoffSteps, quad1_numTakeoffSteps );
		this->disableThrustController();
		this->resetController();

		// Actually takeoff
		this->setFlightMode( TAKEOFF_MODE );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Taking off" << std::endl;
		break;

	case CMD_GOTO_DEFAULT:
		this->setSetpointX( defaultSetpoints[m_quadNum-1][0], coor_global );
		this->setSetpointY( defaultSetpoints[m_quadNum-1][1], coor_global );
		this->setSetpointZ( defaultSetpoints[m_quadNum-1][2], coor_global );

		std::cout << "Crazyflie " << (m_quadNum+1) << " Going to default coordinates" << std::endl;
		break;

	case CMD_LAND:
		this->setFlightMode( LANDING_MODE );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Landing" << std::endl;
		break;

	case CMD_SET_X:
		this->setSetpointX( command.payload, this->m_systemToUse );
		std::cout << "Crazyflie " << (m_quadNum+1) << " X Setpoint = " << command.payload << std::endl;
		break;

	case CMD_SET_Y:
		this->setSetpointY( command.payload, this->m_systemToUse );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Y Setpoint = " << command.payload << std::endl;
		break;

	case CMD_SET_Z:
		this->setSetpointZ( command.payload, this->m_systemToUse );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Z Setpoint = " << command.payload << std::endl;
		break;

	case CMD_SET_YAW:
		this->setSetpointYaw( command.payload );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Yaw Setpoint = " << command.payload << std::endl;
		break;

	case CMD_ALL_MOTORS:
		this->setSetpointX( command.payload, coor_global );
		this->setSetpointY( command.payload, coor_global );
		this->setSetpointZ( command.payload, coor_global );
		this->setSetpointYaw( command.payload );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Setting motors to = " << command.payload << std::endl;
		break;

	case CMD_BRIDGE_HOVER:
		// Determine where on the bridge to land
		lateralPosition bridgePos;
		if ( bridge->getInterpolatedPosition(&bridgePos, command.payload) ) {
			// The position is valid
			this->setSetpointX( bridgePos.x, coor_global);
			this->setSetpointY( bridgePos.y, coor_global);
			this->setSetpointZ( bridgePos.z - 0.5, coor_global);
			this->setFlightMode( BRIDGE_HOVER_MODE );
			std::cout << "Crazyflie " << (m_quadNum+1) << " Hover over bridge" << std::endl;
		} else {
			std::cout << "Bridge is not valid" << std::endl;
		}
		break;

	case CMD_BRIDGE_LAND:
		// Land on the bridge by just changing the Z setpoint
		this->setSetpointZ( this->getSetpointZ(this->m_systemToUse) + 0.7, this->m_systemToUse );
		this->setFlightMode(  BRIDGE_LANDING_MODE );

		std::cout << "Crazyflie " << (m_quadNum+1) << " Land on bridge" << std::endl;
		break;

	case CMD_BRIDGE_TAKEOFF:
		this->setFlightMode(  BRIDGE_TAKEOFF_MODE );
		std::cout << "Crazyflie " << (m_quadNum+1) << " Take off from bridge" << std::endl;
		break;
	
	case CMD_TOGGLE_COOR:
		// Toggle the coordinate system
		if (this->m_systemToUse == coor_local) {
			this->m_systemToUse = coor_global;

			// Remap the setpoints so there is no jump
			this->setSetpointX( this->getSetpointX(coor_local) + this->m_localOrigin.x , coor_global);
			this->setSetpointY( this->getSetpointY(coor_local) + this->m_localOrigin.y , coor_global);
			this->setSetpointZ( this->getSetpointZ(coor_local) + this->m_localOrigin.z , coor_global);

			std::cout << "Crazyflie " << (m_quadNum+1) << " Using global system" << std::endl;
		} else {
			this->m_systemToUse = coor_local;

			// Remap the setpoints so there is no jump
			this->setSetpointX( this->getSetpointX(coor_global) - this->m_localOrigin.x , coor_local);
			this->setSetpointY( this->getSetpointY(coor_global) - this->m_localOrigin.y , coor_local);
			this->setSetpointZ( this->getSetpointZ(coor_global) - this->m_localOrigin.z , coor_local);


			std::cout << "Crazyflie " << (m_quadNum+1) << " Using local system" << std::endl;
		}
		break;

	case CMD_TOGGLE_COMP:
		// Toggle the computation on and off
		if (this->computationStarted) {
			this->disableComputation();
			std::cout << "Crazyflie " << (m_quadNum+1) << " Disabling computation" << std::endl;	
		} else {
			this->enableComputation();
			std::cout << "Crazyflie " << (m_quadNum+1) << " Enabling computation" << std::endl;
		}
	}

	// A command was parsed
	return( true );
}