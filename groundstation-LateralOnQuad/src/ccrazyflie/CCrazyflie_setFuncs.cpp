#include "CCrazyflie.h"

/*
 * This function will set the takeoff profile to use.
 *
 * @param profile Pointer to the takeoff profile to use
 * @param numSteps The number of steps in the takeoff profile
 * 
 */
void CCrazyflie::setTakeoffProfile( takeoffProfileStep *profile, int numSteps ) {
	size_t length = sizeof(takeoffProfileStep) * numSteps;

	// Allocate the memory for the takeoff profile
	this->takeoffProfile = (takeoffProfileStep*) realloc( this->takeoffProfile, length);
	if ( this->takeoffProfile == NULL ) {
		// This is an error...
		std::cout << "Memory allocation for takeoff routine failed" << std::endl;
		exit(-1);
	}

	this->numTakeoffSteps = numSteps;
	memcpy(this->takeoffProfile, profile, length);
}

/**
 * This function will set the flight mode of the Crazyflie
 */
void CCrazyflie::setFlightMode( enum FlightMode mode) {
	this->m_enumFlightMode = mode;

	// Configure certain aspects when a mode is entered
	switch (mode) {
		case TAKEOFF_MODE:
			this->currentStep = 0;
			break;
		case GROUNDED_MODE:
			this->setBaseThrust( 0 );
			break;
		case LANDING_MODE:
			this->setCoordinateSystem( coor_global );
			this->setSetpointZ( 0, coor_global );
			break;
		default:
			// Do nothing
			break;
	}
}

/**
 * These functions will set the setpoints of the quadcopter
 */
void CCrazyflie::setSetpointX(float setpoint, coordinateSystem coor) {
	if (coor == coor_local) {
		// The given coordinate is in the local system
		this->m_desiredPositionLocal.x = setpoint;
		this->m_desiredPositionGlobal.x = setpoint + this->m_localOrigin.x;
	} else {
		// 
		this->m_desiredPositionGlobal.x = setpoint;
		this->m_desiredPositionLocal.x = setpoint - this->m_localOrigin.x;
	}
	
	this->m_sendSetpoints = true;
}

void CCrazyflie::setSetpointY(float setpoint, coordinateSystem coor) {
	if (coor == coor_local) {
		// The given coordinate is in the local system
		this->m_desiredPositionLocal.y = setpoint;
		this->m_desiredPositionGlobal.y = setpoint + this->m_localOrigin.y;
	} else {
		// 
		this->m_desiredPositionGlobal.y = setpoint;
		this->m_desiredPositionLocal.y = setpoint - this->m_localOrigin.y;
	}

	this->m_sendSetpoints = true;
}

void CCrazyflie::setSetpointZ(float setpoint, coordinateSystem coor) {
	if (coor == coor_local) {
		// The given coordinate is in the local system
		this->m_desiredPositionLocal.z = setpoint;
		this->m_desiredPositionGlobal.z = setpoint + this->m_localOrigin.z;
	} else {
		// 
		this->m_desiredPositionGlobal.z = setpoint;
		this->m_desiredPositionLocal.z = setpoint - this->m_localOrigin.z;
	}

	this->m_sendSetpoints = true;
}

void CCrazyflie::setSetpointYaw(float setpoint) {
	this->m_desiredYaw = setpoint;
	this->m_sendSetpoints = true;
}

void CCrazyflie::setBaseThrust(uint16_t thrust) {
	this->m_baseThrust = thrust;
	this->m_sendSetpoints = true;
}
/*
void CCrazyflie::setXYZYawSetpoint(float xSetpoint, float ySetpoint, float zSetpoint,
									float yawSetpoint, uint16_t baseThrust, 
									uint8_t resetController) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	this->m_desiredPosition.x = xSetpoint;
	this->m_desiredPosition.y = ySetpoint;
	this->m_desiredPosition.z = zSetpoint;
	this->m_desiredYaw = yawSetpoint;
	this->m_baseThrust = baseThrust;
	this->resetController = resetController;

	this->m_sendSetpoints = true;

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

void CCrazyflie::setXYZYawPosition(float xPosition, float yPosition, float zPosition,
									float yawPosition) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );

	this->xPosition = xPosition;
	this->yPosition = yPosition;
	this->zPosition = zPosition;
	this->yawPosition = yawPosition;

	if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}
*/
void CCrazyflie::setCameraPosition(lateralPosition cameraPosition) {
	pthread_mutex_lock( &(this->cameraDataMutex) );

	// Compute the VRPN sample time
	double currentTime = this->currentTime();
	if (this->lastPositionTime == 0) {
		// First sample, just ignore
		this->positionTimeDelta = 0;
	} else {
		this->positionTimeDelta = currentTime - this->lastPositionTime;
	}
	this->lastPositionTime = currentTime;
	
	// Set the global position
	this->m_currentCameraPosition = cameraPosition;

	// Set the local position as well
	this->m_currentLocalPosition.x = cameraPosition.x - this->m_localOrigin.x;
	this->m_currentLocalPosition.y = cameraPosition.y - this->m_localOrigin.y;
	this->m_currentLocalPosition.z = cameraPosition.z - this->m_localOrigin.z;

	// Update the position in the network logger
	this->m_network->getNetworkLogger()->setAgentPosition(this->m_quadNum, 
												 		  cameraPosition.x,
														  cameraPosition.y,
														  cameraPosition.z);

	this->m_sendPosition = true;
	pthread_mutex_unlock( &(this->cameraDataMutex) );
}

void CCrazyflie::setCameraAngle(angularPosition cameraAngle) {
	pthread_mutex_lock( &(this->cameraDataMutex) );
	this->m_currentCameraAngle = cameraAngle;
	this->m_sendPosition = true;
	pthread_mutex_unlock( &(this->cameraDataMutex) );
}

void CCrazyflie::setCameraQuat(quaternionPosition cameraQuat) {
	pthread_mutex_lock( &(this->cameraDataMutex) );
	this->m_currentCameraQuat = cameraQuat;
	pthread_mutex_unlock( &(this->cameraDataMutex) );
}

void CCrazyflie::setNetworkForwarding(NetworkForwarding *network) {
	this->m_network = network;
}

void CCrazyflie::setLocalOrigin(lateralPosition newOrigin) {
	this->m_localOrigin = newOrigin;
}

void CCrazyflie::setCoordinateSystem(coordinateSystem coor) {
	this->m_systemToUse = coor;
	this->m_sendSetpoints = true;
	this->m_sendPosition = true;
}