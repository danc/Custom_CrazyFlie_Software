
#include "vrpn.h"
#include <unistd.h>
#include "crazyflieGroundStation.h"

#include "quadcopterData.h"
#include "callbacks.h"
#include "userInput.h"
#include "userOutput.h"

#include <iostream>
using namespace std;

vrpn_Connection *connection;
vrpn_Tracker_Remote *trackerHand;
vrpn_Tracker_Remote *trackerBridge1;
vrpn_Tracker_Remote *trackerBridge2;

vrpn_Tracker_Remote *trackerLocalizationTarget;

char keystroke_now = 0;
char keystroke_prev = 0;
int keyPressed = 0;
int killCount = 0;
int trackCount = 0;

extern double loopTimeTotalStart;
extern double loopTimeTotalEnd;
extern double loopTimeTotal;		//Sum of Callback Time Totals
extern double loopTimeTotalPrev;
extern double loopTimeTotalDelta;

extern char landingMode;
extern char mirrorMode;
extern char takeOff1;
extern char takeOff2;
extern char takeOff3;
extern char takeOff4;

extern char handMode;
extern char trackHand;
extern char tcp_server_ON;
extern char tcp_client_ON;
extern char flap;

extern double takeOffTime1;
extern double takeOffTime2;
extern double takeOffTime3;
extern double takeOffTime4;


/**
 * Initialize the VRPN subsystem
 *
 * @param connectionName The IP address and port number (in a string) for the VRPN server
 * @param callbackHand Function pointer to the tracker callback function
 */
void vrpn_init(std::string connectionName, void (*callbackHand)(void*, const vrpn_TRACKERCB)) {
	// Get the VRPN connection information
	connection = vrpn_get_connection_by_name(connectionName.c_str());

	// Initialize the hand trackable
#if USE_HAND
	trackerHand = new vrpn_Tracker_Remote("hand", connection);
	trackerHand->register_change_handler(0, callbackHand);
#endif

#ifdef USE_BRIDGE
	trackerBridge1 = new vrpn_Tracker_Remote("Bridge1", connection);
	trackerBridge1->register_change_handler(bridge, Bridge::end1Callback);

	trackerBridge2 = new vrpn_Tracker_Remote("Bridge2", connection);
	trackerBridge2->register_change_handler(bridge, Bridge::end2Callback);
#endif

#ifdef USE_LOCALIZATION
	trackerLocalizationTarget = new vrpn_Tracker_Remote("Target", connection);
	trackerLocalizationTarget->register_change_handler(compLogger, LocalizationLogger::vrpnTrackerCallback);
#endif
	
	// Initialize all the quadcopter trackables
	for (int i = 0; i < NUM_QUADS; i++) {
		crazyflie_info[i].vrpn_tracker = new vrpn_Tracker_Remote(crazyflie_info[i].vrpn_trackableName, connection);
		crazyflie_info[i].vrpn_tracker->register_change_handler(crazyflie_info[i].cflieCopter,
																CCrazyflie::vrpnTrackerCallback);
	}

	usleep(2000);
}


/**
 * The main VRPN loop
 */
void* vrpn_go(void *threadID) {
	int i = 0;  //for ending the while loop

    printf("Spawning VRPN Thread\n");

	while(!i) {  //1 replace 'i<20000' when done testing ******

		// This must be done before all other VRPN processing
		connection->mainloop();

		// Iterate over every single quadcopter
		for (int j = 0; j < NUM_QUADS; j++) {
			
			// Go through the main loop for the Crazyflie trackables
			crazyflie_info[j].vrpn_tracker->mainloop();
		}

#if USE_HAND
		updateHand();
#endif // END USE_HAND

		loopTimeTotalStart = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;
		loopTimeTotalDelta = loopTimeTotalStart - loopTimeTotalPrev;
		loopTimeTotalPrev = loopTimeTotalStart;

#if USE_HAND
		// Go through the mainloop for the hand trackable
		trackerHand->mainloop();
#endif	// END USE_HAND

#ifdef USE_BRIDGE
		// Main loop the bridge trackable
		trackerBridge1->mainloop();
		trackerBridge2->mainloop();
#endif 	// END USE_BRIDGE

#ifdef USE_LOCALIZATION
	trackerLocalizationTarget->mainloop();
#endif

		usleep(200);  //Was 200

		compLogger->logData();

		// Check if the program should exit
		if (exitProgram) {
			std::cout << "Stopping thread for vrpn" << endl;
			return(NULL);
		}

	}//END WHILE

	loopTimeTotalEnd = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;
	loopTimeTotal = loopTimeTotalEnd - loopTimeTotalStart;
	return (0);
}	//END VRPN GO

void updateHand() {
	if(handMode == 1) {
	//	cout << "Hand Roll: " << rollHand << endl;
		if(killCount > 5) {
			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = GROUNDED_MODE;
			}
			handMode = 0;
			trackHand = 0;
			cout << "Hand Mode: Killing Quads" << endl;
			killCount = 0;
		} else if(trackCount > 5) {
			trackHand = 1;
			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = HAND_MODE;
			}
			trackCount = 0;
		} else if((hand.rollAngle > 70.0) || (hand.rollAngle < -70.0)) {
			killCount++;
		} else if( hand.pitchAngle < -70.0 ) {		//|| (pitchHand > 70.0)
			trackCount++;
		} else{
			killCount = 0;
			trackCount = 0;
			if(hand.zPosition > 1.6) {
	//			trackHand = 0;
				for (int i = 0; i < NUM_QUADS; i++) {
					crazyflie_info[i].cflieCopter->m_enumFlightMode = TAKEOFF_MODE;
				}
				cout << "Hand Mode: Launching Quads" << endl;
			} else if(hand.zPosition < 0.4)	{
				trackHand = 0;
				for (int i = 0; i < NUM_QUADS; i++) {
					crazyflie_info[i].cflieCopter->m_enumFlightMode = LANDING_MODE;
				}
				cout << "Hand Mode: Landing Quads" << endl;
			}
		}
	}
}	//END updateHand();

//////////////////////////////////////////////////
// kbhit                                        //
// Descption: used select to do a non blocking  //
// read of stdin.                               //
//                                              //
//////////////////////////////////////////////////
int kbhit() {
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}



//////////////////////////////////////////////////
// nonblock                                     //
// Descption: Turn off connoical mode so the    //
// user does not need to hit enter after        //
// pressing a key                               //
//                                              //
//////////////////////////////////////////////////
void nonblock(int state) {
    struct termios ttystate;

    //get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    if (state==1) {
        //turn off canonical mode
        ttystate.c_lflag &= ~ICANON;
        //minimum of number input read.
        ttystate.c_cc[VMIN] = 1;
    } else if (state==0) {
        //turn on canonical mode
        ttystate.c_lflag |= ICANON;
    }
    //set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}
