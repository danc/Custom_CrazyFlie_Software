#include "Bridge.h"
#include <vrpn_Tracker.h>
#include "structures.h"
#include "quadcopterData.h"
#include "crazyflieGroundStation.h"
#include <cmath>

Bridge::Bridge() {
	// The endpoints are not valid yet
	this->endpoint1Valid = false;
	this->endpoint2Valid = false;
}

Bridge::~Bridge() {

}


/**
 * This function will find the point located between the two endpoints.
 * If the second endpoint is missing, then the valid endpoint is returned
 * 
 * @param pos Pointer to a lateral position struct
 * @param loc Location between the two endpoints to find (loc must be in [0,1])
 */
bool Bridge::getInterpolatedPosition(lateralPosition *pos, int loc) {

	bool retVal = false;
	if (this->endpoint2Valid && this->endpoint1Valid) {
		// If both endpoints are valid, then interpolate the location between the points
		pos->x = loc * this->endpoint1.x + (1 - loc) * this->endpoint2.x;
 		pos->y = loc * this->endpoint1.y + (1 - loc) * this->endpoint2.y;
 		pos->z = loc * this->endpoint1.z + (1 - loc) * this->endpoint2.z;
 		retVal = true;
	} else if (this->endpoint1Valid) {
		// Return the 1st endpoint
		pos->x = this->endpoint1.x;
		pos->y = this->endpoint1.y;
		pos->z = this->endpoint1.z;
		retVal = true;
	} else if (this->endpoint2Valid) {
		// Return the 2nd endpoint
		pos->x = this->endpoint2.x;
		pos->y = this->endpoint2.y;
		pos->z = this->endpoint2.z;
		retVal = true;
	} else {
		// Neither endpoint is valid
		retVal = false;
	}

	return( retVal );
}

/**
* Calculates the distance between the Bridge and each of the active Crazyflie
* 
* @param pos Pointer to a lateral position struct
* @param dis Distance between Bridge and Quadcopters, up to four
* @return True if a location is valid, false if no location is able to found
*/
bool Bridge::getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1) {
	bool retVal = false;
	if(this->endpoint1Valid){	
			coordinateSystem coor = crazyflie_info[0].cflieCopter->getCoordinateSystem();
			this->disToQuad1.x = this->endpoint1.x - crazyflie_info[0].cflieCopter->getPositionX( coor );
			this->disToQuad1.y = this->endpoint1.y - crazyflie_info[0].cflieCopter->getPositionY( coor );
			this->disToQuad1.z = this->endpoint1.z - crazyflie_info[0].cflieCopter->getPositionZ( coor );
			this->disToQuad1.dis = ((this->disToQuad1.x)*(this->disToQuad1.x))+((this->disToQuad1.y)*(this->disToQuad1.y))+((this->disToQuad1.z)*(this->disToQuad1.z));
			this->disToQuad1.dis = sqrt(this->disToQuad1.dis);
	retVal = true;
	}
	return retVal;
}

bool Bridge::getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1, targetDistance *disToQuad2) {
	bool retVal = false;
	if(this->endpoint1Valid){	
			coordinateSystem coor0 = crazyflie_info[0].cflieCopter->getCoordinateSystem();
			this->disToQuad1.x = this->endpoint1.x - crazyflie_info[0].cflieCopter->getPositionX( coor0 );
			this->disToQuad1.y = this->endpoint1.y - crazyflie_info[0].cflieCopter->getPositionY( coor0 );
			this->disToQuad1.z = this->endpoint1.z - crazyflie_info[0].cflieCopter->getPositionZ( coor0 );
			this->disToQuad1.dis = ((this->disToQuad1.x)*(this->disToQuad1.x))+((this->disToQuad1.y)*(this->disToQuad1.y))+((this->disToQuad1.z)*(this->disToQuad1.z));
			this->disToQuad1.dis = sqrt(this->disToQuad1.dis);
			coordinateSystem coor1 = crazyflie_info[1].cflieCopter->getCoordinateSystem();
			this->disToQuad2.x = this->endpoint1.x - crazyflie_info[1].cflieCopter->getPositionX( coor1 );
			this->disToQuad2.y = this->endpoint1.y - crazyflie_info[1].cflieCopter->getPositionY( coor1 );
			this->disToQuad2.z = this->endpoint1.z - crazyflie_info[1].cflieCopter->getPositionZ( coor1 );
			this->disToQuad2.dis = ((this->disToQuad2.x)*(this->disToQuad2.x))+((this->disToQuad2.y)*(this->disToQuad2.y))+((this->disToQuad2.z)*(this->disToQuad2.z));
			this->disToQuad2.dis = sqrt(this->disToQuad2.dis);
	retVal = true;
	}
	return retVal;
}

 bool Bridge::getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1, targetDistance *disToQuad2, targetDistance *disToQuad3) {
	bool retVal = false;
	if(this->endpoint1Valid){	
			coordinateSystem coor0 = crazyflie_info[0].cflieCopter->getCoordinateSystem();
			this->disToQuad1.x = this->endpoint1.x - crazyflie_info[0].cflieCopter->getPositionX( coor0 );
			this->disToQuad1.y = this->endpoint1.y - crazyflie_info[0].cflieCopter->getPositionY( coor0 );
			this->disToQuad1.z = this->endpoint1.z - crazyflie_info[0].cflieCopter->getPositionZ( coor0 );
			this->disToQuad1.dis = ((this->disToQuad1.x)*(this->disToQuad1.x))+((this->disToQuad1.y)*(this->disToQuad1.y))+((this->disToQuad1.z)*(this->disToQuad1.z));
			this->disToQuad1.dis = sqrt(this->disToQuad1.dis);
			coordinateSystem coor1 = crazyflie_info[1].cflieCopter->getCoordinateSystem();
			this->disToQuad2.x = this->endpoint1.x - crazyflie_info[1].cflieCopter->getPositionX( coor1 );
			this->disToQuad2.y = this->endpoint1.y - crazyflie_info[1].cflieCopter->getPositionY( coor1 );
			this->disToQuad2.z = this->endpoint1.z - crazyflie_info[1].cflieCopter->getPositionZ( coor1 );
			this->disToQuad2.dis = ((this->disToQuad2.x)*(this->disToQuad2.x))+((this->disToQuad2.y)*(this->disToQuad2.y))+((this->disToQuad2.z)*(this->disToQuad2.z));
			this->disToQuad2.dis = sqrt(this->disToQuad2.dis);
			coordinateSystem coor2 = crazyflie_info[2].cflieCopter->getCoordinateSystem();
			this->disToQuad3.x = this->endpoint1.x - crazyflie_info[2].cflieCopter->getPositionX( coor2 );
			this->disToQuad3.y = this->endpoint1.y - crazyflie_info[2].cflieCopter->getPositionY( coor2 );
			this->disToQuad3.z = this->endpoint1.z - crazyflie_info[2].cflieCopter->getPositionZ( coor2 );
			this->disToQuad3.dis = ((this->disToQuad3.x)*(this->disToQuad3.x))+((this->disToQuad3.y)*(this->disToQuad3.y))+((this->disToQuad3.z)*(this->disToQuad3.z));
			this->disToQuad3.dis = sqrt(this->disToQuad3.dis);
	retVal = true;
	}
	return retVal;
}


 bool Bridge::getTargetDistance(lateralPosition *pos, targetDistance *disToQuad1, targetDistance *disToQuad2, targetDistance *disToQuad3, targetDistance *disToQuad4) {
	bool retVal = false;
	if(this->endpoint1Valid){	
			coordinateSystem coor0 = crazyflie_info[0].cflieCopter->getCoordinateSystem();
			this->disToQuad1.x = this->endpoint1.x - crazyflie_info[0].cflieCopter->getPositionX( coor0 );
			this->disToQuad1.y = this->endpoint1.y - crazyflie_info[0].cflieCopter->getPositionY( coor0 );
			this->disToQuad1.z = this->endpoint1.z - crazyflie_info[0].cflieCopter->getPositionZ( coor0 );
			this->disToQuad1.dis = ((this->disToQuad1.x)*(this->disToQuad1.x))+((this->disToQuad1.y)*(this->disToQuad1.y))+((this->disToQuad1.z)*(this->disToQuad1.z));
			this->disToQuad1.dis = sqrt(this->disToQuad1.dis);
			coordinateSystem coor1 = crazyflie_info[1].cflieCopter->getCoordinateSystem();
			this->disToQuad2.x = this->endpoint1.x - crazyflie_info[1].cflieCopter->getPositionX( coor1 );
			this->disToQuad2.y = this->endpoint1.y - crazyflie_info[1].cflieCopter->getPositionY( coor1 );
			this->disToQuad2.z = this->endpoint1.z - crazyflie_info[1].cflieCopter->getPositionZ( coor1 );
			this->disToQuad2.dis = ((this->disToQuad2.x)*(this->disToQuad2.x))+((this->disToQuad2.y)*(this->disToQuad2.y))+((this->disToQuad2.z)*(this->disToQuad2.z));
			this->disToQuad2.dis = sqrt(this->disToQuad2.dis);
			coordinateSystem coor2 = crazyflie_info[2].cflieCopter->getCoordinateSystem();
			this->disToQuad3.x = this->endpoint1.x - crazyflie_info[2].cflieCopter->getPositionX( coor2 );
			this->disToQuad3.y = this->endpoint1.y - crazyflie_info[2].cflieCopter->getPositionY( coor2 );
			this->disToQuad3.z = this->endpoint1.z - crazyflie_info[2].cflieCopter->getPositionZ( coor2 );
			this->disToQuad3.dis = ((this->disToQuad3.x)*(this->disToQuad3.x))+((this->disToQuad3.y)*(this->disToQuad3.y))+((this->disToQuad3.z)*(this->disToQuad3.z));
			this->disToQuad3.dis = sqrt(this->disToQuad3.dis);
			coordinateSystem coor3 = crazyflie_info[3].cflieCopter->getCoordinateSystem();
			this->disToQuad4.x = this->endpoint1.x - crazyflie_info[3].cflieCopter->getPositionX( coor3 );
			this->disToQuad4.y = this->endpoint1.y - crazyflie_info[3].cflieCopter->getPositionY( coor3 );
			this->disToQuad4.z = this->endpoint1.z - crazyflie_info[3].cflieCopter->getPositionZ( coor3 );
			this->disToQuad4.dis = ((this->disToQuad4.x)*(this->disToQuad4.x))+((this->disToQuad4.y)*(this->disToQuad4.y))+((this->disToQuad4.z)*(this->disToQuad4.z));
			this->disToQuad4.dis = sqrt(this->disToQuad4.dis);
	retVal = true;
	}
	return retVal;
}


/**
 * Callbacks for the VRPN trackers on either end of the bridge
 */
void VRPN_CALLBACK Bridge::end1Callback(void *inst, const vrpn_TRACKERCB t) {
	Bridge *bridge = (Bridge*) inst;

	// Save the position data
	bridge->endpoint1.x = t.pos[0];
	bridge->endpoint1.y = t.pos[1];
	bridge->endpoint1.z = t.pos[2];

	// Say the endpoint is valid
	bridge->endpoint1Valid = true;
}

void VRPN_CALLBACK Bridge::end2Callback(void *inst, const vrpn_TRACKERCB t) {
	Bridge *bridge = (Bridge*) inst;

	// Save the position data
	bridge->endpoint2.x = t.pos[0];
	bridge->endpoint2.y = t.pos[1];
	bridge->endpoint2.z = t.pos[2];

	// Say the endpoint is valid
	bridge->endpoint2Valid = true;
}
