#include "crazyflieGroundStation.h"
#include "quadcopterData.h"
#include "callbacks.h"
#include "vrpn_Tracker.h"
#include <quat.h>

#include "helperFunctions.h"

using namespace std;

void processTrackableCallback(int crazyflieNum, int radioNum, 
							  CONTROLLER_DATA_t *controllerData,
							  CCrazyflie *cflieCopter,
							  const vrpn_TRACKERCB t);

/**
 * The VRPN callback for the 1st quadcopter
 */
/*
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t) {
	// Variables needed for the mirror mode
	static double lastTime = crazyflie_info[0].cflieCopter->currentTime();

	// Temp variables for the landing on a bridge
	float landingX = 0;
	float landingY = 0;
	float landingZ = 0;

	// Pull out the X, Y, Z, Yaw position of the quadcopter
	EXTRACT_POSITION(0, t);

	// Get the battery and act accordingly
//	CHECK_BATTERY(0);

	// Don't reset the controller by default
	crazyflie_info[0].controllerData.resetController = 0;
	crazyflie_info[0].controllerData.baseThrust = 0;

	// Just make yaw 0
//	crazyflie_info[0].controllerData.yawSetpoint = 0;
	
	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[0].cflieCopter->m_enumFlightMode) {
	case BRIDGE_HOVER_MODE:
		// Hover over the bridge
		if (bridge1.isSet == 0 || bridge2.isSet == 0) {
			// Bridge trackable isn't set right now, just skip it
			break;
		}

		crazyflie_info[0].controllerData.baseThrust = BASE_THRUST;
		break;
	case BRIDGE_LANDING_MODE:
		if (bridge1.isSet == 0 || bridge2.isSet == 0) {
			// Bridge trackable isn't set right now, just skip it
			break;
		}

		// The quad is above the bridge coordinates, change the Z coordinate
		crazyflie_info[0].controllerData.baseThrust = BASE_THRUST;

		COMPUTE_BRIDGE_COORDINATE(crazyflie_info[0].controllerData.bridgePos,
								  landingX, landingY, landingZ);

		if ( abs( crazyflie_info[0].controllerData.zPosition - landingZ ) < 0.05 ) {
			bridge1.landingState = LANDED;
			crazyflie_info[0].cflieCopter->m_enumFlightMode = GROUNDED_MODE;
			cout << "Landed on bridge\r\n";
		}

		// Land on the bridge
		break;
	case BRIDGE_TAKEOFF_MODE:
		// Take off from the bridge
		if ( !crazyflie_info[0].controllerData.resetSent ) {
			crazyflie_info[0].controllerData.resetController = 1;
			crazyflie_info[0].controllerData.resetSent = true;
		}

		crazyflie_info[0].controllerData.zSetpoint -= 0.5;
		crazyflie_info[0].controllerData.baseThrust = HOVER_THRUST( crazyflie_info[0].takeoff );
		crazyflie_info[0].cflieCopter->m_enumFlightMode = HOVER_MODE;

		break;
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[0].controllerData.zSetpoint = 0.0;
		crazyflie_info[0].controllerData.baseThrust = HOVER_THRUST( crazyflie_info[0].takeoff );

		break;
	case MIRROR_MODE:
		// This quadcopter is the first quad, so it is going to do steps that the others will follow
		if (crazyflie_info[0].cflieCopter->currentTime() - lastTime > 4.0) {
			cout << "Stepping Crazyflies" << endl;
			// Change the setpoint
			crazyflie_info[0].controllerData.xSetpoint = -0.012;
			crazyflie_info[0].controllerData.ySetpoint = -0.200;
			crazyflie_info[0].controllerData.zSetpoint = -0.750;
			lastTime = crazyflie_info[0].cflieCopter->currentTime();
		} else {
			// Change the setpoint back
			cout << "Stepping Crazyflies" << endl;
			crazyflie_info[0].controllerData.xSetpoint = -0.012 + 0.65;
			crazyflie_info[0].controllerData.ySetpoint = -0.200 + 0.65;
			crazyflie_info[0].controllerData.zSetpoint = -0.750;
			lastTime = crazyflie_info[0].cflieCopter->currentTime();
		}
		crazyflie_info[0].controllerData.baseThrust = HOVER_THRUST( crazyflie_info[0].takeoff );

		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		if ( !crazyflie_info[0].controllerData.resetSent ) {
			crazyflie_info[0].controllerData.resetController = 1;
			crazyflie_info[0].controllerData.resetSent = true;
		}
		
		// This function will figure out the appropriate steps for the takeoff routine
		takeoffRoutine(0,
					   &(crazyflie_info[0].controllerData),
					   crazyflie_info[0].cflieCopter,
				       &(crazyflie_info[0].takeoff));

		break;
	case IDLE_MODE:
		crazyflie_info[0].controllerData.baseThrust = 20000;
		break;
	case HOVER_MODE:
		crazyflie_info[0].controllerData.baseThrust = HOVER_THRUST( crazyflie_info[0].takeoff );

		break;
	case HAND_MODE:
		crazyflie_info[0].controllerData.xSetpoint = hand.xPosition + handOffsets[0][0];
		crazyflie_info[0].controllerData.ySetpoint = hand.yPosition + handOffsets[0][1];
		crazyflie_info[0].controllerData.baseThrust = HOVER_THRUST( crazyflie_info[0].takeoff );
		
		break;
	case GROUNDED_MODE:
		crazyflie_info[0].controllerData.baseThrust = 0;
		break;
	default:
		cout << "HELP!!!!" << endl;
		break;
	}

	// Do the main control loop
	processTrackableCallback(0,
							 crazyflie_info[0].radioNumber,
							 &(crazyflie_info[0].controllerData),
							 crazyflie_info[0].cflieCopter,
							 t);
}*/

/**
 * The VRPN callback for the 2nd quadcopter
 */
/*void VRPN_CALLBACK handle_Crazyflie2(void*, const vrpn_TRACKERCB t) {
	// Pull out the X, Y, Z, Yaw position of the quadcopter
	EXTRACT_POSITION(1, t);

	// Temp variables for the landing on a bridge
	float landingX = 0;
	float landingY = 0;
	float landingZ = 0;

	// Get the battery and act accordingly
	CHECK_BATTERY(1);

	// Don't reset the controller by default
	crazyflie_info[1].controllerData.resetController = 0;
	crazyflie_info[1].controllerData.baseThrust = 0;

	// Just make yaw 0
	crazyflie_info[1].controllerData.yawSetpoint = 0;

	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[1].cflieCopter->m_enumFlightMode) {
	case BRIDGE_HOVER_MODE:
		// Hover over the bridge
		if (bridge1.isSet == 0 || bridge2.isSet == 0) {
			// Bridge trackable isn't set right now, just skip it
			break;
		}

		crazyflie_info[1].controllerData.baseThrust = BASE_THRUST;
		break;
	case BRIDGE_LANDING_MODE:
		if (bridge1.isSet == 0 || bridge2.isSet == 0) {
			// Bridge trackable isn't set right now, just skip it
			break;
		}

		// The quad is above the bridge coordinates, change the Z coordinate
		crazyflie_info[1].controllerData.baseThrust = BASE_THRUST;

		COMPUTE_BRIDGE_COORDINATE(crazyflie_info[1].controllerData.bridgePos,
								  landingX, landingY, landingZ);

		if ( abs( crazyflie_info[1].controllerData.zPosition - landingZ ) < 0.05 ) {
			bridge1.landingState = LANDED;
			crazyflie_info[1].cflieCopter->m_enumFlightMode = GROUNDED_MODE;
			cout << "Landed on bridge\r\n";
		}

		// Land on the bridge
		break;
	case BRIDGE_TAKEOFF_MODE:
		// Take off from the bridge
		crazyflie_info[1].controllerData.zSetpoint -= 0.5;
		crazyflie_info[1].controllerData.baseThrust = BASE_THRUST;
		crazyflie_info[1].controllerData.resetController = 1;
		crazyflie_info[1].cflieCopter->m_enumFlightMode = HOVER_MODE;
		break;
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[1].controllerData.zSetpoint = 0;
		crazyflie_info[1].controllerData.baseThrust = BASE_THRUST;
		break;
	case MIRROR_MODE:
		// The quadcopter should mirror the first quadcopter
		crazyflie_info[1].controllerData.xSetpoint = crazyflie_info[0].controllerData.xSetpoint + mirrorOffsets[1][0];
		crazyflie_info[1].controllerData.ySetpoint = crazyflie_info[0].controllerData.ySetpoint + mirrorOffsets[1][1];
		crazyflie_info[1].controllerData.baseThrust = BASE_THRUST;
		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		// The default setpoints are ok for this
		static uint16_t throttle = 0;

		if ( !crazyflie_info[1].controllerData.resetSent ) {
			crazyflie_info[1].controllerData.resetController = 1;
			crazyflie_info[1].controllerData.resetSent = true;
		}

		if (throttle < BASE_THRUST) {
			throttle++;
		}

		//crazyflie_info[1].controllerData.baseThrust = BASE_THRUST + 10000;

		if ( crazyflie_info[1].controllerData.zPosition < -0.3 ) {
			crazyflie_info[1].cflieCopter->m_enumFlightMode = HOVER_MODE;
		}
		break;
	case IDLE_MODE:
		crazyflie_info[1].controllerData.baseThrust = 20000;
		break;
	case HOVER_MODE:
		crazyflie_info[1].controllerData.baseThrust = BASE_THRUST + 10000;
		break;
	case HAND_MODE:
		crazyflie_info[1].controllerData.xSetpoint = hand.xPosition + handOffsets[1][0];
		crazyflie_info[1].controllerData.ySetpoint = hand.yPosition + handOffsets[1][1];
		crazyflie_info[1].controllerData.baseThrust = BASE_THRUST;
		break;
	case GROUNDED_MODE:
		crazyflie_info[1].controllerData.baseThrust = 0;
		break;
	default:
		cout << "HELP!!!!" << endl;
		break;
	}

	// Do the main control loop
//	processTrackableCallback(1, crazyflie_info[1].radioNumber, &(crazyflie_info[1].controllerData), crazyflie_info[1].cflieCopter, t);
		// Do the main control loop
	processTrackableCallback(1,
							 crazyflie_info[1].radioNumber,
							 &(crazyflie_info[1].controllerData),
							 crazyflie_info[1].cflieCopter,
							 t);
}
*/
/**
 * The VRPN callback for the 2nd quadcopter
 */
/*void VRPN_CALLBACK handle_Crazyflie3(void*, const vrpn_TRACKERCB t) {
	// Pull out the X, Y, Z, Yaw position of the quadcopter
	EXTRACT_POSITION(2, t);

	// Get the battery and act accordingly
	CHECK_BATTERY(2);

	// Don't reset the controller by default
	crazyflie_info[2].controllerData.resetController = 0;
	crazyflie_info[2].controllerData.baseThrust = 0;

	// Just make yaw 0
	crazyflie_info[2].controllerData.yawSetpoint = 0;

	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[2].cflieCopter->m_enumFlightMode) {
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[2].controllerData.zSetpoint = 0;
		crazyflie_info[2].controllerData.baseThrust = BASE_THRUST;
		break;
	case MIRROR_MODE:
		// The quadcopter should mirror the first quadcopter
		crazyflie_info[2].controllerData.xSetpoint = crazyflie_info[0].controllerData.xSetpoint + mirrorOffsets[2][0];
		crazyflie_info[2].controllerData.ySetpoint = crazyflie_info[0].controllerData.ySetpoint + mirrorOffsets[2][1];
		crazyflie_info[2].controllerData.baseThrust = BASE_THRUST;
		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		// The default setpoints are ok for this
		crazyflie_info[2].controllerData.baseThrust = BASE_THRUST;
		crazyflie_info[2].controllerData.baseThrust = BASE_THRUST;
		crazyflie_info[2].cflieCopter->m_enumFlightMode = HOVER_MODE;
		break;
	case HOVER_MODE:
		crazyflie_info[2].controllerData.baseThrust = BASE_THRUST;
		break;
	case HAND_MODE:
		crazyflie_info[2].controllerData.xSetpoint = hand.xPosition + handOffsets[2][0];
		crazyflie_info[2].controllerData.ySetpoint = hand.yPosition + handOffsets[2][1];
		crazyflie_info[2].controllerData.baseThrust = BASE_THRUST;
		break;
	case GROUNDED_MODE:
		crazyflie_info[2].controllerData.baseThrust = 0;
		break;
	default:
		cout << "HELP!!!!" << endl;
		break;
	}

	// Do the main control loop
	processTrackableCallback(2, crazyflie_info[2].radioNumber, &(crazyflie_info[2].controllerData), crazyflie_info[2].cflieCopter, t);
}
*/
/**
 * The VRPN callback for the 2nd quadcopter
 */
/*void VRPN_CALLBACK handle_Crazyflie4(void*, const vrpn_TRACKERCB t) {
	// Pull out the X, Y, Z, Yaw position of the quadcopter
	EXTRACT_POSITION(3, t);

	// Get the battery and act accordingly
	CHECK_BATTERY(3);

	// Don't reset the controller by default
	crazyflie_info[3].controllerData.resetController = 0;
	crazyflie_info[3].controllerData.baseThrust = 0;

	// Just make yaw 0
	crazyflie_info[3].controllerData.yawSetpoint = 0;

	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[3].cflieCopter->m_enumFlightMode) {
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[3].controllerData.zSetpoint = 0;
		crazyflie_info[3].controllerData.baseThrust = BASE_THRUST;
		break;
	case MIRROR_MODE:
		// The quadcopter should mirror the first quadcopter
		crazyflie_info[3].controllerData.xSetpoint = crazyflie_info[0].controllerData.xSetpoint + mirrorOffsets[3][0];
		crazyflie_info[3].controllerData.ySetpoint = crazyflie_info[0].controllerData.ySetpoint + mirrorOffsets[3][1];
		crazyflie_info[3].controllerData.baseThrust = BASE_THRUST;
		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		// The default setpoints are ok for this
		crazyflie_info[3].controllerData.baseThrust = BASE_THRUST;
		crazyflie_info[3].controllerData.baseThrust = BASE_THRUST;
		crazyflie_info[3].cflieCopter->m_enumFlightMode = HOVER_MODE;
		break;
	case HOVER_MODE:
		crazyflie_info[3].controllerData.baseThrust = BASE_THRUST;
		break;
	case HAND_MODE:
		crazyflie_info[3].controllerData.xSetpoint = hand.xPosition + handOffsets[3][0];
		crazyflie_info[3].controllerData.ySetpoint = hand.yPosition + handOffsets[3][1];
		crazyflie_info[3].controllerData.baseThrust = BASE_THRUST;
		break;
	case GROUNDED_MODE:
		crazyflie_info[3].controllerData.baseThrust = 0;
		break;
	default:
		cout << "HELP!!!!" << endl;
		break;
	}

	// Do the main control loop
	processTrackableCallback(3, crazyflie_info[3].radioNumber, &(crazyflie_info[3].controllerData), crazyflie_info[3].cflieCopter, t);
}

*/
#if USE_HAND
/**
 * VRPN callback for the hand trackable
 */
void VRPN_CALLBACK handle_hand(void*, const vrpn_TRACKERCB t) {

	if(hand.vrpnPacketQueueLen < 2)	//If VRPN Buffer does NOT have old packets stored up, run main code
	{
		double loopTimeHandStart = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;	
		double loopTimeHandDelta = loopTimeHandStart - hand.loopTimeStartPrevious;	//Calculates time difference between each loop start
		hand.loopTimeStartPrevious = loopTimeHandStart;

		double vrpnPacketTimeHand = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
		double vrpnPacketDeltaHand= vrpnPacketTimeHand - hand.vrpnTimePrevious;			//Calculates time between VRPN Data Packets (almost always 0.01 sec)
		hand.vrpnTimePrevious = vrpnPacketTimeHand;

		hand.vrpnPacketQueueLen = ceil(loopTimeHandDelta/vrpnPacketDeltaHand);		//Calculates estimated # of packets built up in VRPN buffer.

		// Get the euler angles
		q_vec_type euler;
		q_to_euler(euler, t.quat);

		hand.xPosition = t.pos[0];
		hand.yPosition = t.pos[1];
		hand.zPosition = -t.pos[2] + 0.05;

		hand.yawAngle = Q_RAD_TO_DEG(euler[0]);
		hand.rollAngle = Q_RAD_TO_DEG(euler[1]);
		hand.pitchAngle = Q_RAD_TO_DEG(euler[2]);

		double loopTimeHandEnd = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;
		hand.loopTime = loopTimeHandEnd - loopTimeHandStart;

#if USE_LOGGING
		fprintf(hand.logfile, "%.6f\t\t", cflieCopter1->currentTime() - initTime1 );
		fprintf(hand.logfile, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f",
					euler[1], 
					euler[2], 
					euler[0], 
					t.pos[0], 
					t.pos[1], 
					-t.pos[2],
					loopTimeHand,
					loopTimeHandDelta);	//
		fprintf(hand.logfile, "\n");
#endif

	}
	else if(hand.vrpnPacketQueueLen < 0){	//Failsafe so can't go below 0 (should never happen) |   || (vrpnPacketBackup4 > 20)?
		hand.vrpnPacketQueueLen = 0;
	}
	else if(hand.vrpnPacketQueueLen >= 2){
		hand.vrpnPacketQueueLen --;
	}
}
#endif // END USE_HAND

/**
 * This function will process the controller callback for every quadcopter using the
 * data passed into from the VRPM callback for the quadcopter.
 *
 * @param crazyflieNum The number of the crazyflie (in the quadcopter information array)
 * @param radioNum The number of the radio the crazyflie uses (in the radio array)
 * @param controllerData The data structure containing information about the controller
 * @param vrpn_TRACKERCB The VRPN data structure
 */
/*void processTrackableCallback(int crazyflieNum, int radioNum,
							  CONTROLLER_DATA_t *controllerData,
							  CCrazyflie *cflieCopter,
							  const vrpn_TRACKERCB t) {

	if(controllerData->vrpnPacketQueueLen < 2) {
		//If VRPN Buffer does NOT have old packets stored up, run main code

		// Calculate the delta in the control loop time
		double loopStartTime = cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime;
		double loopTimeDelta = loopStartTime - controllerData->loopTimeStartPrevious;
		controllerData->timeBetweenLoop = loopTimeDelta;
		controllerData->loopTimeStartPrevious = loopStartTime;

		// Calculate the delta in the VRPN packet time
		double vrpnPacketTime = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
		double vrpnPacketDelta = vrpnPacketTime - controllerData->vrpnTimePrevious;
		controllerData->vrpnTimePrevious = vrpnPacketTime;

		// Calculates estimated # of packets built up in VRPN buffer.
		controllerData->vrpnPacketQueueLen = ceil(loopTimeDelta/vrpnPacketDelta);

		// Debug statement to say what controller is running
#if USE_PRINTOUT
		// Get the current flightmode
		enum FlightMode flightMode = cflieCopter->m_enumFlightMode;

		if (flightMode != GROUNDED_MODE) {
			cout << endl << "Controller " << crazyflieNum << endl;
			cout << "Battery: " << cflieCopter->batteryLevel() << endl;

			// Output the X, Y, Z position onto the terminal		
			cout << "X: " << controllerData->xPosition << endl;
			cout << "Y: " << controllerData->yPosition << endl;
			cout << "Z: " << controllerData->zPosition << endl;
			cout << "Yaw: " << controllerData->yawPosition << endl;
			cout << "Thrust: " << controllerData->baseThrust << endl;
		}
#endif	// END USE_PRINTOUT

		// This is the meat of the controller stuff
	    cflieCopter->setXYZYawSetpoint(controllerData->xSetpoint,
									   controllerData->ySetpoint,
									   controllerData->zSetpoint,
					  				   controllerData->yawSetpoint,
					  				   controllerData->baseThrust,
									   controllerData->resetController);

	    cflieCopter->setXYZYawPosition(controllerData->xPosition,
									   controllerData->yPosition,
									   controllerData->zPosition,
					  				   controllerData->yawPosition);

	    cflieCopter->setSendSetpoints(true);

//-
		// Safety check the crazyflie, if Crazyflie is vertical kill motors, Crash Imminent
        if (cflieCopter->roll() > 90) {
            thrustControlOutput = 10000;
        } else if (cflieCopter->roll() < -90) {
            thrustControlOutput = 10000;
        } else if (cflieCopter->pitch() > 90) {
            thrustControlOutput = 10000;
        } else if (cflieCopter->pitch() < -90) {
            thrustControlOutput = 10000;
        }
*//*
#if USE_PRINTOUT
        if (cflieCopter->m_enumFlightMode != GROUNDED_MODE) {
            cout << "Roll: " << cflieCopter->roll() << endl;
            cout << "Pitch: " << cflieCopter->pitch() << endl;
            cout << "Yaw: " << cflieCopter->yaw() << endl;
            cout << "Thrust: " << cflieCopter->thrust() << endl;
            cout << "PID: " << endl;
        }
#endif // END USE_PRINTOUT


#if USE_LOGGING		
#if USE_BASIC_LOGGING
		fprintf(crazyflie_info[crazyflieNum].logfile, "%.6f\t\t", crazyflie_info[crazyflieNum].cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime );
		fprintf(crazyflie_info[crazyflieNum].logfile,
				"%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%d",
				cflieCopter->roll(), 
				cflieCopter->pitch(), 
				quadYaw, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg,
				correctedYaw,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime,
				loopTimeDelta,
				vrpnPacketDelta,
				vrpnPacketTime,
				vrpnPacketBackup,
				cflieCopter->radioRSSI());	//
		fprintf(crazyflie_info[crazyflieNum].logfile, "\n");

#else	
		fprintf(crazyflie_info[crazyflieNum].logfile, "%.6f\t\t", cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime );
		fprintf(crazyflie_info[crazyflieNum].logfile,
				"%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%lu\t\t%lu\t\t%lu\t\t%lu\t\t%.3f\t\t%.3f\t\t%.3f",
				cflieCopter->roll(), 
				cflieCopter->pitch(), 
				quadYaw1, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg,
				correctedYaw,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime,
				loopTimeDelta,
				vrpnPacketDelta,
				vrpnPacketTime,
				vrpnPacketBackup,
				xPositionDesired,
				yPositionDesired,
				zPositionDesired,
				yawDesired,
				pitchControlOutput,
				rollControlOutput,
				yawControlOutput,
				thrustControlOutput,
				crazyflie_info[crazyflieNum].cflieCopter->radioRSSI(),
				euler[0],
				euler[1],
				euler[2],
				cflieCopter->motor1(),
				cflieCopter->motor2(),
				cflieCopter->motor3(),
				cflieCopter->motor4(),
				cflieCopter->gyroX(),
				cflieCopter->gyroY(),
				cflieCopter->gyroZ());
		fprintf(crazyflie_info[crazyflieNum].logfile, "\n");
#endif
#endif	// END: USE_LOGGING

		double loopTimeEnd = cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime;
		controllerData->loopTime = loopTimeEnd - loopStartTime;
	} else 	if(controllerData->vrpnPacketQueueLen >= 2) {
		// If the VRPN buffer has more than 2, then skip the packet
		controllerData->vrpnPacketQueueLen --;
		return;
	} else if(controllerData->vrpnPacketQueueLen < 0) {
		// Failsafe so can't go below 0 (should never happen)
		controllerData->vrpnPacketQueueLen = 0;
		return;
	}
}*/
