#include "CCrazyRadio.h"
#include "CCrazyflie.h"

/**
 * Add a Crazyflie to the radio
 *
 * @param flie Pointer to the CCrazyflie object
 */
void CCrazyRadio::addCrazyflie(CCrazyflie *flie) {
	crazyflies.push_back(flie);
	std::cout << "Added Crazyflie " << flie->getQuadcopterNumber()+1;
	std::cout << " to radio " << this->m_nDongleNumber << std::endl;
}

void CCrazyRadio::setARC(int nARC) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(nARC:%d)\n", __FUNCTION__, nARC  );
	m_nARC = nARC;
	this->writeControl(NULL, 0, 0x06, nARC, 0);
}

void CCrazyRadio::setChannel(int nChannel) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(nChannel:%d)\n", __FUNCTION__, nChannel  );
	//m_nChannel = nChannel;  -- removing for multi flie support
	this->writeControl(NULL, 0, 0x01, nChannel, 0);
}

void CCrazyRadio::setDataRate(XferRate rate) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(rate:%d)\n", __FUNCTION__, rate );
	this->writeControl(NULL, 0, 0x03, (int)rate, 0);
}

void CCrazyRadio::setARDTime(int nARDTime) { // in uSec
    if( ALL_THE_DEBUG ) printf( "Enter: %s(nARDTime:%d)\n", __FUNCTION__, nARDTime  );
	m_nARDTime = nARDTime;

	int nT = int((nARDTime / 250) - 1);
	if (nT < 0) {
		nT = 0;
	} else if (nT > 0xf) {
		nT = 0xf;
	}

	this->writeControl(NULL, 0, 0x05, nT, 0);
    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

void CCrazyRadio::setARDBytes(int nARDBytes) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(nARDBytes:%d)\n", __FUNCTION__, nARDBytes  );
	m_nARDBytes = nARDBytes;

	this->writeControl(NULL, 0, 0x05, 0x80 | nARDBytes, 0);
    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

enum Power CCrazyRadio::power() {
	return m_enumPower;
}

void CCrazyRadio::setPower(enum Power enumPower) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(enumPower:%d)\n", __FUNCTION__, enumPower  );
	m_enumPower = enumPower;

	this->writeControl(NULL, 0, 0x04, enumPower, 0);
    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

void CCrazyRadio::setAddress(char *cAddress) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(cAddress:0x%016lx)\n", __FUNCTION__, (unsigned long)cAddress  );
	m_cAddress = cAddress;

	this->writeControl(cAddress, 5, 0x02, 0, 0);
    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

void CCrazyRadio::setContCarrier(bool bContCarrier) {
    if( ALL_THE_DEBUG ) printf( "Enter: %s(bContCarrier:%d)\n", __FUNCTION__, bContCarrier  );
	m_bContCarrier = bContCarrier;

	this->writeControl(NULL, 0, 0x20, (bContCarrier ? 1 : 0), 0);
    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}