// Copyright (c) 2013, Jan Winkler <winkler@cs.uni-bremen.de>
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of Universität Bremen nor the names of its
//       contributors may be used to endorse or promote products derived from
//       this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "CCrazyRadio.h"
#include "CCrazyflie.h"

CCrazyRadio::CCrazyRadio(int dongleNumber) {
	if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	//m_strRadioIdentifier = strRadioIdentifier;  -- multi flie support
	m_enumPower = P_M18DBM;
	m_nDongleNumber = dongleNumber;
	m_ctxContext = NULL;
	m_hndlDevice = NULL;

	m_bAckReceived = false;

	m_exitThread = false;

	/*int nReturn = */libusb_init(&m_ctxContext);

	// Do error checking here.
    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

CCrazyRadio::~CCrazyRadio() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	this->closeDevice();

	if (m_ctxContext) {
		libusb_exit(m_ctxContext);
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
}

// returns true if successfully started, else false on error
bool CCrazyRadio::startRadio() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	if (this->openUSBDongle()) {
		std::stringstream sts;

		// Read device version
		libusb_device_descriptor ddDescriptor;
		libusb_get_device_descriptor(m_devDevice, &ddDescriptor);
		sts.clear();
		sts.str(std::string());
		sts << (ddDescriptor.bcdDevice >> 8);
		sts << ".";
		sts << (ddDescriptor.bcdDevice & 0x0ff);
		std::sscanf(sts.str().c_str(), "%f", &m_fDeviceVersion);

		std::cout << "Got device version " << m_fDeviceVersion << std::endl;
		if (m_fDeviceVersion < 0.3) {
		    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
			return false;
		}

		// Set active configuration to 1
		libusb_set_configuration(m_hndlDevice, 1);

		std::cout << "Attempt Interface Claim" << std::endl;

		// Claim interface
		if (this->claimInterface(0)) {
			// Set power-up settings for dongle (>= v0.4)

			std::cout << "Interface Claim Success" << std::endl;

            if( ALL_THE_DEBUG ) printf( "setting channel to 2\n" );
            this->setDataRate(XFER_2M);
            this->setChannel(2);

			if (m_fDeviceVersion >= 0.4) {

				std::cout << "Device Version > 0.4" << std::endl;

				this->setContCarrier(false);
				char cAddress[5];
				cAddress[0] = 0xe7;
				cAddress[1] = 0xe7;
				cAddress[2] = 0xe7;
				cAddress[3] = 0xe7;
				cAddress[4] = 0xe7;
				this->setAddress(cAddress);
				this->setPower(P_0DBM);
				this->setARC(3);
				this->setARDBytes(32);
			}

			// Initialize device
			if (m_fDeviceVersion >= 0.4) {
				this->setARC(10);
			}

            if( ALL_THE_DEBUG ) printf( "*HACK* hardcoding channel to 60\n" );
            this->setChannel(60);
            this->setDataRate(XFER_2M);

    		if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
			return true;
		}
	}

    if( ALL_THE_DEBUG ) printf( "Exit: %s\n", __FUNCTION__  );
	return false;
}

bool CCrazyRadio::ackReceived() {
    if( ALL_THE_DEBUG ) printf( "Enter: %s\n", __FUNCTION__  );
	return m_bAckReceived;
}
