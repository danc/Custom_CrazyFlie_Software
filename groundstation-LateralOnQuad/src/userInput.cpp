#include <pthread.h>
#include <stdio.h>
#include <curses.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <pty.h>
#include <stdint.h>

#include "CCrazyflie.h"
#include "quadcopterData.h"
#include "userInput.h"
#include "callbacks.h"
#include "crazyflieGroundStation.h"
#include "helperFunctions.h"

using namespace std;

/**
 * This thread will get user input
 */
void* UIThread(void *threadID) {
	char tempbuf[255];
	uint8_t counter = 0;

	struct termios ttystate;

	printf("Spawning User Input Thread\n");

    //get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    ttystate.c_lflag &= ~ICANON;		//turn off canonical mode
    ttystate.c_cc[VMIN] = 1;		    //minimum of number input read

    //set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);

	// Loop forever while getting input
	while (1) {

		// Get a character from the terminal
		tempbuf[counter] = getchar();

		// If the character is a newline, the command is entered
		if (tempbuf[counter] == '\n') {
			// Add a null terminator to the buffer
			tempbuf[counter] = 0;

			// No command to parse
			if ( counter == 0 ) {
				continue;
			}

			uint8_t commandLength = strlen(tempbuf);

			if ( commandLength < 2) {
				if (tempbuf[0] == 'q') {
					// Exit the program
					ctrlc_handler(1);
					break;
				} else {
					// Invalid command
					counter = 0;
					continue;
				}
			}

			// Get the quadcopter number
			char temp[2];
			temp[0] = tempbuf[0];
			temp[1] = 0;
			uint8_t quadcopterNumber = atoi( temp );

			// Get the command type and the payload
			userCommands_t command;
			command.type = tempbuf[1];
			command.payload = atof(&tempbuf[2]);

			// Pass the command to the Crazyflie
			if (quadcopterNumber == 0) {
				// This command is for every quadcopter
				for (int i=0; i < NUM_QUADS; i++) {
					crazyflie_info[i].cflieCopter->passUserCommand(command);
				}
			} else {
				crazyflie_info[quadcopterNumber-1].cflieCopter->passUserCommand(command);
			}

			// Print it out and then reset the counter
			printf("\nReceived command: %s\n", tempbuf);
			counter = 0;

		} else {
			counter++;
		}

		// Check if the program should terminate
		if (exitProgram) {
			std::cout << "Stopping thread for user input" << endl;
			return(NULL);
		}
		usleep(1000);
	}

	std::cout << "Stopping thread for user input" << endl;
    return NULL;
}
