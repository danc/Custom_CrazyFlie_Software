Custom CrazyFlie Firmware

This repository contains various firmware versions developed for the CrazyFlie 2.0.

Firmware Versions

crazyflie-firmware-2014.12.1 - This is the mainline of the CrazyFlie firmware from December 2014


Modified Scripts
This directory contains modified scripts to allow for compilation. Follow the instructions in the included Readme.
