/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie Firmware
 *
 * Copyright (C) 2011-2016 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include <math.h>
#include <stdlib.h>

#include "FreeRTOS.h"
#include "task.h"

#include "system.h"
#include "log.h"
#include "param.h"
#include "debug.h"
#include "motors.h"

#include "stabilizer.h"

#include "sensors.h"
#include "commander_XYZ.h"
#include "sitaw.h"
#include "controller.h"
#include "power_distribution.h"
#include "controller_update.h"
#include "computation_func.h"

#ifdef ESTIMATOR_TYPE_kalman
#include "estimator_kalman.h"
#else
#include "estimator.h"
#endif

static bool isInit;

// State variables for the stabilizer
static setpoint_t setpoint;
static sensorData_t sensorData;
static state_t state;
static control_t control;

static uint8_t resetControllers;

// Variables to hold the amount of time things have taken
static uint16_t controllerDuration;
static uint16_t estimatorDuration;

static void stabilizerTask(void* param);

void stabilizerInit(void)
{
  if(isInit)
    return;

  sensorsInit();
  controllerUpdate_Init();
  stateEstimatorInit();
  stateControllerInit();
  powerDistributionInit();
#if defined(SITAW_ENABLED)
  sitAwInit();
#endif

  xTaskCreate(stabilizerTask, STABILIZER_TASK_NAME,
              STABILIZER_TASK_STACKSIZE, NULL, STABILIZER_TASK_PRI, NULL);

#ifdef BYPASS_CONTROLLERS
  #pragma message "Bypassing Controllers"
  DEBUG_PRINT("Controllers Bypassed\r\n");
#endif

  isInit = true;
}

bool stabilizerTest(void)
{
  bool pass = true;

  pass &= sensorsTest();
  pass &= stateEstimatorTest();
  pass &= stateControllerTest();
  pass &= powerDistributionTest();

  return pass;
}

/* The stabilizer loop runs at 1kHz (stock) or 500Hz (kalman). It is the
 * responsibility of the different functions to run slower by skipping call
 * (ie. returning without modifying the output structure).
 */

static void stabilizerTask(void* param)
{
  uint32_t tick = 0;
  uint32_t lastWakeTime;
  vTaskSetApplicationTaskTag(0, (void*)TASK_STABILIZER_ID_NBR);

  //Wait for the system to be fully started to start stabilization loop
  systemWaitStart();

  // Wait for sensors to be calibrated
  lastWakeTime = xTaskGetTickCount ();
  while(!sensorsAreCalibrated()) {
    vTaskDelayUntil(&lastWakeTime, F2T(RATE_MAIN_LOOP));
  }

  while(1) {
    vTaskDelayUntil(&lastWakeTime, F2T(RATE_MAIN_LOOP));

    uint32_t estimatorStart = xTaskGetTickCount();

#ifdef ESTIMATOR_TYPE_kalman
    stateEstimatorUpdate(&state, &sensorData, &control);
#else
    sensorsAcquire(&sensorData, tick);
    stateEstimator(&state, &sensorData, tick);
    commanderXYZ_GetPosition(&state, tick);
#endif

    uint32_t estimatorEnd = xTaskGetTickCount();

    commanderXYZ_GetSetpoint(&setpoint, &state, &resetControllers);

    // Interface the control loop with the computation system
    computation_hook_state(&state, &sensorData);
    computation_hook_setpoints(&setpoint);

    #ifndef BYPASS_CONTROLLERS
      uint32_t controllerStart = xTaskGetTickCount();
      stateController(&control, &sensorData, &state, &setpoint, tick, resetControllers);

      // Perform a check to see if the quadcopter is upside-down
      if ( ( abs(state.attitude.roll) > 100 ) || ( abs(state.attitude.pitch) > 100 ) ) {
        // Zero out the control inputs
        control.thrust = 0;
        control.roll = 0;
        control.pitch = 0;
        control.yaw = 0;
      }

    #else
      control.thrust = setpoint.position.x;
      control.roll = setpoint.position.y;
      control.pitch = setpoint.position.z;
      control.yaw = setpoint.attitude.yaw;
    #endif

    powerDistribution(&control);

    uint32_t controllerEnd = xTaskGetTickCount();

    // Convert the number of ticks to milliseconds elapsed
    controllerDuration = (controllerEnd - controllerStart) * (1000/configTICK_RATE_HZ);
    estimatorDuration = (estimatorEnd - estimatorStart) * (1000/configTICK_RATE_HZ);


    tick++;
  }
}

LOG_GROUP_START(controlStats)
LOG_ADD(LOG_UINT16, estimDur, &estimatorDuration)
LOG_ADD(LOG_UINT16, contDur, &controllerDuration)
LOG_GROUP_STOP(controlStats)

LOG_GROUP_START(ctrltarget)
LOG_ADD(LOG_FLOAT, roll, &setpoint.attitude.roll)
LOG_ADD(LOG_FLOAT, pitch, &setpoint.attitude.pitch)
LOG_ADD(LOG_FLOAT, yaw, &setpoint.attitudeRate.yaw)
LOG_GROUP_STOP(ctrltarget)

LOG_GROUP_START(stabilizer)
LOG_ADD(LOG_FLOAT, roll, &state.attitude.roll)
LOG_ADD(LOG_FLOAT, pitch, &state.attitude.pitch)
LOG_ADD(LOG_FLOAT, yaw, &state.attitude.yaw)
LOG_ADD(LOG_UINT16, thrust, &control.thrust)
LOG_GROUP_STOP(stabilizer)

LOG_GROUP_START(acc)
LOG_ADD(LOG_FLOAT, x, &sensorData.acc.x)
LOG_ADD(LOG_FLOAT, y, &sensorData.acc.y)
LOG_ADD(LOG_FLOAT, z, &sensorData.acc.z)
LOG_GROUP_STOP(acc)

LOG_GROUP_START(baro)
LOG_ADD(LOG_FLOAT, asl, &sensorData.baro.asl)
LOG_ADD(LOG_FLOAT, temp, &sensorData.baro.temperature)
LOG_ADD(LOG_FLOAT, pressure, &sensorData.baro.pressure)
LOG_GROUP_STOP(baro)

LOG_GROUP_START(gyro)
LOG_ADD(LOG_FLOAT, x, &sensorData.gyro.x)
LOG_ADD(LOG_FLOAT, y, &sensorData.gyro.y)
LOG_ADD(LOG_FLOAT, z, &sensorData.gyro.z)
LOG_GROUP_STOP(gyro)

LOG_GROUP_START(mag)
LOG_ADD(LOG_FLOAT, x, &sensorData.mag.x)
LOG_ADD(LOG_FLOAT, y, &sensorData.mag.y)
LOG_ADD(LOG_FLOAT, z, &sensorData.mag.z)
LOG_GROUP_STOP(mag)

LOG_GROUP_START(mixer)
LOG_ADD(LOG_FLOAT, ctr_thrust, &control.thrust)
LOG_ADD(LOG_INT16, ctr_roll, &control.roll)
LOG_ADD(LOG_INT16, ctr_pitch, &control.pitch)
LOG_ADD(LOG_INT16, ctr_yaw, &control.yaw)
LOG_GROUP_STOP(mixer)
