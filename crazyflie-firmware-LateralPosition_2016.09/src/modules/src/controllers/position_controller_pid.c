/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie Firmware
 *
 * Copyright (C) 2016 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * position_estimator_pid.c: PID-based implementation of the position controller
 */

#include <math.h>
#include "num.h"

#include "commander.h"
#include "log.h"
#include "param.h"
#include "pid.h"
#include "num.h"
#include "position_controller.h"
#include "controller_pid.h"

#include "debug.h"

struct pidConst_s {
  float kp;
  float ki;
  float kd;
  float limit;
};

// PID Controllers
PidObject pid_x;
PidObject pid_y;
PidObject pid_z;

// PID Constants
struct pidConst_s pid_x_const = {
  .kp = -10,
  .ki = -0.5,
  .kd = -11,
  .limit = 40
};
struct pidConst_s pid_y_const = {
  .kp = 10,
  .ki = 0.5,
  .kd = 11,
  .limit = 40
};
struct pidConst_s pid_z_const = {
  .kp = -10000,
  .ki = -2000,
  .kd = -15000,
  .limit = 10000
};

static float pid_x_setpoint = 0;
static float pid_y_setpoint = 0;
static float pid_z_setpoint = 0;

static float pid_x_output = 0;
static float pid_y_output = 0;
static float pid_z_output = 0;

// Maximum roll/pitch angle permited
static float rpLimit = 20;

#define USE_PSEUDO_NONLINEAR_CORRECTION

#define POSITION_DT 0.01

/*
 * Initialize the PID controllers
 */
void positionControllerInit() {
  pidInit(&pid_x, 0, pid_x_const.kp, pid_x_const.ki, pid_x_const.kd, POSITION_DT);
  pidInit(&pid_y, 0, pid_y_const.kp, pid_y_const.ki, pid_y_const.kd, POSITION_DT);
  pidInit(&pid_z, 0, pid_z_const.kp, pid_z_const.ki, pid_z_const.kd, POSITION_DT);

  pidSetIntegralLimit(&pid_x, pid_x_const.limit);
  pidSetIntegralLimit(&pid_y, pid_y_const.limit);
  pidSetIntegralLimit(&pid_z, pid_z_const.limit);

  controller_PID_RegisterPID(0, &pid_x);
  controller_PID_RegisterPID(1, &pid_y);
  controller_PID_RegisterPID(2, &pid_z);
}

void positionController(float* thrust, attitude_t *attitude, const state_t *state,
                                                             const setpoint_t *setpoint)
{

//  DEBUG_PRINT("POS X:%4.3f Y:%4.3f Z:%4.3f\n", state->position.x, state->position.y, state->position.z);
  
  // Store the setpoint information
  pid_x_setpoint = setpoint->position.x;
  pid_y_setpoint = setpoint->position.y;
  pid_z_setpoint = setpoint->position.z;

//  DEBUG_PRINT("SET X:%4.3f Y:%4.3f Z:%4.3f\n", pid_x_setpoint, pid_y_setpoint, pid_z_setpoint);

  // Implement the pseudo-nonlinear extension to take into account the yaw
  float xerror = pid_x_setpoint - state->position.x;
  float yerror = pid_y_setpoint - state->position.y;

  // If the correction should be done, do it
#ifdef USE_PSEUDO_NONLINEAR_CORRECTION
  float yawRad = state->cameraYaw * (float)M_PI / 180;
  xerror = (xerror*cosf(yawRad)) - (yerror*sinf(yawRad));
  yerror = (xerror*sinf(yawRad)) + (yerror*cosf(yawRad));
#endif

  // Compute the X PID
  pidSetError(&pid_x, xerror);
  pid_x_output = pidUpdate(&pid_x, state->position.x, false);

  // Compute the Y PID
  pidSetError(&pid_y, yerror);
  pid_y_output = pidUpdate(&pid_y, state->position.y, false);

  // Compute the Z PID  
  pidSetDesired(&pid_z, pid_z_setpoint);
  pid_z_output = pidUpdate(&pid_z, state->position.z, true);

  // Saturate the roll and pitch to their max limit
  attitude->roll = max(min(pid_y_output, rpLimit), -rpLimit);
  attitude->pitch = max(min(pid_x_output, rpLimit), -rpLimit);
  
  attitude->yaw = setpoint->attitude.yaw;

  // Set thrust
  *thrust = pid_z_output;
}

void positionControllerResetAllPID() {
  pidReset(&pid_x);
  pidReset(&pid_y);
  pidReset(&pid_z);
}


LOG_GROUP_START(posCtlAlt)
LOG_ADD(LOG_FLOAT, targetX, &pid_x_setpoint)
LOG_ADD(LOG_FLOAT, targetY, &pid_y_setpoint)
LOG_ADD(LOG_FLOAT, targetZ, &pid_z_setpoint)

LOG_ADD(LOG_FLOAT, outx, &pid_x_output)
LOG_ADD(LOG_FLOAT, outy, &pid_y_output)
LOG_ADD(LOG_FLOAT, outz, &pid_z_output)
LOG_GROUP_STOP(posCtlAlt)

PARAM_GROUP_START(posCtlPid)

PARAM_ADD(PARAM_FLOAT, xKp, &pid_x.kp)
PARAM_ADD(PARAM_FLOAT, xKi, &pid_x.ki)
PARAM_ADD(PARAM_FLOAT, xKd, &pid_x.kd)

PARAM_ADD(PARAM_FLOAT, yKp, &pid_y.kp)
PARAM_ADD(PARAM_FLOAT, yKi, &pid_y.ki)
PARAM_ADD(PARAM_FLOAT, yKd, &pid_y.kd)

PARAM_ADD(PARAM_FLOAT, zKp, &pid_z.kp)
PARAM_ADD(PARAM_FLOAT, zKi, &pid_z.ki)
PARAM_ADD(PARAM_FLOAT, zKd, &pid_z.kd)

PARAM_ADD(PARAM_FLOAT, rpLimit, &rpLimit)
PARAM_GROUP_STOP(posCtlPid)
