#include <string.h>

#include "stabilizer.h"
#include "stabilizer_types.h"
#include "debug.h"
#include "crtp.h"

#include "controller.h"
#include "attitude_controller.h"
#include "sensfusion6.h"
#include "position_controller.h"
#include "controller_pid.h"
#include "controller_update.h"
#include "pm.h"
#include "log.h"
#include "param.h"

#define ATTITUDE_RATE RATE_500_HZ
#define POSITION_RATE RATE_100_HZ

const int stateControllerType = CONTROLLER_PID;

static bool tiltCompensationEnabled = false;

static attitude_t attitudeDesired;
static attitude_t rateDesired;
static float actuatorThrust;

// Flag for whether or not the thrust controller should be disabled
static int disableThrustControl = 0;

#define CONTROLLER_PID_NUM_PIDS 9

static PidObject* registeredPID[CONTROLLER_PID_NUM_PIDS];

void stateControllerInit(void)
{
  #pragma message "Using PID Controller"
  DEBUG_PRINT("Using PID Controller\r\n");
  positionControllerInit();
  attitudeControllerInit();
}

bool stateControllerTest(void)
{
  bool pass = true;

  pass &= attitudeControllerTest();

  return pass;
}

/**
 * Register a PID controller so its constants can be modified
 *
 * @param controllerID The ID number for the PID controller
 * @param pid The PidObject to modify
 */
void controller_PID_RegisterPID(uint8_t controllerID, PidObject* pid) {
  // If the ID is a valid ID, store the PID into the list
  if (controllerID < CONTROLLER_PID_NUM_PIDS) {
    registeredPID[controllerID] = pid;
  }
}

/**
 * Callback function called by a CRTP port callback.
 * This callback will enable or disable the throttle controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerThrottleUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Create the packet header
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_SWITCH_THROTTLE_CTRL);
  sendP->size = 1;

  switch ( recvP->data[0] ) {
    case 1:
      // Disable the Z controller output
      disableThrustControl = 1;
      sendP->data[0] = 1;
      DEBUG_PRINT("Disabling Thrust Control\n");
      break;
    case 2:
      // Enable the Z controller output
      disableThrustControl = 0;
      DEBUG_PRINT("Enabling Thrust Control\n");
      sendP->data[0] = 0;
      break;
  }
}

/*
 * Callback function called by a CRTP port callback.
 * This function will allow the client/groundstation to modify
 * parameters of the state controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Copy the received gains into a data structure for parsing
  struct controller_PID_CrtpGainValues gains;
  memcpy(&gains, recvP->data, 17);

  // Create the packet header
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_CALL_UPDATE);
  sendP->size = 1;

  // Default to saying that the requested controller is not valid
  sendP->data[0] = 1;

  // Actually go through and update the gains
  uint8_t id = gains.controllerID;
  if (id < CONTROLLER_PID_NUM_PIDS) {
    registeredPID[id]->kp = gains.Kp;
    registeredPID[id]->kd = gains.Kd;
    registeredPID[id]->ki = gains.Ki;
    registeredPID[id]->iLimit = gains.iLimit;

    DEBUG_PRINT("UP %d, Kp=%2.2f\n", id, registeredPID[id]->kp);
    DEBUG_PRINT("UP %d, Ki=%2.2f\n", id, registeredPID[id]->ki);
    DEBUG_PRINT("UP %d, Kd=%2.2f\n", id, registeredPID[id]->kd);
    DEBUG_PRINT("UP %d, ilim=%2.2f\n", id, registeredPID[id]->iLimit);

    // Return a success to the client
    sendP->data[0] = 0;
  }
}

void stateController(control_t *control, const sensorData_t *sensors,
                                         const state_t *state,
                                         const setpoint_t *setpoint,
                                         const uint32_t tick,
                                         const uint8_t resetControllers)
{
  // If the thrust setpoint is 0, then nothing should be moving
  if (setpoint->thrust == 0) {
    control->thrust = 0;
    control->roll = 0;
    control->pitch = 0;
    control->yaw = 0;
    return;
  }

  // Reset the PID controllers
  if (resetControllers == 1) {
    DEBUG_PRINT("Controller Reset\n");
    attitudeControllerResetAllPID();
    positionControllerResetAllPID();

    // Reset the calculated YAW angle for rate control
    attitudeDesired.yaw = state->cameraYaw;
  }


  if (RATE_DO_EXECUTE(POSITION_RATE, tick)) {
    positionController(&actuatorThrust, &attitudeDesired, state, setpoint);
  }

  if (RATE_DO_EXECUTE(ATTITUDE_RATE, tick)) {
    attitudeControllerCorrectAttitudePID(state->attitude.roll, state->attitude.pitch, state->cameraYaw,
                                attitudeDesired.roll, attitudeDesired.pitch, attitudeDesired.yaw,
                                &rateDesired.roll, &rateDesired.pitch, &rateDesired.yaw);

    // Removed negation on gyro.y feed into pitch rate controller
    attitudeControllerCorrectRatePID(sensors->gyro.x, sensors->gyro.y, sensors->gyro.z,
                             rateDesired.roll, rateDesired.pitch, rateDesired.yaw);

    attitudeControllerGetActuatorOutput(&control->roll,
                                        &control->pitch,
                                        &control->yaw);
  }

  // Set thrust and then saturate it
  float thrust = 0;
  if (disableThrustControl == 1) {
    // Disable the thrust controller
    thrust = setpoint->thrust;
  } else {
    // Enable the thrust controller
    thrust = actuatorThrust + setpoint->thrust;
  }
  if (thrust > UINT16_MAX) {
    thrust = UINT16_MAX;
  } else if (thrust < 0) {
    thrust = 0;
  }

  control->thrust = controller_thrustAdjustment( pmGetBatteryVoltage() ) * thrust;
}


LOG_GROUP_START(controller)
LOG_ADD(LOG_FLOAT, actuatorThrust, &actuatorThrust)
LOG_ADD(LOG_FLOAT, roll, &attitudeDesired.roll)
LOG_ADD(LOG_FLOAT, pitch, &attitudeDesired.pitch)
LOG_ADD(LOG_FLOAT, yaw, &attitudeDesired.yaw)
LOG_GROUP_STOP(controller)

PARAM_GROUP_START(controller)
PARAM_ADD(PARAM_UINT8, tiltComp, &tiltCompensationEnabled)
PARAM_GROUP_STOP(controller)
