#include <stdint.h>
#include "crtp.h"
#include "controller.h"
#include "controller_update.h"

#include "debug.h"

// Is the stuff initialized
static bool isInit = 0;

// Packet structure used to return stuff to the client
static CRTPPacket p;

/**
 * Callback for the controller update CRTP packet
 */
static void controllerUpdate_GainsCB(CRTPPacket* pk) {

  switch( pk->channel ) {
    case CONTROLLER_SWITCH_THROTTLE_CTRL:
      // The throttle controller is being modified, call the callback
      stateControllerThrottleUpdate(pk, &p);
      break;
    case CONTROLLER_CALL_UPDATE:
      // Call the state controller's update routine with the packet
      stateControllerUpdate(pk, &p);
      break;
    case CONTROLLER_QUERY_TYPE:
    default:
      // Unknown channel, just return the controller type
      p.header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_QUERY_TYPE);
      p.size = 1;
      p.data[0] = stateControllerType;
  }

  // Pass the return packet for sending
  crtpSendPacket(&p);
}

/* Public functions */
void controllerUpdate_Init(void) {
  if(isInit) {
    return;
  }

  crtpInit();
  crtpRegisterPortCB(CRTP_PORT_CONTROLLER_UPDATE, controllerUpdate_GainsCB);

  isInit = true;
}

bool controllerUpdate_Test(void) {
  crtpTest();
  return isInit;
}
