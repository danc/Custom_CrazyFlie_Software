#include "controller.h"
#include "controller_update.h"
#include "stabilizer.h"
#include "stabilizer_types.h"
#include "debug.h"
#include <math.h>

#include "sensfusion6.h"

#include "pm.h"
#include "log.h"
#include "param.h"
#include "crtp.h"

enum stateOrdering {
 state_u = 0,
 state_v,
 state_w,
 state_p,
 state_q,
 state_r,
 state_x,
 state_y,
 state_z,
 state_phi,
 state_theta,
 state_psi
};

static int disableThrustControl = 0;

// Variable to signify what kind of controller this is
const int stateControllerType = CONTROLLER_LQR;

#define NUM_LQR_STATES  12

#define CONTROLLER_RATE RATE_100_HZ

#define DEG_TO_RAD_F(x)  ( x * ((float)M_PI / 180.0))

#define USE_PSEUDO_NONLINEAR_CORRECTION

/* This is a very aggressive LQR, and is very oscillatory on X & Y (100 weight on X & Y)
float K[4][12] = {
{ 0.000, 0.000, -6362.910, 0.000, 0.000, 0.000, 0.000, 0.000, -5000.000, 0.000, 0.000, 0.000 },
{ 0.000, 5109.467, 0.000, 1636.731, 0.000, 0.000, 0.000, 10000.000, 0.000, 12809.304, 0.000, 0.000 },
{ -5117.338, 0.000, 0.000, 0.000, 1644.304, 0.000, -10000.000, 0.000, 0.000, 0.000, 12848.790, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1822.820, 0.000, 0.000, 0.000, 0.000, 0.000, 5000.000 }
};*/
/* This is somewhat oscillatory on X & Y (50 weight on X & Y)
float K[4][12] = {
{ 0.000, 0.000, -6362.910, 0.000, 0.000, 0.000, 0.000, 0.000, -5000.000, 0.000, 0.000, 0.000 },
{ 0.000, 3037.715, 0.000, 1376.145, 0.000, 0.000, 0.000, 5000.000, 0.000, 9056.373, 0.000, 0.000 },
{ -3042.397, 0.000, 0.000, 0.000, 1382.513, 0.000, -5000.000, 0.000, 0.000, 0.000, 9084.294, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1822.820, 0.000, 0.000, 0.000, 0.000, 0.000, 5000.000 }
};*/

/* This is still oscillatory on X & Y (30 weight on X & Y)
float K[4][12] = {
{ 0.000, 0.000, -6005.247, 0.000, 0.000, 0.000, 0.000, 0.000, -5000.000, 0.000, 0.000, 0.000 },
{ 0.000, 2071.112, 0.000, 1226.387, 0.000, 0.000, 0.000, 3000.000, 0.000, 7059.255, 0.000, 0.000 },
{ -2074.344, 0.000, 0.000, 0.000, 1232.087, 0.000, -3000.000, 0.000, 0.000, 0.000, 7081.161, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1846.069, 0.000, 0.000, 0.000, 0.000, 0.000, 5000.000 }
};*/

/* (20 weight on X & Y) 
float K[4][12] = {
{ 0.000, 0.000, -6005.247, 0.000, 0.000, 0.000, 0.000, 0.000, -5000.000, 0.000, 0.000, 0.000 },
{ 0.000, 1526.125, 0.000, 1106.506, 0.000, 0.000, 0.000, 2000.000, 0.000, 5755.885, 0.000, 0.000 },
{ -1528.513, 0.000, 0.000, 0.000, 1111.654, 0.000, -2000.000, 0.000, 0.000, 0.000, 5773.771, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1846.069, 0.000, 0.000, 0.000, 0.000, 0.000, 5000.000 }
};*/

/* (20 weight on X & Y, 4 weight on angles)
float K[4][12] = {
{ 0.000, 0.000, -6005.247, 0.000, 0.000, 0.000, 0.000, 0.000, -5000.000, 0.000, 0.000, 0.000 },
{ 0.000, 1429.607, 0.000, 1036.449, 0.000, 0.000, 0.000, 2000.000, 0.000, 5406.229, 0.000, 0.000 },
{ -1432.155, 0.000, 0.000, 0.000, 1041.498, 0.000, -2000.000, 0.000, 0.000, 0.000, 5424.114, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1846.069, 0.000, 0.000, 0.000, 0.000, 0.000, 5000.000 }
};*/

/* These gains seem to work nicely. They do have some oscillation on the Z axis immediately after takeoff
float K[4][12] = {
{ 0.000, 0.000, -12413.329, 0.000, 0.000, 0.000, 0.000, 0.000, -7500.000, 0.000, 0.000, 0.000 },
{ 0.000, 2803.265, 0.000, 1278.407, 0.000, 0.000, 0.000, 2500.000, 0.000, 8169.974, 0.000, 0.000 },
{ -2805.337, 0.000, 0.000, 0.000, 1284.241, 0.000, -2500.000, 0.000, 0.000, 0.000, 8192.772, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1911.929, 0.000, 0.000, 0.000, 0.000, 0.000, 5000.000 }
};*/

float K[4][12] = {
{ 0.000, 0.000, -16584.814, 0.000, 0.000, 0.000, 0.000, 0.000, -7500.000, 0.000, 0.000, 0.000 },
{ 0.000, 3251.943, 0.000, 1365.118, 0.000, 0.000, 0.000, 2500.000, 0.000, 9085.910, 0.000, 0.000 },
{ -3253.849, 0.000, 0.000, 0.000, 1371.253, 0.000, -2500.000, 0.000, 0.000, 0.000, 9110.242, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1927.848, 0.000, 0.000, 0.000, 0.000, 0.000, 5000.000 }
};

// Array to store the calculated errors
float error[NUM_LQR_STATES];

static inline int16_t saturateSignedInt16(float in)
{
  // don't use INT16_MIN, because later we may negate it, which won't work for that value.
  if (in > INT16_MAX)
    return INT16_MAX;
  else if (in < -INT16_MAX)
    return -INT16_MAX;
  else
    return (int16_t)in;
}

void stateControllerInit(void)
{
  #pragma message "Using LQR Controller"
  DEBUG_PRINT("Using LQR Controller\r\n");

  // Make sure thrust is enabled
  disableThrustControl = 0;
}

bool stateControllerTest(void)
{
  bool pass = true;

  return pass;
}


/**
 * Callback function called by a CRTP port callback.
 * This callback will enable or disable the throttle controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerThrottleUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Create the packet header
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_SWITCH_THROTTLE_CTRL);
  sendP->size = 1;

  switch ( recvP->data[0] ) {
    case 1:
      // Disable the Z controller output
      disableThrustControl = 1;
      sendP->data[0] = 1;
      DEBUG_PRINT("Disabling Thrust Control\n");
      break;
    case 2:
      // Enable the Z controller output
      disableThrustControl = 0;
      sendP->data[0] = 0;
      DEBUG_PRINT("Enabling Thrust Control\n");
      break;
  }
}

/*
 * Callback function called by a CRTP port callback.
 * This function will allow the client/groundstation to modify
 * parameters of the state controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Create the packet header and put garbage data into it
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_CALL_UPDATE);
  sendP->size = 1;
  sendP->data[0] = 1;
}

void stateController(control_t *control, const sensorData_t *sensors,
                                         const state_t *state,
                                         const setpoint_t *setpoint,
                                         const uint32_t tick,
                                         const uint8_t resetControllers)
{
  float scale, thrust, roll, pitch, yaw;

  // If the thrust setpoint is 0, then nothing should be moving
  if (setpoint->thrust == 0) {
    control->thrust = 0;
    control->roll = 0;
    control->pitch = 0;
    control->yaw = 0;
    return;
  }

  if (RATE_DO_EXECUTE(CONTROLLER_RATE, tick)) {
    // Implement the pseudo-nonlinear extension to take into account the yaw
    float xVelError = -1.0 * state->velocity.x;
    float yVelError = -1.0 * state->velocity.y;

    float xPosError = setpoint->position.x - state->position.x;
    float yPosError = setpoint->position.y - state->position.y;

    // If the correction should be done, do it
    #ifdef USE_PSEUDO_NONLINEAR_CORRECTION
      float yawRad = DEG_TO_RAD_F(state->cameraYaw);// * (float)M_PI / 180;
      error[state_u] = (xVelError*cosf(yawRad)) - (yVelError*sinf(yawRad));
      error[state_v] = (xVelError*sinf(yawRad)) + (yVelError*cosf(yawRad));
      error[state_w] = -1.0 * state->velocity.z;

      error[state_x] = (xPosError*cosf(yawRad)) - (yPosError*sinf(yawRad));
      error[state_y] = (xPosError*sinf(yawRad)) + (yPosError*cosf(yawRad));
      error[state_z] = setpoint->position.z - state->position.z;
    #else
      error[state_u] = xVelError;
      error[state_v] = yVelError;
      error[state_w] = -1.0 * state->velocity.z;

      error[state_x] = xPosError;
      error[state_y] = yPosError;
      error[state_z] = setpoint->position.z - state->position.z;
    #endif

    error[state_p] = -1.0 * DEG_TO_RAD_F(sensors->gyro.x);// * (float)M_PI / 180;
    error[state_q] = -1.0 * DEG_TO_RAD_F(sensors->gyro.y);// * (float)M_PI / 180;
    error[state_r] = -1.0 * DEG_TO_RAD_F(sensors->gyro.z);// * (float)M_PI / 180;

    error[state_phi] = -1.0 * DEG_TO_RAD_F(state->attitude.roll);// * (float)M_PI / 180;
    error[state_theta] = -1.0 * DEG_TO_RAD_F(state->attitude.pitch);// * (float)M_PI / 180;
    error[state_psi] = DEG_TO_RAD_F(setpoint->attitude.yaw - state->cameraYaw);// * (float)M_PI / 180;

    // Do the controller calculation
    float controllerOutputs[4] = {0.0, 0.0, 0.0, 0.0};
    for (uint8_t i = 0; i < NUM_LQR_STATES; i++) {
      controllerOutputs[0] += (error[i]*K[0][i]);
      controllerOutputs[1] += (error[i]*K[1][i]);
      controllerOutputs[2] += (error[i]*K[2][i]);
      controllerOutputs[3] += (error[i]*K[3][i]);
    }

    // Set the thrust controller
    if (disableThrustControl) {
      // No thrust control, just the setpoint thrust
      thrust = setpoint->thrust;
    } else {
      // Thrust control active
      thrust = setpoint->thrust + controllerOutputs[0];
    }

    // Compensate for the battery voltage drop
    scale = controller_thrustAdjustment( pmGetBatteryVoltage() );
    thrust = scale * thrust;
    roll = scale * ( controllerOutputs[1] );
    pitch = scale * ( controllerOutputs[2] );
    yaw = scale * ( controllerOutputs[3] );

    // Saturate the control outputs
    control->thrust = thrust;
    control->roll = saturateSignedInt16( roll );
    control->pitch = saturateSignedInt16( pitch );
    control->yaw = saturateSignedInt16( yaw ); 
  }
}

LOG_GROUP_START(lqrError)
LOG_ADD(LOG_FLOAT, state_u, &error[state_u])
LOG_ADD(LOG_FLOAT, state_v, &error[state_v])
LOG_ADD(LOG_FLOAT, state_w, &error[state_w])
LOG_ADD(LOG_FLOAT, state_p, &error[state_p])
LOG_ADD(LOG_FLOAT, state_q, &error[state_q])
LOG_ADD(LOG_FLOAT, state_r, &error[state_r])
LOG_ADD(LOG_FLOAT, state_x, &error[state_x])
LOG_ADD(LOG_FLOAT, state_y, &error[state_y])
LOG_ADD(LOG_FLOAT, state_z, &error[state_z])
LOG_ADD(LOG_FLOAT, state_phi, &error[state_phi])
LOG_ADD(LOG_FLOAT, state_theta, &error[state_theta])
LOG_ADD(LOG_FLOAT, state_psi, &error[state_psi])
LOG_GROUP_STOP(lqrError)