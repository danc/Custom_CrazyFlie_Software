#include "controller.h"
#include "controller_update.h"
#include "stabilizer.h"
#include "stabilizer_types.h"
#include "debug.h"
#include <math.h>

#include "sensfusion6.h"

#include "pm.h"
#include "log.h"
#include "param.h"
#include "crtp.h"

enum stateOrdering {
 state_u = 0,
 state_v,
 state_w,
 state_p,
 state_q,
 state_r,
 state_x,
 state_y,
 state_z,
 state_phi,
 state_theta,
 state_psi,
 state_x_integ,
 state_y_integ,
 state_z_integ,
 state_psi_integ
};

enum integratorOrdering {
  x_integ = 0,
  y_integ,
  z_integ,
  psi_integ
};

static int disableThrustControl = 0;

// Variable to signify what kind of controller this is
const int stateControllerType = CONTROLLER_LQI;

#define NUM_LQR_STATES  16

#define CONTROLLER_RATE RATE_100_HZ
#define CONTROLLER_DT 0.01

#define DEG_TO_RAD_F(x)  ( x * ((float)M_PI / 180.0))

/*float K[4][16] = {
{ 0.000, 0.000, -17865.955, 0.000, 0.000, 0.000, 0.000, 0.000, -14113.031, 0.000, 0.000, 0.000, 0.000, 0.000, 4000.000, 0.000 },
{ 0.000, 3335.500, 0.000, 2363.287, 0.000, 0.000, 0.000, 3640.412, 0.000, 7357.024, 0.000, 0.000, 0.000, -600.000, 0.000, 0.000 },
{ -3987.920, 0.000, 0.000, 0.000, 2809.309, 0.000, -2834.696, 0.000, 0.000, 0.000, 17810.028, 0.000, 600.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1962.159, 0.000, 0.000, 0.000, 0.000, 0.000, 5192.510, 0.000, 0.000, 0.000, -500.000 }
};*/

/* This controller makes a hopping motion...
float K[4][16] = {
{ 0.000, 0.000, -17865.955, 0.000, 0.000, 0.000, 0.000, 0.000, -14113.031, 0.000, 0.000, 0.000, 0.000, 0.000, 4000.000, 0.000 },
{ 0.000, 3792.048, 0.000, 1427.693, 0.000, 0.000, 0.000, 4339.827, 0.000, 9886.001, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -3795.314, 0.000, 0.000, 0.000, 1434.202, 0.000, -4340.579, 0.000, 0.000, 0.000, 9913.790, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1962.159, 0.000, 0.000, 0.000, 0.000, 0.000, 5192.510, 0.000, 0.000, 0.000, -500.000 }
};*/

/* It pinwheels around... very pretty
float K[4][16] = {
{ 0.000, 0.000, -16982.101, 0.000, 0.000, 0.000, 0.000, 0.000, -9498.116, 0.000, 0.000, 0.000, 0.000, 0.000, 1000.000, 0.000 },
{ 0.000, 3318.645, 0.000, 1373.102, 0.000, 0.000, 0.000, 2722.816, 0.000, 9186.008, 0.000, 0.000, 0.000, -100.000, 0.000, 0.000 },
{ -3320.718, 0.000, 0.000, 0.000, 1379.286, 0.000, -2722.893, 0.000, 0.000, 0.000, 9210.774, 0.000, 100.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1962.159, 0.000, 0.000, 0.000, 0.000, 0.000, 5192.510, 0.000, 0.000, 0.000, -500.000 }
}; */

/*
float K[4][16] = {
{ 0.000, 0.000, -16982.101, 0.000, 0.000, 0.000, 0.000, 0.000, -9498.116, 0.000, 0.000, 0.000, 0.000, 0.000, 1000.000, 0.000 },
{ 0.000, 3256.970, 0.000, 1365.547, 0.000, 0.000, 0.000, 2530.098, 0.000, 9091.279, 0.000, 0.000, 0.000, -100.000, 0.000, 0.000 },
{ -3258.902, 0.000, 0.000, 0.000, 1371.687, 0.000, -2530.174, 0.000, 0.000, 0.000, 9115.652, 0.000, 100.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1962.159, 0.000, 0.000, 0.000, 0.000, 0.000, 5192.510, 0.000, 0.000, 0.000, -500.000 }
}; */

/*
float K[4][16] = {
{ 0.000, 0.000, -21599.944, 0.000, 0.000, 0.000, 0.000, 0.000, -9972.456, 0.000, 0.000, 0.000, 0.000, 0.000, 1000.000, 0.000 },
{ 0.000, 6297.790, 0.000, 1714.227, 0.000, 0.000, 0.000, 5370.808, 0.000, 14007.603, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -6300.886, 0.000, 0.000, 0.000, 1721.759, 0.000, -5371.385, 0.000, 0.000, 0.000, 14043.113, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1962.159, 0.000, 0.000, 0.000, 0.000, 0.000, 5192.510, 0.000, 0.000, 0.000, -500.000 }
}; */

/* This has a decent response, but oscillation on the Z axis still Zdot = 400, Z = 75 
float K[4][16] = {
{ 0.000, 0.000, -40625.316, 0.000, 0.000, 0.000, 0.000, 0.000, -7553.973, 0.000, 0.000, 0.000, 0.000, 0.000, 10.000, 0.000 },
{ 0.000, 11370.158, 0.000, 2115.858, 0.000, 0.000, 0.000, 6999.308, 0.000, 21050.179, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -11373.279, 0.000, 0.000, 0.000, 2124.936, 0.000, -6999.754, 0.000, 0.000, 0.000, 21099.874, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 1928.195, 0.000, 0.000, 0.000, 0.000, 0.000, 5001.928, 0.000, 0.000, 0.000, -5.000 }
};*/

/* Decent reponse, but angles still had some oscillation
float K[4][16] = {
{ 0.000, 0.000, -8171.783, 0.000, 0.000, 0.000, 0.000, 0.000, -7608.177, 0.000, 0.000, 0.000, 0.000, 0.000, 100.000, 0.000 },
{ 0.000, 3147.229, 0.000, 1339.962, 0.000, 0.000, 0.000, 3541.816, 0.000, 8817.296, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -3149.968, 0.000, 0.000, 0.000, 1346.024, 0.000, -3542.589, 0.000, 0.000, 0.000, 8840.965, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 5654.404, 0.000, 0.000, 0.000, 0.000, 0.000, 10056.385, 0.000, 0.000, 0.000, -100.000 }
};*/

/* Decent Z, but a lot os oscillation on the X & Y and angles
float K[4][16] = {
{ 0.000, 0.000, -8171.783, 0.000, 0.000, 0.000, 0.000, 0.000, -7608.177, 0.000, 0.000, 0.000, 0.000, 0.000, 100.000, 0.000 },
{ 0.000, 3194.482, 0.000, 1390.027, 0.000, 0.000, 0.000, 3555.132, 0.000, 9043.023, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -3197.213, 0.000, 0.000, 0.000, 1396.188, 0.000, -3555.900, 0.000, 0.000, 0.000, 9066.888, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 5952.081, 0.000, 0.000, 0.000, 0.000, 0.000, 15039.628, 0.000, 0.000, 0.000, -100.000 }
};*/

/* Removed oscillation on angles
float K[4][16] = {
{ 0.000, 0.000, -8171.783, 0.000, 0.000, 0.000, 0.000, 0.000, -7608.177, 0.000, 0.000, 0.000, 0.000, 0.000, 100.000, 0.000 },
{ 0.000, 2648.399, 0.000, 2295.351, 0.000, 0.000, 0.000, 3208.862, 0.000, 6008.080, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -2650.308, 0.000, 0.000, 0.000, 2297.779, 0.000, -3209.457, 0.000, 0.000, 0.000, 6023.184, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 5952.081, 0.000, 0.000, 0.000, 0.000, 0.000, 15039.628, 0.000, 0.000, 0.000, -100.000 }
};*/

/*
float K[4][16] = {
{ 0.000, 0.000, -8171.783, 0.000, 0.000, 0.000, 0.000, 0.000, -7608.177, 0.000, 0.000, 0.000, 0.000, 0.000, 100.000, 0.000 },
{ 0.000, 4571.021, 0.000, 2328.002, 0.000, 0.000, 0.000, 4259.348, 0.000, 6699.197, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -4573.488, 0.000, 0.000, 0.000, 2331.100, 0.000, -4259.927, 0.000, 0.000, 0.000, 6724.994, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 5952.081, 0.000, 0.000, 0.000, 0.000, 0.000, 15039.628, 0.000, 0.000, 0.000, -100.000 }
};*/

/*float K[4][16] = {
{ 0.000, 0.000, -8171.783, 0.000, 0.000, 0.000, 0.000, 0.000, -7608.177, 0.000, 0.000, 0.000, 0.000, 0.000, 100.000, 0.000 },
{ 0.000, 6498.151, 0.000, 3249.373, 0.000, 0.000, 0.000, 5098.657, 0.000, 7234.794, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -4527.843, 0.000, 0.000, 0.000, 3232.260, 0.000, -4249.198, 0.000, 0.000, 0.000, 6686.306, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 5952.081, 0.000, 0.000, 0.000, 0.000, 0.000, 15039.628, 0.000, 0.000, 0.000, -100.000 }
};*/

float K[4][16] = {
{ 0.000, 0.000, -8345.105, 0.000, 0.000, 0.000, 0.000, 0.000, -8037.108, 0.000, 0.000, 0.000, 0.000, 0.000, 500.000, 0.000 },
{ 0.000, 6604.037, 0.000, 2400.728, 0.000, 0.000, 0.000, 5119.382, 0.000, 8173.638, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -4585.428, 0.000, 0.000, 0.000, 2340.517, 0.000, -4262.729, 0.000, 0.000, 0.000, 6825.156, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 5952.081, 0.000, 0.000, 0.000, 0.000, 0.000, 15039.628, 0.000, 0.000, 0.000, -100.000 }
};

static float integrators[4];

// Array to store the calculated errors
float error[NUM_LQR_STATES];

static inline int16_t saturateSignedInt16(float in)
{
  // don't use INT16_MIN, because later we may negate it, which won't work for that value.
  if (in > INT16_MAX)
    return INT16_MAX;
  else if (in < -INT16_MAX)
    return -INT16_MAX;
  else
    return (int16_t)in;
}

void stateControllerInit(void)
{
  #pragma message "Using LQI Controller"
  DEBUG_PRINT("Using LQI Controller\r\n");

  // Make sure thrust is enabled
  disableThrustControl = 0;
}

bool stateControllerTest(void)
{
  bool pass = true;

  return pass;
}


/**
 * Callback function called by a CRTP port callback.
 * This callback will enable or disable the throttle controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerThrottleUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Create the packet header
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_SWITCH_THROTTLE_CTRL);
  sendP->size = 1;

  switch ( recvP->data[0] ) {
    case 1:
      // Disable the Z controller output
      disableThrustControl = 1;
      sendP->data[0] = 1;
      DEBUG_PRINT("Disabling Thrust Control\n");
      break;
    case 2:
      // Enable the Z controller output
      disableThrustControl = 0;
      sendP->data[0] = 0;
      DEBUG_PRINT("Enabling Thrust Control\n");
      break;
  }
}

/*
 * Callback function called by a CRTP port callback.
 * This function will allow the client/groundstation to modify
 * parameters of the state controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Create the packet header and put garbage data into it
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_CALL_UPDATE);
  sendP->size = 1;
  sendP->data[0] = 1;
}

void stateController(control_t *control, const sensorData_t *sensors,
                                         const state_t *state,
                                         const setpoint_t *setpoint,
                                         const uint32_t tick,
                                         const uint8_t resetControllers)
{
  float scale, thrust, roll, pitch, yaw;

  // If the thrust setpoint is 0, then nothing should be moving
  if (setpoint->thrust == 0) {
    control->thrust = 0;
    control->roll = 0;
    control->pitch = 0;
    control->yaw = 0;
    return;
  }

  if (RATE_DO_EXECUTE(CONTROLLER_RATE, tick)) {
    // Implement the pseudo-nonlinear extension to take into account the yaw
    float xVelError = -1.0 * state->velocity.x;
    float yVelError = -1.0 * state->velocity.y;

    float xPosError = setpoint->position.x - state->position.x;
    float yPosError = setpoint->position.y - state->position.y;
    float zPosError = setpoint->position.z - state->position.z;

    // Integrate the position errors
    integrators[x_integ] += (xPosError * CONTROLLER_DT);
    integrators[y_integ] += (yPosError * CONTROLLER_DT);
    integrators[z_integ] += (zPosError * CONTROLLER_DT);

    // If the correction should be done, do it
    #ifdef USE_PSEUDO_NONLINEAR_CORRECTION
      float yawRad = DEG_TO_RAD_F(state->cameraYaw);
      
      controller_yawCorrection(&error[state_u], &error[state_v],
                               xVelError, yVelError, yawRad);
      controller_yawCorrection(&error[state_x], &error[state_y],
                               xPosError, yPosError, yawRad);
      controller_yawCorrection(&error[state_x_integ], &error[state_y_integ],
                               integrators[x_integ], integrators[y_integ], yawRad);

      error[state_x_integ] = -1*error[state_x_integ];
      error[state_y_integ] = -1*error[state_y_integ];
    #else
      error[state_u] = xVelError;
      error[state_v] = yVelError;

      error[state_x] = xPosError;
      error[state_y] = yPosError;

      error[state_x_integ] = -integrators[x_integ];
      error[state_y_integ] = -integrators[y_integ];
    #endif

    // The w, Z, and Z integrated do not need compensation
    error[state_w] = -1.0 * state->velocity.z;
    error[state_z] = zPosError;
    error[state_z_integ] = -integrators[z_integ];

    // Compute the error for the angular velocity
    error[state_p] = -1.0 * DEG_TO_RAD_F(sensors->gyro.x);
    error[state_q] = -1.0 * DEG_TO_RAD_F(sensors->gyro.y);
    error[state_r] = -1.0 * DEG_TO_RAD_F(sensors->gyro.z);

    // Compute the angle error
    error[state_phi] = -1.0 * DEG_TO_RAD_F(state->attitude.roll);
    error[state_theta] = -1.0 * DEG_TO_RAD_F(state->attitude.pitch);
    error[state_psi] = DEG_TO_RAD_F(setpoint->attitude.yaw - state->cameraYaw);

    // Integrate the yaw error and compute the integral error
    integrators[psi_integ] += (error[state_psi] * CONTROLLER_DT);
    error[state_psi_integ] = -integrators[psi_integ];

    // Do the controller calculation
    float controllerOutputs[4] = {0.0, 0.0, 0.0, 0.0};
    for (uint8_t i = 0; i < NUM_LQR_STATES; i++) {
      controllerOutputs[0] += (error[i]*K[0][i]);
      controllerOutputs[1] += (error[i]*K[1][i]);
      controllerOutputs[2] += (error[i]*K[2][i]);
      controllerOutputs[3] += (error[i]*K[3][i]);
    }

    // Set the thrust controller
    if (disableThrustControl) {
      // No thrust control, just the setpoint thrust
      thrust = setpoint->thrust;
    } else {
      // Thrust control active
      thrust = setpoint->thrust + controllerOutputs[0];
    }

    // Compensate for the battery voltage drop
    scale = controller_thrustAdjustment( pmGetBatteryVoltage() );
    thrust = scale * thrust;
    roll = scale * ( controllerOutputs[1] );
    pitch = scale * ( controllerOutputs[2] );
    yaw = scale * ( controllerOutputs[3] );

    // Saturate the control outputs
    control->thrust = thrust;
    control->roll = saturateSignedInt16( roll );
    control->pitch = saturateSignedInt16( pitch );
    control->yaw = saturateSignedInt16( yaw ); 
  }
}

LOG_GROUP_START(lqrError)
LOG_ADD(LOG_FLOAT, state_u, &error[state_u])
LOG_ADD(LOG_FLOAT, state_v, &error[state_v])
LOG_ADD(LOG_FLOAT, state_w, &error[state_w])
LOG_ADD(LOG_FLOAT, state_p, &error[state_p])
LOG_ADD(LOG_FLOAT, state_q, &error[state_q])
LOG_ADD(LOG_FLOAT, state_r, &error[state_r])
LOG_ADD(LOG_FLOAT, state_x, &error[state_x])
LOG_ADD(LOG_FLOAT, state_y, &error[state_y])
LOG_ADD(LOG_FLOAT, state_z, &error[state_z])
LOG_ADD(LOG_FLOAT, state_phi, &error[state_phi])
LOG_ADD(LOG_FLOAT, state_theta, &error[state_theta])
LOG_ADD(LOG_FLOAT, state_psi, &error[state_psi])
LOG_ADD(LOG_FLOAT, state_x_integ, &error[state_x_integ])
LOG_ADD(LOG_FLOAT, state_y_integ, &error[state_y_integ])
LOG_ADD(LOG_FLOAT, state_z_integ, &error[state_z_integ])
LOG_ADD(LOG_FLOAT, state_psi_integ, &error[state_psi_integ])
LOG_GROUP_STOP(lqrError)