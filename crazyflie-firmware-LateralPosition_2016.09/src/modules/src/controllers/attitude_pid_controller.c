/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie Firmware
 *
 * Copyright (C) 2011-2012 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * attitude_pid_controller.c: Attitude controler using PID correctors
 */
#include <stdbool.h>

#include "FreeRTOS.h"

#include "attitude_controller.h"
#include "pid.h"
#include "param.h"
#include "imu.h"
#include "log.h"

#include "controller_pid.h"

static inline int16_t saturateSignedInt16(float in)
{
  // don't use INT16_MIN, because later we may negate it, which won't work for that value.
  if (in > INT16_MAX)
    return INT16_MAX;
  else if (in < -INT16_MAX)
    return -INT16_MAX;
  else
    return (int16_t)in;
}

PidObject pidRollRate;
PidObject pidPitchRate;
PidObject pidYawRate;
PidObject pidRoll;
PidObject pidPitch;
PidObject pidYaw;

static float log_eulerPitch;
static float log_eulerRoll;
static float log_eulerYaw;
static float log_pitchSet;
static float log_rollSet;
static float log_yawSet;


static float log_eulerRatePitch;
static float log_eulerRateRoll;
static float log_eulerRateYaw;
static float log_ratePitchSet;
static float log_rateRollSet;
static float log_rateYawSet;

int16_t rollOutput;
int16_t pitchOutput;
int16_t yawOutput;

static bool isInit;

void attitudeControllerInit()
{
  if(isInit)
    return;

  //TODO: get parameters from configuration manager instead
  pidInit(&pidRollRate, 0, PID_ROLL_RATE_KP, PID_ROLL_RATE_KI, PID_ROLL_RATE_KD, IMU_UPDATE_DT);
  pidInit(&pidPitchRate, 0, PID_PITCH_RATE_KP, PID_PITCH_RATE_KI, PID_PITCH_RATE_KD, IMU_UPDATE_DT);
  pidInit(&pidYawRate, 0, PID_YAW_RATE_KP, PID_YAW_RATE_KI, PID_YAW_RATE_KD, IMU_UPDATE_DT);
  pidSetIntegralLimit(&pidRollRate, PID_ROLL_RATE_INTEGRATION_LIMIT);
  pidSetIntegralLimit(&pidPitchRate, PID_PITCH_RATE_INTEGRATION_LIMIT);
  pidSetIntegralLimit(&pidYawRate, PID_YAW_RATE_INTEGRATION_LIMIT);

  pidInit(&pidRoll, 0, PID_ROLL_KP, PID_ROLL_KI, PID_ROLL_KD, IMU_UPDATE_DT);
  pidInit(&pidPitch, 0, PID_PITCH_KP, PID_PITCH_KI, PID_PITCH_KD, IMU_UPDATE_DT);
  pidInit(&pidYaw, 0, PID_YAW_KP, PID_YAW_KI, PID_YAW_KD, IMU_UPDATE_DT);
  pidSetIntegralLimit(&pidRoll, PID_ROLL_INTEGRATION_LIMIT);
  pidSetIntegralLimit(&pidPitch, PID_PITCH_INTEGRATION_LIMIT);
  pidSetIntegralLimit(&pidYaw, PID_YAW_INTEGRATION_LIMIT);

  controller_PID_RegisterPID(3, &pidRoll);
  controller_PID_RegisterPID(4, &pidPitch);
  controller_PID_RegisterPID(5, &pidYaw);

  controller_PID_RegisterPID(6, &pidRollRate);
  controller_PID_RegisterPID(7, &pidPitchRate);
  controller_PID_RegisterPID(8, &pidYawRate);

  isInit = true;
}

bool attitudeControllerTest()
{
  return isInit;
}

void attitudeControllerCorrectRatePID(
       float rollRateActual, float pitchRateActual, float yawRateActual,
       float rollRateDesired, float pitchRateDesired, float yawRateDesired)
{
  log_eulerRatePitch = pitchRateActual;
  log_eulerRateRoll = rollRateActual;
  log_eulerRateYaw = yawRateActual;
  log_ratePitchSet = pitchRateDesired;
  log_rateRollSet = rollRateDesired;
  log_rateYawSet = yawRateDesired;

  pidSetDesired(&pidRollRate, rollRateDesired);
  rollOutput = saturateSignedInt16(pidUpdate(&pidRollRate, rollRateActual, true));

  pidSetDesired(&pidPitchRate, pitchRateDesired);
  pitchOutput = saturateSignedInt16(pidUpdate(&pidPitchRate, pitchRateActual, true));

  pidSetDesired(&pidYawRate, yawRateDesired);
  yawOutput = saturateSignedInt16(pidUpdate(&pidYawRate, yawRateActual, true));
}

void attitudeControllerCorrectAttitudePID(
       float eulerRollActual, float eulerPitchActual, float eulerYawActual,
       float eulerRollDesired, float eulerPitchDesired, float eulerYawDesired,
       float* rollRateDesired, float* pitchRateDesired, float* yawRateDesired)
{
  log_eulerPitch = eulerPitchActual;
  log_eulerRoll = eulerRollActual;
  log_eulerYaw = eulerYawActual;
  log_pitchSet = eulerPitchDesired;
  log_rollSet = eulerRollDesired;
  log_yawSet = eulerYawDesired;

  pidSetDesired(&pidRoll, eulerRollDesired);
  *rollRateDesired = pidUpdate(&pidRoll, eulerRollActual, true);

  // Update PID for pitch axis
  pidSetDesired(&pidPitch, eulerPitchDesired);
  *pitchRateDesired = pidUpdate(&pidPitch, eulerPitchActual, true);

  // Update PID for yaw axis
  float yawError;
  yawError = eulerYawDesired - eulerYawActual;
  if (yawError > 180.0)
    yawError -= 360.0;
  else if (yawError < -180.0)
    yawError += 360.0;
  pidSetError(&pidYaw, yawError);
  *yawRateDesired = pidUpdate(&pidYaw, eulerYawActual, false);
}

void attitudeControllerResetAllPID(void)
{
  pidReset(&pidRoll);
  pidReset(&pidPitch);
  pidReset(&pidYaw);
  pidReset(&pidRollRate);
  pidReset(&pidPitchRate);
  pidReset(&pidYawRate);
}

void attitudeControllerGetActuatorOutput(int16_t* roll, int16_t* pitch, int16_t* yaw)
{
  *roll = rollOutput;
  *pitch = pitchOutput;
  *yaw = yawOutput;
}

PARAM_GROUP_START(pid_attitude)
PARAM_ADD(PARAM_FLOAT, roll_kp, &pidRoll.kp)
PARAM_ADD(PARAM_FLOAT, roll_ki, &pidRoll.ki)
PARAM_ADD(PARAM_FLOAT, roll_kd, &pidRoll.kd)
PARAM_ADD(PARAM_FLOAT, pitch_kp, &pidPitch.kp)
PARAM_ADD(PARAM_FLOAT, pitch_ki, &pidPitch.ki)
PARAM_ADD(PARAM_FLOAT, pitch_kd, &pidPitch.kd)
PARAM_ADD(PARAM_FLOAT, yaw_kp, &pidYaw.kp)
PARAM_ADD(PARAM_FLOAT, yaw_ki, &pidYaw.ki)
PARAM_ADD(PARAM_FLOAT, yaw_kd, &pidYaw.kd)
PARAM_GROUP_STOP(pid_attitude)

PARAM_GROUP_START(pid_rate)
PARAM_ADD(PARAM_FLOAT, roll_kp, &pidRollRate.kp)
PARAM_ADD(PARAM_FLOAT, roll_ki, &pidRollRate.ki)
PARAM_ADD(PARAM_FLOAT, roll_kd, &pidRollRate.kd)
PARAM_ADD(PARAM_FLOAT, pitch_kp, &pidPitchRate.kp)
PARAM_ADD(PARAM_FLOAT, pitch_ki, &pidPitchRate.ki)
PARAM_ADD(PARAM_FLOAT, pitch_kd, &pidPitchRate.kd)
PARAM_ADD(PARAM_FLOAT, yaw_kp, &pidYawRate.kp)
PARAM_ADD(PARAM_FLOAT, yaw_ki, &pidYawRate.ki)
PARAM_ADD(PARAM_FLOAT, yaw_kd, &pidYawRate.kd)
PARAM_GROUP_STOP(pid_rate)

// Inner pitch rate PID logging
LOG_GROUP_START(pitchRatePID)
LOG_ADD(LOG_FLOAT, actual, &log_eulerRatePitch)
LOG_ADD(LOG_FLOAT, setpoint, &log_ratePitchSet)
LOG_ADD(LOG_INT16, output, &pitchOutput)
LOG_ADD(LOG_FLOAT, integral, &pidPitchRate.integ)
LOG_ADD(LOG_FLOAT, error, &pidPitchRate.error)
LOG_GROUP_STOP(pitchRatePID)

// Inner roll rate PID logging
LOG_GROUP_START(rollRatePID)
LOG_ADD(LOG_FLOAT, actual, &log_eulerRateRoll)
LOG_ADD(LOG_FLOAT, setpoint, &log_rateRollSet)
LOG_ADD(LOG_INT16, output, &rollOutput)
LOG_ADD(LOG_FLOAT, integral, &pidRollRate.integ)
LOG_ADD(LOG_FLOAT, error, &pidRollRate.error)
LOG_GROUP_STOP(rollRatePID)

// Inner yaw rate PID logging
LOG_GROUP_START(yawRatePID)
LOG_ADD(LOG_FLOAT, actual, &log_eulerRateYaw)
LOG_ADD(LOG_FLOAT, setpoint, &log_rateYawSet)
LOG_ADD(LOG_INT16, output, &yawOutput)
LOG_ADD(LOG_FLOAT, integral, &pidYawRate.integ)
LOG_ADD(LOG_FLOAT, error, &pidYawRate.error)
LOG_ADD(LOG_FLOAT, dt, &pidYawRate.dt)
LOG_GROUP_STOP(yawRatePID)

// Outer pitch PID logging
LOG_GROUP_START(pitchPID)
LOG_ADD(LOG_FLOAT, actual, &log_eulerPitch)
LOG_ADD(LOG_FLOAT, setpoint, &log_pitchSet)
LOG_ADD(LOG_FLOAT, output, &pidPitch.out)
LOG_ADD(LOG_FLOAT, integral, &pidPitch.integ)
LOG_ADD(LOG_FLOAT, error, &pidPitch.error)
LOG_GROUP_STOP(pitchPID)

// Outer roll PID logging
LOG_GROUP_START(rollPID)
LOG_ADD(LOG_FLOAT, actual, &log_eulerRoll)
LOG_ADD(LOG_FLOAT, setpoint, &log_rollSet)
LOG_ADD(LOG_FLOAT, output, &pidRoll.out)
LOG_ADD(LOG_FLOAT, integral, &pidRoll.integ)
LOG_ADD(LOG_FLOAT, error, &pidRoll.error)
LOG_GROUP_STOP(rollPID)

// Outer yaw PID logging
LOG_GROUP_START(yawPID)
LOG_ADD(LOG_FLOAT, actual, &log_eulerYaw)
LOG_ADD(LOG_FLOAT, setpoint, &log_yawSet)
LOG_ADD(LOG_FLOAT, output, &pidYaw.out)
LOG_ADD(LOG_FLOAT, integral, &pidYaw.integ)
LOG_ADD(LOG_FLOAT, error, &pidYaw.error)
LOG_GROUP_STOP(yawPID)
