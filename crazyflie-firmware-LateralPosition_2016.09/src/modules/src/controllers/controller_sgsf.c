#include "controller.h"
#include "controller_update.h"
#include "stabilizer.h"
#include "stabilizer_types.h"
#include "debug.h"
#include <math.h>
#include <string.h>
#include "controller_sgsf.h"

#include "sensfusion6.h"

#include "pm.h"
#include "log.h"
#include "param.h"
#include "crtp.h"

enum outputOrdering {
  u_t = 0,
  u_a,
  u_e,
  u_r,
  OUTPUT_END = u_r,
};

// The ordering of the states for the K matrix
enum stateOrdering {
 state_u = 0,
 state_v,
 state_w,
 state_p,
 state_q,
 state_r,
 state_x,
 state_y,
 state_z,
 state_phi,
 state_theta,
 state_psi,

 // Lateral velocity integrator states
 state_u_integ,
 state_v_integ,
 state_w_integ,

 // Angular rate integrator states
 state_p_integ,
 state_q_integ,
 state_r_integ,

 // Position integrator states
 state_x_integ,
 state_y_integ,
 state_z_integ,

 // Euler angle integrator states
 state_phi_integ,
 state_theta_integ,
 state_psi_integ,

 // This is an item that will help to know how many states there are
 STATE_END = state_psi_integ,
};

// The ordering of the integrators in the internal data structure
enum integratorOrdering {
  // Lateral velocity integrators
  u_integ = 0,
  v_integ,
  w_integ,

  // Angular rate integrators
  p_integ,
  q_integ,
  r_integ,

  // Position integrators
  x_integ,
  y_integ,
  z_integ,

  // Euler angle integrators
  phi_integ,
  theta_integ,
  psi_integ,

  // This is a value that will help to know how many integrators there are
  INTEGRATOR_END = psi_integ,
};

static int disableThrustControl = 0;

// Variable to signify what kind of controller this is
const int stateControllerType = CONTROLLER_SGSF;

#define CONTROLLER_RATE RATE_100_HZ
#define CONTROLLER_DT 0.01

#define DEG_TO_RAD_F(x)  ( x * ((float)M_PI / 180.0))

float K[OUTPUT_END+1][STATE_END+1] = {
{ 0.000, 0.000, -8345.105, 0.000, 0.000, 0.000, 0.000, 0.000, -8037.108, 0.000, 0.000, 0.000, 0.000, 0.000, 500.000, 0.000 },
{ 0.000, 6604.037, 0.000, 2400.728, 0.000, 0.000, 0.000, 5119.382, 0.000, 8173.638, 0.000, 0.000, 0.000, -1000.000, 0.000, 0.000 },
{ -4585.428, 0.000, 0.000, 0.000, 2340.517, 0.000, -4262.729, 0.000, 0.000, 0.000, 6825.156, 0.000, 1000.000, 0.000, 0.000, 0.000 },
{ 0.000, 0.000, 0.000, 0.000, 0.000, 5952.081, 0.000, 0.000, 0.000, 0.000, 0.000, 15039.628, 0.000, 0.000, 0.000, -100.000 }
};

static float integrators[INTEGRATOR_END+1];

// Array to store the calculated errors
float error[STATE_END+1];

static inline int16_t saturateSignedInt16(float in)
{
  // don't use INT16_MIN, because later we may negate it, which won't work for that value.
  if (in > INT16_MAX)
    return INT16_MAX;
  else if (in < -INT16_MAX)
    return -INT16_MAX;
  else
    return (int16_t)in;
}

void stateControllerInit(void)
{
  #pragma message "Using SGSF Controller"
  DEBUG_PRINT("Using SGSF Controller\r\n");

  // Make sure thrust is enabled
  disableThrustControl = 0;
}

bool stateControllerTest(void)
{
  bool pass = true;

  return pass;
}


/**
 * Callback function called by a CRTP port callback.
 * This callback will enable or disable the throttle controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerThrottleUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Create the packet header
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_SWITCH_THROTTLE_CTRL);
  sendP->size = 1;

  switch ( recvP->data[0] ) {
    case 1:
      // Disable the Z controller output
      disableThrustControl = 1;
      sendP->data[0] = 1;
      DEBUG_PRINT("Disabling Thrust Control\n");
      break;
    case 2:
      // Enable the Z controller output
      disableThrustControl = 0;
      sendP->data[0] = 0;
      DEBUG_PRINT("Enabling Thrust Control\n");
      break;
  }
}

/*
 * Callback function called by a CRTP port callback.
 * This function will allow the client/groundstation to modify
 * parameters of the state controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerUpdate(CRTPPacket *recvP, CRTPPacket *sendP) {
  // Create the packet header and put garbage data into it
  sendP->header = CRTP_HEADER(CRTP_PORT_CONTROLLER_UPDATE, CONTROLLER_CALL_UPDATE);
  sendP->size = 1;
  sendP->data[0] = 1;

  CONTROLLER_SGSF_GAINS *recvGains;
  CONTROLLER_SGSF_GAINS sendGains;

  switch( recvP->data[0] ) {
    case CONT_SGSF_UPDATE_GAINS:
      // Get the gains from the packet
      recvGains = (CONTROLLER_SGSF_GAINS*) &(recvP->data[1]);

      DEBUG_PRINT("UP State Gains: %d\n", recvGains->state);

      // Update the K matrix with the gains
      K[u_t][recvGains->state] = recvGains->ut;
      K[u_a][recvGains->state] = recvGains->ua;
      K[u_e][recvGains->state] = recvGains->ue;
      K[u_r][recvGains->state] = recvGains->ur;
      break;

    case CONT_SGSF_RETRIEVE_GAINS:
      // Get the gains to send
      sendGains.state = recvP->data[1];
      sendGains.ut = K[u_t][sendGains.state];
      sendGains.ua = K[u_a][sendGains.state];
      sendGains.ue = K[u_e][sendGains.state];
      sendGains.ur = K[u_r][sendGains.state];

      // Move the gains into the packet to send
      sendP->data[0] = CONT_SGSF_RETRIEVE_GAINS;
      memcpy(&(sendP->data[1]), &sendGains, sizeof(CONTROLLER_SGSF_GAINS));
      break;

    default:
      // Do nothing
      break;
  }
}

void stateController(control_t *control, const sensorData_t *sensors,
                                         const state_t *state,
                                         const setpoint_t *setpoint,
                                         const uint32_t tick,
                                         const uint8_t resetControllers)
{
  float scale, thrust, roll, pitch, yaw;

  // If the thrust setpoint is 0, then nothing should be moving
  if (setpoint->thrust == 0) {
    control->thrust = 0;
    control->roll = 0;
    control->pitch = 0;
    control->yaw = 0;
    return;
  }

  if (RATE_DO_EXECUTE(CONTROLLER_RATE, tick)) {
    // Implement the pseudo-nonlinear extension to take into account the yaw
    float xVelError = -1.0 * state->velocity.x;
    float yVelError = -1.0 * state->velocity.y;
    float zVelError = -1.0 * state->velocity.z;

    float xPosError = setpoint->position.x - state->position.x;
    float yPosError = setpoint->position.y - state->position.y;
    float zPosError = setpoint->position.z - state->position.z;

    // Integrate the lateral velocity errors
    integrators[u_integ] += (xVelError * CONTROLLER_DT);
    integrators[v_integ] += (yVelError * CONTROLLER_DT);
    integrators[w_integ] += (zVelError * CONTROLLER_DT);

    // Integrate the position errors
    integrators[x_integ] += (xPosError * CONTROLLER_DT);
    integrators[y_integ] += (yPosError * CONTROLLER_DT);
    integrators[z_integ] += (zPosError * CONTROLLER_DT);

    float yawRad = DEG_TO_RAD_F(state->cameraYaw);
    
    // Do the non-linear correction on the original states
    controller_yawCorrection(&error[state_u], &error[state_v],
                             xVelError, yVelError, yawRad);
    controller_yawCorrection(&error[state_x], &error[state_y],
                             xPosError, yPosError, yawRad);

    // Do the non-linear correction on the integrated states
    controller_yawCorrection(&error[state_x_integ], &error[state_y_integ],
                             integrators[x_integ], integrators[y_integ], yawRad);

    controller_yawCorrection(&error[state_u_integ], &error[state_v_integ],
                             integrators[u_integ], integrators[v_integ], yawRad);

    // Reverse the sign on the integrated state errors
    error[state_x_integ] = -1*error[state_x_integ];
    error[state_y_integ] = -1*error[state_y_integ];
    error[state_u_integ] = -1*error[state_u_integ];
    error[state_v_integ] = -1*error[state_v_integ];

    // The w, w integrated, Z, and Z integrated do not need compensation
    error[state_w] = zVelError;
    error[state_w_integ] = -integrators[w_integ];

    error[state_z] = zPosError;
    error[state_z_integ] = -integrators[z_integ];

    // Compute the error for the angular velocity
    error[state_p] = -1.0 * DEG_TO_RAD_F(sensors->gyro.x);
    error[state_q] = -1.0 * DEG_TO_RAD_F(sensors->gyro.y);
    error[state_r] = -1.0 * DEG_TO_RAD_F(sensors->gyro.z);

    // Integrate the angular velocity error
    integrators[p_integ] += (error[state_p] * CONTROLLER_DT);
    integrators[q_integ] += (error[state_q] * CONTROLLER_DT);
    integrators[r_integ] += (error[state_r] * CONTROLLER_DT);

    // Create the error for the integrator states
    error[state_p_integ] = -integrators[p_integ];
    error[state_q_integ] = -integrators[q_integ];
    error[state_r_integ] = -integrators[r_integ];

    // Compute the euler angle error
    error[state_phi] = -1.0 * DEG_TO_RAD_F(state->attitude.roll);
    error[state_theta] = -1.0 * DEG_TO_RAD_F(state->attitude.pitch);
    error[state_psi] = DEG_TO_RAD_F(setpoint->attitude.yaw - state->cameraYaw);

    // Integrate the yaw error and compute the integral error
    integrators[phi_integ]   += (error[state_phi] * CONTROLLER_DT);
    integrators[theta_integ] += (error[state_theta] * CONTROLLER_DT);
    integrators[psi_integ]   += (error[state_psi] * CONTROLLER_DT);
    
    error[state_phi_integ]   = -integrators[phi_integ];
    error[state_theta_integ] = -integrators[theta_integ];
    error[state_psi_integ]   = -integrators[psi_integ];

    // Do the controller calculation
    float controllerOutputs[OUTPUT_END+1] = {0.0, 0.0, 0.0, 0.0};
    for (uint8_t i = 0; i <= STATE_END; i++) {
      controllerOutputs[u_t] += (error[i]*K[u_t][i]);
      controllerOutputs[u_a] += (error[i]*K[u_a][i]);
      controllerOutputs[u_e] += (error[i]*K[u_e][i]);
      controllerOutputs[u_r] += (error[i]*K[u_r][i]);
    }

    // Set the thrust controller
    if (disableThrustControl) {
      // No thrust control, just the setpoint thrust
      thrust = setpoint->thrust;
    } else {
      // Thrust control active
      thrust = setpoint->thrust + controllerOutputs[u_t];
    }

    // Compensate for the battery voltage drop
    scale = controller_thrustAdjustment( pmGetBatteryVoltage() );
    thrust = scale * thrust;
    roll = scale * ( controllerOutputs[u_a] );
    pitch = scale * ( controllerOutputs[u_e] );
    yaw = scale * ( controllerOutputs[u_r] );

    // Saturate the control outputs
    control->thrust = thrust;
    control->roll = saturateSignedInt16( roll );
    control->pitch = saturateSignedInt16( pitch );
    control->yaw = saturateSignedInt16( yaw ); 
  }
}

LOG_GROUP_START(lqrError)
LOG_ADD(LOG_FLOAT, state_u, &error[state_u])
LOG_ADD(LOG_FLOAT, state_v, &error[state_v])
LOG_ADD(LOG_FLOAT, state_w, &error[state_w])
LOG_ADD(LOG_FLOAT, state_p, &error[state_p])
LOG_ADD(LOG_FLOAT, state_q, &error[state_q])
LOG_ADD(LOG_FLOAT, state_r, &error[state_r])
LOG_ADD(LOG_FLOAT, state_x, &error[state_x])
LOG_ADD(LOG_FLOAT, state_y, &error[state_y])
LOG_ADD(LOG_FLOAT, state_z, &error[state_z])
LOG_ADD(LOG_FLOAT, state_phi, &error[state_phi])
LOG_ADD(LOG_FLOAT, state_theta, &error[state_theta])
LOG_ADD(LOG_FLOAT, state_psi, &error[state_psi])
LOG_ADD(LOG_FLOAT, state_x_integ, &error[state_x_integ])
LOG_ADD(LOG_FLOAT, state_y_integ, &error[state_y_integ])
LOG_ADD(LOG_FLOAT, state_z_integ, &error[state_z_integ])
LOG_ADD(LOG_FLOAT, state_psi_integ, &error[state_psi_integ])
LOG_GROUP_STOP(lqrError)