#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "crtp.h"
#include "debug.h"
#include "config.h"
#include "log.h"

#include "FreeRTOS.h"
#include "task.h"

#include "stabilizer_types.h"

#include "computation.h"
#include "computation_func.h"

// Is the stuff initialized
static bool isInit = 0;

// Packet structure used to return stuff to the client
static CRTPPacket p;

static bool computationEnabled = false;

// Rate at which the computation should run (in Hz)
static uint16_t computationRate;

static uint16_t computationDuration;

/**
 * Callback for the computation CRTP packet
 */
static void Computation_crtpCB(CRTPPacket* pk) {
  p.header = CRTP_HEADER(CRTP_PORT_COMP, COMP_CHAN_QUERY);
  p.size = 1;
  p.data[0] = 0;
  switch( pk->channel ) {
    default:
    case COMP_CHAN_QUERY:
      // The quadcopter is being queried as to the type of computation and if it exists
      p.data[0] = computationType;
      p.channel = COMP_CHAN_QUERY;

      // Pass the return packet for sending
      crtpSendPacket(&p);
      break;
    
    case COMP_CHAN_CONFIG:
      p.header = CRTP_HEADER(CRTP_PORT_COMP, COMP_CHAN_CONFIG);
      p.size = 1;
      p.data[0] = 1;
      switch (pk->data[0]) {
        case COMP_CONFIG_ENABLE:
          // Toggle the computation on or off
          computationEnabled = pk->data[1];
          if (computationEnabled) {
            computation_algorithm_reset();
            DEBUG_PRINT("Enabled Computation\n");
          } else {
            DEBUG_PRINT("Disabled Computation\n");
          }
          break;

        case COMP_CONFIG_RATE:
          // Configure the computation rate
          p.header = CRTP_HEADER(CRTP_PORT_COMP, COMP_CHAN_CONFIG);
          p.size = 1;
          p.data[0] = 1;

          // Copy the new computation rate into the variable
          memcpy(&computationRate, &(pk->data[1]), sizeof(uint16_t));
          break;

        case COMP_CONFIG_FUNC:
        default:
          // Pass the config packet into the callback
          computation_configReceived(pk, &p);
      }
      
      // Pass the return packet for sending
      crtpSendPacket(&p);
      break;

    case COMP_CHAN_COMPUTE_DATA:
      // Call the function to process the received data
      computation_packetReceived(pk);
      break;
  }

}

void Computation_Task() {
  DEBUG_PRINT("Started Computation Task\n");

  uint32_t lastWakeTime;
  lastWakeTime = xTaskGetTickCount();

  while(1) {
    vTaskDelayUntil(&lastWakeTime, F2T(computationRate));
    if (computationEnabled) {
      uint32_t computationStart = xTaskGetTickCount();
      // Perform the computations
      computation_processing();
      uint32_t computationEnd = xTaskGetTickCount();

      // Convert the number of ticks to milliseconds elapsed
      computationDuration = (computationEnd - computationStart) * (1000/configTICK_RATE_HZ);
    }
  }
}

void Computation_Init() {
  // Call the CRTP initialization stuff for the callback
  crtpInit();
  crtpRegisterPortCB(CRTP_PORT_COMP, Computation_crtpCB);

  // Default the computation to using a 100Hz rate
  computationRate = RATE_100_HZ;
  computationDuration = 0;

  computation_algorithm_init();

  // Create the computation task
  xTaskCreate(Computation_Task, COMPUTATION_TASK_NAME,
            COMPUTATION_TASK_STACKSIZE, NULL, COMPUTATION_TASK_PRI, NULL);

  isInit = true;
}

bool Computation_Test() {
  crtpTest();
  return isInit;
}

LOG_GROUP_START(compStats)
LOG_ADD(LOG_UINT16, dur, &computationDuration)
LOG_GROUP_STOP(compStats)