#include "computation.h"
#include "computation_func.h"
#include "computation_localization.h"
#include "debug.h"
#include "locodeck.h"
#include "crtp.h"
#include "stabilizer_types.h"
#include "task.h"

// Because memcpy is in string.h for some reason...
#include <string.h>


#define MAX_NUM_NODES 10

computationID computationType = COMPUTATION_LOCALIZATION;


// This is the data local to this host
static float a;
static float mu;
static computation_localization_node_data localData;
static point_t localPosition;

static CRTPPacket dataPacket;
static float ranges[LOCODECK_NR_OF_ANCHORS];
static uint8_t anchorNumber;

static float twok1;
static float twok2;
static float twok3;
static float stepSize;
static uint8_t numNodes;
static uint8_t nodeNumber;

static float dmu;
static float da;
static float dx;
static float dy;
static float dz;
static float dalpha;
static float dbeta;
static float dgamma;


// This is the data from the neighbors
static computation_localization_node_data nodeData[MAX_NUM_NODES];
static uint8_t useNode[MAX_NUM_NODES];
static uint32_t lastNodeTime[MAX_NUM_NODES];

extern void setTDMAslot(int slotNumber);

/*
 * This function will configure the algorithm to be in its default state
 */
void computation_algorithm_init() {
	// Zero out the stuff for this node
	a = 0.0;
	mu = 0.0;
	localData.measuredRadius = 0.0;
	localData.estimatedX = 0.0;
	localData.estimatedY = 0.0;
	localData.estimatedZ = 0.0;
	localData.alpha = 0.0;
	localData.beta = 0.0;
	localData.gamma = 0.0;

	localPosition.x = 0.0;
	localPosition.y = 0.0;
	localPosition.z = 0.0;

	dmu = 0;
	da = 0;
	dx = 0;
	dy = 0;
	dz = 0;
	dalpha = 0;
	dbeta = 0;
	dgamma = 0;

	twok1 = 0.0;
	twok2 = 0.0;
	twok3 = 0.0;
	stepSize = 0.0;
	numNodes = 0;

	// Zero out the ranges array
	for (uint8_t i = 0; i < LOCODECK_NR_OF_ANCHORS; i++) {
		ranges[i] = 0.0;
	}

	// Zero out the stuff from the other nodes
	for (uint8_t i = 0; i < MAX_NUM_NODES; i++) {
		useNode[i] = 0;

		lastNodeTime[i] = 0.0;

		nodeData[i].measuredRadius = 0.0;
		nodeData[i].estimatedX = 0.0;
		nodeData[i].estimatedY = 0.0;
		nodeData[i].estimatedZ = 0.0;
		nodeData[i].alpha = 0.0;
		nodeData[i].beta = 0.0;
		nodeData[i].gamma = 0.0;
	}
}

void computation_algorithm_reset() {
	// Create the initial conditions
	a = -1.0 * ranges[anchorNumber];
	localData.measuredRadius = ranges[anchorNumber];
	localData.estimatedX = localPosition.x;
	localData.estimatedY = localPosition.y;
	localData.estimatedZ = localPosition.z;
	
	// Zero out the dual variables
	mu = 0.0;
	localData.alpha = 0.0;
	localData.beta = 0.0;
	localData.gamma = 0.0;

	// Zero the derivative terms
	dmu = 0;
	da = 0;
	dx = 0;
	dy = 0;
	dz = 0;
	dalpha = 0;
	dbeta = 0;
	dgamma = 0;

	// Zero out the stuff from the other nodes
	for (uint8_t i = 0; i < MAX_NUM_NODES; i++) {
		useNode[i] = 0;

		lastNodeTime[i] = 0.0;

		nodeData[i].measuredRadius = 0.0;
		nodeData[i].estimatedX = 0.0;
		nodeData[i].estimatedY = 0.0;
		nodeData[i].estimatedZ = 0.0;
		nodeData[i].alpha = 0.0;
		nodeData[i].beta = 0.0;
		nodeData[i].gamma = 0.0;
	}
}

/**
 * This is used to get the distance from the ranging deck.
 */
void computation_localization_newDistance(int anchor, float newRange) {
	ranges[anchor] = newRange;
}

/*
 * This function is called by stabilizer once a new state has been determined.
 */
void computation_hook_state( state_t *state, sensorData_t *sensor) {
	localPosition.x = state->position.x;
	localPosition.y = state->position.y;
	localPosition.z = state->position.z;
}

/*
 * This function is called by stabilizer before it does the control
 */
void computation_hook_setpoints( setpoint_t *setpoints ) {

}

void computation_configReceived( CRTPPacket *pk, CRTPPacket *p ) {
	// Save the received packet.
	// Note that the config starts at index 1 because index 0 is the opcode for the config
	computation_localization_config_packet config;
	memcpy(&config, &(pk->data[1]), sizeof(computation_localization_config_packet) );

	// Parse the config
	stepSize = config.stepSize;
	nodeNumber = config.nodeNumber;
	numNodes = config.numTotalNodes;
	anchorNumber = config.anchor;
	twok1 = 2.0*config.k1;
	twok2 = 2.0*config.k2;
	twok3 = 2.0*config.k3;

	setTDMAslot(config.nodeNumber);

	DEBUG_PRINT("COMP: Step %2.2f\n", stepSize);
	DEBUG_PRINT("COMP: Node %d\n", nodeNumber);
	DEBUG_PRINT("COMP: NumNodes %d\n", numNodes);
	DEBUG_PRINT("COMP: Anchor %d\n", anchorNumber);
	DEBUG_PRINT("COMP: twok1 %2.2f\n", twok1);
	DEBUG_PRINT("COMP: twok2 %2.2f\n", twok2);
	DEBUG_PRINT("COMP: twok3 %2.2f\n", twok3);
}

void computation_packetReceived( CRTPPacket *pk ) {
	// The node the packet came from
	uint8_t i = pk->data[0];

	// Copy the data into the storage array slot for the node
	if (i < MAX_NUM_NODES) {
		memcpy(&(nodeData[i]), &(pk->data[1]), sizeof(computation_localization_node_data));
		if (useNode[i] == 0) {
			DEBUG_PRINT("Node %d activated\n", i);
		}
		useNode[i] = 1;
		lastNodeTime[i] = xTaskGetTickCount();	// Update the time received
	}
}

/*
 * This function does the actual computation
 */
void computation_processing() {
	// The number of nodes in the computation
	float numComputingNodes = 0;

	// Go through and make sure all the data is valid
	uint32_t currentTick = xTaskGetTickCount();
	for ( int i = 0; i < MAX_NUM_NODES; i++ ) {
		float timeDelta = currentTick - lastNodeTime[i];
		if ( (timeDelta > M2T(1000)) && (useNode[i] != 0) ) {
			useNode[i] = 0;
			DEBUG_PRINT("Node %d has gone stale\n", i);
		}

		numComputingNodes += (float) useNode[i];
	}

	localData.measuredRadius = ranges[anchorNumber];

	float diffX = (localData.estimatedX - localPosition.x);
	float diffY = (localData.estimatedY - localPosition.y);
	float diffZ = (localData.estimatedZ - localPosition.z);
	float diffA = (a - localData.measuredRadius);

	// This term is needed by the others
	dmu = (diffX * diffX ) + (diffY * diffY) + (diffZ * diffZ) - (diffA * diffA);

	float pd = (2.0*dmu + mu);

	da = 2.0 * pd * diffA - 2.0*a;

	// These are based upon data from the other nodes
	dx = -2.0 * pd * diffX
	     - numComputingNodes*localData.alpha
	     - twok1 * numComputingNodes*localData.estimatedX;
	dy = -2.0 * pd * diffY
	     - numComputingNodes*localData.beta
	     - twok2 * numComputingNodes*localData.estimatedY;
	dz = -2.0 * pd * diffZ
	     - numComputingNodes*localData.gamma
	     - twok3 * numComputingNodes*localData.estimatedZ;
	
	dalpha = numComputingNodes*localData.estimatedX;
	dbeta  = numComputingNodes*localData.estimatedY;
	dgamma = numComputingNodes*localData.estimatedZ;

	// Go through and compute the updates 
	for (int i = 0; i < MAX_NUM_NODES; i++ ) {
		// Make sure the node should be used
		if (useNode[i] != 0) {	
			dx += ( nodeData[i].alpha + twok1*nodeData[i].estimatedX );
			dy += ( nodeData[i].beta  + twok2*nodeData[i].estimatedY );
			dz += ( nodeData[i].gamma + twok3*nodeData[i].estimatedZ );

			dalpha -= nodeData[i].estimatedX;
			dbeta  -= nodeData[i].estimatedY;
			dgamma -= nodeData[i].estimatedZ;
		}
	}

	// Update the local information (simple Euler's method)
	a  += (stepSize * da);
	mu += (stepSize * dmu);

	// Update the information to share (simple Euler's method)
	localData.estimatedX += (stepSize * dx);
	localData.estimatedY += (stepSize * dy);
	localData.estimatedZ += (stepSize * dz);
	
	localData.alpha += (stepSize * dalpha);
	localData.beta  += (stepSize * dbeta);
	localData.gamma += (stepSize * dgamma);

	// Package the information into a packet and then send it
	dataPacket.header = CRTP_HEADER(CRTP_PORT_COMP, COMP_CHAN_COMPUTE_DATA);
	dataPacket.data[0] = nodeNumber;
	dataPacket.size = sizeof(computation_localization_node_data) + 1;
	memcpy(&(dataPacket.data[1]), &localData, sizeof(computation_localization_node_data));
	crtpSendPacket(&dataPacket);
}
