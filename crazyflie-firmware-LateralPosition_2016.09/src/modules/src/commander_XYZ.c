/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie Firmware
 *
 * Copyright (C) 2011-2012 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 *
 */
#include <math.h>

#include "FreeRTOS.h"
#include "task.h"

#include "debug.h"

#include "commander_XYZ.h"
#include "crtp.h"
#include "configblock.h"
#include "param.h"
#include "num.h"

#define MIN_THRUST  1000
#define MAX_THRUST  60000

#define POS_UPDATE_RATE RATE_100_HZ

/**
 * Commander position data
 */
typedef struct
{
  struct CommanderXYZ_PosCrtpValues targetVal[2];
  bool activeSide;
  uint32_t timestamp; // FreeRTOS ticks
} CommanderPosCache;

/**
 * Commander setpoint data
 */
typedef struct
{
  struct CommanderXYZ_SetCrtpValues targetVal[2];
  bool activeSide;
  uint32_t timestamp; // FreeRTOS ticks
} CommanderSetCache;

static bool isInit;
static CommanderPosCache crtpPosCache;
static CommanderSetCache crtpSetCache;

static uint32_t lastUpdate;
static bool isInactive;

static uint8_t watchdogTriggered = 0;

static void commanderXYZ_PosCrtpCB(CRTPPacket* pk);
static void commanderXYZ_SetCrtpCB(CRTPPacket* pk);

/**
 * Functions to get position information
 */
static float commanderXYZ_GetPositionX() {
  return( crtpPosCache.targetVal[crtpPosCache.activeSide].x );
}

static float commanderXYZ_GetPositionY() {
  return( crtpPosCache.targetVal[crtpPosCache.activeSide].y );
}

static float commanderXYZ_GetPositionZ() {
  return( crtpPosCache.targetVal[crtpPosCache.activeSide].z );
}

static float commanderXYZ_GetPositionYaw() {
  return( crtpPosCache.targetVal[crtpPosCache.activeSide].yaw );
}

static float commanderXYZ_GetPositionTimestamp() {
  return( crtpPosCache.timestamp );
}

/**
 * Functions to get setpoint information
 */
static float commanderXYZ_GetSetpointX() {
  return( crtpSetCache.targetVal[crtpSetCache.activeSide].x );
}

static float commanderXYZ_GetSetpointY() {
  return( crtpSetCache.targetVal[crtpSetCache.activeSide].y );
}

static float commanderXYZ_GetSetpointZ() {
  return( crtpSetCache.targetVal[crtpSetCache.activeSide].z );
}

static float commanderXYZ_GetSetpointYaw() {
  return( crtpSetCache.targetVal[crtpSetCache.activeSide].yaw );
}

static uint16_t commanderXYZ_GetBaseThrust() {
  return( crtpSetCache.targetVal[crtpSetCache.activeSide].baseThrust ); 
}

static uint8_t commanderXYZ_GetWatchdog() {
  return( watchdogTriggered );
}

static uint8_t commanderXYZ_GetControllerState() {
  return( crtpSetCache.targetVal[crtpSetCache.activeSide].resetControllers );
}

/**
 * A watchdog for the current position data packet
 */
static void commanderXYZ_WatchdogTick(void) {
  uint32_t tickNow = xTaskGetTickCount();

  // If the timeout occurs, drop the quad to the ground
  if ((tickNow - crtpPosCache.timestamp) > COMMANDER_XYZ_WDT_TIMEOUT_SHUTDOWN) {
    watchdogTriggered = 1;
  } 
}

/**
 * Callback for the position CRTP packet
 */
static void commanderXYZ_PosCrtpCB(CRTPPacket* pk) {
  crtpPosCache.targetVal[!crtpPosCache.activeSide] = *((struct CommanderXYZ_PosCrtpValues*)pk->data);
  crtpPosCache.activeSide = !crtpPosCache.activeSide;
  crtpPosCache.timestamp = xTaskGetTickCount();
}

/**
 * Callback for the setpoint CRTP packet
 */
static void commanderXYZ_SetCrtpCB(CRTPPacket* pk) {
  crtpSetCache.targetVal[!crtpSetCache.activeSide] = *((struct CommanderXYZ_SetCrtpValues*)pk->data);
  crtpSetCache.activeSide = !crtpSetCache.activeSide;
  crtpSetCache.timestamp = xTaskGetTickCount();

  // Reset the watchdog if the controllers are reset
  if ( crtpSetCache.targetVal[crtpSetCache.activeSide].resetControllers ) {
    watchdogTriggered = 0;
  }
}

/* Public functions */
void commanderXYZ_Init(void) {
  if(isInit) {
    return;
  }

  crtpInit();
  crtpRegisterPortCB(CRTP_PORT_XYZ_SET, commanderXYZ_SetCrtpCB);
  crtpRegisterPortCB(CRTP_PORT_XYZ_POS, commanderXYZ_PosCrtpCB);

  lastUpdate = xTaskGetTickCount();
  isInactive = true;
  watchdogTriggered = 0;
  isInit = true;
}

bool commanderXYZ_Test(void) {
  crtpTest();
  return isInit;
}

uint32_t commanderXYZ_GetInactivityTime(void) {
  return xTaskGetTickCount() - lastUpdate;
}

void commanderXYZ_GetSetpoint(setpoint_t *setpoint, const state_t *state, uint8_t *resetControllers) {
  // Tell the controller everything should be in absolute mode
  setpoint->mode.x = modeAbs;
  setpoint->mode.y = modeAbs;
  setpoint->mode.z = modeAbs;
  setpoint->mode.roll = modeAbs;
  setpoint->mode.pitch = modeAbs;
  setpoint->mode.yaw = modeAbs;

  // Set the setpoint stuff
  setpoint->position.x = commanderXYZ_GetSetpointX();
  setpoint->position.y = commanderXYZ_GetSetpointY();
  setpoint->position.z = commanderXYZ_GetSetpointZ();
  setpoint->attitude.yaw = commanderXYZ_GetSetpointYaw();

  setpoint->attitude.roll  = 0;
  setpoint->attitude.pitch = 0;

  // Get the thrust to use in the controllers
  if ( commanderXYZ_GetWatchdog() ) {
    setpoint->thrust = 0;
  } else {
    setpoint->thrust = commanderXYZ_GetBaseThrust();
  }

  // Get whether to reset the controllers or not
  *resetControllers = commanderXYZ_GetControllerState();
}

void commanderXYZ_GetPosition(state_t *state, const uint32_t tick) {
  // Check the watchdog and feed it
  commanderXYZ_WatchdogTick();

   // Save the previous position values
  static float oldX = 0;
  static float oldY = 0;
  static float oldZ = 0;

  if (RATE_DO_EXECUTE(POS_UPDATE_RATE, tick)) {
    // Get the new positions
    float newX = commanderXYZ_GetPositionX();
    float newY = commanderXYZ_GetPositionY();
    float newZ = commanderXYZ_GetPositionZ();

    // Compute the linear velocity
    state->velocity.x = (newX - oldX) / 0.01;
    state->velocity.y = (newY - oldY) / 0.01;
    state->velocity.z = (newZ - oldZ) / 0.01;

    // Set the current position stuff in the structure
    state->cameraYaw = commanderXYZ_GetPositionYaw();
    state->position.x = newX;
    state->position.y = newY;
    state->position.z = newZ;

    // Save the position for the next velocity calculation
    oldX = newX;
    oldY = newY;
    oldZ = newZ;

    state->position.timestamp = commanderXYZ_GetPositionTimestamp();
  }
}
