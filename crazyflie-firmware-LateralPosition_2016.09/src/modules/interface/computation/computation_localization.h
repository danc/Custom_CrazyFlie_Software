#ifndef COMPUTATION_LOCALIZATION_H_
#define COMPUTATION_LOCALIZATION_H_

typedef struct computation_localization_config_packet {
	uint8_t nodeNumber;
	uint8_t numTotalNodes;
	float stepSize;
	uint8_t anchor;
	float k1;
	float k2;
	float k3;
} __attribute__((packed)) computation_localization_config_packet ;

typedef struct computation_localization_shared_node_data {
	float measuredRadius;
	float estimatedX;
	float estimatedY;
	float estimatedZ;
	float alpha;
	float beta;
	float gamma;
} __attribute__((packed)) computation_localization_node_data;

void computation_localization_newDistance(int anchor, float newRange);

#endif