#ifndef COMPUTATION_H_
#define COMPUTATION_H_

#include <stdbool.h>
#include "crtp.h"

// These are various codes used to configure the computation framework
enum comp_config_codes {
  COMP_CONFIG_ENABLE = 0,
  COMP_CONFIG_FUNC,
  COMP_CONFIG_RATE,
};

enum Computation_channels {
	COMP_CHAN_QUERY = 0,
	COMP_CHAN_COMPUTE_DATA,
	COMP_CHAN_CONFIG,
};


void Computation_Init();
bool Computation_Test();


#endif