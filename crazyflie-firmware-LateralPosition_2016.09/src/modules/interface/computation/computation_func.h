#ifndef COMPUATION_FUNC_H_
#define COMPUATION_FUNC_H_

#include "stabilizer_types.h"

typedef enum computationID {
	COMPUTATION_NONE = 0,
	COMPUTATION_LOCALIZATION,
} computationID;

extern computationID computationType;

void computation_configReceived( CRTPPacket *pk, CRTPPacket *p );
void computation_packetReceived( CRTPPacket *pk );

void computation_processing();
void computation_algorithm_init();
void computation_algorithm_reset();
void computation_hook_state( state_t *state, sensorData_t *sensor );
void computation_hook_setpoints( setpoint_t *setpoints );

#endif