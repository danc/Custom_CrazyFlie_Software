#ifndef CONTROLLER_SGSF_H_
#define CONTROLLER_SGSF_H_

enum CONTROLLER_SGSF_OPCODES {
	CONT_SGSF_UPDATE_GAINS = 0,
	CONT_SGSF_RETRIEVE_GAINS,
};

typedef struct CONTROLLER_SGSF_GAINS {
	uint8_t state;
	float ut;
	float ua;
	float ue;
	float ur;
} __attribute__((packed)) CONTROLLER_SGSF_GAINS;

#endif