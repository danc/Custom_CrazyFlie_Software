#ifndef CONTROLLER_UPDATE_H_
#define CONTROLLER_UPDATE_H_

#include <stdint.h>

// Channels in the CRTP packet describing the update requested
enum controllerUpdate_Channels {
  CONTROLLER_QUERY_TYPE = 0,
  CONTROLLER_CALL_UPDATE,
  CONTROLLER_SWITCH_THROTTLE_CTRL
};

/**
 * Start the update controller CRTP task
 */
void controllerUpdate_Init(void);

bool controllerUpdate_Test(void);

#endif