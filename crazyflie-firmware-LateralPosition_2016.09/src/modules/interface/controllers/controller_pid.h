#ifndef CONTROLLER_PID_H_
#define CONTROLLER_PID_H_

#include "pid.h"

/**
 * CRTP Data struct for transmitting PID controller gains
 */
struct controller_PID_CrtpGainValues
{
  float Kp;					// Kp Gain
  float Ki;					// Ki Gain
  float Kd;					// Kd Gain
  float iLimit;				// Integrator limit
  uint8_t controllerID;		// PID Controller to modify
} __attribute__((packed));

/**
 * Register a PID controller so its constants can be modified
 *
 * @param controllerID The ID number for the PID controller
 * @param pid The PidObject to modify
 */
void controller_PID_RegisterPID(uint8_t controllerID, PidObject* pid);

#endif