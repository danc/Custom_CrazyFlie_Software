/**
 *    ||          ____  _ __
 * +------+      / __ )(_) /_______________ _____  ___
 * | 0xBC |     / __  / / __/ ___/ ___/ __ `/_  / / _ \
 * +------+    / /_/ / / /_/ /__/ /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\___/_/   \__,_/ /___/\___/
 *
 * Crazyflie control firmware
 *
 * Copyright (C) 2011-2016 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * controller.h - State controller interface
 */
#ifndef __CONTROLLER_H__
#define __CONTROLLER_H__

#include "stabilizer_types.h"
#include "crtp.h"

#include <math.h>

#define USE_PSEUDO_NONLINEAR_CORRECTION

#define USE_CONTROLLER_THRUST_COMPENSATION

enum STATE_CONTROLLER_TYPE {
	CONTROLLER_BYPASS = 0,
	CONTROLLER_PID = 1,
	CONTROLLER_LQR = 2,
	CONTROLLER_LQI = 3,
  CONTROLLER_SGSF = 4
};

extern const int stateControllerType;

/**
 * Compute the scaling value to take into account the decreased battery voltage
 * when doing the controller values.
 *
 * @param batteryVolt The battery voltage
 * @return Scale factor [0, 2]
 */
#ifdef USE_CONTROLLER_THRUST_COMPENSATION
inline float controller_thrustAdjustment(float batteryVolt) {
  float scale = 1 + (3.7 - batteryVolt)/3.7;

  return( scale );
}
#else
inline float controller_thrustAdjustment(float batteryVolt) {
  return( 1 );
}
#endif


/**
 * Pseudo-nonlinear correction to correct the X & Y for a non zero yaw
 *
 * @param correctedX Pointer to the variable to store the body-frame X
 * @param correctedY Pointer to the variable to store the body-frame Y
 * @param x The earth-frame X location
 * @param y The earth-frame Y location
 * @param yaw The yaw angle in radians
 */
#ifdef USE_PSEUDO_NONLINEAR_CORRECTION
  inline void controller_yawCorrection(float *correctedX, float *correctedY, float x, float y, float yaw) {
    *correctedX = (x*cosf(yaw)) - (y*sinf(yaw));
    *correctedY = (x*sinf(yaw)) + (y*cosf(yaw));
  }
#else
  inline void controller_yawCorrection(float *correctedX, float *correctedY, float x, float y, float yaw) {
    *correctedX = x;
    *correctedY = y;
  }
#endif

void stateControllerInit(void);
bool stateControllerTest(void);
void stateController(control_t *control, const sensorData_t *sensors,
                                         const state_t *state,
                                         const setpoint_t *setpoint,
                                         const uint32_t tick,
                                         const uint8_t resetControllers);

/**
 * Callback function called by a CRTP port callback.
 * This callback will enable or disable the throttle controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerThrottleUpdate(CRTPPacket *recvP, CRTPPacket *sendP);

/*
 * Callback function called by a CRTP port callback.
 * This function will allow the client/groundstation to modify
 * parameters of the state controller.
 *
 * @param recvP The CRTPPacket structure received over the datalink
 * @param sendP The CRTPpacket structure to send back to the client
 */
void stateControllerUpdate(CRTPPacket *recvP, CRTPPacket *sendP);

#endif //__CONTROLLER_H__
