/**
 *    ||          ____  _ __  ______
 * +------+      / __ )(_) /_/ ____/_________ _____  ___
 * | 0xBC |     / __  / / __/ /    / ___/ __ `/_  / / _	\
 * +------+    / /_/ / / /_/ /___ / /  / /_/ / / /_/  __/
 *  ||  ||    /_____/_/\__/\____//_/   \__,_/ /___/\___/
 *
 * Crazyflie control firmware
 *
 * Copyright (C) 2011-2012 Bitcraze AB
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, in version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COMMANDER_XYZ_H_
#define COMMANDER_XYZ_H_
#include <stdint.h>
#include <stdbool.h>
#include "config.h"
#include "stabilizer_types.h"

#define COMMANDER_XYZ_WDT_TIMEOUT_STABILIZE  M2T(500)
#define COMMANDER_XYZ_WDT_TIMEOUT_SHUTDOWN   M2T(2000)	// Shutdown after 2000ms

/**
 * CRTP commander data struct for the position packet (port 7)
 */
struct CommanderXYZ_PosCrtpValues
{
  float x;		// meters
  float y;		// meters
  float z;		// meters
  float yaw;	// degrees
} __attribute__((packed));

/**
 * CRTP commander data struct for the position packet (port 6)
 */
struct CommanderXYZ_SetCrtpValues
{
  float x;					// meters
  float y;					// meters
  float z;					// meters
  float yaw;				// degrees
  uint16_t baseThrust;		// Base unit for the thrust
  uint8_t resetControllers;	// Reset the controllers if 1
} __attribute__((packed));

void commanderXYZ_Init(void);
bool commanderXYZ_Test(void);
uint32_t commanderXYZ_GetInactivityTime(void);

void commanderXYZ_GetSetpoint(setpoint_t *setpoint, const state_t *state, uint8_t *resetController);
void commanderXYZ_GetPosition(state_t *state, const uint32_t tick);

#endif /* COMMANDER_XYZ_H_ */
