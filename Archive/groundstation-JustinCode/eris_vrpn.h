#ifndef ERIS_VRPN
#define ERIS_VRPN

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>

#include <quat.h>
#include "vrpn.h"

//void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);

void query_vrpn();

void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);

#endif
