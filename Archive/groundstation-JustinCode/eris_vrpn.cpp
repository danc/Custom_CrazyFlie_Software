//#include <iostream>
//#include <iomanip>
//#include <string>
//#include <cstdlib>
//#include <signal.h>
//#include <unistd.h>

#include "eris_vrpn.h"
#include "simple.h"
#include "pid.h"
#include "controller.h"
#include "CCrazyflie.h"
#include "vrpn.h"
#include "errno.h"

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//#include <quat.h>
//#include "vrpn.h"

const std::string erisIP = "192.168.0.196"; //TODO Change to IP of Other Linux Computer***
const int port = 60551;
const int buffLen = 8;

//double vrpnNow;
//double vrpnPrev;
//double vrpnTime0;

using namespace std;
void VRPN_CALLBACK handle_hand(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_Crazyflie2(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_Crazyflie3(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_Crazyflie4(void*, const vrpn_TRACKERCB t);
//void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);

void initMulticast();
void sendMulticast();
void readMulticast();

static void ctrlc_handler(int sig) ;

CCrazyRadio *crRadio1 = 0;
CCrazyRadio *crRadio2 = 0;
CCrazyflie *cflieCopter1 = 0;
CCrazyflie *cflieCopter2 = 0;
CCrazyflie *cflieCopter3 = 0;
CCrazyflie *cflieCopter4 = 0;
ControllerObject pidCtrl1 = { 0 }; // init all fields to zero
ControllerObject pidCtrl2 = { 0 }; // init all fields to zero
ControllerObject pidCtrl3 = { 0 }; // init all fields to zero
ControllerObject pidCtrl4 = { 0 }; // init all fields to zero

float rollControlOutput1;
float pitchControlOutput1;
float yawControlOutput1;
float thrustControlOutput1;

float rollControlOutput2;
float pitchControlOutput2;
float yawControlOutput2;
float thrustControlOutput2;

float rollControlOutput3;
float pitchControlOutput3;
float yawControlOutput3;
float thrustControlOutput3;

float rollControlOutput4;
float pitchControlOutput4;
float yawControlOutput4;
float thrustControlOutput4;

float cfliePitch = 0;
float cflieRoll = 0;
float cflieYaw = 0;
float cflieThrust = 0;
double lastTime = 0;
double curTime = 0;
double initTime1 = 0;
double initTime2 = 0;
double initTime3 = 0;
double initTime4 = 0;
double initTimeVrpn = 0;

int vrpnPacketBackupHand = 0;
double vrpnBackupTimeHand = 0;
double vrpnBackupTimeHandPrev = 0;
double vrpnBackupTimeHandDelta = 0;

int vrpnPacketBackup1 = 0;
double vrpnBackupTime1 = 0;
double vrpnBackupTime1Prev = 0;
double vrpnBackupTime1Delta = .01;

int vrpnPacketBackup2 = 0;
double vrpnBackupTime2 = 0;
double vrpnBackupTime2Prev = 0;
double vrpnBackupTime2Delta = 0;

int vrpnPacketBackup3 = 0;
double vrpnBackupTime3 = 0;
double vrpnBackupTime3Prev = 0;
double vrpnBackupTime3Delta = 0;

int vrpnPacketBackup4 = 0;
double vrpnBackupTime4 = 0;
double vrpnBackupTime4Prev = 0;
double vrpnBackupTime4Delta = 0;

//*cflieCopter=0
float xPositionDesired1 = -0.475;
float yPositionDesired1 = 0.340;
float zPositionDesired1 = 0.750;
float yawDesired1 = 0.0;

float yawDifference1 = 0.0;
float quadYaw1 = 0.0;
float prevQuadYaw1 = 0.0;
float camYawDeg1 = 0.0;
float camYawDeg1Off = 0.0;
float quadYaw1Off = 0.0;
float correctedYaw1 = 0.0;

float xError1 = 0.0;
float yError1 = 0.0;

float xStepPositionError1 = 0.0;
float yStepPositionError1 = 0.0;

float xPositionDesired2 = 0.681;
float yPositionDesired2 = 0.360;
float zPositionDesired2 = 0.6;
float yawDesired2 = 0.0;

float yawDifference2 = 0.0;
float quadYaw2 = 0.0;
float prevQuadYaw2 = 0.0;
float camYawDeg2 = 0.0;
float camYawDeg2Off = 0.0;
float quadYaw2Off = 0.0;
float correctedYaw2 = 0.0;

float xError2 = 0.0;
float yError2 = 0.0;

float xPositionDesired3 = 0.751;
float yPositionDesired3 = -0.450;
float zPositionDesired3 = 0.5;
float yawDesired3 = 0.0;

float yawDifference3 = 0.0;
float quadYaw3 = 0.0;
float prevQuadYaw3 = 0.0;
float camYawDeg3 = 0.0;
float camYawDeg3Off = 0.0;
float quadYaw3Off = 0.0;
float correctedYaw3 = 0.0;

float xError3 = 0.0;
float yError3 = 0.0;

float xPositionDesired4 = -0.200;
float yPositionDesired4 = -0.600;
float zPositionDesired4 = 0.4;
float yawDesired4 = 0.0;

float yawDifference4 = 0.0;
float quadYaw4 = 0.0;
float prevQuadYaw4 = 0.0;
float camYawDeg4 = 0.0;
float camYawDeg4Off = 0.0;
float quadYaw4Off = 0.0;
float correctedYaw4 = 0.0;

float xError4 = 0.0;
float yError4 = 0.0;

float xPositionHand = -0.012;
float yPositionHand = -0.008;
float zPositionHand = 0.0;
float pitchHand = 0.0;
float rollHand = 0.0;
float yawHand = 0.0;

float xPosition1 = -0.012;
float yPosition1 = -0.008;
float zPosition1 = 0.75;

float xPosition2 = 0.681;
float yPosition2 = 0.129;
float zPosition2 = 0.60;

float xPosition3 = 0.561;
float yPosition3 = -0.450;
float zPosition3 = 0.5;

float xPosition4 = -0.540;
float yPosition4 = -0.714;
float zPosition4 = 0.4;

int frameCount = 0;
double takeOffTime1 = 0;
double takeOffTime2 = 0;
double takeOffTime3 = 0;
double takeOffTime4 = 0;
double endTakeOffTime = 0;
float takeOffSetpoint = 0.75;

char landingMode = 0;
char mirrorMode = 0;
char takeOff1 = 0;
char takeOff2 = 0;
char takeOff3 = 0;
char takeOff4 = 0;
char handMode = 0;
char trackHand = 0;
char tcp_server_ON = 0;
char tcp_client_ON = 0;
char flap = 0;

//=========Testing Variables===============
int j = 0; //Testing Index Variable****
int dongleNum = 0;
bool toggle = 0;

double loopTimeTotalStart = 0;
double loopTimeTotalEnd = 0;
double loopTimeTotal = 0;		//Sum of Callback Time Totals
double loopTimeTotalPrev = 0;
double loopTimeTotalDelta = 0;

double loopTimeHandStart = 0;
double loopTimeHandEnd = 0;
double loopTimeHand = 0;
double loopTimeHandPrev = 0;
double loopTimeHandDelta = 0;

double loopTime1Start = 0;
double loopTime1End = 0;
double loopTime1 = 0;
double loopTime1Prev = 0;
double loopTime1Delta = 0;

double loopTime2Start = 0;
double loopTime2End = 0;
double loopTime2 = 0;
double loopTime2Prev = 0;
double loopTime2Delta = 0;

double loopTime3Start = 0;
double loopTime3End = 0;
double loopTime3 = 0;
double loopTime3Prev = 0;
double loopTime3Delta = 0;

double loopTime4Start = 0;
double loopTime4End = 0;
double loopTime4 = 0;
double loopTime4Prev = 0;
double loopTime4Delta = 0;

double vrpnPacketTimeHand = 0;
double vrpnPacketTimeHandPrev = 0;
double vrpnPacketDeltaHand = 0;

double vrpnPacketTime1 = 0;
double vrpnPacketTime1Prev = 0;
double vrpnPacketDelta1 = 0;

double vrpnPacketTime2 = 0;
double vrpnPacketTime2Prev = 0;
double vrpnPacketDelta2 = 0;

double vrpnPacketTime3 = 0;
double vrpnPacketTime3Prev = 0;
double vrpnPacketDelta3 = 0;

double vrpnPacketTime4 = 0;
double vrpnPacketTime4Prev = 0;
double vrpnPacketDelta4 = 0;

//========End Testing Variables============

FILE * outHand = NULL;
FILE * out1 = NULL;
FILE * out2 = NULL;
FILE * out3 = NULL;
FILE * out4 = NULL;

#define PI 3.14159265

#define USE_VRPN 1
#define USE_KEYBOARD 1
#define USE_PRINTOUT 0
#define USE_LOGGING 0
#define USE_BASIC_LOGGING 0
#define USE_PR_YAW_CORRECT 0	//DO NOT NEED (NOW IMPLEMENTED ON FIRMWARE)
#define USE_HAND 0

#define NUM_QUADS 4		//Use this to Enable Desired Number of Crazyflies to be initialized.

int main(int argc, char **argv) {
	sleep(1); //***FOR TESTING PURPOSES*** (REMOVE)

	signal(SIGINT, &ctrlc_handler);
	// init the tcp server that will transmit data to Eris
	//TCPServer serv(erisIP, port, buffLen);
	//serv.createSocket();

	// init vrpn and Attitude Controller and then run
#if NUM_QUADS >= 1
	controllerResetAllPID( &pidCtrl1 );
	controllerInit( &pidCtrl1 );
#endif
#if NUM_QUADS >= 2
	controllerResetAllPID( &pidCtrl2 );
	controllerInit( &pidCtrl2 );
#endif
#if NUM_QUADS >=3
	controllerResetAllPID( &pidCtrl3 );
	controllerInit( &pidCtrl3 );
#endif
#if NUM_QUADS >=4
	controllerResetAllPID( &pidCtrl4 );
	controllerInit( &pidCtrl4 );
#endif

	initMulticast();

	#if USE_VRPN
		#if USE_HAND
#if NUM_QUADS == 1
		vrpn_init("192.168.0.120:3883", handle_hand, handle_pos, NULL, NULL, NULL, NULL);
#elif NUM_QUADS == 2
		vrpn_init("192.168.0.120:3883", handle_hand, handle_pos, handle_Crazyflie2, NULL, NULL, NULL);
//		vrpn_init2("192.168.0.120:3883", handle_hand, handle_pos, handle_Crazyflie2);
#elif NUM_QUADS == 3
		vrpn_init("192.168.0.120:3883", handle_hand, handle_pos, handle_Crazyflie2, handle_Crazyflie3, NULL, NULL);
#elif NUM_QUADS == 4
		vrpn_init("192.168.0.120:3883", handle_hand, handle_pos, handle_Crazyflie2, handle_Crazyflie3, handle_Crazyflie4, NULL);
#endif

		#else

#if NUM_QUADS == 1
		vrpn_init("192.168.0.120:3883", NULL, handle_pos, NULL, NULL, NULL, NULL);
#elif NUM_QUADS == 2
		vrpn_init("192.168.0.120:3883", NULL, handle_pos, handle_Crazyflie2, NULL, NULL, NULL);
#elif NUM_QUADS == 3
		vrpn_init("192.168.0.120:3883", NULL, handle_pos, handle_Crazyflie2, handle_Crazyflie3, NULL, NULL);
#elif NUM_QUADS == 4
		vrpn_init("192.168.0.120:3883", NULL, handle_pos, handle_Crazyflie2, handle_Crazyflie3, handle_Crazyflie4, NULL);
#endif
		#endif
	#endif

	crRadio1 = new CCrazyRadio(0); // dongle number


	if( crRadio1->startRadio() )
	{
#if NUM_QUADS >= 1
		cflieCopter1 = new CCrazyflie(crRadio1, 5);
#endif
#if NUM_QUADS >= 2
		cflieCopter2 = new CCrazyflie(crRadio1, 80);
#endif
	}
	else
	{
        std::cerr << "Could not connect to dongle 1. Did you plug it in?" << std::endl;
        exit(-1);
	}
	usleep(10000);
#if NUM_QUADS >= 3
	crRadio2 = new CCrazyRadio(1); // dongle number

	if( crRadio2->startRadio() )
	{
#if NUM_QUADS >= 3
		cflieCopter3 = new CCrazyflie(crRadio2, 25);
#endif
#if NUM_QUADS >= 4
				cflieCopter4 = new CCrazyflie(crRadio2, 45);
#endif
	}
	else
	{
        std::cerr << "Could not connect to dongle 2. Did you plug it in?" << std::endl;
        exit(-1);
	}
#endif

	usleep(10000);

#if USE_LOGGING
//===============Initialize the Logging Files=====================
	//const char * cols = "Timestamp, Roll, Pitch, Yaw, X, Y, Z";
#if USE_BASIC_LOGGING
		const char * logHeaderHand = "#Hand Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tLoopTime\t\tLoopTimeDelta\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tseconds\t\tseconds\n";

		const char * logHeader1 = "#Crazyflie1 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\t\tRadioRSSI\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\t\tstrength\n";
										
	const char * logHeader2 = "#Crazyflie2 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\n";

	const char * logHeader3 = "#Crazyflie3 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\n";

	const char * logHeader4 = "#Crazyflie4 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\n";
	
#if USE_HAND
	outHand = fopen("hand.txt", "w");
		
			if(outHand == NULL)
			{
				printf("Could not open hand.log: errno %d\n", errno);
				exit(-1);
			}
		
	fprintf(outHand, "%s\n", logHeaderHand);
#endif
	if(cflieCopter1)
	{
		out1 = fopen("cflie1.txt", "w");
		
		if(out1 == NULL)
		{
			printf("Could not open cflie1.log: errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out1, "%s\n", logHeader1);
	}
		
	if(cflieCopter2)
	{
		out2 = fopen("cflie2.txt", "w");
		
		if(out2 == NULL)
		{
			printf("Could not open cflie2.log:errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out2, "%s\n", logHeader2);
	}

	if(cflieCopter3)
	{
		out3 = fopen("cflie3.txt", "w");
		
		if(out3 == NULL)
		{
			printf("Could not open cflie3.log:errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out3, "%s\n", logHeader3);
	}
	if(cflieCopter4)
	{
		out4 = fopen("cflie4.txt", "w");
		
		if(out4 == NULL)
		{
			printf("Could not open cflie4.log:errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out4, "%s\n", logHeader4);
	}
//===========End Logfile Init==============		

#else
		const char * logHeaderHand = "#Hand Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tLoopTime\t\tLoopTimeDelta\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tseconds\t\tseconds\n";

	const char * logHeader1 = "#Crazyflie1 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\t\tXSetpoint\t\tYSetpoint\t\tZSetpoint\t\tYawSetpoint\t\tPitchDesired\t\tRollDesired\t\tYawDesired\t\tThrustDesired\t\tRadioRSSI\t\tCamYawRad\t\tCamPitch\t\tCamRoll\t\tMotor1\t\tMotor2\t\tMotor3\t\tMotor4\t\tRollRate\t\tPitchRate\t\tYawRate\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tN/A\t\tdB\t\trad\t\trad\t\trad\t\tN/A\t\tN/A\t\tN/A\t\tN/A\t\trad/s\t\trad/s\t\trad/s\n";
										
	const char * logHeader2 = "#Crazyflie2 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\t\tXSetpoint\t\tYSetpoint\t\tZSetpoint\t\tYawSetpoint\t\tPitchDesired\t\tRollDesired\t\tYawDesired\t\tThrustDesired\t\tRadioRSSI\t\tCamYawRad\t\tCamPitch\t\tCamRoll\t\tMotor1\t\tMotor2\t\tMotor3\t\tMotor4\t\tRollRate\t\tPitchRate\t\tYawRate\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tN/A\t\tdB\t\trad\t\trad\t\trad\t\tN/A\t\tN/A\t\tN/A\t\tN/A\t\trad/s\t\trad/s\t\trad/s\n";

	const char * logHeader3 = "#Crazyflie3 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\t\tXSetpoint\t\tYSetpoint\t\tZSetpoint\t\tYawSetpoint\t\tPitchDesired\t\tRollDesired\t\tYawDesired\t\tThrustDesired\t\tRadioRSSI\t\tCamYawRad\t\tCamPitch\t\tCamRoll\t\tMotor1\t\tMotor2\t\tMotor3\t\tMotor4\t\tRollRate\t\tPitchRate\t\tYawRate\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tN/A\t\tdB\t\trad\t\trad\t\trad\t\tN/A\t\tN/A\t\tN/A\t\tN/A\t\trad/s\t\trad/s\t\trad/s\n";

	const char * logHeader4 = "#Crazyflie4 Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\t\tXSetpoint\t\tYSetpoint\t\tZSetpoint\t\tYawSetpoint\t\tPitchDesired\t\tRollDesired\t\tYawDesired\t\tThrustDesired\t\tRadioRSSI\t\tCamYawRad\t\tCamPitch\t\tCamRoll\t\tMotor1\t\tMotor2\t\tMotor3\t\tMotor4\t\tRollRate\t\tPitchRate\t\tYawRate\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tN/A\t\tdB\t\trad\t\trad\t\trad\t\tN/A\t\tN/A\t\tN/A\t\tN/A\t\trad/s\t\trad/s\t\trad/s\n";

#if USE_HAND
	outHand = fopen("hand.txt", "w");
		
			if(out1 == NULL)
			{
				printf("Could not open hand.log: errno %d\n", errno);
				exit(-1);
			}
		
	fprintf(outHand, "%s\n", logHeaderHand);
#endif
	
	if(cflieCopter1)
	{
		out1 = fopen("cflie1.txt", "w");
		
		if(out1 == NULL)
		{
			printf("Could not open cflie1.log: errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out1, "%s\n", logHeader1);
	}
		
	if(cflieCopter2)
	{
		out2 = fopen("cflie2.txt", "w");
		
		if(out2 == NULL)
		{
			printf("Could not open cflie2.log:errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out2, "%s\n", logHeader2);
	}

	if(cflieCopter3)
	{
		out3 = fopen("cflie3.txt", "w");
		
		if(out3 == NULL)
		{
			printf("Could not open cflie3.log:errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out3, "%s\n", logHeader3);
	}
	if(cflieCopter4)
	{
		out4 = fopen("cflie4.txt", "w");
		
		if(out4 == NULL)
		{
			printf("Could not open cflie4.log:errno %d\n", errno);
			exit(-1);
		}
		
		fprintf(out4, "%s\n", logHeader4);
	}
//===========End Logfile Init==============		
#endif
#endif	

	crRadio1->setARDTime(2000); //Sets Wait Time between Packet Retries (**MUST BE IN INCREMENTS OF 250**)
	crRadio1->setARC(0);		//Sets Number of times Retries Packet Send
	crRadio1->setPower(P_0DBM);

if(crRadio2){
	crRadio2->setARDTime(2000);
	crRadio2->setARC(0);
	crRadio2->setPower(P_0DBM);
}

#if USE_VRPN
#if NUM_QUADS >= 1
		cflieCopter1->setSendSetpoints( true );
		initTime1 = cflieCopter1->currentTime();
		cflieCopter1->m_enumFlightMode = GROUNDED_MODE;
#endif
#if NUM_QUADS >= 2
		cflieCopter2->setSendSetpoints( true );
		initTime2 = cflieCopter2->currentTime();
		cflieCopter2->m_enumFlightMode = GROUNDED_MODE;
#endif
#if NUM_QUADS >= 3
		cflieCopter3->setSendSetpoints( true );
		initTime3 = cflieCopter3->currentTime();
		cflieCopter3->m_enumFlightMode = GROUNDED_MODE;
#endif
#if NUM_QUADS >= 4
		cflieCopter4->setSendSetpoints( true );
		initTime4 = cflieCopter4->currentTime();
		cflieCopter4->m_enumFlightMode = GROUNDED_MODE;
#endif

//		prevQuadYaw1 = cflieCopter1->yaw();			//TODO HACK TO AVOID PACKET CROSSING****
//		sleep(0.5);
//		prevQuadYaw2 = cflieCopter2->yaw();

	cout << "The Init has completed" << endl;
//#if NUM_QUADS == 2
//		vrpn_go2();
//#else
		vrpn_go();
//#endif

#else
	cflieCopter1->setSendSetpoints( true );
	cflieCopter2->setSendSetpoints( true );
	
	cflieCopter1->setThrust( 20000 );

	cflieCopter2->setThrust( 20000 );

	
	bool toggle = 0;
	lastTime = cflieCopter2->currentTime();
	while (1) {
		curTime = cflieCopter2->currentTime();
		
		if( curTime - lastTime > 1.0 )
		{
		   toggle = !toggle;
		   lastTime = curTime;
		}

		cflieCopter1->setThrust( 20000 );
		
		if( toggle ){
			cflieCopter1->setThrust( 20000 );
			
			printf("Low  %.3lf ", curTime);
		}
		else
		{
			cflieCopter1->setThrust( 40000 );

			printf("High %.3lf ", curTime);
		}

		if( cflieCopter1 )
		{
			cflieCopter1->cycle();
			printf	( "CR1: %5.2f %5.2f %5.2f (%c%c)  ",
				cflieCopter1->pitch(),
				cflieCopter1->roll(),
				cflieCopter1->yaw(),
				cflieCopter1->isInitialized() ? 'I' : 'i',
				cflieCopter1->copterInRange() ? 'R' : 'r'
//				cflieCopter1->currentTime()
				);
		}
		if( cflieCopter2 )
		{
			cflieCopter2->cycle();
			printf	( "CR2: %5.2f %5.2f %5.2f (%c%c)",
				cflieCopter2->pitch(),
				cflieCopter2->roll(),
				cflieCopter2->yaw(),
				cflieCopter2->isInitialized() ? 'I' : 'i',
				cflieCopter2->copterInRange() ? 'R' : 'r'
//				cflieCopter1->currentTime()
				);
		}
		printf( "\n" );
	}
#endif

#if USE_LOGGING
	#if USE_HAND
	fclose(outHand);
	#endif
	#if NUM_QUADS >= 1
	fclose(out1);
	#endif
	#if NUM_QUADS >= 2
	fclose(out2);
	#endif
	#if NUM_QUADS >= 3
	fclose(out3);
	#endif
	#if NUM_QUADS >= 4
	fclose(out4);
	#endif
#endif

	// All done.
#if NUM_QUADS >= 1
	delete cflieCopter1;
#endif
#if NUM_QUADS >= 2
	delete cflieCopter2;
#endif
#if NUM_QUADS >= 3
	delete cflieCopter3;
#endif
#if NUM_QUADS >= 4
	delete cflieCopter4;
#endif
	delete crRadio1;
	delete crRadio2;

	return 0;
	//vrpn_Connection::removeReference(); // NEED TO ADD THIS TO KILL CONNECTION*******
}
#if NUM_QUADS >= 1
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t) {
//cout << "First Callback" << endl;
//vrpnBackupTime1 = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
//vrpnBackupTime1Delta = vrpnBackupTime1 - vrpnBackupTime1Prev;
//vrpnBackupTime1Prev = vrpnBackupTime1;

if(vrpnPacketBackup1 < 2)	//If VRPN Buffer does NOT have old packets stored up, run main code
{
	loopTime1Start = cflieCopter1->currentTime() - initTime1;	
	loopTime1Delta = loopTime1Start - loopTime1Prev;	//Calculates time difference between each loop start
	loopTime1Prev = loopTime1Start;

	vrpnPacketTime1 = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnPacketDelta1 = vrpnPacketTime1 - vrpnPacketTime1Prev;			//Calculates time between VRPN Data Packets (almost always 0.01 sec)
	vrpnPacketTime1Prev = vrpnPacketTime1;

	vrpnPacketBackup1 = ceil(loopTime1Delta/vrpnPacketDelta1);		//Calculates estimated # of packets built up in VRPN buffer.

	controllerSetAllDt(&pidCtrl1, vrpnPacketDelta1);	//Variable dt for controller based on time between packets | vrpnPacketDelta1
	
#if NUM_QUADS == 2
	cflieCopter2->cheat_process_packets();
#elif NUM_QUADS == 3
	cflieCopter3->cheat_process_packets();
#elif NUM_QUADS == 4
   cflieCopter2->cheat_process_packets();
#endif

   crRadio1->clearPackets();
	crRadio1->setChannel( cflieCopter1->radioChannel() );

//	crRadio1->sendDummyPacket(cflieCopter1->radioChannel());

	frameCount++;

	// Get the euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);

	xPosition1 = t.pos[0];
	yPosition1 = t.pos[1];
	zPosition1 = -t.pos[2] + 0.05;

	//cout << "\033[2J\033[1;1H";  //Clears Terminal Window to display clean output
#if USE_PRINTOUT
	cout << "PID Radio 1" << endl << endl;
#endif

#if 0

	cfliePitch = cflieCopter1->pitch();
	cflieRoll = cflieCopter1->roll();
	cflieYaw = cflieCopter1->yaw();
	cflieThrust = cflieCopter1->thrust();

	/*
	 * Position
	 * x      t.pos[0]
	 * y      t.pos[1]
	 * z      t.pos[2]
	 *
	 * Orientation
	 * yaw    euler[0]  Rotation
	 * pitch  euler[1]  East-West movement
	 * roll   euler[2]  North-South movement
	 * Euler's range goes from -pi : +pi
	 */
	QUAD quad;
	quad.vrpnNow = 0;
	vrpn_data_t *vrpnData;

	vrpnData = (vrpn_data_t*) malloc(sizeof(vrpn_data_t));
	vrpnData->usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnData->x = t.pos[0];
	vrpnData->y = t.pos[1];
	vrpnData->z = -t.pos[2] + 0.05; //Higher Altitude = Negative z  //Added Offset for floor below camera system origin
	vrpnData->yaw = euler[0];
	vrpnData->pitch = euler[1];
	vrpnData->roll = euler[2];

	quad.vrpnPrev = quad.vrpnNow;
	if (quad.vrpnNow == 0) {
		quad.vrpnPrev = 0;
		quad.vrpnTime0 = vrpnData->usec;
		quad.vrpnNow = vrpnData->usec;
	} else {
		quad.vrpnNow = vrpnData->usec - quad.vrpnTime0;
	}
#endif

	//****INSERT RUNTIME CODE HERE****

	/*
	 printf( "Do you want keyboard or RC input?\n");
	 printf( "k = Keyboard\n");
	 printf( "r = RC Joysticks\n" );

	 while( !validChoiceMade )
	 {
	 if ( kbhit() != 0 )
	 {
	 choice = fgetc( stdin );
	 if ( choice == 'k' )
	 {
	 controls = KEYBOARD;
	 validChoiceMade = 1;
	 }
	 else if ( choice == 'r' )
	 {flap
	 controls = RC;
	 validChoiceMade = 1;
	 }
	 }
	 }
	 */
//==============YAW CORRECTION========================= (IMPORTANT!!!: YAW CONTROLLER INPUT IS INVERTED IN simple.cpp FILE***)
	camYawDeg1 = -(euler[0] * 57.2958); //Converts From Radians TO Degrees
	quadYaw1 = cflieCopter1->yaw();

//cout << "Yaw 1: " << quadYaw1 << endl;

//	if((prevQuadYaw1 - quadYaw1 < -50) || (prevQuadYaw1-quadYaw1 > 50))		//TODO HACK TO ELIMINATE PACKET CROSSING****
//	{
//		cout << "Yaw1 Prev: " << prevQuadYaw1 << endl;
//		cout << "Yaw1 Current: " << quadYaw1 << endl;
//		quadYaw1 = prevQuadYaw1;
//	}

	if (frameCount == 50) {
		if (camYawDeg1 < 0.0)
			camYawDeg1Off = camYawDeg1 + 360.0;			//Converts from +-180 to full 360 degree form
		else
			camYawDeg1Off = camYawDeg1;


		if(quadYaw1 < 0.0)
			quadYaw1Off = quadYaw1 + 360.0;
		else
			quadYaw1Off = quadYaw1;

		yawDifference1 = quadYaw1Off - camYawDeg1Off;
		cout << "Yaw1 Offset Taken: " << yawDifference1 << endl;
	}

	correctedYaw1 = camYawDeg1 + yawDifference1;

	if(correctedYaw1 > 180.0)
		correctedYaw1 -= 360.0;
   else if(correctedYaw1 < -180.0)
		correctedYaw1 += 360.0;

//	yawDesired1 = yawDesired1 + yawDifference1; //Fixes difference in camera and quad yaw measurements

//	if (yawDesired1 > 180.0)
//		yawDesired1 -= 360.0;
//	else if (yawDesired1 < -180.0)
//		yawDesired1 += 360.0;

//	prevQuadYaw1 = quadYaw1;				//TODO HACK TO ELIMINATE PACKET CROSSING****

//===============END YAW CORRECTION=========================
#if USE_PRINTOUT
		cout << "X: " << t.pos[0] << endl;
		cout << "Y: " << t.pos[1] << endl;
		cout << "Z: " << -t.pos[2] + 0.05 << endl;

	//	cout << "Quad Yaw: " << cflieCopter1->yaw() << endl;
	//	cout << "Camera Yaw: " << camYawDeg1 << endl;

	//	cout << "Yaw Offset: " << yawDifference1 << endl;
	//	cout << "New Desired Yaw: " << yawDesired1 << endl;
	cout << "Frame Count: " << frameCount << endl;
#endif

//	free(vrpnData);

#if USE_KEYBOARD
	if (cflieCopter1->m_enumFlightMode == LANDING_MODE) //Landing Mode (TODO Change to be a keystroke when implemented)
	{

	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
//	xError1 = cos(correctedYaw1 / 57.2958)*(xPositionDesired1 - xPosition1) - sin(correctedYaw1 / 57.2958)*(yPositionDesired1 - yPosition1);	
//	yError1 = sin(correctedYaw1 / 57.2958)*(xPositionDesired1 - xPosition1) + cos(correctedYaw1 / 57.2958)*(yPositionDesired1 - yPosition1);

	xError1 = xPositionDesired1 - xPosition1;
	yError1 = yPositionDesired1 - yPosition1;
	controllerSetXYError(&pidCtrl1, xError1, yError1);

		controllerCorrectAttitudePID(&pidCtrl1, t.pos[0], t.pos[1], zPosition1, correctedYaw1,
				xPositionDesired1, yPositionDesired1, -0.4, yawDesired1,
				&rollControlOutput1, &pitchControlOutput1, &yawControlOutput1,
				&thrustControlOutput1);
		takeOff1 = 0;

	} else if (cflieCopter1->m_enumFlightMode == MIRROR_MODE) {		//Acts as Master
		xPositionDesired1 = -0.012;
		yPositionDesired1 = -0.200;
		zPositionDesired1 = 0.75;

		if( loopTime1Start - lastTime > 4.0 )
		{
		   toggle = !toggle;
		   lastTime = loopTime1Start;
		}

#if USE_PRINTOUT
		cout << "New xPositionDesired: " << xPositionDesired1 << endl;
#endif
if(toggle)
{
xStepPositionError1 = xPositionDesired1 - xPosition1;
yStepPositionError1 = yPositionDesired1 - yPosition1;
controllerSetXYError(&pidCtrl1, xStepPositionError1, yStepPositionError1);

		controllerCorrectAttitudePID(&pidCtrl1, t.pos[0], t.pos[1], zPosition1, correctedYaw1,
				xPositionDesired1, yPositionDesired1, zPositionDesired1,
				yawDesired1, &rollControlOutput1, &pitchControlOutput1,
				&yawControlOutput1, &thrustControlOutput1);
}
else
{
xStepPositionError1 = (xPositionDesired1 + 0.65) - xPosition1;
yStepPositionError1 = (yPositionDesired1 + 0.65) - yPosition1;
controllerSetXYError(&pidCtrl1, xStepPositionError1, yStepPositionError1);

		controllerCorrectAttitudePID(&pidCtrl1, t.pos[0], t.pos[1], zPosition1, correctedYaw1,
				xPositionDesired1 + 0.65, yPositionDesired1 + 0.65, zPositionDesired1,
				yawDesired1, &rollControlOutput1, &pitchControlOutput1,
				&yawControlOutput1, &thrustControlOutput1);

//cout << "Toggled!" << endl;
}

	} else if (cflieCopter1->m_enumFlightMode == TAKEOFF_MODE) {		//correctedYaw1
//		zPositionDesired1 = 1.0;

	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
//	xError1 = cos(correctedYaw1 / 57.2958)*(xPositionDesired1 - xPosition1) - sin(correctedYaw1 / 57.2958)*(yPositionDesired1 - yPosition1);	
//	yError1 = sin(correctedYaw1 / 57.2958)*(xPositionDesired1 - xPosition1) + cos(correctedYaw1 / 57.2958)*(yPositionDesired1 - yPosition1);

	xError1 = xPositionDesired1 - xPosition1;
	yError1 = yPositionDesired1 - yPosition1;
	controllerSetXYError(&pidCtrl1, xError1, yError1);
/*
		if ((cflieCopter1->currentTime() - takeOffTime1) > 0.02) //To reduce Ground Effect - Boost Thrust for first 0.5 seconds of takeoff
		{
			controllerCorrectAttitudePID(&pidCtrl1, t.pos[0], t.pos[1], zPosition1, correctedYaw1,
					xPositionDesired1, yPositionDesired1, takeOffSetpoint,
					yawDesired1, &rollControlOutput1, &pitchControlOutput1,
					&yawControlOutput1, &thrustControlOutput1);
		}
		else{*/
			controllerCorrectAttitudePID(&pidCtrl1, t.pos[0], t.pos[1], zPosition1, correctedYaw1,
					xPositionDesired1, yPositionDesired1, zPositionDesired1,
					yawDesired1, &rollControlOutput1, &pitchControlOutput1,
					&yawControlOutput1, &thrustControlOutput1);
//		}

	}
	 else if (cflieCopter1->m_enumFlightMode == HAND_MODE) {		//correctedYaw1

	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
//	xError1 = cos(correctedYaw1 / 57.2958)*(xPositionDesired1 - xPosition1) - sin(correctedYaw1 / 57.2958)*(yPositionDesired1 - yPosition1);	
//	yError1 = sin(correctedYaw1 / 57.2958)*(xPositionDesired1 - xPosition1) + cos(correctedYaw1 / 57.2958)*(yPositionDesired1 - yPosition1);

	xError1 = (xPositionHand - 0.5) - xPosition1;
	yError1 = (yPositionHand + 0.5) - yPosition1;
	controllerSetXYError(&pidCtrl1, xError1, yError1);

	controllerCorrectAttitudePID(&pidCtrl1, t.pos[0], t.pos[1], zPosition1, correctedYaw1,
			xPositionDesired1, yPositionDesired1, zPositionDesired1,
			yawDesired1, &rollControlOutput1, &pitchControlOutput1,
			&yawControlOutput1, &thrustControlOutput1);

	}
		else if(flap){
//		cflieCopter1->setThrust(20000);
//		cflieCopter2->setThrust(20000);  //45000 Thrust  of Crazyflie2.0 to match Crazyflie1.0
		
//		cflieCopter2->setYaw(90);


//		lastTime = cflieCopter2->currentTime();
		curTime = cflieCopter2->currentTime();
		
		if( curTime - lastTime > 1.0 )
		{
		   toggle = !toggle;
		   lastTime = curTime;
		}
		
		if( toggle ){
			cflieCopter1->setThrust( 20000 );
			
			printf("Low  %.3lf ", curTime);
		}
		else
		{
			cflieCopter1->setThrust( 40000 );

			printf("High %.3lf ", curTime);
		}
/*
		if( cflieCopter1 )
		{

			printf	( "CR1: %5.2f %5.2f %5.2f (%c%c) %.3lf ",
				cflieCopter1->pitch(),
				cflieCopter1->roll(),
				cflieCopter1->yaw(),
				cflieCopter1->isInitialized() ? 'I' : 'i',
				cflieCopter1->copterInRange() ? 'R' : 'r',
				cflieCopter1->currentTime()
				);
		
		}
		printf( "\n" );
*/
	}

	else if(cflieCopter1->m_enumFlightMode == GROUNDED_MODE){
		controllerResetAllPID(&pidCtrl1);
		rollControlOutput1 = 0;
		pitchControlOutput1 = 0;
		yawControlOutput1 = 0;
		thrustControlOutput1 = 0;
	}

	else{		//Emergency Case (Should never happen)
		controllerResetAllPID(&pidCtrl1);
		rollControlOutput1 = 0;
		pitchControlOutput1 = 0;
		yawControlOutput1 = 0;
		thrustControlOutput1 = 0;
	}

#else
	controllerCorrectAttitudePID(&pidCtrl1, t.pos[0], t.pos[1], -t.pos[2], correctedYaw1,
					xPositionDesired1, yPositionDesired1, zPositionDesired1,
								yawDesired1, &rollControlOutput1, &pitchControlOutput1,
													&yawControlOutput1, &thrustControlOutput1);
#endif
	RunCrazyflie(crRadio1, cflieCopter1, rollControlOutput1,
			pitchControlOutput1, yawControlOutput1, thrustControlOutput1);

//			cflieCopter1->sendSetpoint(rollControlOutput1, pitchControlOutput1, -yawControlOutput1, thrustControlOutput1);

//         cflieCopter1->setThrust(thrustControlOutput1);
//         cflieCopter1->setRoll(rollControlOutput1);
//         cflieCopter1->setPitch(pitchControlOutput1);
//         cflieCopter1->setYaw(-yawControlOutput1);

//			cflieCopter1->cycle();

#if USE_LOGGING		
#if USE_BASIC_LOGGING
	fprintf(out1, "%.6f\t\t", cflieCopter1->currentTime() - initTime1 );
	fprintf(out1, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%d",
				cflieCopter1->roll(), 
				cflieCopter1->pitch(), 
				quadYaw1, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg1,
				correctedYaw1,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime1,
				loopTime1Delta,
				vrpnPacketDelta1,
				vrpnPacketTime1,
				vrpnPacketBackup1,
				cflieCopter1->radioRSSI());	//
	fprintf(out1, "\n");

#else	
	fprintf(out1, "%.6f\t\t", cflieCopter1->currentTime() - initTime1 );
	fprintf(out1, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%lu\t\t%lu\t\t%lu\t\t%lu\t\t%.3f\t\t%.3f\t\t%.3f",
				cflieCopter1->roll(), 
				cflieCopter1->pitch(), 
				quadYaw1, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg1,
				correctedYaw1,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime1,
				loopTime1Delta,
				vrpnPacketDelta1,
				vrpnPacketTime1,
				vrpnPacketBackup1,
				xPositionDesired1,
				yPositionDesired1,
				zPositionDesired1,
				yawDesired1,
				pitchControlOutput1,
				rollControlOutput1,
				yawControlOutput1,
				thrustControlOutput1,
				cflieCopter1->radioRSSI(),
				euler[0],
				euler[1],
				euler[2],
				cflieCopter1->motor1(),
				cflieCopter1->motor2(),
				cflieCopter1->motor3(),
				cflieCopter1->motor4(),
				cflieCopter1->gyroX(),
				cflieCopter1->gyroY(),
				cflieCopter1->gyroZ());	//
	fprintf(out1, "\n");
#endif
#endif

#if 0
	if (tcp_client_ON) {
		readMulticast(); //(argc < 1 = Listen Mode)
//		runTCPClient();
	}
#endif

	//cout << "PID Radio 2" << endl << endl;

	loopTime1End = cflieCopter1->currentTime() - initTime1;
	loopTime1 = loopTime1End - loopTime1Start;

}	
else if(vrpnPacketBackup1 < 0){	//Failsafe so can't go below 0 (should never happen)	| || (vrpnPacketBackup1 > 20)?
	vrpnPacketBackup1 = 0;
//	RunCrazyflie(crRadio1, cflieCopter1, rollControlOutput1,
//			pitchControlOutput1, yawControlOutput1, thrustControlOutput1);
}
else if(vrpnPacketBackup1 >= 2){
	vrpnPacketBackup1 --;
//	RunCrazyflie(crRadio1, cflieCopter1, rollControlOutput1,
//			pitchControlOutput1, yawControlOutput1, thrustControlOutput1);
//	cout << "VRPN Packet Backup 1 Detected: " << vrpnPacketBackup1 << endl;
}//End Packet Backup If check

}	//End Callback 1
#endif
#if NUM_QUADS >= 2
void VRPN_CALLBACK handle_Crazyflie2(void*, const vrpn_TRACKERCB t) {	//Start of Callback2

if(vrpnPacketBackup2 < 2)	//If VRPN Buffer does NOT have old packets stored up, run main code
{
	loopTime2Start = cflieCopter2->currentTime() - initTime2;
	loopTime2Delta = loopTime2Start - loopTime2Prev;		//Calculates time difference between each loop start
	loopTime2Prev = loopTime2Start;

	vrpnPacketTime2 = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnPacketDelta2 = vrpnPacketTime2 - vrpnPacketTime2Prev;
	vrpnPacketTime2Prev = vrpnPacketTime2;

	vrpnPacketBackup2 = ceil(loopTime2Delta/vrpnPacketDelta2);		//Calculates estimated # of packets built up in VRPN buffer.

	controllerSetAllDt(&pidCtrl2, vrpnPacketDelta2);	//Variable dt for controller based on time between packets | vrpnPacketDelta2

   cflieCopter1->cheat_process_packets();		//Flush out other Copter Packets
//   cflieCopter3->cheat_process_packets();		//Flush out other Copter Packets
   crRadio1->clearPackets();
	crRadio1->setChannel( cflieCopter2->radioChannel() );

	// Get the euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);
#if USE_PRINTOUT
	cout << "PID Radio 2" << endl << endl;
#endif

	xPosition2 = t.pos[0];
	yPosition2 = t.pos[1];
	zPosition2 = -t.pos[2] + 0.05;
#if 0
	//cout << "\033[2J\033[1;1H";  //Clears Terminal Window to display clean output

	/*
	 * Position
	 * x      t.pos[0]
	 * y      t.pos[1]
	 * z      t.pos[2]
	 *
	 * Orientation
	 * yaw    euler[0]  Rotation
	 * pitch  euler[1]  East-West movement
	 * roll   euler[2]  North-South movement
	 * Euler's range goes from -pi : +pi
	 */
	QUAD quad;
	quad.vrpnNow = 0;
	vrpn_data_t *vrpnData;

	vrpnData = (vrpn_data_t*) malloc(sizeof(vrpn_data_t));
	vrpnData->usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnData->x = t.pos[0];
	vrpnData->y = t.pos[1];
	vrpnData->z = -t.pos[2] + 0.05; //Higher Altitude = Negative z  //Added Offset for floor below camera system origin
	vrpnData->yaw = euler[0];
	vrpnData->pitch = euler[1];
	vrpnData->roll = euler[2];

	cout << "xPosition of Crazyflie2: " << xPosition2 << endl;

	quad.vrpnPrev = quad.vrpnNow;
	if (quad.vrpnNow == 0) {
		quad.vrpnPrev = 0;
		quad.vrpnTime0 = vrpnData->usec;
		quad.vrpnNow = vrpnData->usec;
	} else {
		quad.vrpnNow = vrpnData->usec - quad.vrpnTime0;
	}
#endif
//==============YAW CORRECTION========================= (IMPORTANT!!!: YAW CONTROLLER INPUT IS INVERTED IN simple.cpp FILE***)
	camYawDeg2 = -(euler[0] * 57.2958); //Converts From Radians TO Degrees
	quadYaw2 = cflieCopter2->yaw();

//	if((prevQuadYaw2 - quadYaw2 < -50) || (prevQuadYaw2-quadYaw2 > 50))		//TODO HACK TO ELIMINATE PACKET CROSSING****
//	{
		//cout << "Yaw2 Prev: " << prevQuadYaw2 << endl;
		//cout << "Yaw2 Current: " << quadYaw2 << endl;
//      printf( "Yaw2 prev %8.2f  curr %8.2f\n", prevQuadYaw2, quadYaw2 );
//		quadYaw2 = prevQuadYaw2;
//	}

	if (frameCount == 50) {
		if (camYawDeg2 < 0.0)
			camYawDeg2Off = camYawDeg2 + 360.0;					//Converts from +-180 to full 360 degree form
		else
			camYawDeg2Off = camYawDeg2;

		

		if(quadYaw2 < 0.0)
			quadYaw2Off = quadYaw2 + 360.0;
		else
			quadYaw2Off = quadYaw2;

		yawDifference2 = quadYaw2Off - camYawDeg2Off;
		cout << "Yaw2 Offset Taken: " << yawDifference2 << endl;
	}

	correctedYaw2 = camYawDeg2 + yawDifference2;

	if(correctedYaw2 > 180.0)
		correctedYaw2 -= 360.0;
	else if(correctedYaw2 < -180.0)
		correctedYaw2 += 360.0;

//	yawDesired2 = yawDesired2 + yawDifference2; //Fixes difference in camera and quad yaw measurements

//	if (yawDesired2 > 180.0)
//		yawDesired2 -= 360.0;
//	else if (yawDesired2 < -180.0)
//		yawDesired2 += 360.0;

//	prevQuadYaw2 = quadYaw2;				//TODO HACK TO ELIMINATE PACKET CROSSING****

//===============END YAW CORRECTION=========================

#if USE_PRINTOUT
		cout << "X: " << t.pos[0] << endl;
		cout << "Y: " << t.pos[1] << endl;
		cout << "Z: " << -t.pos[2] + 0.05 << endl;
#endif

//	free(vrpnData);

	//****INSERT RUNTIME CODE HERE****

#if USE_KEYBOARD
	if (cflieCopter2->m_enumFlightMode == LANDING_MODE) //Landing Mode (TODO Change to be a keystroke when implemented)
	{
#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError2 = cos(correctedYaw2 / 57.2958)*(xPositionDesired2 - xPosition2) - sin(correctedYaw2 / 57.2958)*(yPositionDesired2 - yPosition2);	
	yError2 = sin(correctedYaw2 / 57.2958)*(xPositionDesired2 - xPosition2) + cos(correctedYaw2 / 57.2958)*(yPositionDesired2 - yPosition2);
#else
	xError2 = xPositionDesired2 - xPosition2;
	yError2 = yPositionDesired2 - yPosition2;
#endif

	controllerSetXYError(&pidCtrl2, xError2, yError2);

		controllerCorrectAttitudePID(&pidCtrl2, t.pos[0], t.pos[1], -t.pos[2], correctedYaw2,
				xPositionDesired2, yPositionDesired2, -0.4, yawDesired2,
				&rollControlOutput2, &pitchControlOutput2, &yawControlOutput2,
				&thrustControlOutput2);
		takeOff2 = 0;
	} 
	else if (cflieCopter2->m_enumFlightMode == MIRROR_MODE) {									//***REMOVED (NEED SEPARATE ENABLE VARIABLE)****
		xPositionDesired2 = (xPosition1 + 0.45);
		yPositionDesired2 = (yPosition1);
//		zPositionDesired2 = (zPosition1);

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError2 = cos(correctedYaw2 / 57.2958)*(xPositionDesired2 - xPosition2) - sin(correctedYaw2 / 57.2958)*(yPositionDesired2 - yPosition2);	
	yError2 = sin(correctedYaw2 / 57.2958)*(xPositionDesired2 - xPosition2) + cos(correctedYaw2 / 57.2958)*(yPositionDesired2 - yPosition2);
#else
	xError2 = xPositionDesired2 - xPosition2;
	yError2 = yPositionDesired2 - yPosition2;
#endif

	controllerSetXYError(&pidCtrl2, xError2, yError2);
#if USE_PRINTOUT
		cout << "New xPositionDesired: " << xPositionDesired2 << endl;
#endif
		controllerCorrectAttitudePID(&pidCtrl2, t.pos[0], t.pos[1], -t.pos[2], correctedYaw2,
				xPositionDesired2, yPositionDesired2, zPositionDesired2,
				yawDesired2, &rollControlOutput2, &pitchControlOutput2,
				&yawControlOutput2, &thrustControlOutput2);

	} 
	else if (cflieCopter2->m_enumFlightMode == TAKEOFF_MODE) {			//correctedYaw2
//		zPositionDesired2 = 1.0;

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError2 = cos(correctedYaw2 / 57.2958)*(xPositionDesired2 - xPosition2) - sin(correctedYaw2 / 57.2958)*(yPositionDesired2 - yPosition2);	
	yError2 = sin(correctedYaw2 / 57.2958)*(xPositionDesired2 - xPosition2) + cos(correctedYaw2 / 57.2958)*(yPositionDesired2 - yPosition2);
#else
	xError2 = xPositionDesired2 - xPosition2;
	yError2 = yPositionDesired2 - yPosition2;
#endif

	controllerSetXYError(&pidCtrl2, xError2, yError2);

/*		if ((cflieCopter2->currentTime() - takeOffTime2) > 0.02) //To reduce Ground Effect - Boost Thrust for first 0.5 seconds of takeoff
		{
			controllerCorrectAttitudePID(&pidCtrl2, t.pos[0], t.pos[1], zPosition2, correctedYaw2,
					xPositionDesired2, yPositionDesired2, takeOffSetpoint,
					yawDesired2, &rollControlOutput2, &pitchControlOutput2,
					&yawControlOutput2, &thrustControlOutput2);
		}
		else{*/
			controllerCorrectAttitudePID(&pidCtrl2, t.pos[0], t.pos[1], -t.pos[2], correctedYaw2,
					xPositionDesired2, yPositionDesired2, zPositionDesired2,
					yawDesired2, &rollControlOutput2, &pitchControlOutput2,
					&yawControlOutput2, &thrustControlOutput2);
//		}
	}

	 else if (cflieCopter2->m_enumFlightMode == HAND_MODE) {		//correctedYaw2
#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError2 = cos(correctedYaw2 / 57.2958)*((xPositionHand + 0.5) - xPosition2) - sin(correctedYaw2 / 57.2958)*((yPositionHand + 0.5) - yPosition2);	
	yError2 = sin(correctedYaw2 / 57.2958)*((xPositionHand + 0.5) - xPosition2) + cos(correctedYaw2 / 57.2958)*((yPositionHand + 0.5) - yPosition2);
#else
	xError2 = (xPositionHand + 0.5) - xPosition2;
	yError2 = (yPositionHand + 0.5) - yPosition2;
#endif

	controllerSetXYError(&pidCtrl2, xError2, yError2);

	controllerCorrectAttitudePID(&pidCtrl2, t.pos[0], t.pos[1], zPosition2, correctedYaw2,
			xPositionDesired2, yPositionDesired2, zPositionDesired2,
			yawDesired2, &rollControlOutput2, &pitchControlOutput2,
			&yawControlOutput2, &thrustControlOutput2);

	}


	else if(cflieCopter2->m_enumFlightMode == GROUNDED_MODE){
		controllerResetAllPID(&pidCtrl2);
		rollControlOutput2 = 0;
		pitchControlOutput2 = 0;
		yawControlOutput2 = 0;
		thrustControlOutput2 = 0;
	}

	else {
		controllerResetAllPID(&pidCtrl2);
		rollControlOutput2 = 0;
		pitchControlOutput2 = 0;
		yawControlOutput2 = 0;
		thrustControlOutput2 = 0;
	}
#endif

#if !USE_KEYBOARD
	controllerCorrectAttitudePID(&pidCtrl2, t.pos[0], t.pos[1], -t.pos[2], correctedYaw2,
											xPositionDesired2, yPositionDesired2, zPositionDesired2, yawDesired2,
												&rollControlOutput2, &pitchControlOutput2, &yawControlOutput2,
													&thrustControlOutput2);
#endif
	RunCrazyflie(crRadio1, cflieCopter2,
							rollControlOutput2, pitchControlOutput2, yawControlOutput2, thrustControlOutput2);

#if USE_LOGGING	
#if USE_BASIC_LOGGING 
	fprintf(out2, "%.6f\t\t", cflieCopter2->currentTime() - initTime2 );
	fprintf(out2, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d",
				cflieCopter2->pitch(), 
				cflieCopter2->roll(), 
				quadYaw2, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg2,
				correctedYaw2,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime2,
				loopTime2Delta,
				vrpnPacketDelta2,
				vrpnPacketTime2,
				vrpnPacketBackup2);	//
	fprintf(out2, "\n");
#else						
	fprintf(out2, "%.6f\t\t", cflieCopter2->currentTime() - initTime2 );
	fprintf(out2, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%lu\t\t%lu\t\t%lu\t\t%lu\t\t%.3f\t\t%.3f\t\t%.3f",
				cflieCopter2->pitch(), 
				cflieCopter2->roll(), 
				quadYaw2, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg2,
				correctedYaw2,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime2,
				loopTime2Delta,
				vrpnPacketDelta2,
				vrpnPacketTime2,
				vrpnPacketBackup2,
				xPositionDesired2,
				yPositionDesired2,
				zPositionDesired2,
				yawDesired2,
				pitchControlOutput2,
				rollControlOutput2,
				yawControlOutput2,
				thrustControlOutput2,
				cflieCopter2->radioRSSI(),
				euler[0],
				euler[1],
				euler[2],
				cflieCopter2->motor1(),
				cflieCopter2->motor2(),
				cflieCopter2->motor3(),
				cflieCopter2->motor4(),
				cflieCopter2->gyroX(),
				cflieCopter2->gyroY(),
				cflieCopter2->gyroZ());	//
	fprintf(out2, "\n");
#endif
#endif

	loopTime2End = cflieCopter2->currentTime() - initTime2;
	loopTime2 = loopTime2End - loopTime2Start;
//	loopTimeTotal = loopTime1Start - loopTime2End;	

}	//End Packet Backup If check	
else if(vrpnPacketBackup2 < 0){	//Failsafe so can't go below 0 (should never happen) |   || (vrpnPacketBackup2 > 20)?
	vrpnPacketBackup2 = 0;
//	RunCrazyflie(crRadio1, cflieCopter2,
//							rollControlOutput2, pitchControlOutput2, yawControlOutput2, thrustControlOutput2);
}
else if(vrpnPacketBackup2 >= 2){
//	cout << "VRPN Packet Backup 2 Detected: " << vrpnPacketBackup2 << endl;
	vrpnPacketBackup2 --;
//	RunCrazyflie(crRadio1, cflieCopter2,
//							rollControlOutput2, pitchControlOutput2, yawControlOutput2, thrustControlOutput2);
}//End Packet Backup Purge

}	//END CALLBACK 2
#endif
#if NUM_QUADS >= 3
void VRPN_CALLBACK handle_Crazyflie3(void*, const vrpn_TRACKERCB t) {

if(vrpnPacketBackup3 < 2)	//If VRPN Buffer does NOT have old packets stored up, run main code
{
	loopTime3Start = cflieCopter3->currentTime() - initTime3;
	loopTime3Delta = loopTime3Start - loopTime3Prev;		//Calculates time difference between each loop start
	loopTime3Prev = loopTime3Start;

	vrpnPacketTime3 = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnPacketDelta3 = vrpnPacketTime3 - vrpnPacketTime3Prev;
	vrpnPacketTime3Prev = vrpnPacketTime3;

	vrpnPacketBackup3 = ceil(loopTime3Delta/vrpnPacketDelta3);		//Calculates estimated # of packets built up in VRPN buffer.

	controllerSetAllDt(&pidCtrl3, vrpnPacketDelta3);	//Variable dt for controller based on time between packets | vrpnPacketDelta2

//   cflieCopter1->cheat_process_packets();		//Flush out other Copter Packets
//   cflieCopter4->cheat_process_packets();		//Flush out other Copter Packets
   crRadio2->clearPackets();
	crRadio2->setChannel( cflieCopter3->radioChannel() );

	// Get the euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);

	xPosition3 = t.pos[0];
	yPosition3 = t.pos[1];
	zPosition3 = -t.pos[2] + 0.05;
#if USE_PRINTOUT
	cout << "PID Radio 3" << endl << endl;
#endif
#if 0
	//cout << "\033[2J\033[1;1H";  //Clears Terminal Window to display clean output

	/*
	 * Position
	 * x      t.pos[0]
	 * y      t.pos[1]
	 * z      t.pos[2]
	 *
	 * Orientation
	 * yaw    euler[0]  Rotation
	 * pitch  euler[1]  East-West movement
	 * roll   euler[2]  North-South movement
	 * Euler's range goes from -pi : +pi
	 */
	QUAD quad;
	quad.vrpnNow = 0;
	vrpn_data_t *vrpnData;

	vrpnData = (vrpn_data_t*) malloc(sizeof(vrpn_data_t));
	vrpnData->usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnData->x = t.pos[0];
	vrpnData->y = t.pos[1];
	vrpnData->z = -t.pos[2] + 0.05; //Higher Altitude = Negative z  //Added Offset for floor below camera system origin
	vrpnData->yaw = euler[0];
	vrpnData->pitch = euler[1];
	vrpnData->roll = euler[2];

	xPosition3 = t.pos[0];
	yPosition3 = t.pos[1];
	zPosition3 = -t.pos[2] + 0.05;

	cout << "xPosition of Crazyflie3: " << xPosition3 << endl;

	quad.vrpnPrev = quad.vrpnNow;
	if (quad.vrpnNow == 0) {
		quad.vrpnPrev = 0;
		quad.vrpnTime0 = vrpnData->usec;
		quad.vrpnNow = vrpnData->usec;
	} else {
		quad.vrpnNow = vrpnData->usec - quad.vrpnTime0;
	}
#endif
//==============YAW CORRECTION========================= (IMPORTANT!!!: YAW CONTROLLER INPUT IS INVERTED IN simple.cpp FILE***)
	camYawDeg3 = -(euler[0] * 57.2958); //Converts From Radians TO Degrees
	quadYaw3 = cflieCopter3->yaw();

//	if((prevQuadYaw3 - quadYaw3 < -50) || (prevQuadYaw3-quadYaw3 > 50))		//TODO HACK TO ELIMINATE PACKET CROSSING****
//	{
		//cout << "Yaw3 Prev: " << prevQuadYaw3 << endl;
		//cout << "Yaw3 Current: " << quadYaw3 << endl;
//      printf( "Yaw3 prev %8.2f  curr %8.2f\n", prevQuadYaw3, quadYaw3 );
//		quadYaw3 = prevQuadYaw3;
//	}

	if (frameCount == 50) {
		if (camYawDeg3 < 0.0)
			camYawDeg3Off = camYawDeg3 + 360.0;					//Converts from +-180 to full 360 degree form
		else
			camYawDeg3Off = camYawDeg3;

		

		if(quadYaw3 < 0.0)
			quadYaw3Off = quadYaw3 + 360.0;
		else
			quadYaw3Off = quadYaw3;

		yawDifference3 = quadYaw3Off - camYawDeg3Off;
		cout << "Yaw3 Offset Taken: " << yawDifference3 << endl;
	}

	correctedYaw3 = camYawDeg3 + yawDifference3;

	if(correctedYaw3 > 180.0)
		correctedYaw3 -= 360.0;
	else if(correctedYaw3 < -180.0)
		correctedYaw3 += 360.0;

//	yawDesired3 = yawDesired3 + yawDifference3; //Fixes difference in camera and quad yaw measurements

//	if (yawDesired3 > 180.0)
//		yawDesired3 -= 360.0;
//	else if (yawDesired3 < -180.0)
//		yawDesired3 += 360.0;

//	prevQuadYaw3 = quadYaw3;				//TODO HACK TO ELIMINATE PACKET CROSSING****

//===============END YAW CORRECTION=========================

#if USE_PRINTOUT
		cout << "X: " << t.pos[0] << endl;
		cout << "Y: " << t.pos[1] << endl;
		cout << "Z: " << -t.pos[2] + 0.05 << endl;
#endif

//	free(vrpnData);

	//****INSERT RUNTIME CODE HERE****

#if USE_KEYBOARD
	if (cflieCopter3->m_enumFlightMode == LANDING_MODE) //Landing Mode (TODO Change to be a keystroke when implemented)
	{
#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError3 = cos(correctedYaw3 / 57.2958)*(xPositionDesired3 - xPosition3) - sin(correctedYaw3 / 57.2958)*(yPositionDesired3 - yPosition3);	
	yError3 = sin(correctedYaw3 / 57.2958)*(xPositionDesired3 - xPosition3) + cos(correctedYaw3 / 57.2958)*(yPositionDesired3 - yPosition3);
#else
	xError3 = xPositionDesired3 - xPosition3;
	yError3 = yPositionDesired3 - yPosition3;
#endif

	controllerSetXYError(&pidCtrl3, xError3, yError3);

		controllerCorrectAttitudePID(&pidCtrl3, t.pos[0], t.pos[1], -t.pos[2], correctedYaw3,
				xPositionDesired3, yPositionDesired3, -0.4, yawDesired3,
				&rollControlOutput3, &pitchControlOutput3, &yawControlOutput3,
				&thrustControlOutput3);
		takeOff3 = 0;
	} 
	else if (cflieCopter3->m_enumFlightMode == MIRROR_MODE) {									//***REMOVED (NEED SEPARATE ENABLE VARIABLE)****
		xPositionDesired3 = (xPosition1 - 0.5);
		yPositionDesired3 = (yPosition1);
//		zPositionDesired3 = (zPosition1);

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError3 = cos(correctedYaw3 / 57.2958)*(xPositionDesired3 - xPosition3) - sin(correctedYaw3 / 57.2958)*(yPositionDesired3 - yPosition3);	
	yError3 = sin(correctedYaw3 / 57.2958)*(xPositionDesired3 - xPosition3) + cos(correctedYaw3 / 57.2958)*(yPositionDesired3 - yPosition3);
#else
	xError3 = xPositionDesired3 - xPosition3;
	yError3 = yPositionDesired3 - yPosition3;
#endif

	controllerSetXYError(&pidCtrl3, xError3, yError3);

#if USE_PRINTOUT
		cout << "New xPositionDesired: " << xPositionDesired3 << endl;
#endif

		controllerCorrectAttitudePID(&pidCtrl3, t.pos[0], t.pos[1], -t.pos[2], correctedYaw3,
				xPositionDesired3, yPositionDesired3, zPositionDesired3,
				yawDesired3, &rollControlOutput3, &pitchControlOutput3,
				&yawControlOutput3, &thrustControlOutput3);

	} 
	else if (cflieCopter3->m_enumFlightMode == TAKEOFF_MODE) {			//correctedYaw3
//		zPositionDesired3 = 1.0;

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError3 = cos(correctedYaw3 / 57.2958)*(xPositionDesired3 - xPosition3) - sin(correctedYaw3 / 57.2958)*(yPositionDesired3 - yPosition3);	
	yError3 = sin(correctedYaw3 / 57.2958)*(xPositionDesired3 - xPosition3) + cos(correctedYaw3 / 57.2958)*(yPositionDesired3 - yPosition3);
#else
	xError3 = xPositionDesired3 - xPosition3;
	yError3 = yPositionDesired3 - yPosition3;
#endif

	controllerSetXYError(&pidCtrl3, xError3, yError3);

/*		if ((cflieCopter3->currentTime() - takeOffTime3) > 0.02) //To reduce Ground Effect - Boost Thrust for first 0.5 seconds of takeoff
		{
			controllerCorrectAttitudePID(&pidCtrl3, t.pos[0], t.pos[1], -t.pos[2], correctedYaw3,
					xPositionDesired3, yPositionDesired3, takeOffSetpoint,
					yawDesired3, &rollControlOutput3, &pitchControlOutput3,
					&yawControlOutput3, &thrustControlOutput3);
		}
		else{*/
			controllerCorrectAttitudePID(&pidCtrl3, t.pos[0], t.pos[1], -t.pos[2], correctedYaw3,
					xPositionDesired3, yPositionDesired3, zPositionDesired3,
					yawDesired3, &rollControlOutput3, &pitchControlOutput3,
					&yawControlOutput3, &thrustControlOutput3);
//		}
	}

	else if (cflieCopter3->m_enumFlightMode == HAND_MODE) {			//correctedYaw3

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError3 = cos(correctedYaw3 / 57.2958)*((xPositionHand + 0.5) - xPosition3) - sin(correctedYaw3 / 57.2958)*((yPositionHand - 0.5) - yPosition3);	
	yError3 = sin(correctedYaw3 / 57.2958)*((xPositionHand + 0.5) - xPosition3) + cos(correctedYaw3 / 57.2958)*((yPositionHand - 0.5) - yPosition3);
#else
	xError3 = (xPositionHand + 0.5) - xPosition3;
	yError3 = (yPositionHand - 0.5) - yPosition3;
#endif

	controllerSetXYError(&pidCtrl3, xError3, yError3);

	controllerCorrectAttitudePID(&pidCtrl3, t.pos[0], t.pos[1], -t.pos[2], correctedYaw3,
			xPositionDesired3, yPositionDesired3, zPositionDesired3,
			yawDesired3, &rollControlOutput3, &pitchControlOutput3,
			&yawControlOutput3, &thrustControlOutput3);
	}
	else if(cflieCopter3->m_enumFlightMode == GROUNDED_MODE){
		controllerResetAllPID(&pidCtrl3);
		rollControlOutput3 = 0;
		pitchControlOutput3 = 0;
		yawControlOutput3 = 0;
		thrustControlOutput3 = 0;
	}

	else {
		controllerResetAllPID(&pidCtrl3);
		rollControlOutput3 = 0;
		pitchControlOutput3 = 0;
		yawControlOutput3 = 0;
		thrustControlOutput3 = 0;
	}
#endif

#if !USE_KEYBOARD
	controllerCorrectAttitudePID(&pidCtrl3, t.pos[0], t.pos[1], -t.pos[2], correctedYaw3,
											xPositionDesired3, yPositionDesired3, zPositionDesired3, yawDesired3,
												&rollControlOutput3, &pitchControlOutput3, &yawControlOutput3,
													&thrustControlOutput3);
#endif
	RunCrazyflie(crRadio2, cflieCopter3,
							rollControlOutput3, pitchControlOutput3, yawControlOutput3, thrustControlOutput3);

#if USE_LOGGING	
#if USE_BASIC_LOGGING  
		fprintf(out3, "%.6f\t\t", cflieCopter3->currentTime() - initTime3 );
		fprintf(out3, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d",
				cflieCopter3->pitch(), 
				cflieCopter3->roll(), 
				quadYaw3, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg3,
				correctedYaw3,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime3,
				loopTime3Delta,
				vrpnPacketDelta3,
				vrpnPacketTime3,
				vrpnPacketBackup3);//
	fprintf(out3, "\n");

#else					
	fprintf(out3, "%.6f\t\t", cflieCopter3->currentTime() - initTime3 );
	fprintf(out3, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%lu\t\t%lu\t\t%lu\t\t%lu\t\t%.3f\t\t%.3f\t\t%.3f",
				cflieCopter3->pitch(), 
				cflieCopter3->roll(), 
				quadYaw3, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg3,
				correctedYaw3,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime3,
				loopTime3Delta,
				vrpnPacketDelta3,
				vrpnPacketTime3,
				vrpnPacketBackup3,
				xPositionDesired3,
				yPositionDesired3,
				zPositionDesired3,
				yawDesired3,
				pitchControlOutput3,
				rollControlOutput3,
				yawControlOutput3,
				thrustControlOutput3,
				cflieCopter3->radioRSSI(),
				euler[0],
				euler[1],
				euler[2],
				cflieCopter3->motor1(),
				cflieCopter3->motor2(),
				cflieCopter3->motor3(),
				cflieCopter3->motor4(),
				cflieCopter3->gyroX(),
				cflieCopter3->gyroY(),
				cflieCopter3->gyroZ());//
	fprintf(out3, "\n");
#endif
#endif

	loopTime3End = cflieCopter3->currentTime() - initTime3;
	loopTime3 = loopTime3End - loopTime3Start;
//	loopTimeTotal = loopTime1Start - loopTime3End;	

}	//End Packet Backup If check	
else if(vrpnPacketBackup3 < 0){	//Failsafe so can't go below 0 (should never happen) |   || (vrpnPacketBackup3 > 20)?
	vrpnPacketBackup3 = 0;
//	RunCrazyflie(crRadio1, cflieCopter3,
//							rollControlOutput3, pitchControlOutput3, yawControlOutput3, thrustControlOutput3);
}
else if(vrpnPacketBackup3 >= 2){
//	cout << "VRPN Packet Backup 3 Detected: " << vrpnPacketBackup3 << endl;
	vrpnPacketBackup3 --;
//	RunCrazyflie(crRadio1, cflieCopter3,
//							rollControlOutput3, pitchControlOutput3, yawControlOutput3, thrustControlOutput3);
}//End Packet Backup Purge

}	//END CALLBACK 3
#endif
#if NUM_QUADS >= 4
void VRPN_CALLBACK handle_Crazyflie4(void*, const vrpn_TRACKERCB t) {

if(vrpnPacketBackup4 < 2)	//If VRPN Buffer does NOT have old packets stored up, run main code
{
	loopTime4Start = cflieCopter4->currentTime() - initTime4;
	loopTime4Delta = loopTime4Start - loopTime4Prev;		//Calculates time difference between each loop start
	loopTime4Prev = loopTime4Start;

	vrpnPacketTime4 = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnPacketDelta4 = vrpnPacketTime4 - vrpnPacketTime4Prev;
	vrpnPacketTime4Prev = vrpnPacketTime4;

	vrpnPacketBackup4 = ceil(loopTime4Delta/vrpnPacketDelta4);		//Calculates estimated # of packets built up in VRPN buffer.

	controllerSetAllDt(&pidCtrl4, vrpnPacketDelta4);	//Variable dt for controller based on time between packets | vrpnPacketDelta2

//   cflieCopter1->cheat_process_packets();		//Flush out other Copter Packets
   cflieCopter3->cheat_process_packets();		//Flush out other Copter Packets
   crRadio2->clearPackets();
	crRadio2->setChannel( cflieCopter4->radioChannel() );

	// Get the euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);

	xPosition4 = t.pos[0];
	yPosition4 = t.pos[1];
	zPosition4 = -t.pos[2] + 0.05;
#if USE_PRINTOUT
	cout << "PID Radio 4" << endl << endl;
#endif
#if 0
	//cout << "\033[2J\033[1;1H";  //Clears Terminal Window to display clean output

	/*
	 * Position
	 * x      t.pos[0]
	 * y      t.pos[1]
	 * z      t.pos[2]
	 *
	 * Orientation
	 * yaw    euler[0]  Rotation
	 * pitch  euler[1]  East-West movement
	 * roll   euler[2]  North-South movement
	 * Euler's range goes from -pi : +pi
	 */
	QUAD quad;
	quad.vrpnNow = 0;
	vrpn_data_t *vrpnData;

	vrpnData = (vrpn_data_t*) malloc(sizeof(vrpn_data_t));
	vrpnData->usec = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnData->x = t.pos[0];
	vrpnData->y = t.pos[1];
	vrpnData->z = -t.pos[2] + 0.05; //Higher Altitude = Negative z  //Added Offset for floor below camera system origin
	vrpnData->yaw = euler[0];
	vrpnData->pitch = euler[1];
	vrpnData->roll = euler[2];

	xPosition4 = t.pos[0];
	yPosition4 = t.pos[1];
	zPosition4 = -t.pos[2] + 0.05;

	cout << "xPosition of Crazyflie4: " << xPosition4 << endl;

	quad.vrpnPrev = quad.vrpnNow;
	if (quad.vrpnNow == 0) {
		quad.vrpnPrev = 0;
		quad.vrpnTime0 = vrpnData->usec;
		quad.vrpnNow = vrpnData->usec;
	} else {
		quad.vrpnNow = vrpnData->usec - quad.vrpnTime0;
	}
#endif
//==============YAW CORRECTION========================= (IMPORTANT!!!: YAW CONTROLLER INPUT IS INVERTED IN simple.cpp FILE***)
	camYawDeg4 = -(euler[0] * 57.2958); //Converts From Radians TO Degrees
	quadYaw4 = cflieCopter4->yaw();

//	if((prevQuadYaw4 - quadYaw4 < -50) || (prevQuadYaw4-quadYaw4 > 50))		//TODO HACK TO ELIMINATE PACKET CROSSING****
//	{
		//cout << "Yaw4 Prev: " << prevQuadYaw4 << endl;
		//cout << "Yaw4 Current: " << quadYaw4 << endl;
//      printf( "Yaw4 prev %8.2f  curr %8.2f\n", prevQuadYaw4, quadYaw4 );
//		quadYaw4 = prevQuadYaw4;
//	}

	if (frameCount == 50) {
		if (camYawDeg4 < 0.0)
			camYawDeg4Off = camYawDeg4 + 360.0;					//Converts from +-180 to full 360 degree form
		else
			camYawDeg4Off = camYawDeg4;

		

		if(quadYaw4 < 0.0)
			quadYaw4Off = quadYaw4 + 360.0;
		else
			quadYaw4Off = quadYaw4;

		yawDifference4 = quadYaw4Off - camYawDeg4Off;
		cout << "Yaw4 Offset Taken: " << yawDifference4 << endl;
	}

	correctedYaw4 = camYawDeg4 + yawDifference4;

	if(correctedYaw4 > 180.0)
		correctedYaw4 -= 360.0;
	else if(correctedYaw4 < -180.0)
		correctedYaw4 += 360.0;

//	yawDesired4 = yawDesired4 + yawDifference4; //Fixes difference in camera and quad yaw measurements

//	if (yawDesired4 > 180.0)
//		yawDesired4 -= 360.0;
//	else if (yawDesired4 < -180.0)
//		yawDesired4 += 360.0;

//	prevQuadYaw4 = quadYaw4;				//TODO HACK TO ELIMINATE PACKET CROSSING****

//===============END YAW CORRECTION=========================

#if USE_PRINTOUT
		cout << "X: " << t.pos[0] << endl;
		cout << "Y: " << t.pos[1] << endl;
		cout << "Z: " << -t.pos[2] + 0.05 << endl;
#endif

//	free(vrpnData);

	//****INSERT RUNTIME CODE HERE****

#if USE_KEYBOARD
	if (cflieCopter4->m_enumFlightMode == LANDING_MODE) //Landing Mode (TODO Change to be a keystroke when implemented)
	{
#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError4 = cos(correctedYaw4 / 57.2958)*(xPositionDesired4 - xPosition4) - sin(correctedYaw4 / 57.2958)*(yPositionDesired4 - yPosition4);	
	yError4 = sin(correctedYaw4 / 57.2958)*(xPositionDesired4 - xPosition4) + cos(correctedYaw4 / 57.2958)*(yPositionDesired4 - yPosition4);
#else
	xError4 = xPositionDesired4 - xPosition4;
	yError4 = yPositionDesired4 - yPosition4;
#endif

	controllerSetXYError(&pidCtrl4, xError4, yError4);

		controllerCorrectAttitudePID(&pidCtrl4, t.pos[0], t.pos[1], -t.pos[2], correctedYaw4,
				xPositionDesired4, yPositionDesired4, -0.4, yawDesired4,
				&rollControlOutput4, &pitchControlOutput4, &yawControlOutput4,
				&thrustControlOutput4);
		takeOff4 = 0;
	} 
	else if (cflieCopter4->m_enumFlightMode == MIRROR_MODE) {									//***REMOVED (NEED SEPARATE ENABLE VARIABLE)****
		xPositionDesired4 = (xPosition1);
		yPositionDesired4 = (yPosition1 + 0.5);
//		zPositionDesired4 = (zPosition1);

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError4 = cos(correctedYaw4 / 57.2958)*(xPositionDesired4 - xPosition4) - sin(correctedYaw4 / 57.2958)*(yPositionDesired4 - yPosition4);	
	yError4 = sin(correctedYaw4 / 57.2958)*(xPositionDesired4 - xPosition4) + cos(correctedYaw4 / 57.2958)*(yPositionDesired4 - yPosition4);
#else
	xError4 = xPositionDesired4 - xPosition4;
	yError4 = yPositionDesired4 - yPosition4;
#endif

	controllerSetXYError(&pidCtrl4, xError4, yError4);

#if USE_PRINTOUT
		cout << "New xPositionDesired: " << xPositionDesired4 << endl;
#endif

		controllerCorrectAttitudePID(&pidCtrl4, t.pos[0], t.pos[1], -t.pos[2], correctedYaw4,
				xPositionDesired4, yPositionDesired4, zPositionDesired4,
				yawDesired4, &rollControlOutput4, &pitchControlOutput4,
				&yawControlOutput4, &thrustControlOutput4);

	} 
	else if (cflieCopter4->m_enumFlightMode == TAKEOFF_MODE) {			//correctedYaw4
//		zPositionDesired4 = 1.0;

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError4 = cos(correctedYaw4 / 57.2958)*(xPositionDesired4 - xPosition4) - sin(correctedYaw4 / 57.2958)*(yPositionDesired4 - yPosition4);	
	yError4 = sin(correctedYaw4 / 57.2958)*(xPositionDesired4 - xPosition4) + cos(correctedYaw4 / 57.2958)*(yPositionDesired4 - yPosition4);
#else
	xError4 = xPositionDesired4 - xPosition4;
	yError4 = yPositionDesired4 - yPosition4;
#endif

	controllerSetXYError(&pidCtrl4, xError4, yError4);

/*		if ((cflieCopter4->currentTime() - takeOffTime4) > 0.02) //To reduce Ground Effect - Boost Thrust for first 0.5 seconds of takeoff
		{
		controllerCorrectAttitudePID(&pidCtrl4, t.pos[0], t.pos[1], -t.pos[2], correctedYaw4,
				xPositionDesired4, yPositionDesired4, takeOffSetpoint,
				yawDesired4, &rollControlOutput4, &pitchControlOutput4,
				&yawControlOutput4, &thrustControlOutput4);
		}
		else{*/
		controllerCorrectAttitudePID(&pidCtrl4, t.pos[0], t.pos[1], -t.pos[2], correctedYaw4,
				xPositionDesired4, yPositionDesired4, zPositionDesired4,
				yawDesired4, &rollControlOutput4, &pitchControlOutput4,
				&yawControlOutput4, &thrustControlOutput4);
//		}
	}

	else if (cflieCopter4->m_enumFlightMode == HAND_MODE) {			//correctedYaw4

#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	xError4 = cos(correctedYaw4 / 57.2958)*((xPositionHand - 0.5) - xPosition4) - sin(correctedYaw4 / 57.2958)*((yPositionHand - 0.5) - yPosition4);	
	yError4 = sin(correctedYaw4 / 57.2958)*((xPositionHand - 0.5) - xPosition4) + cos(correctedYaw4 / 57.2958)*((yPositionHand - 0.5) - yPosition4);
#else
	xError4 = (xPositionHand - 0.5) - xPosition4;
	yError4 = (yPositionHand - 0.5) - yPosition4;
#endif

	controllerSetXYError(&pidCtrl4, xError4, yError4);

	controllerCorrectAttitudePID(&pidCtrl4, t.pos[0], t.pos[1], -t.pos[2], correctedYaw4,
			xPositionDesired4, yPositionDesired4, zPositionDesired4,
			yawDesired4, &rollControlOutput4, &pitchControlOutput4,
			&yawControlOutput4, &thrustControlOutput4);
	}


	else if(cflieCopter4->m_enumFlightMode == GROUNDED_MODE){
		controllerResetAllPID(&pidCtrl4);
		rollControlOutput4 = 0;
		pitchControlOutput4 = 0;
		yawControlOutput4 = 0;
		thrustControlOutput4 = 0;
	}

	else {
		controllerResetAllPID(&pidCtrl4);
		rollControlOutput4 = 0;
		pitchControlOutput4 = 0;
		yawControlOutput4 = 0;
		thrustControlOutput4 = 0;
	}
#endif

#if !USE_KEYBOARD
	controllerCorrectAttitudePID(&pidCtrl4, t.pos[0], t.pos[1], -t.pos[2], correctedYaw4,
											xPositionDesired4, yPositionDesired4, zPositionDesired4, yawDesired4,
												&rollControlOutput4, &pitchControlOutput4, &yawControlOutput4,
													&thrustControlOutput4);
#endif
	RunCrazyflie(crRadio2, cflieCopter4,
							rollControlOutput4, pitchControlOutput4, yawControlOutput4, thrustControlOutput4);

#if USE_LOGGING		
#if USE_BASIC_LOGGING  
	fprintf(out4, "%.6f\t\t", cflieCopter4->currentTime() - initTime4 );
	fprintf(out4, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d",
				cflieCopter4->pitch(), 
				cflieCopter4->roll(), 
				quadYaw4, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg4,
				correctedYaw4,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime4,
				loopTime4Delta,
				vrpnPacketDelta4,
				vrpnPacketTime4,
				vrpnPacketBackup4);//
	fprintf(out4, "\n");
#else					
	fprintf(out4, "%.6f\t\t", cflieCopter4->currentTime() - initTime4 );
	fprintf(out4, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%lu\t\t%lu\t\t%lu\t\t%lu\t\t%.3f\t\t%.3f\t\t%.3f",
				cflieCopter4->pitch(), 
				cflieCopter4->roll(), 
				quadYaw4, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg4,
				correctedYaw4,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime4,
				loopTime4Delta,
				vrpnPacketDelta4,
				vrpnPacketTime4,
				vrpnPacketBackup4,
				xPositionDesired4,
				yPositionDesired4,
				zPositionDesired4,
				yawDesired4,
				pitchControlOutput4,
				rollControlOutput4,
				yawControlOutput4,
				thrustControlOutput4,
				cflieCopter4->radioRSSI(),
				euler[0],
				euler[1],
				euler[2],
				cflieCopter4->motor1(),
				cflieCopter4->motor2(),
				cflieCopter4->motor3(),
				cflieCopter4->motor4(),
				cflieCopter4->gyroX(),
				cflieCopter4->gyroY(),
				cflieCopter4->gyroZ());//
	fprintf(out4, "\n");
#endif
#endif

	loopTime4End = cflieCopter4->currentTime() - initTime4;
	loopTime4 = loopTime4End - loopTime4Start;
//	loopTimeTotal = loopTime1Start - loopTime4End;	

}	//End Packet Backup If check	
else if(vrpnPacketBackup4 < 0){	//Failsafe so can't go below 0 (should never happen) |   || (vrpnPacketBackup4 > 20)?
	vrpnPacketBackup4 = 0;
//	RunCrazyflie(crRadio1, cflieCopter4,
//							rollControlOutput4, pitchControlOutput4, yawControlOutput4, thrustControlOutput4);
}
else if(vrpnPacketBackup4 >= 2){
//	cout << "VRPN Packet Backup 4 Detected: " << vrpnPacketBackup4 << endl;
	vrpnPacketBackup4 --;
//	RunCrazyflie(crRadio1, cflieCopter4,
//							rollControlOutput4, pitchControlOutput4, yawControlOutput4, thrustControlOutput4);
}//End Packet Backup Purge

}	//END CALLBACK 4
#endif

#if USE_HAND
void VRPN_CALLBACK handle_hand(void*, const vrpn_TRACKERCB t) {
//vrpnBackupTimeHand = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
//vrpnBackupTimeHandDelta = vrpnBackupTimeHand - vrpnBackupTimeHandPrev;
//vrpnBackupTimeHandPrev = vrpnBackupTimeHand;

if(vrpnPacketBackupHand < 2)	//If VRPN Buffer does NOT have old packets stored up, run main code
{
	loopTimeHandStart = cflieCopter1->currentTime() - initTime1;	
	loopTimeHandDelta = loopTimeHandStart - loopTimeHandPrev;	//Calculates time difference between each loop start
	loopTimeHandPrev = loopTimeHandStart;

	vrpnPacketTimeHand = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
	vrpnPacketDeltaHand= vrpnPacketTimeHand - vrpnPacketTimeHandPrev;			//Calculates time between VRPN Data Packets (almost always 0.01 sec)
	vrpnPacketTimeHandPrev = vrpnPacketTimeHand;

	vrpnPacketBackupHand = ceil(loopTimeHandDelta/vrpnPacketDeltaHand);		//Calculates estimated # of packets built up in VRPN buffer.

	// Get the euler angles
	q_vec_type euler;
	q_to_euler(euler, t.quat);

	xPositionHand = t.pos[0];
	yPositionHand = t.pos[1];
	zPositionHand = -t.pos[2] + 0.05;

	yawHand = euler[0] * 57.2958;
	rollHand = euler[1] * 57.2958;
	pitchHand = euler[2] * 57.2958;

	loopTimeHandEnd = cflieCopter1->currentTime() - initTime1;
	loopTimeHand = loopTimeHandEnd - loopTimeHandStart;

#if USE_LOGGING
	fprintf(outHand, "%.6f\t\t", cflieCopter1->currentTime() - initTime1 );
	fprintf(outHand, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f",
				euler[1], 
				euler[2], 
				euler[0], 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				loopTimeHand,
				loopTimeHandDelta);	//
	fprintf(outHand, "\n");
#endif

//	cout << "Hand Height: " << zPositionHand << endl;
	}	//End Packet Backup If check	
	else if(vrpnPacketBackupHand < 0){	//Failsafe so can't go below 0 (should never happen) |   || (vrpnPacketBackup4 > 20)?
		vrpnPacketBackupHand = 0;
	}
	else if(vrpnPacketBackupHand >= 2){
	//	cout << "VRPN Packet Backup Hand Detected: " << vrpnPacketBackupHand << endl;
		vrpnPacketBackupHand --;
}//End Packet Backup Purge
}	//END CALLBACK HAND
#endif
#if 0
void runTCPClient() {

	// IP of the server computer
	string servIP = "192.168.0.196";
	int portNumClient = 60555;
	int buffLenClient = 8;

	TCPClient client(servIP, portNumClient, buffLenClient);

	//    while (1)
	//    {
	client.createSocket();
	client.connectToServer();

	float* rec = (float*) calloc(buffLenClient, sizeof(float));
	client.getMessage(rec);

	cout << "Time (ms): " << (int) rec[0] * 1000 + (int) rec[1] / 1000;
	cout << std::setprecision(3) << fixed << setw(4);
	cout << "\tAngle: " << rec[5] << ", " << rec[6] << ", " << rec[7];
	cout << "\tPos: " << rec[2] << ", " << rec[3] << ", " << rec[4];
	cout << endl;

	free(rec);
	client.closeConnection();
	//    }

	client.closeConnection();

}
#endif

static void ctrlc_handler(int sig) {
	cflieCopter1->setThrust(10000);
	cflieCopter2->setThrust(10000);
	cflieCopter3->setThrust(10000);
	cflieCopter4->setThrust(10000);
	cout << "Battery1: " << cflieCopter1->batteryLevel() << endl;
	cout << "Battery2: " << cflieCopter2->batteryLevel() << endl;
	cout << "Battery3: " << cflieCopter3->batteryLevel() << endl;
	cout << "Battery4: " << cflieCopter4->batteryLevel() << endl;
	cflieCopter1->cycle();
	cflieCopter2->cycle();
	cflieCopter3->cycle();
	cflieCopter4->cycle();

#if USE_LOGGING
	#if USE_HAND
	fclose(outHand);
	#endif
	#if NUM_QUADS >= 1
	fclose(out1);
	#endif
	#if NUM_QUADS >= 2
	fclose(out2);
	#endif
	#if NUM_QUADS >= 3
	fclose(out3);
	#endif
	#if NUM_QUADS >= 4
	fclose(out4);
	#endif
#endif

	// All done.
#if NUM_QUADS >= 1
	delete cflieCopter1;
#endif
#if NUM_QUADS >= 2
	delete cflieCopter2;
#endif
#if NUM_QUADS >= 3
	delete cflieCopter3;
#endif
#if NUM_QUADS >= 4
	delete cflieCopter4;
#endif
	delete crRadio1;
	delete crRadio2;

//	TCPServer::getInstance()->disconnect();
//	TCPServer::getInstance()->~TCPServer();
//	TCPClient::getInstance()->closeConnection();

	exit(1);
}

