
#include "vrpn.h"
#include <unistd.h>
#include "simple.h"

#include <iostream>
using namespace std;

vrpn_Connection *connection;
vrpn_Tracker_Remote *trackerHand;
vrpn_Tracker_Remote *tracker;
vrpn_Tracker_Remote *tracker2;
vrpn_Tracker_Remote *tracker3;
vrpn_Tracker_Remote *tracker4;

char keystroke_now = 0;
char keystroke_prev = 0;
int keyPressed = 0;
int killCount = 0;
int trackCount = 0;

extern float xPositionDesired1;
extern float yPositionDesired1;
extern float zPositionDesired1;
extern float yawDesired1;

extern float xPosition1;
extern float yPosition1;
extern float zPosition1;

extern float xPosition2;
extern float yPosition2;
extern float zPosition2;

extern float xPosition3;
extern float yPosition3;
extern float zPosition3;

extern float xPosition4;
extern float yPosition4;
extern float zPosition4;

extern float xPositionHand;
extern float yPositionHand;
extern float zPositionHand;
extern float yawHand;
extern float pitchHand;
extern float rollHand;

extern CCrazyflie *cflieCopter1;
extern CCrazyflie *cflieCopter2;
extern CCrazyflie *cflieCopter3;
extern CCrazyflie *cflieCopter4;

extern double initTime1;
extern double initTime2;
extern double initTime3;
extern double initTime4;

extern double loopTimeTotalStart;
extern double loopTimeTotalEnd;
extern double loopTimeTotal;		//Sum of Callback Time Totals
extern double loopTimeTotalPrev;
extern double loopTimeTotalDelta;

extern char landingMode;
extern char mirrorMode;
extern char takeOff1;
extern char takeOff2;
extern char takeOff3;
extern char takeOff4;

extern char handMode;
extern char trackHand;
extern char tcp_server_ON;
extern char tcp_client_ON;
extern char flap;

extern double takeOffTime1;
extern double takeOffTime2;
extern double takeOffTime3;
extern double takeOffTime4;

#define USE_HAND 0

void vrpn_init(std::string connectionName, void (*callbackHand)(void*, const vrpn_TRACKERCB), void (*callback)(void*, const vrpn_TRACKERCB), void (*callback2)(void*, const vrpn_TRACKERCB), void (*callback3)(void*, const vrpn_TRACKERCB), void (*callback4)(void*, const vrpn_TRACKERCB), char *key) {
	connection = vrpn_get_connection_by_name(connectionName.c_str());
#if USE_HAND
	trackerHand = new vrpn_Tracker_Remote("hand", connection);
#endif

	tracker = new vrpn_Tracker_Remote("Crazyflie21", connection);
	
	tracker2 = new vrpn_Tracker_Remote("Crazyflie2", connection);

	tracker3 = new vrpn_Tracker_Remote("Crazyflie22", connection);

	tracker4 = new vrpn_Tracker_Remote("Crazyflie23", connection);
#if USE_HAND
	trackerHand->register_change_handler(0, callbackHand);
#endif
	tracker->register_change_handler(0, callback);
	tracker2->register_change_handler(0, callback2);
	tracker3->register_change_handler(0, callback3);
	tracker4->register_change_handler(0, callback4);
//	keyVal = key;
	usleep(2000);
}

void vrpn_init2(std::string connectionName, void (*callbackHand)(void*, const vrpn_TRACKERCB), void (*callback)(void*, const vrpn_TRACKERCB), void (*callback2)(void*, const vrpn_TRACKERCB)) {
	connection = vrpn_get_connection_by_name(connectionName.c_str());
#if USE_HAND
	trackerHand = new vrpn_Tracker_Remote("hand", connection);
#endif

	tracker = new vrpn_Tracker_Remote("Crazyflie21", connection);
	
	tracker2 = new vrpn_Tracker_Remote("Crazyflie2", connection);

#if USE_HAND
	trackerHand->register_change_handler(0, callbackHand);
#endif
	tracker->register_change_handler(0, callback);
	tracker2->register_change_handler(0, callback2);

//	keyVal = key;
	usleep(2000);
}

void vrpn_go() {
	int i = 0;  //for ending the while loop

	while(!i){  //1 replace 'i<20000' when done testing ******
		readKeyboard();
		nearGround();	
#if USE_HAND
		updateHand();
#endif

		loopTimeTotalStart = cflieCopter1->currentTime() - initTime1;
		loopTimeTotalDelta = loopTimeTotalStart - loopTimeTotalPrev;
		loopTimeTotalPrev = loopTimeTotalStart;

		connection->mainloop();
#if USE_HAND
		trackerHand->mainloop();
#endif
		tracker->mainloop();
		tracker2->mainloop();
		tracker3->mainloop();
		tracker4->mainloop();
		usleep(200);  //Was 200

		//Testing Routines

		//i++;

	//nonblock(0);
	nonblock(1);
		usleep(1);
		i = kbhit();		//keyboard hit (state change)

		if (i!=0)
		{
			keystroke_now = fgetc(stdin);
		    if (keystroke_now == 'q')
			i=1;

		else{
			i=0;
		}

		//fprintf(stderr,"%d ",i);
	   }

	}//END WHILE

    if(keystroke_now == 'q'){
	cout << "Program Forced End" << endl;
    }
	loopTimeTotalEnd = cflieCopter1->currentTime() - initTime1;
	loopTimeTotal = loopTimeTotalEnd - loopTimeTotalStart;
}	//END VRPN GO

void vrpn_go2() {
//cout << "vrpn_go2!" << endl;
	int i = 0;  //for ending the while loop

	while(!i){  //1 replace 'i<20000' when done testing ******
//		cout << "starting while loop!" << endl;
		readKeyboard();
//cout << "keyboard" << endl;
		nearGround();	
//cout << "ground" << endl;
#if USE_HAND
		updateHand();
//cout << "hand" << endl;
#endif
//cout << "before timing" << endl;
		loopTimeTotalStart = cflieCopter1->currentTime() - initTime1;
		loopTimeTotalDelta = loopTimeTotalStart - loopTimeTotalPrev;
		loopTimeTotalPrev = loopTimeTotalStart;
//cout << "before connection" << endl;
		connection->mainloop();
//cout << "connection Mainloop" << endl;
#if USE_HAND
		trackerHand->mainloop();
#endif
		tracker->mainloop();
		tracker2->mainloop();

		usleep(200);  //Was 200

		//Testing Routines

		//i++;

	//nonblock(0);
	nonblock(1);
		usleep(1);
		i = kbhit();		//keyboard hit (state change)

		if (i!=0)
		{
			keystroke_now = fgetc(stdin);
		    if (keystroke_now == 'q')
			i=1;

		else{
			i=0;
		}

		//fprintf(stderr,"%d ",i);
	   }

	}//END WHILE
//cout << "ending While loop" << endl;
    if(keystroke_now == 'q'){
	cout << "Program Forced End" << endl;
    }
	loopTimeTotalEnd = cflieCopter1->currentTime() - initTime1;
	loopTimeTotal = loopTimeTotalEnd - loopTimeTotalStart;
}	//END VRPN GO

void readKeyboard()
{
		//========Reads Keystrokes from Keyboard==========
	nonblock(1);
	if (keystroke_now != keystroke_prev) {
		//keystroke = fgetc( stdin );
/*
		if (keystroke_now == 'w') {
			yPositionDesired1 -= 0.2;
			cout << "New Y Setpoint: " << yPositionDesired1 << endl;
		} else if (keystroke_now == 's') {
			yPositionDesired1 += 0.2;
			cout << "New Y Setpoint: " << yPositionDesired1 << endl;
		} else if (keystroke_now == 'a') {
			xPositionDesired1 -= 0.2;
			cout << "New X Setpoint: " << xPositionDesired1 << endl;
		} else if (keystroke_now == 'd') {
			xPositionDesired1 += 0.2;
			cout << "New X Setpoint: " << xPositionDesired1 << endl;
		} else if (keystroke_now == 'r') {
			zPositionDesired1 += 0.2;
			cout << "New Z Setpoint: " << zPositionDesired1 << endl;
		} else if (keystroke_now == 'f') {
			zPositionDesired1 -= 0.2;
			cout << "New Z Setpoint: " << zPositionDesired1 << endl;
		} 
*/
		if (keystroke_now == '1') {
			yawDesired1 += 45.0;
			cout << "\n\r" << "New Yaw Setpoint: " << yawDesired1 << endl;
		} else if (keystroke_now == '2') {
			yawDesired1 -= 45.0;
			cout << "\n\r" << "New Yaw Setpoint: " << yawDesired1 << endl;
		} 
		else if (keystroke_now == 'k') {
				cflieCopter1->m_enumFlightMode = GROUNDED_MODE;
			if(cflieCopter2){
				cflieCopter2->m_enumFlightMode = GROUNDED_MODE;}
			if(cflieCopter3){
				cflieCopter3->m_enumFlightMode = GROUNDED_MODE;}
			if(cflieCopter4){
				cflieCopter4->m_enumFlightMode = GROUNDED_MODE;}

		} else if (keystroke_now == 'e') {
			cflieCopter1->m_enumFlightMode = LANDING_MODE;
		if(cflieCopter2){
			cflieCopter2->m_enumFlightMode = LANDING_MODE;}
		if(cflieCopter3){
			cflieCopter3->m_enumFlightMode = LANDING_MODE;}
		if(cflieCopter4){
			cflieCopter4->m_enumFlightMode = LANDING_MODE;}

			handMode = 0;
//			landingMode ^= 1;
//			mirrorMode = 0;
			cout << "\n\r" <<"Landing Mode Enabled" << endl;
		} else if (keystroke_now == 'm') {
			cflieCopter1->m_enumFlightMode = MIRROR_MODE;
		if(cflieCopter2){
			cflieCopter2->m_enumFlightMode = MIRROR_MODE;}
		if(cflieCopter3){
			cflieCopter3->m_enumFlightMode = MIRROR_MODE;}
		if(cflieCopter4){
			cflieCopter4->m_enumFlightMode = MIRROR_MODE;}
//			mirrorMode ^= 1;
			cout << "\n\r" << "Mirror Mode Enabled" << endl;
		} else if (keystroke_now == 'h') {
			handMode = 1;

			cout << "\n\r" << "Hand Mode Enabled" << endl;
		} else if (keystroke_now == 't') {
			cflieCopter1->m_enumFlightMode = TAKEOFF_MODE;
//			takeOff1 = 1;
			takeOffTime1 = cflieCopter1->currentTime() - initTime1;
			cout << "\n\r" << "Crazyflie 1 Take-Off" << endl;
		}
		else if (keystroke_now == 'y') {
			cflieCopter2->m_enumFlightMode = TAKEOFF_MODE;
//			takeOff2 = 1;
			takeOffTime2 = cflieCopter2->currentTime() - initTime2;
			cout << "\n\r" << "Crazyflie 2 Take-Off" << endl;
		}
		else if (keystroke_now == 'u') {
			cflieCopter3->m_enumFlightMode = TAKEOFF_MODE;
//			takeOff3 = 1;
			takeOffTime3 = cflieCopter3->currentTime() - initTime3;
			cout << "\n\r" << "Crazyflie 3 Take-Off" << endl;
		}
		else if (keystroke_now == 'i') {
			cflieCopter4->m_enumFlightMode = TAKEOFF_MODE;
//			takeOff4 = 1;
			takeOffTime4 = cflieCopter4->currentTime() - initTime4;
			cout << "\n\r" << "Crazyflie 4 Take-Off" << endl;
		} 
//		else if (keystroke_now == '`') {
//			tcp_server_ON ^= 1;
//		} else if (keystroke_now == '1') {
//			tcp_client_ON ^= 1;
//		}
//		else if (keystroke_now == 'o') {
//			flap = 1;
//		}
//		else if (keystroke_now == 'l') {
//			flap = 0;
//		}
		
		keystroke_prev = keystroke_now;

	}

    nonblock(0);
}//END readKeyboard()

void nearGround()			//During Landing Mode checks how close to ground quad is for shutdown
{
	if((cflieCopter1->m_enumFlightMode == LANDING_MODE)  && (zPosition1 < 0.05))
	{
		cflieCopter1->m_enumFlightMode = GROUNDED_MODE;
	}
	if((cflieCopter2->m_enumFlightMode == LANDING_MODE)  && (zPosition2 < 0.05))
	{
		cflieCopter2->m_enumFlightMode = GROUNDED_MODE;
	}
if(cflieCopter3){
	if((cflieCopter3->m_enumFlightMode == LANDING_MODE)  && (zPosition3 < 0.05))
	{
		cflieCopter3->m_enumFlightMode = GROUNDED_MODE;
	}
}
if(cflieCopter4){
	if((cflieCopter4->m_enumFlightMode == LANDING_MODE)  && (zPosition4 < 0.05))
	{
		cflieCopter4->m_enumFlightMode = GROUNDED_MODE;
	}
}
}

void updateHand()
{
if(handMode == 1)
{
//	cout << "Hand Roll: " << rollHand << endl;
	if(killCount > 5){
		cflieCopter1->m_enumFlightMode = GROUNDED_MODE;
		cflieCopter2->m_enumFlightMode = GROUNDED_MODE;
		cflieCopter3->m_enumFlightMode = GROUNDED_MODE;
		cflieCopter4->m_enumFlightMode = GROUNDED_MODE;
		handMode = 0;
		trackHand = 0;
		cout << "Hand Mode: Killing Quads" << endl;
		killCount = 0;
	}
	else if(trackCount > 5){
		trackHand = 1;
			cflieCopter1->m_enumFlightMode = HAND_MODE;
			cflieCopter2->m_enumFlightMode = HAND_MODE;
			cflieCopter3->m_enumFlightMode = HAND_MODE;
			cflieCopter4->m_enumFlightMode = HAND_MODE;
		trackCount = 0;
	}
   else if((rollHand > 70.0) || (rollHand < -70.0))
	{
		killCount++;
	}
	else if( pitchHand < -70.0 )		//|| (pitchHand > 70.0)
	{
		trackCount++;
	}
	else{
		killCount = 0;
		trackCount = 0;
		if(zPositionHand > 1.6)
		{
//			trackHand = 0;
			cflieCopter1->m_enumFlightMode = TAKEOFF_MODE;
			cflieCopter2->m_enumFlightMode = TAKEOFF_MODE;
			cflieCopter3->m_enumFlightMode = TAKEOFF_MODE;
			cflieCopter4->m_enumFlightMode = TAKEOFF_MODE;

			cout << "Hand Mode: Launching Quads" << endl;
		}
	
		else if(zPositionHand < 0.4)
		{
			trackHand = 0;
			cflieCopter1->m_enumFlightMode = LANDING_MODE;
			cflieCopter2->m_enumFlightMode = LANDING_MODE;
			cflieCopter3->m_enumFlightMode = LANDING_MODE;
			cflieCopter4->m_enumFlightMode = LANDING_MODE;

			cout << "Hand Mode: Landing Quads" << endl;
		}

	}
}
}	//END updateHand();

//////////////////////////////////////////////////
// kbhit                                        //
// Descption: used select to do a non blocking  //
// read of stdin.                               //
//                                              //
//////////////////////////////////////////////////
int kbhit()
{
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}



//////////////////////////////////////////////////
// nonblock                                     //
// Descption: Turn off connoical mode so the    //
// user does not need to hit enter after        //
// pressing a key                               //
//                                              //
//////////////////////////////////////////////////
void nonblock(int state)
{
    struct termios ttystate;

    //get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    if (state==1)
    {
        //turn off canonical mode
        ttystate.c_lflag &= ~ICANON;
        //minimum of number input read.
        ttystate.c_cc[VMIN] = 1;
    }
    else if (state==0)
    {
        //turn on canonical mode
        ttystate.c_lflag |= ICANON;
    }
    //set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);

}


