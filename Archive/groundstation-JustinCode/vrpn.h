#ifndef VRPN_H
#define VRPN_H

#include <string>
#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"
#include <stdio.h>
#include <sys/select.h>
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/time.h> // for get time of day

void vrpn_init(std::string connectionName, void (*callbackHand)(void*, const vrpn_TRACKERCB), void (*callback)(void*, const vrpn_TRACKERCB), void (*callback2)(void*, const vrpn_TRACKERCB), void (*callback3)(void*, const vrpn_TRACKERCB), void (*callback4)(void*, const vrpn_TRACKERCB), char *key);

void vrpn_init2(std::string connectionName, void (*callbackHand)(void*, const vrpn_TRACKERCB), void (*callback)(void*, const vrpn_TRACKERCB), void (*callback2)(void*, const vrpn_TRACKERCB));

void vrpn_go();

void vrpn_go2();

void nonblock(int state);
void readKeyboard();
void nearGround();
void updateHand();
int kbhit();

//NEW EDIT 6/26/2015

typedef struct //Defines Data packet Struct
{
	double usec;
	float x;
	float y;
	float z;
	float yaw;
	float pitch;
	float roll;
} vrpn_data_t;

typedef struct
{
	double vrpnNow;
	double vrpnPrev;
	double vrpnTime0;
} QUAD;


#endif

