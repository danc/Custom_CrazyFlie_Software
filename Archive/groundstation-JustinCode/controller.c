#include <stdbool.h>
#include "controller.h"
#include "pid.h"
#include "CCrazyflie.h"


void controllerInit( ControllerObject * ctrl )
{
	if(ctrl->isInit)
		return;
		
	pidInit(&ctrl->pidY, 0, PID_Y_KP, PID_Y_KI, PID_Y_KD, CAMERA_UPDATE_DT);  //for Roll PID
	pidInit(&ctrl->pidX, 0, PID_X_KP, PID_X_KI, PID_X_KD, CAMERA_UPDATE_DT);  //for Pitch PID
	pidInit(&ctrl->pidYaw, 0, PID_YAW_KP, PID_YAW_KI, PID_YAW_KD, CAMERA_UPDATE_DT);	
	pidInit(&ctrl->pidZ, 0, PID_Z_KP, PID_Z_KI, PID_Z_KD, CAMERA_UPDATE_DT);  //*****NEED TO FIND THESE VALUES IN ALTITUDE CONTROLLER****
	
	pidSetIntegralLimit(&ctrl->pidY, PID_Y_INTEGRATION_LIMIT);
	pidSetIntegralLimit(&ctrl->pidX, PID_X_INTEGRATION_LIMIT);
	pidSetIntegralLimit(&ctrl->pidYaw, PID_YAW_INTEGRATION_LIMIT);
	pidSetIntegralLimit(&ctrl->pidZ, PID_Z_INTEGRATION_LIMIT);  //***NEED TO MODIFY PARAMS***
	
	ctrl->isInit = true;
}

bool controllerTest(ControllerObject * ctrl)
{
	return ctrl->isInit;
}

void controllerCorrectAttitudePID(ControllerObject * ctrl,
float xPositionActual, float yPositionActual, float zPositionActual, float eulerYawActual, float xPositionDesired, float yPositionDesired, float zPositionDesired, float eulerYawDesired, float* rollControlOutput, float* pitchControlOutput, float* yawControlOutput, float* thrustControlOutput)
{
	pidSetDesired(&ctrl->pidX, xPositionDesired);
	*pitchControlOutput = pidUpdateAngle(&ctrl->pidX, xPositionActual, false);		//Originally true
	
	pidSetDesired(&ctrl->pidY, yPositionDesired);
	*rollControlOutput = pidUpdateAngle(&ctrl->pidY, yPositionActual, false);		//Originally true
	
	pidSetDesired(&ctrl->pidZ, zPositionDesired);
	*thrustControlOutput = pidUpdateThrust(&ctrl->pidZ,zPositionActual, true);
	
	float yawError;
	pidSetDesired(&ctrl->pidYaw, eulerYawDesired);
	yawError = eulerYawDesired - eulerYawActual;
	if(yawError > 180.0)
		yawError -= 360.0;
	else if(yawError < -180.0)
		yawError += 360.0;
	pidSetError(&ctrl->pidYaw, yawError);
	
	//****NEED TO APPLY YAW FIX FOR CAMERA SYSTEM****
	
	*yawControlOutput = pidUpdateYaw(&ctrl->pidYaw, eulerYawActual, true);  	//originally false
}

/*
void controllerCorrectAttitudePID(float eulerRollActual, float eulerPitchActual, float eulerYawActual, float altitudeActual, float eulerRollDesired, float eulerPitchDesired, float eulerYawDesired, float altitudeDesired)  //***ADDED ALTITUDE TO ARGS***
{
	//***FIGURE THIS OUT***
	
	//Update PID for Roll Axis
	
	//Update PID for Pitch Axis
	
	//Update PID for Yaw Axis
	
	//Update PID for Altitude

}
*/

void controllerResetAllPID(ControllerObject * ctrl)
{
	pidReset(&ctrl->pidY);
	pidReset(&ctrl->pidX);
	pidReset(&ctrl->pidYaw);
	pidReset(&ctrl->pidZ);
}

void controllerSetAllDt(ControllerObject * ctrl, double dt)
{
	pidSetDt(&ctrl->pidY, dt);
	pidSetDt(&ctrl->pidX, dt);
	pidSetDt(&ctrl->pidZ, dt);
	pidSetDt(&ctrl->pidYaw, dt);
}

void controllerSetXYError(ControllerObject * ctrl, const float xError, const float yError)
{
	pidSetError(&ctrl->pidX, xError);
	pidSetError(&ctrl->pidY, yError);
}


//void controllerGetActuatorOutput(cflieCopter* roll, cflieCopter* pitch, cflieCopter* yaw, cflieCopter* ////thrust)  //***NOT SURE IF NECESSARY, ALREADY HAVE STRUCT WITH THIS INFO***
//{
//	*roll = cflieCopter->roll;
//	*pitch = cflieCopter->pitch;
//	*yaw = cflieCopter->yaw;
//	*thrust = cflieCopter->thrust;
//}
	


