/* 

multicast.c

The following program sends or receives multicast packets. If invoked
with one argument, it sends a packet containing the current time to an
arbitrarily chosen multicast group and UDP port. If invoked with no
arguments, it receives and prints these packets. Start it as a sender on
just one host and as a receiver on all the other hosts

*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <stdio.h>
#include "eris_vrpn.h"
#include "simple.h"

#define EXAMPLE_PORT 60551        //Originally 6000
#define EXAMPLE_GROUP "239.0.0.1"

   struct sockaddr_in addr;
   int addrlen, sock, cnt;
   struct ip_mreq mreq;
   char message[50];

void initMulticast()
{


/* ====================set up socket=================== */
   sock = socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) {
     perror("socket");
     exit(1);
   }
   bzero((char *)&addr, sizeof(addr));
   addr.sin_family = AF_INET;
   addr.sin_addr.s_addr = htonl(INADDR_ANY);
   addr.sin_port = htons(EXAMPLE_PORT);
   addrlen = sizeof(addr);
}

void sendMulticast()
{
/*===================== send ======================*/
      addr.sin_addr.s_addr = inet_addr(EXAMPLE_GROUP);
      printf("Send Mode");	//Added Debug
//      while (1) {
	 time_t t = time(0);
	 sprintf(message, "time is %-24.24s", ctime(&t));
	 printf("sending: %s\n", message);
	 cnt = sendto(sock, message, sizeof(message), 0,
		      (struct sockaddr *) &addr, addrlen);
	 if (cnt < 0) {
 	    perror("sendto");
	    exit(1);
	 }
//	 sleep(1);		//TODO Sleeps too long? (originally 5)***
//      }
}

void readMulticast()
{
/*===================== receive ===========================*/
      if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {        //TODO: NEED TO FIX SOCKET ISSUE
         perror("bind");
	 exit(1);
      }    
      mreq.imr_multiaddr.s_addr = inet_addr(EXAMPLE_GROUP);         
      mreq.imr_interface.s_addr = htonl(INADDR_ANY);         
      if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
		     &mreq, sizeof(mreq)) < 0) {
	 perror("setsockopt mreq");
	 exit(1);
      }         
      printf("Receive Mode");	//Added for Debug
      while (1) {
 	 cnt = recvfrom(sock, message, sizeof(message), 0, \
			(struct sockaddr *)&addr, (socklen_t*)&addrlen);				//Originally &addrlen | Should be (struct sockaddr_in *)???
	 if (cnt < 0) {
	    perror("recvfrom");
	    exit(1);
	 } else if (cnt == 0) {
 	    break;
	 }
	 printf("%s: message = \"%s\"\n", inet_ntoa(addr.sin_addr), message);
        }
    }

