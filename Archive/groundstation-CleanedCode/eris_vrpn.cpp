//#include <iostream>
//#include <iomanip>
//#include <string>
//#include <cstdlib>
//#include <signal.h>
//#include <unistd.h>

#include "eris_vrpn.h"
#include "simple.h"
#include "pid.h"
#include "controller.h"
#include "CCrazyflie.h"
#include "vrpn.h"
#include "errno.h"


#include "quadcopterData.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//#include <quat.h>
//#include "vrpn.h"

const std::string erisIP = "192.168.0.196"; //TODO Change to IP of Other Linux Computer***
const int port = 60551;
const int buffLen = 8;

using namespace std;

void initMulticast();
void sendMulticast();
void readMulticast();

void closeoutProgram();
static void ctrlc_handler(int sig) ;

float cfliePitch = 0;
float cflieRoll = 0;
float cflieYaw = 0;
float cflieThrust = 0;
double lastTime = 0;
double curTime = 0;
double initTimeVrpn = 0;

int frameCount = 0;
double takeOffTime1 = 0;
double takeOffTime2 = 0;
double takeOffTime3 = 0;
double takeOffTime4 = 0;
double endTakeOffTime = 0;
float takeOffSetpoint = 0.75;

char landingMode = 0;
char mirrorMode = 0;
char takeOff1 = 0;
char takeOff2 = 0;
char takeOff3 = 0;
char takeOff4 = 0;
char handMode = 0;
char trackHand = 0;
char tcp_server_ON = 0;
char tcp_client_ON = 0;
char flap = 0;

//=========Testing Variables===============
int j = 0; //Testing Index Variable****
int dongleNum = 0;
bool toggle = 0;

double loopTimeTotalStart = 0;
double loopTimeTotalEnd = 0;
double loopTimeTotal = 0;		//Sum of Callback Time Totals
double loopTimeTotalPrev = 0;
double loopTimeTotalDelta = 0;

//========End Testing Variables============


#define PI 3.14159265

int main(int argc, char **argv) {
	sleep(1); //***FOR TESTING PURPOSES*** (REMOVE)

	signal(SIGINT, &ctrlc_handler);
	// init the tcp server that will transmit data to Eris
	//TCPServer serv(erisIP, port, buffLen);
	//serv.createSocket();

	// init vrpn and Attitude Controller and then run
	for (int i = 0; i < NUM_QUADS; i++) {
		controllerResetAllPID( &(crazyflie_info[i].controllerData.controller) );
		controllerInit( &(crazyflie_info[i].controllerData.controller) );
	}

	initMulticast();

	// Initialize the crazyflie radios
	for (int i = 0; i < NUM_RADIOS; i++) {
		cout << "Initializing Radio " << i << endl;
		// Create the radio object
		radios[i].radio = new CCrazyRadio(i);

		// Start the radio
		if (radios[i].radio->startRadio() == 0) {
			// The radio failed to initialize, error out
			std::cerr << "Error: Radio " << i << " did not initialize." << std::endl;
			exit(1);
		}
		radios[i].radio->setARDTime( radios[i].ARDtime );
		radios[i].radio->setARC( radios[i].ARC );
		radios[i].radio->setPower( radios[i].powerLevel );
		radios[i].radio->setDataRate( radios[i].dataRate );
	}

	// Initialize the CrazyFlie quadcopters
	for (int i = 0; i < NUM_QUADS; i++) {
		cout << "Initializing Crazyflie " << i << endl;
		crazyflie_info[i].cflieCopter = new CCrazyflie(radios[crazyflie_info[i].radioNumber].radio,
		 											   crazyflie_info[i].channelNumber);
		crazyflie_info[i].cflieCopter->m_enumFlightMode = GROUNDED_MODE;
		crazyflie_info[i].initTime = crazyflie_info[i].cflieCopter->currentTime();

		// Go through the init sequence
		radios[crazyflie_info[i].radioNumber].radio->clearPackets();
		radios[crazyflie_info[i].radioNumber].radio->setChannel( crazyflie_info[i].channelNumber );

		while( crazyflie_info[i].cflieCopter->getCurrentState() != STATE_NORMAL_OPERATION ) {
			crazyflie_info[i].cflieCopter->cycle();
		}
	}

	// Initialize the VRPN connections
#if USE_HAND
	vrpn_init("192.168.0.120:3883", handle_hand);
#else
	vrpn_init("192.168.0.120:3883", NULL);
#endif // END USE_HAND

	usleep(10000);

#if USE_LOGGING
//===============Initialize the Logging Files=====================
	//const char * cols = "Timestamp, Roll, Pitch, Yaw, X, Y, Z";
#if USE_BASIC_LOGGING
	const char * logHeaderHand = "#Hand Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tLoopTime\t\tLoopTimeDelta\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tseconds\t\tseconds\n";

	const char * logHeader = "\%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\t\tRadioRSSI\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\t\tstrength\n";

#else
	const char * logHeaderHand = "#Hand Log File\n\
%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tLoopTime\t\tLoopTimeDelta\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tseconds\t\tseconds\n";

	const char * logHeader = "\%Time\t\tPitch\t\tRoll\t\tYaw\t\tX\t\tY\t\tZ\t\tCamYaw\t\tCorrectedYaw\t\tLoopTimeTotal\t\tLoopTimeTotalDelta\t\tLoopTime\t\tLoopTimeDelta\t\tvrpnPacketDelta\t\tvrpnPacketTime\t\tvrpnPacketBackup\t\tXSetpoint\t\tYSetpoint\t\tZSetpoint\t\tYawSetpoint\t\tPitchDesired\t\tRollDesired\t\tYawDesired\t\tThrustDesired\t\tRadioRSSI\t\tCamYawRad\t\tCamPitch\t\tCamRoll\t\tMotor1\t\tMotor2\t\tMotor3\t\tMotor4\t\tRollRate\t\tPitchRate\t\tYawRate\n\
&sec\t\tdegrees\t\tdegrees\t\tdegrees\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tseconds\t\tpackets\t\tmeters\t\tmeters\t\tmeters\t\tdegrees\t\tdegrees\t\tdegrees\t\tdegrees\t\tN/A\t\tdB\t\trad\t\trad\t\trad\t\tN/A\t\tN/A\t\tN/A\t\tN/A\t\trad/s\t\trad/s\t\trad/s\n";

#endif // END: USE_BASIC_LOGGING

#if USE_HAND
	hand.logfile = fopen(hand.logfileName, "w");
		
			if(hand.logfile == NULL)
			{
				printf("Could not open %s: errno %d\n", hand.logfileName, errno);
				exit(-1);
			}
		
	fprintf(hand.logfile, "%s\n", logHeaderHand);
#endif 	// END USE_HAND

	// Initialize the logging structure
	for (int i = 0; i < NUM_QUADS; i++) {
		crazyflie_info[i].logfile = fopen(crazyflie_info[i].logfileName, "w");

		// Make sure the log file opened correctly
		if (crazyflie_info[i].logfile == NULL) {
			printf("Could not open %s: errno %d\n", crazyflie_info[i].logfileName, errno);
			exit(-1);
		}

		// Place the header information into the log file
		fprintf(crazyflie_info[i].logfile, "\#Crazyflie%d\n", i+1);
		fprintf(crazyflie_info[i].logfile, "%s\n", logHeader);
	}

//===========End Logfile Init==============		
#endif	// END USE_LOGGING

	cout << "The Init has completed" << endl;
	
	// Run the VRPN main loop
	vrpn_go();

	// End the program
	cout << "Closing program" << endl;
	closeoutProgram();
	return 0;
}


/**
 * This function is registered as the handler for when Ctrl-C is pressed
 */
static void ctrlc_handler(int sig) {
	// Close the program
	closeoutProgram();
	exit(1);
}


/**
 * Function that closes out the program properly and deletes all objects
 */
void closeoutProgram() {
	for (int i = 0; i < NUM_QUADS; i++) {
		// Turn off the crazyflies
		if ( crazyflie_info[i].cflieCopter->copterInRange() ) {
			// Only do this if the Crazyflies are in range and responding
			crazyflie_info[i].cflieCopter->setThrust(10000);
			crazyflie_info[i].cflieCopter->cycle();
			cout << "Battery" << i << ": " << crazyflie_info[i].cflieCopter->batteryLevel() << endl;
			crazyflie_info[i].cflieCopter->cycle();
		} else {
			cout << "Crazyflie " << i << " Out of range" << endl;
		}
		delete crazyflie_info[i].cflieCopter;
	}

	for (int i = 0; i < NUM_RADIOS; i++) {
		delete radios[i].radio;
	}

	// Close out of the log files
#if USE_LOGGING
	#if USE_HAND
	fclose(outHand);
	#endif

	for (int i = 0; i < NUM_QUADS; i++) {
		fclose( crazyflie_info[i].logfile);
	}
#endif
}
