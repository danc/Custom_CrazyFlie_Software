#ifndef ERIS_VRPN
#define ERIS_VRPN

#include <iostream>
#include <iomanip>
#include <string>
#include <cstdlib>
#include <signal.h>
#include <unistd.h>
#include <string>

#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"

#include <quat.h>

//void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);

// The lowest battery level to fly with
#define LOW_BATTERY_LEVEL	3.2

#define USE_VRPN 1
#define USE_KEYBOARD 1
#define USE_PRINTOUT 0
#define USE_LOGGING 0
#define USE_BASIC_LOGGING 0
#define USE_PR_YAW_CORRECT 0	//DO NOT NEED (NOW IMPLEMENTED ON FIRMWARE)
#define USE_HAND 0

void query_vrpn();

#endif
