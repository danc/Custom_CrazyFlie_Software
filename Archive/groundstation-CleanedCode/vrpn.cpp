
#include "vrpn.h"
#include <unistd.h>
#include "simple.h"
#include "eris_vrpn.h"

#include "quadcopterData.h"

#include <iostream>
using namespace std;

vrpn_Connection *connection;
vrpn_Tracker_Remote *trackerHand;

char keystroke_now = 0;
char keystroke_prev = 0;
int keyPressed = 0;
int killCount = 0;
int trackCount = 0;

extern double loopTimeTotalStart;
extern double loopTimeTotalEnd;
extern double loopTimeTotal;		//Sum of Callback Time Totals
extern double loopTimeTotalPrev;
extern double loopTimeTotalDelta;

extern char landingMode;
extern char mirrorMode;
extern char takeOff1;
extern char takeOff2;
extern char takeOff3;
extern char takeOff4;

extern char handMode;
extern char trackHand;
extern char tcp_server_ON;
extern char tcp_client_ON;
extern char flap;

extern double takeOffTime1;
extern double takeOffTime2;
extern double takeOffTime3;
extern double takeOffTime4;


/**
 * Initialize the VRPN subsystem
 *
 * @param connectionName The IP address and port number (in a string) for the VRPN server
 * @param callbackHand Function pointer to the tracker callback function
 */
void vrpn_init(std::string connectionName, void (*callbackHand)(void*, const vrpn_TRACKERCB)) {
	// Get the VRPN connection information
	connection = vrpn_get_connection_by_name(connectionName.c_str());

	// Initialize the hand trackable
#if USE_HAND
	trackerHand = new vrpn_Tracker_Remote("hand", connection);
	trackerHand->register_change_handler(0, callbackHand);
#endif

	// Initialize all the quadcopter trackables
	for (int i = 0; i < NUM_QUADS; i++) {
		crazyflie_info[i].vrpn_tracker = new vrpn_Tracker_Remote(crazyflie_info[i].vrpn_trackableName, connection);
		crazyflie_info[i].vrpn_tracker->register_change_handler(0, crazyflie_info[i].vrpn_callback);
	}

	usleep(2000);
}


/**
 * The main VRPN loop
 */
void vrpn_go() {
	int i = 0;  //for ending the while loop

	while(!i) {  //1 replace 'i<20000' when done testing ******
		for (int j = 0; j < NUM_QUADS; j++) {			
			// Get the radio ready for controlling the crazyflie
			radios[crazyflie_info[j].radioNumber].radio->clearPackets();
			radios[crazyflie_info[j].radioNumber].radio->setChannel( crazyflie_info[j].channelNumber );
			crazyflie_info[j].cflieCopter->cycle();
		}

		readKeyboard();		// Act on keyboard commands
		nearGround();		// See if the quadcopters have landed yet
#if USE_HAND
		updateHand();
#endif // END USE_HAND

		loopTimeTotalStart = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;
		loopTimeTotalDelta = loopTimeTotalStart - loopTimeTotalPrev;
		loopTimeTotalPrev = loopTimeTotalStart;

		connection->mainloop();
#if USE_HAND
		// Go through the mainloop for the hand trackable
		trackerHand->mainloop();
#endif	// END USE_HAND

		// Go through the main loop for the Crazyflie trackables
		for (int j = 0; j < NUM_QUADS; j++) {
			crazyflie_info[j].vrpn_tracker->mainloop();
		}

		usleep(200);  //Was 200

		//Testing Routines

		//i++;

	// Get keybaord input and see if q has been used to stop this thing
	nonblock(1);
		usleep(1);
		i = kbhit();		//keyboard hit (state change)

		if (i!=0) {
			keystroke_now = fgetc(stdin);
		    if (keystroke_now == 'q') {
				i=1;
		    } else {
				i=0;
			}
		}

	}//END WHILE

    if(keystroke_now == 'q'){
		cout << "Program Forced End" << endl;
    }
	loopTimeTotalEnd = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;
	loopTimeTotal = loopTimeTotalEnd - loopTimeTotalStart;
}	//END VRPN GO


/**
 * Read commands from the keyboard
 */
void readKeyboard() {
	nonblock(1);
	if (keystroke_now != keystroke_prev) {
		//keystroke = fgetc( stdin );
/*
		if (keystroke_now == 'w') {
			yPositionDesired1 -= 0.2;
			cout << "New Y Setpoint: " << yPositionDesired1 << endl;
		} else if (keystroke_now == 's') {
			yPositionDesired1 += 0.2;
			cout << "New Y Setpoint: " << yPositionDesired1 << endl;
		} else if (keystroke_now == 'a') {
			xPositionDesired1 -= 0.2;
			cout << "New X Setpoint: " << xPositionDesired1 << endl;
		} else if (keystroke_now == 'd') {
			xPositionDesired1 += 0.2;
			cout << "New X Setpoint: " << xPositionDesired1 << endl;
		} else if (keystroke_now == 'r') {
			zPositionDesired1 += 0.2;
			cout << "New Z Setpoint: " << zPositionDesired1 << endl;
		} else if (keystroke_now == 'f') {
			zPositionDesired1 -= 0.2;
			cout << "New Z Setpoint: " << zPositionDesired1 << endl;
		} 
*/
		if (keystroke_now == '1') {
			// Change the yaw setpoint of quadcopter 1 by +45 degrees
			crazyflie_info[0].controllerData.yawSetpoint += 45.0;
			cout << "\n\r" << "New Yaw Setpoint: " << crazyflie_info[0].controllerData.yawSetpoint << endl;
		} else if (keystroke_now == '2') {
			// Change the yaw setpoint of quadcopter 1 by -45 degrees
			crazyflie_info[0].controllerData.yawSetpoint -= 45.0;
			cout << "\n\r" << "New Yaw Setpoint: " << crazyflie_info[0].controllerData.yawSetpoint << endl;
		} else if (keystroke_now == 'k') {
			// Place the quads into grounded mode
			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = GROUNDED_MODE;
			}
		} else if (keystroke_now == 'e') {
			// Place the quads into landing mode
			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = LANDING_MODE;
				crazyflie_info[i].controllerData.zSetpoint = -0.4;
			}
			handMode = 0;
			cout << "\n\r" <<"Landing Mode Enabled" << endl;
		} else if (keystroke_now == 'm') {
			// Place the quads into mirror mode
			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = MIRROR_MODE;
			}
			cout << "\n\r" << "Mirror Mode Enabled" << endl;
		} else if (keystroke_now == 'h') {
			// Place the quads into hand mode
/*			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = HAND_MODE;
			}
*/			handMode = 1;

			cout << "\n\r" << "Hand Mode Enabled" << endl;
		} else if (keystroke_now == 't') {
			// Have quad 1 takeoff
			crazyflie_info[0].cflieCopter->m_enumFlightMode = TAKEOFF_MODE;
			takeOffTime1 = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;
			cout << "\n\r" << "Crazyflie 1 Take-Off" << endl;
		} else if (keystroke_now == 'y') {
			// Have quad 2 takeoff
			crazyflie_info[1].cflieCopter->m_enumFlightMode = TAKEOFF_MODE;
			takeOffTime2 = crazyflie_info[1].cflieCopter->currentTime() - crazyflie_info[1].initTime;
			cout << "\n\r" << "Crazyflie 2 Take-Off" << endl;
		} else if (keystroke_now == 'u') {
			// Have quad 3 takeoff
			crazyflie_info[2].cflieCopter->m_enumFlightMode = TAKEOFF_MODE;
			takeOffTime3 = crazyflie_info[2].cflieCopter->currentTime() - crazyflie_info[2].initTime;
			cout << "\n\r" << "Crazyflie 3 Take-Off" << endl;
		} else if (keystroke_now == 'i') {
			// Have quad 4 takeoff
			crazyflie_info[3].cflieCopter->m_enumFlightMode = TAKEOFF_MODE;
			takeOffTime4 = crazyflie_info[3].cflieCopter->currentTime() - crazyflie_info[3].initTime;
			cout << "\n\r" << "Crazyflie 4 Take-Off" << endl;
		} 
		else if (keystroke_now == 'b') {
			// Display the battery voltage for all the quads
			cout << endl << "Battery Levels" << endl;
			for (int i = 0; i < NUM_QUADS; i++) {
				cout << "Crazyflie " << i << ": " << crazyflie_info[i].cflieCopter->batteryLevel();
				if (crazyflie_info[i].cflieCopter->batteryState() == 1) {
					cout << " Charging" << endl;
				} else if (crazyflie_info[i].cflieCopter->batteryState() == 2) {
					cout << " Charged" << endl;
				} else if (crazyflie_info[i].cflieCopter->batteryState() == 3) {
					cout << " Low Power Mode" << endl;
				} else {
					cout << endl;
				}
				

			}
		}
//		else if (keystroke_now == '`') {
//			tcp_server_ON ^= 1;
//		} else if (keystroke_now == '1') {
//			tcp_client_ON ^= 1;
//		}
//		else if (keystroke_now == 'o') {
//			flap = 1;
//		}
//		else if (keystroke_now == 'l') {
//			flap = 0;
//		}
		
		keystroke_prev = keystroke_now;
	}

    nonblock(0);
}//END readKeyboard()


/**
 * During Landing Mode checks how close to ground quad is for shutdown
 */
void nearGround() {
	for (int i = 0; i < NUM_QUADS; i++) {
		if((crazyflie_info[i].cflieCopter->m_enumFlightMode == LANDING_MODE)  && (crazyflie_info[i].controllerData.zPosition < 0.05)) {
			crazyflie_info[i].cflieCopter->m_enumFlightMode = GROUNDED_MODE;
		}
	}
}

void updateHand() {
	if(handMode == 1) {
	//	cout << "Hand Roll: " << rollHand << endl;
		if(killCount > 5) {
			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = GROUNDED_MODE;
			}
			/*
			cflieCopter1->m_enumFlightMode = GROUNDED_MODE;
			cflieCopter2->m_enumFlightMode = GROUNDED_MODE;
			cflieCopter3->m_enumFlightMode = GROUNDED_MODE;
			cflieCopter4->m_enumFlightMode = GROUNDED_MODE;
			*/
			handMode = 0;
			trackHand = 0;
			cout << "Hand Mode: Killing Quads" << endl;
			killCount = 0;
		} else if(trackCount > 5) {
			trackHand = 1;
			for (int i = 0; i < NUM_QUADS; i++) {
				crazyflie_info[i].cflieCopter->m_enumFlightMode = HAND_MODE;
			}
			/*
				cflieCopter1->m_enumFlightMode = HAND_MODE;
				cflieCopter2->m_enumFlightMode = HAND_MODE;
				cflieCopter3->m_enumFlightMode = HAND_MODE;
				cflieCopter4->m_enumFlightMode = HAND_MODE;
				*/
			trackCount = 0;
		} else if((hand.rollAngle > 70.0) || (hand.rollAngle < -70.0)) {
			killCount++;
		} else if( hand.pitchAngle < -70.0 ) {		//|| (pitchHand > 70.0)
			trackCount++;
		} else{
			killCount = 0;
			trackCount = 0;
			if(hand.zPosition > 1.6) {
	//			trackHand = 0;
				for (int i = 0; i < NUM_QUADS; i++) {
					crazyflie_info[i].cflieCopter->m_enumFlightMode = TAKEOFF_MODE;
				}
				cout << "Hand Mode: Launching Quads" << endl;
			} else if(hand.zPosition < 0.4)	{
				trackHand = 0;
				for (int i = 0; i < NUM_QUADS; i++) {
					crazyflie_info[i].cflieCopter->m_enumFlightMode = LANDING_MODE;
				}
				cout << "Hand Mode: Landing Quads" << endl;
			}
		}
	}
}	//END updateHand();

//////////////////////////////////////////////////
// kbhit                                        //
// Descption: used select to do a non blocking  //
// read of stdin.                               //
//                                              //
//////////////////////////////////////////////////
int kbhit() {
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return FD_ISSET(STDIN_FILENO, &fds);
}



//////////////////////////////////////////////////
// nonblock                                     //
// Descption: Turn off connoical mode so the    //
// user does not need to hit enter after        //
// pressing a key                               //
//                                              //
//////////////////////////////////////////////////
void nonblock(int state) {
    struct termios ttystate;

    //get the terminal state
    tcgetattr(STDIN_FILENO, &ttystate);

    if (state==1) {
        //turn off canonical mode
        ttystate.c_lflag &= ~ICANON;
        //minimum of number input read.
        ttystate.c_cc[VMIN] = 1;
    } else if (state==0) {
        //turn on canonical mode
        ttystate.c_lflag |= ICANON;
    }
    //set the terminal attributes.
    tcsetattr(STDIN_FILENO, TCSANOW, &ttystate);
}
