#ifndef SIMPLE_H
#define SIMPLE_H

#include <iostream>
#include "CCrazyflie.h"
//#include "eris_vrpn.h"

CCrazyflie* InitCrazyRadio1(CCrazyRadio *crRadio1); //, CCrazyRadio *crRadio2);

CCrazyflie* InitCrazyRadio2(CCrazyRadio *crRadio2);

int RunCrazyflie(CCrazyRadio *crRadio,  CCrazyflie *cflieCopter, float rollControl, float pitchControl, float yawControl, float thrustControl);

void EndCrazyRadio(CCrazyRadio *crRadio, CCrazyflie *cflieCopter);

#endif
