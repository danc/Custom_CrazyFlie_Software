#include <math.h>
#include "pid.h"
#include <stdbool.h>

void pidInit(PidObject* pid, const float desired, const float kp,
					const float ki, const float kd, const float dt)
{
	pid->error = 0;
	pid->prevError = 0;
	pid->integ = 0;
	pid->deriv = 0;
	pid->desired = desired;
	pid->kp = kp;
	pid->ki = ki;
	pid->kd = kd;
	pid->iLimit = DEFAULT_PID_INTEGRATION_LIMIT;
	pid->iLimitLow = -DEFAULT_PID_INTEGRATION_LIMIT;
	pid->angularLimit = DEFAULT_PID_ANGLE_OUTPUT;
	pid->angularLimitLow = -DEFAULT_PID_ANGLE_OUTPUT;
	pid->thrustLimit = DEFAULT_PID_HIGH_THRUST_LIMIT;
	pid->thrustLimitLow = DEFAULT_PID_LOW_THRUST_LIMIT;
	pid->dt = dt;
}

float pidUpdateYaw(PidObject* pid, const float measured, const bool updateError) //**USED FOR ATTITUDE CONTROL
{
	float output;
	
	if(updateError)
	{
		pid->error = pid->desired - measured;
	}
	
	pid->integ += pid->error * pid->dt;
	if(pid->integ > pid->iLimit)
	{
		pid->integ = pid->iLimit;
	}
	else if(pid->integ < pid->iLimitLow)
	{
		pid->integ = pid->iLimitLow;
	}
	
	pid->deriv = (pid->error - pid->prevError) / pid->dt;
	
	pid->outP = pid->kp* pid->error;
	pid->outI = pid->ki * pid->integ;
	pid->outD = pid->kd * pid->deriv;
	
	output = pid->outP + pid->outI + pid->outD;
	
	pid->prevError = pid->error;
	
	return output;
}

float pidUpdateAngle(PidObject* pid, const float measured, const bool updateError) //**USED FOR ATTITUDE CONTROL
{
	float output;
	
	if(updateError)
	{
		pid->error = pid->desired - measured;
	}
	
	pid->integ += pid->error * pid->dt;
	if(pid->integ > pid->iLimit)
	{
		pid->integ = pid->iLimit;
	}
	else if(pid->integ < pid->iLimitLow)
	{
		pid->integ = pid->iLimitLow;
	}
	
	pid->deriv = (pid->error - pid->prevError) / pid->dt;
	
	pid->outP = pid->kp* pid->error;
	pid->outI = pid->ki * pid->integ;
	pid->outD = pid->kd * pid->deriv;
	
	output = pid->outP + pid->outI + pid->outD;
	
	if(output > pid->angularLimit)	//Limits amount Crazyflie can tip
	{
		output = pid->angularLimit;
	}
	
	if(output < pid->angularLimitLow)	//Limits amount Crazyflie can tip
	{
		output = pid->angularLimitLow;
	}
	
	pid->prevError = pid->error;
	
	return output;
}

float pidUpdateThrust(PidObject* pid, const float measured, const bool updateError) //**USED FOR ALTITUDE CONTROL (DIFFERENT OUTPUT LIMITS)
{
	float output;
	
	if(updateError)
	{
		pid->error = pid->desired - measured;
	}
	
	//if(pid->error > 0.3)
	//{
	//	pidSetKi(pid, 0.0);
	//}
	//else
	//{
	//	pidSetKi(pid, PID_Z_KI);
	//}
	
	pid->integ += pid->error * pid->dt;
	if(pid->integ > pid->iLimit)
	{
		pid->integ = pid->iLimit;
	}
	else if(pid->integ < pid->iLimitLow)
	{
		pid->integ = pid->iLimitLow;
	}
	
	pid->deriv = (pid->error - pid->prevError) / pid->dt;
	
	pid->outP = pid->kp * pid->error;
	pid->outI = pid->ki * pid->integ;
	pid->outD = pid->kd * pid->deriv;
	
	output = pid->outP + pid->outI + pid->outD;
	
	output += 43000;  //Thrust Offset Required to Maintain Hover
	
	if(output > pid->thrustLimit)  //Limits Upper Thrust Output Command of the PID
	{
		output = pid->thrustLimit;
	}
	
	if(output < pid->thrustLimitLow)		//Limits Lower Thrust Output Command of the PID
	{
		output = pid->thrustLimitLow;
	}
	
	pid->prevError = pid->error;
	
	return output;
}

//******NEED TO MAKE THE YAW CONTROL UPDATE FUNCTION******

void pidSetIntegralLimit(PidObject* pid, const float limit)
{
	pid->iLimit = limit;
}

void pidSetIntegralLimitLow(PidObject* pid, const float limitLow)
{
	pid->iLimitLow = limitLow;
}

void pidReset(PidObject* pid)
{
	pid->error = 0;
	pid->prevError = 0;
	pid->integ = 0;
	pid->deriv = 0;
}

void pidSetError(PidObject* pid, const float error)
{
	pid->error = error;
}

void pidSetDesired(PidObject* pid, const float desired)
{
	pid->desired = desired;
}

float pidGetDesired(PidObject* pid)
{
	return pid->desired;
}

bool pidIsActive(PidObject* pid)
{
	bool isActive = true;
	
	if(pid->kp < 0.0001 && pid->ki <0.0001 && pid->kd < 0.0001)
	{
		isActive = false;
	}
	
	return isActive;
}

void pidSetKp(PidObject* pid, const float kp)
{
	pid->kp = kp;
}

void pidSetKi(PidObject* pid, const float ki)
{
	pid->ki = ki;
}

void pidSetKd(PidObject* pid, const float kd)
{
	pid->kd = kd;
}

void pidSetDt(PidObject* pid, const float dt)
{
	pid->dt = dt;
}
