#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <stdbool.h>
#include "CCrazyflie.h"
#include "pid.h"

typedef struct
{
	PidObject pidRoll;
	PidObject pidPitch;
	PidObject pidYaw;

	PidObject pidX;
	PidObject pidY;
	PidObject pidZ;

	bool isInit;
	
} ControllerObject;

void controllerInit( ControllerObject * ctrl );
bool controllerTest(ControllerObject * ctrl);

void controllerCorrectAttitudePID(ControllerObject * ctrl, 
                                float xPositionActual, float yPositionActual, float zPositionActual, 
											float eulerYawActual, float xPositionDesired, float yPositionDesired,
										 float zPositionDesired, float eulerYawDesired, float* rollControlOutput,
									 float* pitchControlOutput, float* yawControlOutput, float* thrustControlOutput);
									 
void controllerResetAllPID(ControllerObject * ctrl);

void controllerSetAllDt(ControllerObject * ctrl, double dt);

void controllerSetXYError(ControllerObject * ctrl, const float xError, const float yError);

//void controllerGetActuatorOutput(cflieCopter* roll, cflieCopter* pitch, cflieCopter* yaw, cflieCopter* thrust);

#endif /* CONTROLLER_H_ */
