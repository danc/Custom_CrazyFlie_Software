#ifndef VRPN_CALLBACKS_H_
#define VRPN_CALLBACKS_H_

#include "vrpn_Connection.h"
#include "vrpn_Tracker.h"
#include <math.h>
#include <quat.h>

// Function prototypes for the VRPN callback routines
void VRPN_CALLBACK handle_hand(void*, const vrpn_TRACKERCB t);

void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_Crazyflie2(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_Crazyflie3(void*, const vrpn_TRACKERCB t);
void VRPN_CALLBACK handle_Crazyflie4(void*, const vrpn_TRACKERCB t);


/**
 *	Correct the X and Y position to be relative to the body axis instead of the camera axis (due to yaw change)
 * 
 * @param yaw The current yaw angle
 * @param x The uncorrected x position
 * @param y The uncorrected y position
 * @param xCorrected Pointer to the variable to contain the corrected x value
 * @param yCorrected Pointer to the variable to contain the corrected y value
 */
inline void yawCorrection(float yaw, float x, float y, float *xCorrected, float *yCorrected) {
#if USE_PR_YAW_CORRECT
	//CORRECTS PITCH AND ROLL PID ERRORS TO ACCOUNT FOR QUADS CURRENT YAW DIRECTION
	*xCorrected = cos( Q_DEG_TO_RAD(yaw) )*x - sin( Q_DEG_TO_RAD(yaw) )*y;	
	*yCorrected = sin( Q_DEG_TO_RAD(yaw) )*x + cos( Q_DEG_TO_RAD(yaw) )*y;
#else
	*xCorrected = x;
	*yCorrected = y;
#endif	// END USE_PR_YAW_CORRECT
}

#endif // VRPN_CALLBACKS_H_