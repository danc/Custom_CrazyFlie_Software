#include "quadcopterData.h"
//#include "eris_vrpn.h"
#include "callbacks.h"

// Offsets to be used during mirror flight
float mirrorOffsets[][2] = {
	{ 0.00,  0.00},		// Quadcopter 1 is the master
	{-0.50,  0.00},		// Quadcopter 2
	{ 0.45,  0.00},		// Quadcopter 3
	{ 0.00, -0.50}		// Quadcopter 4
};

// Offsets to be used during mirror flight
float handOffsets[][2] = {
	{-0.50,  0.50},		// Quadcopter 1
	{ 0.50,  0.50},		// Quadcopter 2
	{ 0.50, -0.50},		// Quadcopter 3
	{-0.50, -0.50}		// Quadcopter 4
};

// Structure for the radio
RADIOS_t radios[] = {
	// Radio 0
	{NULL,			// Radio object pointer (initialized by main)
	 2000,
	 0,
	 P_0DBM,
	 XFER_2M
	},

	// Radio 1
	{NULL,
	 2000,
	 0,
	 P_0DBM,
	 XFER_2M
	}
};

// Strucuture for the hand data
HAND_t hand = {
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, "hand.txt", NULL
};

// Initialize the quadcopter data
QUADCOPTERS_t crazyflie_info[] = {
	// Quadcopter 1
	{"Crazyflie2",
	 handle_pos,
	 NULL,
	 NULL,
	 80,
	 0,
	 0,
	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  { {0},{0},{0},{0},{0},{0},0 },	// The controller object
	  0,								// The default yaw offset
	  0,								// The default yaw setpoint
	  0.681,							// The default x setpoint
	  0.360,							// The default y setpoint
	  0.600,							// The default z setpoint
	  0									// The number of frames received
	 },
 	 "cflie1.txt",			// The name of the logfile
	 NULL					// The logfile object
	},	

	// Quadcopter 2
	{"Crazyflie21",			// Trackable name
	 handle_Crazyflie2,		// VRPN callback function
	 NULL,					// VRPN tracker object (initialized in VRPNinit)
	 NULL,					// CrazyFlie copter object (initialized in main)
	 5,						// The channel number of the crazyflie
	 0,						// The radio to use
	 0,						// The init time
	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  { {0},{0},{0},{0},{0},{0},0 },	// The controller object
	  0,								// The default yaw offset
	  0,								// The default yaw setpoint
	  -0.475,							// The default x setpoint
	  0.340,							// The default y setpoint
	  0.750,							// The default z setpoint
	  0,								// Default the X position to 0
	  0,								// Default the Y position to 0
	  0,								// Default the Z position to 0
	  0									// The number of frames received
	 },
	 "cflie2.txt",			// The name of the logfile
	 NULL					// The logfile object
	},

	// Quadcopter 3
	{"Crazyflie22",
	 handle_Crazyflie3,
	 NULL,
	 NULL,
	 25,
	 1,
	 0,
 	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  { {0},{0},{0},{0},{0},{0},0 },	// The controller object
	  0,								// The default yaw offset
	  0,								// The default yaw setpoint
	  0.751,							// The default x setpoint
	  -0.450,							// The default y setpoint
	  0.500,							// The default z setpoint
	  0									// The number of frames received
	 },
	 "cflie3.txt",						// The name of the logfile
	 NULL								// The logfile object
	},

	// Quadcopter 4
	{"Crazyflie23",
	 handle_Crazyflie4,
	 NULL,
	 NULL,
	 45,
	 1,
	 0,
 	 // The controller data structure
	 {0,								// The loop start time
	  0,								// The previous loop run time
	  0,								// The VRPN packet time
	  0,								// The number of VRPN packets to be processed
	  { {0},{0},{0},{0},{0},{0},0 },	// The controller object
	  0,								// The default yaw offset
	  0,								// The default yaw setpoint
	  -0.200,							// The default x setpoint
	  -0.600,							// The default y setpoint
	  0.400,							// The default z setpoint
	  0									// The number of frames received
	 },
 	 "cflie4.txt",			// The name of the logfile
	 NULL					// The logfile object
	}
};