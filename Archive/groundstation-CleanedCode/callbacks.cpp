#include "eris_vrpn.h"
#include "quadcopterData.h"
#include "callbacks.h"
#include "vrpn_Tracker.h"
#include <quat.h>
#include "simple.h"

using namespace std;

void processTrackableCallback(int crazyflieNum, int radioNum, 
							  CONTROLLER_DATA_t *controllerData,
							  CCrazyflie *cflieCopter,
							  const vrpn_TRACKERCB t);

/**
 * The VRPN callback for the 1st quadcopter
 */
void VRPN_CALLBACK handle_pos(void*, const vrpn_TRACKERCB t) {
	// Variables needed for the mirror mode
	static double lastTime = crazyflie_info[0].cflieCopter->currentTime();

	// Pull out the position of the trackable
	crazyflie_info[0].controllerData.xPosition = t.pos[0];
	crazyflie_info[0].controllerData.yPosition = t.pos[1];
	crazyflie_info[0].controllerData.zPosition = -t.pos[2] + 0.05;

	// Get the battery and act accordingly
	float battery = crazyflie_info[0].cflieCopter->batteryLevel();
	if (crazyflie_info[0].cflieCopter->m_enumFlightMode != LANDING_MODE || crazyflie_info[0].cflieCopter->m_enumFlightMode != GROUNDED_MODE) {
		if (battery < LOW_BATTERY_LEVEL && battery != 0) {
			cout << "Crazyflie 0: Battery Level Too Low, Landing Crazyflie" << endl;
			crazyflie_info[0].cflieCopter->m_enumFlightMode = LANDING_MODE;
		}
	}

	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[0].cflieCopter->m_enumFlightMode) {
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[0].controllerData.zSetpoint = -0.4;
		break;
	case MIRROR_MODE:
		// This quadcopter is the first quad, so it is going to do steps that the others will follow
		if (crazyflie_info[0].cflieCopter->currentTime() - lastTime > 4.0) {
			cout << "Stepping Crazyflies" << endl;
			// Change the setpoint
			crazyflie_info[0].controllerData.xSetpoint = -0.012;
			crazyflie_info[0].controllerData.ySetpoint = -0.200;
			crazyflie_info[0].controllerData.zSetpoint =  0.750;
			lastTime = crazyflie_info[0].cflieCopter->currentTime();
		} else {
			// Change the setpoint back
			cout << "Stepping Crazyflies" << endl;
			crazyflie_info[0].controllerData.xSetpoint = -0.012 + 0.65;
			crazyflie_info[0].controllerData.ySetpoint = -0.200 + 0.65;
			crazyflie_info[0].controllerData.zSetpoint =  0.750;
			lastTime = crazyflie_info[0].cflieCopter->currentTime();
		}
		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		// The default setpoints are ok for this
		break;
	case HAND_MODE:
		crazyflie_info[0].controllerData.xSetpoint = hand.xPosition + handOffsets[0][0];
		crazyflie_info[0].controllerData.ySetpoint = hand.yPosition + handOffsets[0][1];
		break;
	case GROUNDED_MODE:
	default:
		break;
	}

	// Do the main control loop
	processTrackableCallback(0, crazyflie_info[0].radioNumber, &(crazyflie_info[0].controllerData), crazyflie_info[0].cflieCopter, t);
}

/**
 * The VRPN callback for the 2nd quadcopter
 */
void VRPN_CALLBACK handle_Crazyflie2(void*, const vrpn_TRACKERCB t) {
	// Pull out the position of the trackable
	crazyflie_info[1].controllerData.xPosition = t.pos[0];
	crazyflie_info[1].controllerData.yPosition = t.pos[1];
	crazyflie_info[1].controllerData.zPosition = -t.pos[2] + 0.05;

	// Get the battery and act accordingly
	float battery = crazyflie_info[1].cflieCopter->batteryLevel();
	if (crazyflie_info[1].cflieCopter->m_enumFlightMode != LANDING_MODE || crazyflie_info[1].cflieCopter->m_enumFlightMode != GROUNDED_MODE) {
		if (battery < LOW_BATTERY_LEVEL && battery != 0) {
			cout << "Crazyflie 1: Battery Level Too Low, Landing Crazyflie" << endl;
			crazyflie_info[1].cflieCopter->m_enumFlightMode = LANDING_MODE;
		}
	}

	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[1].cflieCopter->m_enumFlightMode) {
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[1].controllerData.zSetpoint = -0.4;
		break;
	case MIRROR_MODE:
		// The quadcopter should mirror the first quadcopter
		crazyflie_info[1].controllerData.xSetpoint = crazyflie_info[0].controllerData.xSetpoint + mirrorOffsets[1][0];
		crazyflie_info[1].controllerData.ySetpoint = crazyflie_info[0].controllerData.ySetpoint + mirrorOffsets[1][1];
		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		// The default setpoints are ok for this
		break;
	case HAND_MODE:
		crazyflie_info[1].controllerData.xSetpoint = hand.xPosition + handOffsets[1][0];
		crazyflie_info[1].controllerData.ySetpoint = hand.yPosition + handOffsets[1][1];
		break;
	case GROUNDED_MODE:
	default:
		break;
	}

	// Do the main control loop
	processTrackableCallback(1, crazyflie_info[1].radioNumber, &(crazyflie_info[1].controllerData), crazyflie_info[1].cflieCopter, t);
}

/**
 * The VRPN callback for the 2nd quadcopter
 */
void VRPN_CALLBACK handle_Crazyflie3(void*, const vrpn_TRACKERCB t) {
	// Pull out the position of the trackable
	crazyflie_info[2].controllerData.xPosition = t.pos[0];
	crazyflie_info[2].controllerData.yPosition = t.pos[1];
	crazyflie_info[2].controllerData.zPosition = -t.pos[2] + 0.05;

	// Get the battery and act accordingly
	float battery = crazyflie_info[2].cflieCopter->batteryLevel();
	if (crazyflie_info[2].cflieCopter->m_enumFlightMode != LANDING_MODE || crazyflie_info[2].cflieCopter->m_enumFlightMode != GROUNDED_MODE) {
		if (battery < LOW_BATTERY_LEVEL && battery != 0) {
			cout << "Crazyflie 2: Battery Level Too Low, Landing Crazyflie" << endl;
			crazyflie_info[2].cflieCopter->m_enumFlightMode = LANDING_MODE;
		}
	}

	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[2].cflieCopter->m_enumFlightMode) {
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[2].controllerData.zSetpoint = -0.4;
		break;
	case MIRROR_MODE:
		// The quadcopter should mirror the first quadcopter
		crazyflie_info[2].controllerData.xSetpoint = crazyflie_info[0].controllerData.xSetpoint + mirrorOffsets[2][0];
		crazyflie_info[2].controllerData.ySetpoint = crazyflie_info[0].controllerData.ySetpoint + mirrorOffsets[2][1];
		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		// The default setpoints are ok for this
		break;
	case HAND_MODE:
		crazyflie_info[2].controllerData.xSetpoint = hand.xPosition + handOffsets[2][0];
		crazyflie_info[2].controllerData.ySetpoint = hand.yPosition + handOffsets[2][1];
		break;
	case GROUNDED_MODE:
	default:
		break;
	}

	// Do the main control loop
	processTrackableCallback(2, crazyflie_info[2].radioNumber, &(crazyflie_info[2].controllerData), crazyflie_info[2].cflieCopter, t);
}

/**
 * The VRPN callback for the 2nd quadcopter
 */
void VRPN_CALLBACK handle_Crazyflie4(void*, const vrpn_TRACKERCB t) {
	// Pull out the position of the trackable
	crazyflie_info[3].controllerData.xPosition = t.pos[0];
	crazyflie_info[3].controllerData.yPosition = t.pos[1];
	crazyflie_info[3].controllerData.zPosition = -t.pos[2] + 0.05;

	// Get the battery and act accordingly
	float battery = crazyflie_info[3].cflieCopter->batteryLevel();
	if (crazyflie_info[3].cflieCopter->m_enumFlightMode != LANDING_MODE || crazyflie_info[3].cflieCopter->m_enumFlightMode != GROUNDED_MODE) {
		if (battery < LOW_BATTERY_LEVEL && battery != 0) {
			cout << "Crazyflie 3: Battery Level Too Low, Landing Crazyflie" << endl;
			crazyflie_info[3].cflieCopter->m_enumFlightMode = LANDING_MODE;
		}
	}

	// Figure out the setpoint for x and y based upon flight mode
	switch (crazyflie_info[3].cflieCopter->m_enumFlightMode) {
	case LANDING_MODE:
		// The quadcopter should land
		// Set only z, leave x and y the same
		crazyflie_info[3].controllerData.zSetpoint = -0.4;
		break;
	case MIRROR_MODE:
		// The quadcopter should mirror the first quadcopter
		crazyflie_info[3].controllerData.xSetpoint = crazyflie_info[0].controllerData.xSetpoint + mirrorOffsets[3][0];
		crazyflie_info[3].controllerData.ySetpoint = crazyflie_info[0].controllerData.ySetpoint + mirrorOffsets[3][1];
		break;
	case TAKEOFF_MODE:
		// The quadcopter should takeoff
		// The default setpoints are ok for this
		break;
	case HAND_MODE:
		crazyflie_info[3].controllerData.xSetpoint = hand.xPosition + handOffsets[3][0];
		crazyflie_info[3].controllerData.ySetpoint = hand.yPosition + handOffsets[3][1];
		break;
	case GROUNDED_MODE:
	default:
		break;
	}

	// Do the main control loop
	processTrackableCallback(3, crazyflie_info[3].radioNumber, &(crazyflie_info[3].controllerData), crazyflie_info[3].cflieCopter, t);
}


#if USE_HAND
/**
 * VRPN callback for the hand trackable
 */
void VRPN_CALLBACK handle_hand(void*, const vrpn_TRACKERCB t) {

	if(hand.vrpnPacketQueueLen < 2)	//If VRPN Buffer does NOT have old packets stored up, run main code
	{
		double loopTimeHandStart = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;	
		double loopTimeHandDelta = loopTimeHandStart - hand.loopTimeStartPrevious;	//Calculates time difference between each loop start
		hand.loopTimeStartPrevious = loopTimeHandStart;

		double vrpnPacketTimeHand = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
		double vrpnPacketDeltaHand= vrpnPacketTimeHand - hand.vrpnTimePrevious;			//Calculates time between VRPN Data Packets (almost always 0.01 sec)
		hand.vrpnTimePrevious = vrpnPacketTimeHand;

		hand.vrpnPacketQueueLen = ceil(loopTimeHandDelta/vrpnPacketDeltaHand);		//Calculates estimated # of packets built up in VRPN buffer.

		// Get the euler angles
		q_vec_type euler;
		q_to_euler(euler, t.quat);

		hand.xPosition = t.pos[0];
		hand.yPosition = t.pos[1];
		hand.zPosition = -t.pos[2] + 0.05;

		hand.yawAngle = Q_RAD_TO_DEG(euler[0]);
		hand.rollAngle = Q_RAD_TO_DEG(euler[1]);
		hand.pitchAngle = Q_RAD_TO_DEG(euler[2]);

		double loopTimeHandEnd = crazyflie_info[0].cflieCopter->currentTime() - crazyflie_info[0].initTime;
		hand.loopTime = loopTimeHandEnd - loopTimeHandStart;

#if USE_LOGGING
		fprintf(hand.logfile, "%.6f\t\t", cflieCopter1->currentTime() - initTime1 );
		fprintf(hand.logfile, "%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f",
					euler[1], 
					euler[2], 
					euler[0], 
					t.pos[0], 
					t.pos[1], 
					-t.pos[2],
					loopTimeHand,
					loopTimeHandDelta);	//
		fprintf(hand.logfile, "\n");
#endif

	}
	else if(hand.vrpnPacketQueueLen < 0){	//Failsafe so can't go below 0 (should never happen) |   || (vrpnPacketBackup4 > 20)?
		hand.vrpnPacketQueueLen = 0;
	}
	else if(hand.vrpnPacketQueueLen >= 2){
		hand.vrpnPacketQueueLen --;
	}
}
#endif // END USE_HAND

/**
 * This function will process the controller callback for every quadcopter using the
 * data passed into from the VRPM callback for the quadcopter.
 *
 * @param crazyflieNum The number of the crazyflie (in the quadcopter information array)
 * @param radioNum The number of the radio the crazyflie uses (in the radio array)
 * @param controllerData The data structure containing information about the controller
 * @param vrpn_TRACKERCB The VRPN data structure
 */
void processTrackableCallback(int crazyflieNum, int radioNum,
							  CONTROLLER_DATA_t *controllerData,
							  CCrazyflie *cflieCopter,
							  const vrpn_TRACKERCB t) {

	if(controllerData->vrpnPacketQueueLen < 2) {
		//If VRPN Buffer does NOT have old packets stored up, run main code

		// Calculate the delta in the control loop time
		double loopStartTime = cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime;
		double loopTimeDelta = loopStartTime - controllerData->loopTimeStartPrevious;
		controllerData->loopTimeStartPrevious = loopStartTime;

		// Calculate the delta in the VRPN packet time
		double vrpnPacketTime = t.msg_time.tv_sec + (t.msg_time.tv_usec / 1000000.0);
		double vrpnPacketDelta = vrpnPacketTime - controllerData->vrpnTimePrevious;
		controllerData->vrpnTimePrevious = vrpnPacketTime;

		// Calculates estimated # of packets built up in VRPN buffer.
		controllerData->vrpnPacketQueueLen = ceil(loopTimeDelta/vrpnPacketDelta);

		// Set the rate for the controllers based upon time between VRPN packets
		controllerSetAllDt(&(controllerData->controller), vrpnPacketDelta);

		// Get the current flightmode
		enum FlightMode flightMode = cflieCopter->m_enumFlightMode;

		// Debug statement to say what controller is running
#if USE_PRINTOUT
		if (flightMode != GROUNDED_MODE) {
			cout << endl << "Controller " << crazyflieNum << endl;
			cout << "Battery: " << cflieCopter->batteryLevel() << endl;

		}
#endif	// END USE_PRINTOUT

		// Get the current position information out of the tracker
		// Get the euler angles
		q_vec_type euler;
		q_to_euler(euler, t.quat);

		float camYawDeg = -Q_RAD_TO_DEG(euler[0]); //Converts From Radians to Degrees and invert the angle
		float quadYaw = cflieCopter->yaw();

		/*
		 * Take the yaw position on the 50th frame as the yaw offset
		 */
		double camYawDegWrapped = 0;
		double quadYawWrapped = 0;

		controllerData->frameCount++;
		if (controllerData->frameCount == 50) {
			// Wrap the camera yaw to be 0 - 360
			if (camYawDeg < 0.0)
				camYawDegWrapped = camYawDeg + 360.0;
			else
				camYawDegWrapped = camYawDeg;

			// Wrap the quadcopter yaw to be 0 - 360
			if(quadYaw < 0.0)
				quadYawWrapped = quadYaw + 360.0;
			else
				quadYawWrapped = quadYaw;

			// Determine the yaw offset between camera system and quadcopter
			controllerData->yawOffset = quadYawWrapped - camYawDegWrapped;
			std::cout << "Crazyflie " << crazyflieNum << "- Yaw Offset Taken: " << controllerData->yawOffset << std::endl;
		}

		double correctedYaw = camYawDeg + controllerData->yawOffset;

		// Wrap the yaw to be from -180 to 180
		if(correctedYaw > 180.0)
			correctedYaw -= 360.0;
		else if(correctedYaw < -180.0)
			correctedYaw += 360.0;

		// Output the X, Y, Z position onto the terminal
#if USE_PRINTOUT
		if (flightMode != GROUNDED_MODE) {
			cout << "X: " << t.pos[0] << endl;
			cout << "Y: " << t.pos[1] << endl;
			cout << "Z: " << -t.pos[2] + 0.05 << endl;
		}
#endif	// END USE_PRINTOUT

		// This is the meat of the controller stuff
#if USE_KEYBOARD
		float xError = 0;
		float yError = 0;

		// Correct the X & Y coordinates for the yaw
		yawCorrection(correctedYaw, 
					  (controllerData->xSetpoint - controllerData->xPosition),
					  (controllerData->ySetpoint - controllerData->yPosition), 
					  &xError, &yError);

		// Initialize the outputs to be 0
		float rollControlOutput = 0;
		float pitchControlOutput = 0;
		float yawControlOutput = 0;
		float thrustControlOutput = 0;

		// Run the controller when the quadcopter is not on the ground
		if (flightMode != GROUNDED_MODE) {
			controllerSetXYError(&(controllerData->controller), xError, yError);
			controllerCorrectAttitudePID(&(controllerData->controller),
										 controllerData->xPosition, controllerData->yPosition,
										 controllerData->zPosition, correctedYaw,
										 controllerData->xSetpoint, controllerData->ySetpoint,
										 controllerData->zSetpoint, controllerData->yawSetpoint,
										 &rollControlOutput, &pitchControlOutput, &yawControlOutput,
										 &thrustControlOutput);
		} else {
			rollControlOutput = 0;
			pitchControlOutput = 0;
			yawControlOutput = 0;
			thrustControlOutput = 0;
		}
#else
		// Initialize the outputs to be 0
		float rollControlOutput = 0;
		float pitchControlOutput = 0;
		float yawControlOutput = 0;
		float thrustControlOutput = 0;
#endif	// END USE_KEYBOARD

		// Safety check the crazyflie, if Crazyflie is vertical kill motors, Crash Imminent
        if (cflieCopter->roll() > 90) {
            thrustControlOutput = 10000;
        } else if (cflieCopter->roll() < -90) {
            thrustControlOutput = 10000;
        } else if (cflieCopter->pitch() > 90) {
            thrustControlOutput = 10000;
        } else if (cflieCopter->pitch() < -90) {
            thrustControlOutput = 10000;
        }

        // Set the outputs for the crazyflie 
        cflieCopter->setThrust(thrustControlOutput);
        cflieCopter->setRoll(rollControlOutput);
        cflieCopter->setPitch(pitchControlOutput);
        cflieCopter->setYaw(-yawControlOutput);	//******SET TO NEGATIVE FOR CAMERA SYSTEM YAW BEING REVERSED!!!!****
        cflieCopter->setSendSetpoints(true);

#if USE_PRINTOUT
        if (cflieCopter->m_enumFlightMode != GROUNDED_MODE) {
            cout << "Roll: " << cflieCopter->roll() << endl;
            cout << "Pitch: " << cflieCopter->pitch() << endl;
            cout << "Yaw: " << cflieCopter->yaw() << endl;
            cout << "Thrust: " << cflieCopter->thrust() << endl;
            cout << "PID: " << endl;
            cout << "Desired Roll: " << rollControlOutput << endl;
            cout << "Desired Pitch: " << pitchControlOutput << endl;
            cout << "Desired Yaw: " << yawControlOutput << endl;
            cout << "Desired Thrust: " << thrustControlOutput << endl;
        }
#endif // END USE_PRINTOUT


#if USE_LOGGING		
#if USE_BASIC_LOGGING
		fprintf(crazyflie_info[crazyflieNum].logfile, "%.6f\t\t", crazyflie_info[crazyflieNum].cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime );
		fprintf(crazyflie_info[crazyflieNum].logfile,
				"%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%d",
				cflieCopter->roll(), 
				cflieCopter->pitch(), 
				quadYaw, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg,
				correctedYaw,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime,
				loopTimeDelta,
				vrpnPacketDelta,
				vrpnPacketTime,
				vrpnPacketBackup,
				cflieCopter->radioRSSI());	//
		fprintf(crazyflie_info[crazyflieNum].logfile, "\n");

#else	
		fprintf(crazyflie_info[crazyflieNum].logfile, "%.6f\t\t", cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime );
		fprintf(crazyflie_info[crazyflieNum].logfile,
				"%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%.6f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%.3f\t\t%d\t\t%.3f\t\t%.3f\t\t%.3f\t\t%lu\t\t%lu\t\t%lu\t\t%lu\t\t%.3f\t\t%.3f\t\t%.3f",
				cflieCopter->roll(), 
				cflieCopter->pitch(), 
				quadYaw1, 
				t.pos[0], 
				t.pos[1], 
				-t.pos[2],
				camYawDeg,
				correctedYaw,
				loopTimeTotal,
				loopTimeTotalDelta,
				loopTime,
				loopTimeDelta,
				vrpnPacketDelta,
				vrpnPacketTime,
				vrpnPacketBackup,
				xPositionDesired,
				yPositionDesired,
				zPositionDesired,
				yawDesired,
				pitchControlOutput,
				rollControlOutput,
				yawControlOutput,
				thrustControlOutput,
				crazyflie_info[crazyflieNum].cflieCopter->radioRSSI(),
				euler[0],
				euler[1],
				euler[2],
				cflieCopter->motor1(),
				cflieCopter->motor2(),
				cflieCopter->motor3(),
				cflieCopter->motor4(),
				cflieCopter->gyroX(),
				cflieCopter->gyroY(),
				cflieCopter->gyroZ());
		fprintf(crazyflie_info[crazyflieNum].logfile, "\n");
#endif
#endif	// END: USE_LOGGING

		double loopTimeEnd = cflieCopter->currentTime() - crazyflie_info[crazyflieNum].initTime;
		controllerData->loopTime = loopTimeEnd - loopStartTime;
	} else 	if(controllerData->vrpnPacketQueueLen >= 2) {
		// If the VRPN buffer has more than 2, then skip the packet
		controllerData->vrpnPacketQueueLen --;
		return;
	} else if(controllerData->vrpnPacketQueueLen < 0) {
		// Failsafe so can't go below 0 (should never happen)
		controllerData->vrpnPacketQueueLen = 0;
		return;
	}
}