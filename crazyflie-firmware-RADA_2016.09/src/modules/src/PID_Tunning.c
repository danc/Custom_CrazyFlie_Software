#include <stdint.h>
#include "pid.h"
#include "crtp.h"
#include "PID_Tunning.h"
#include "debug.h"
#include "motors.h"
#include "power_distribution.h"

#include "FreeRTOS.h"
#include "task.h"

#define PID_TUNNING_MAX_ID 10

/*
 * Buffer the PID update requrests
 */
static struct PID_Tunning_CrtpGainValues updateBuffer[PID_TUNNING_MAX_ID];
static uint8_t bufferEnd = 0;

// Is the stuff initialized
static bool isInit = 0;

static PidObject* registeredPID[PID_TUNNING_MAX_ID];

static bool motorsArmed = false;

void PID_Tunning_ArmMotors();


/**
 * Callback for the position CRTP packet
 */
static void PID_Tunning_GainsCB(CRTPPacket* pk) {
  // Only update the buffer if it is not full
  if (bufferEnd < PID_TUNNING_MAX_ID) {
    updateBuffer[bufferEnd] = *((struct PID_Tunning_CrtpGainValues*)pk->data);
    bufferEnd++;
  }
}

/* Public functions */
void PID_Tunning_Init(void) {
  if(isInit) {
    return;
  }

  crtpInit();
  crtpRegisterPortCB(CRTP_PORT_PID_TUNNING, PID_Tunning_GainsCB);

  motorsArmed = false;
  isInit = true;
}

bool PID_Tunning_Test(void) {
  crtpTest();
  return isInit;
}

/**
 * Register a PID controller with the tunning task
 *
 * @param controllerID The ID number for the PID controller
 * @param pid The PidObject to modify
 */
void PID_Tunning_RegisterPID(uint8_t controllerID, PidObject* pid) {
  // If the ID is a valid ID, store the PID into the list
  if (controllerID < PID_TUNNING_MAX_ID) {
    registeredPID[controllerID] = pid;
  }
}

/**
 * Apply all pending PID updates
 */
void PID_Tunning_UpdatePID() {
  // If there is nothing in the buffer, then just return
  if (bufferEnd == 0) {
    return;
  }

  // Iterate through the buffer, updating the PID stuff
  for (int i = 0; i < bufferEnd; i++) {
    // Sanity check the controller ID
    uint8_t id = updateBuffer[i].controllerID;
    if (id == 9 && !motorsArmed) {
      PID_Tunning_ArmMotors();
      motorsArmed = 1;
      break;
    }
    if (id < PID_TUNNING_MAX_ID) {
      registeredPID[id]->kp = updateBuffer[i].Kp;
      registeredPID[id]->kd = updateBuffer[i].Kd;
      registeredPID[id]->ki = updateBuffer[i].Ki;
      registeredPID[id]->iLimit = updateBuffer[i].iLimit;

      DEBUG_PRINT("U: %d, %f, %f, %f, %f\r\n", id, registeredPID[id]->kp, registeredPID[id]->ki, registeredPID[id]->kd, registeredPID[id]->iLimit);
    }
  }
  // The buffer is empty now
  bufferEnd = 0;
}

void PID_Tunning_ArmMotors() {

    DEBUG_PRINT("U: Arming Motors\n");


    powerDistributionMotorsSetDisable();

    motorsSetRatio(MOTOR_M1, 0);
    motorsSetRatio(MOTOR_M2, 0);
    motorsSetRatio(MOTOR_M3, 0);
    motorsSetRatio(MOTOR_M4, 0);

    vTaskDelay(M2T(1000));

    motorsSetRatio(MOTOR_M1, 40000);
    motorsSetRatio(MOTOR_M2, 40000);
    motorsSetRatio(MOTOR_M3, 40000);
    motorsSetRatio(MOTOR_M4, 40000);

    vTaskDelay(M2T(1000));

    motorsSetRatio(MOTOR_M1, 0);
    motorsSetRatio(MOTOR_M2, 0);
    motorsSetRatio(MOTOR_M3, 0);
    motorsSetRatio(MOTOR_M4, 0);

    powerDistributionMotorsSetEnable();

    DEBUG_PRINT("U: Motors Armed\n");
}