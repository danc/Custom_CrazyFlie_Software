#ifndef PID_TUNNING_H_
#define PID_TUNNING_H_

#include <stdint.h>
#include "pid.h"

/**
 * CRTP Data struct for transmitting PID controller gains
 */
struct PID_Tunning_CrtpGainValues
{
  float Kp;					// Kp Gain
  float Ki;					// Ki Gain
  float Kd;					// Kd Gain
  float iLimit;				// Integrator limit
  uint8_t controllerID;		// PID Controller to modify
} __attribute__((packed));


/**
 * Start the PID Tunning CRTP task
 */
void PID_Tunning_Init(void);

/**
 * Register a PID controller with the tunning task
 *
 * @param controllerID The ID number for the PID controller
 * @param pid The PidObject to modify
 */
void PID_Tunning_RegisterPID(uint8_t controllerID, PidObject* pid);

/**
 * Apply all pending PID updates
 */
void PID_Tunning_UpdatePID();

#endif