// Project includes
#include "crazyradio_node.h"
#include "handlers.h"
#include "helperFunctions.h"
#include "radioInterface.h"

// ROS message includes
#include "crazy_msgs/associateQuadcopter.h"

// System level includes
#include <vector>
#include <stdint.h>
#include "ros/ros.h"

/*
 * Callback function to provide the service to associate with a quadcopter
 * 
 */
void handler_associateQuad(crazy_msgs::associateQuadcopter::Request &request,
						   crazy_msgs::associateQuadcopter::Response &response) {

	uint64_t requestedAddress = request.quadcopterAddress;
	uint8_t requestedChannel = request.quadcopterChannel;

	ROS_INFO_STREAM(ros::this_node::getName() << ": Associating with quadcopter " << std::hex << requestedAddress << " on channel " << requestedChannel);

	// Search through the list of quads already associated to see if it is
	for ( std::vector<quadcopterData>::iterator it = associatedQuads.begin() ; it != associatedQuads.end(); ++it ) {
		unsigned int similarityCheck = 0;
		if ( it->address == requestedAddress ) {
			similarityCheck++;
		}
		if ( it->channel == requestedChannel ) {
			similarityCheck++;
		}

		if ( similarityCheck == 2 ) {
			// The quad is already associated
			response.quadcopterNumber = -1;
			ROS_INFO_STREAM(ros::this_node::getName() << ": Already associated with quadcopter");
			return;
		}
	}
	ROS_DEBUG_STREAM(ros::this_node::getName() << ": Pinging quadcopter to make sure it is alive");

	// Make sure the quadcopter is responsive before associating (send a ping packet)
	setChannel(requestedChannel, 0);
	setAddress(requestedAddress, 0);

	int retVal = pingQuadcopter();
	if (retVal != 0) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Error sending ping packet to quadcopter");
	}
}