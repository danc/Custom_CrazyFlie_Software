// Project includes
#include "radioInterface.h"
#include "crazyradio_node.h"
#include "helperFunctions.h"

// Message and service includes
#include "crazy_msgs/crtp.h"
#include "crazy_msgs/crtp_stream.h"
#include "crazy_msgs/associateQuadcopter.h"
#include "crazy_msgs/crtp_cmd_resp.h"

// System level includes
#include "ros/ros.h"
#include <sstream>
#include <vector>

std::vector<quadcopterData> associatedQuads;

/**
 * Main function for the Crazyradio node
 */
int main(int argc, char **argv)
{
  // Start-up the node
  ros::init(argc, argv, "crazyradio");
  ros::NodeHandle node;
  ros::NodeHandle privNode("~");
  ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);


  /*
   * Read in parameters from the private namespace
   */
  // Read in the dongle number to use
  int dongleNumber;
  if ( privNode.hasParam("dongle") ) {
    privNode.getParam("dongle", dongleNumber);
  } else {
    dongleNumber = 0;
  }

  // Read in radio parameters
  radio_settings radioStart;
  int temp;

  // Read in the channel
  if ( privNode.hasParam("channel") ) {
    privNode.getParam("channel", temp);
    radioStart.channel = temp;
  } else {
    radioStart.channel = 80;
  }
  
  // Use ACK length by default
  if ( privNode.hasParam("ackLength") ) {
    privNode.getParam("ackLength", temp);
    radioStart.useACK = true;
    radioStart.useACKlength = true;
    radioStart.ARD_ACK_Value = temp;
  } else if (privNode.hasParam("ardWait") ) {
    privNode.getParam("ardWait", temp);
    radioStart.useACK = true;
    radioStart.useACKlength = false;
    radioStart.ARD_ACK_Value = temp;
  } else {
    radioStart.useACK = false;
  }

  // See if the power level exists
  if ( privNode.hasParam("power") ) {
    privNode.getParam("power", temp);
    radioStart.power = (radio_power) temp;
  } else {
    radioStart.power = r_power_0_dBm;
  }

  // See if the datarate exists
  if ( privNode.hasParam("datarate") ) {
    privNode.getParam("datarate", temp);
    radioStart.dataRate = (radio_dataRate) temp;
  } else {
    radioStart.dataRate = r_dataRate_2_MBPS;
  }

  // See if the retry count
  if ( privNode.hasParam("retryCount") ) {
    privNode.getParam("retryCount", temp);
    radioStart.retryCount = temp;
  } else {
    radioStart.retryCount = 0;
  }

  // See if the address exists
  if ( privNode.hasParam("address") ) {
    std::string address;
    privNode.getParam("address", address);
    radioStart.address = strtol(address.c_str(), NULL, 16);
  } else {
    radioStart.address = 0xE7E7E7E7E7;
  }

  ros::Rate loop_rate(10);


  /*
   * Initialize the radio
   */
  int error = initializeRadio(dongleNumber, radioStart);

  if ( error != 0 ) {
    return(1);
  }

  pingQuadcopter();

  closeRadio();

  return(1);

  /**
   * A count of how many messages we have sent. This is used to create
   * a unique string for each message.
   */
  int count = 0;
  while (ros::ok())
  {
	// Create a CRTP packet object
  crazy_msgs::crtp packet;

	packet.channel = 2;
	packet.port = 2;
	packet.dataLength = 2;
  packet.data[0] = 8;
	packet.data[1] = 9;

  crazy_msgs::crtp_stream sendStream;
  sendStream.quadNum = 1;
  sendStream.quadAddress = 64;
  sendStream.dataPacket = packet;



    /**
     * The publish() function is how you send messages. The parameter
     * is the message object. The type of this object must agree with the type
     * given as a template parameter to the advertise<>() call, as was done
     * in the constructor above.
     */
//    crtp_pub.publish(crtp_packet);

    ros::spinOnce();

    loop_rate.sleep();
    ++count;
  }


  return 0;
}
