
#include "usbInterface.h"
#include "radioInterface.h"

#include "ros/ros.h"
#include "libusb.h"
#include <string>

/*
 * Request numbers for various operations of the radio
 * Info: https://wiki.bitcraze.io/doc:crazyradio:usb:index#dongle_configuration_and_functions_summary
 */
// All of these use request type 0x40
#define SET_RADIO_CHANNEL	0x01
#define SET_RADIO_ADDRESS	0x02
#define SET_DATA_RATE		0x03
#define SET_RADIO_POWER		0x04
#define SET_RADIO_ARD 		0x05
#define SET_RADIO_ARC		0x06
#define SET_ACK_ENABLE		0x10
#define SET_CONT_CARRIER	0x20
#define START_SCAN_CHANNELS	0x21
#define LAUNCH_BOOTLOADER	0xFF

// This request number uses request type 0xC0
#define GET_SCAN_CHANNELS	0x21

radio_settings currentSettings;

static std::string dataRate2str(radio_dataRate rate);
static std::string ard2str(radio_ARD_waitTime time);
static std::string pwr2str(radio_power power);

/**
 * Initialize the radio to default settings
 *
 * @param dongleNumber The radio dongle to open
 * @param startupSettings Settings to configure the radio to have
 *
 * @return Error code
 */
int initializeRadio(unsigned int dongleNumber, radio_settings startupSettings) {
	ROS_INFO_STREAM(ros::this_node::getName() << ": Initializing Radio");
	// Open the actual USB dongle
	int err = initializeUSBsession(dongleNumber);
	if ( err != 0 ) {
		return( err );
	}

	setChannel(startupSettings.channel, 1);


	setUseACK(startupSettings.useACK, 1);

	if (startupSettings.useACK) {
		if (startupSettings.useACKlength) {
			setACKlength_ARDtime(startupSettings.ARD_ACK_Value, true, 1);
		} else {
			setACKlength_ARDtime(startupSettings.ARD_ACK_Value, false, 1);
		}
	}

	setPower(startupSettings.power, 1);
	setDataRate(startupSettings.dataRate, 1);
	setAddress(startupSettings.address, 1);
	setRetryCount(startupSettings.retryCount, 1);

	return(0);
}

/*
 * Set whether the radio should wait for ACK packets.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param useACK True to use ACK, False to not
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, libusb_error otherwise
 */

int setUseACK(bool useACK, bool forceWrite) {
	if ( !forceWrite && currentSettings.useACK != useACK) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": ACK already set to " << useACK);
		return(0);
	}

	ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting ACK enable to " << useACK);

	// Write the setting
	int retVal = writeControlData(NULL, 0, 0x40, SET_ACK_ENABLE, useACK, 0);

	if ( retVal != 0 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Unable to set ACK enable, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	} else {
		currentSettings.useACK = useACK;
	}
	return( retVal );
}

/*
 * Set the length of the ACK payload the radio expects.
 *
 * This can be done in 2 ways:
 *   1) Set an ARD time.
 *   2) Set an ACK packet length
 *
 * If an ACK packet length is set, then the radio auto-calculates the ARD time using
 * the datarate and ACK packet length information.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newValue The new ARD time or ACK length to use
 * @param useACKlength Whether to use an ACK length (true) or ARD time (false)
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, libusb_error otherwise
 */
int setACKlength_ARDtime(unsigned int newValue, bool useACKlength, bool forceWrite) {
	// Bounds check the new value if using ACK length
	if ( useACKlength && newValue > 32 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": ACK payload length cannot be " << newValue << " bytes. It must be between 0 and 32 bytes.");
		return( 1 );
	}

	// Write the setting
	if (useACKlength) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting ACK payload length to " << newValue << " bytes");
	} else {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting retry delay to " << ard2str( (radio_ARD_waitTime) newValue));
	}

	int retVal = writeControlData(NULL, 0, 0x40, SET_RADIO_ARD, newValue, 0);

	// Do error checking
	if ( retVal != 0 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Unable to set ACK Length/ARD Value, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	}
	return( retVal );
}

/*
 * Set the number of retries for the radio.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param retryCount The number of times to try sending the packet (between 0 and 15)
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, libusb_error otherwise
 */
int setRetryCount(unsigned int retryCount, bool forceWrite) {
	// Bounds check the new assignment
	if ( retryCount > 15 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Cannot set retry count to " << retryCount << ". It must be between 0 and 15.");
		return( 1 );
	}

	// See if that is already the configuration, if it is don't write unless forced
	if ( !forceWrite && currentSettings.retryCount == retryCount ) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Already doing " << retryCount << " retries");
		return( 0 );
	}
	ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting number of retries to " << retryCount);

	// Write the setting
	int retVal = writeControlData(NULL, 0, 0x40, SET_RADIO_ARC, retryCount, 0);

	if ( retVal != 0 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Unable to set the retry count, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	} else {
		currentSettings.retryCount = retryCount;
	}
	return( retVal );
}

/*
 * Set the RF channel that the radio operates on
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newChannel A number between 0 and 125 corresponding to the radio channel to use
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, 1 if invalid channel, libusb_error otherwise
 */
int setChannel(unsigned int newChannel, bool forceWrite) {
	// Bounds check the new assignment
	if ( newChannel > 125 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Cannot set channel to " << newChannel << ". It must be between 0 and 125.");
		return( 1 );
	}

	// See if that is already the configuration, if it is don't write unless forced
	if ( !forceWrite && currentSettings.channel == newChannel ) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Already on channel " << newChannel);
		return( 0 );
	}
	ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting channel to " << newChannel);

	// Write the setting
	int retVal = writeControlData(NULL, 0, 0x40, SET_RADIO_CHANNEL, newChannel, 0);

	if ( retVal != 0 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Unable to set the channel, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	} else {
		currentSettings.channel = newChannel;
	}
	return( retVal );
}

/**
 * Set the data transmission and receive rate of the radio.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newRate The new datarate to set the radio to
 * @param forceWrite Force the datarate to be written
 *
 * @return 0 on success, otherwise libusb_error code
 */
int setDataRate(radio_dataRate newRate, bool forceWrite) {
    // See if this is already the configuration, if it is don't write unless forced
    if ( !forceWrite && currentSettings.dataRate == newRate) {
    	ROS_DEBUG_STREAM(ros::this_node::getName() << ": Already set to datarate " << dataRate2str(newRate));
    	return( 0 );
    }
    ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting datarate to " << dataRate2str(newRate));

    // Write the datarate
	int retVal = writeControlData(NULL, 0, 0x40, SET_DATA_RATE, (int) newRate, 0);

	if ( retVal != 0 ){
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Unable to set the datarate, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	} else {
		currentSettings.dataRate = newRate;
	}
	return( retVal );
}

/**
 * Configure the power level the radio will use.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newPower The power level to use for transmissions
 * @param forceWrite Force the wait time to use
 *
 * @return 0 on success, otherwise libusb_error code
 */
int setPower(radio_power newPower, bool forceWrite) {
	// See if this is already the configuration, if it is don't write unless forced
    if ( !forceWrite && currentSettings.power == newPower) {
    	ROS_DEBUG_STREAM(ros::this_node::getName() << ": Already set to power level of " << pwr2str(newPower));
    	return( 0 );
    }
    ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting power level to " << pwr2str(newPower));

	int retVal = writeControlData(NULL, 0, 0x40, SET_RADIO_POWER, (int) newPower, 0);

	if ( retVal != 0 ){
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Unable to set the power level, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	} else {
		currentSettings.power = newPower;
	}
	return( retVal );
}

/**
 * Configure the address the radio uses.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newAddr The address to set the radio to (must be 5 bytes long)
 * @param forceWrite Force the wait time to use
 *
 * @return 0 on success, otherwise libusb_error code
 */
int setAddress(uint64_t newAddr, bool forceWrite) {
	// See if this is already the configuration, if it is don't write unless forced
	if (!forceWrite && newAddr == currentSettings.address) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Already set to address of 0x" << std::hex << newAddr);
   		return( 0 );
	}
    ROS_DEBUG_STREAM(ros::this_node::getName() << ": Setting address to " << std::hex << newAddr);

    char temp[] = {0xE7, 0xE7, 0xE7, 0xE7, 0xE7};
    //&newAddr
	int retVal = writeControlData( (unsigned char*) temp, 5, 0x40, SET_RADIO_ADDRESS, 0, 0);

	if ( retVal < 0 ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Unable to set the address, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	} else {
		currentSettings.address = newAddr;
	}
	return( retVal );
}

/*
 * This function will send raw data over the radio.
 *
 * @param data Pointer to the data array to send
 * @param dataLength Length of the data array
 * @param timeout Amount of time to wait before stopping transaction (in milliseconds)
 *
 * @return returns USB_ERROR_CODE or libUSB error code
 */
int sendData(unsigned char *data, unsigned int dataLength, unsigned int timeout) {
	if (dataLength == 0) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Empty data write requested, not performing write");
		return( 0 );
	}

	int dataTransfered = 0;	// Variable to hold the number of bytes sent
	int retVal = writeTransmissionData(data, dataLength, &dataTransfered, timeout);

	if ( retVal == LIBUSB_ERROR_TIMEOUT ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": USB timeout occured. Sent " << dataTransfered << " bytes out of " << dataLength);
	} else if (retVal != 0) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Error sending data, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	}

	return( retVal );
}

/**
 * Function to receive data from the radio
 *
 * @param data Pointer to an array to hold the data
 * @param lenToRead Number of bytes to read in
 * @param lenRead Number of bytes actually read
 * @param timeout Amount of time to wait before stopping transaction (in milliseconds)
 *
 * @return libUSB error code
 */
int receiveData(unsigned char* data, int lenToRead, int *lenRead, unsigned int timeout) {
	if (lenToRead == 0) {
		ROS_DEBUG_STREAM(ros::this_node::getName() << ": Empty data read requested, not performing read");
		return(0);
	}

	*lenRead = lenToRead;
	int retVal = readTransmissionData(data, lenRead, timeout);

	if ( retVal == LIBUSB_ERROR_TIMEOUT ) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": USB timeout occured. Read " << *lenRead << " bytes out of " << lenToRead);
	} else if (retVal != 0) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Error reading data, code " << retVal << ": " << std::string( libusb_strerror((libusb_error) retVal)) );
	}

	return( retVal );
}

/**
 * Close the radio
 */
int closeRadio() {
	closeUSBsession();
}

static std::string dataRate2str(radio_dataRate rate) {
	switch(rate) {
	case r_dataRate_250_KBPS:
		return("250kbps");
		break;
	case r_dataRate_1_MBPS:
		return("1Mbps");
		break;
	case r_dataRate_2_MBPS:
		return("2Mbps");
		break;
	}
	return("Invalid Rate");
}

static std::string ard2str(radio_ARD_waitTime time) {
	switch(time) {
	case r_ARD_250us:
		return("250us");
		break;
	case r_ARD_500us:
		return("500us");
		break;
	case r_ARD_750us:
		return("750us");
		break;
	case r_ARD_1000us:
		return("1000us");
		break;
	case r_ARD_1250us:
		return("1250us");
		break;
	case r_ARD_1500us:
		return("1500us");
		break;
	case r_ARD_1750us:
		return("1750us");
		break;
	case r_ARD_2000us:
		return("2000us");
		break;
	case r_ARD_2250us:
		return("2250us");
		break;
	case r_ARD_2500us:
		return("2500us");
		break;
	case r_ARD_2750us:
		return("2750us");
		break;
	case r_ARD_3000us:
		return("3000us");
		break;
	case r_ARD_3250us:
		return("3250us");
		break;
	case r_ARD_3500us:
		return("3500us");
		break;
	case r_ARD_3750us:
		return("3750us");
		break;
	case r_ARD_4000us:
		return("4000us");
		break;
	}
	return("Invalid ARD Time");
}

static std::string pwr2str(radio_power power) {
	switch(power) {
	case r_power_MINUS_18_dBm:
		return("-18dBm");
		break;
	case r_power_MINUS_12_dBm:
		return("-12dBm");
		break;
	case r_power_MINUS_6_dBm:
		return("-6dBm");
		break;
	case r_power_0_dBm:
		return("0dBm");
		break;
	}
	return("Invalid Power Level");
}
