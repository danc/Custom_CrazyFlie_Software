/*
 * This overall file was created by Ian McInerney <Ian.S.McInerneny@ieee.org>
 * using some source code that is by Jan Winkler <winkler@cs.uni-bremen.de>
 *
 * The original sections are:
 * Copyright (c) 2013, Jan Winkler <winkler@cs.uni-bremen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Universität Bremen nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/

#include "usbInterface.h"
#include "ros/ros.h"

#include <string>
#include <sstream>
#include <libusb.h>
#include <list>
#include <stdint.h>


static libusb_context *usb_libContext;			// The libusb-1.0 context to use for all operations
static libusb_device *usb_device;				// The USB device information
static libusb_device_handle *usb_device_hnd;	// Handle to the USB device to use

int openUSBDongle(unsigned int dongleNumber);

/**
 * Initialize the hardware subsystem
 *
 * @param dongleNumber The number of the dongle to claim
 *
 * @return The error code (LIBUSB_ERROR_CODES and custom codes)
 */
int initializeUSBsession(unsigned int dongleNumber) {
	// Initialize the USB library
 	if ( libusb_init(&usb_libContext) != 0 ) {
 		// Error initializing libUSB
 		ROS_FATAL_STREAM(ros::this_node::getName() << ": Unable to initialize libUSB");
 		return( USB_INIT_FAILED );
 	}

 	// Open the radio dongle
 	int err = openUSBDongle(dongleNumber);
 	if (err != 0) {
 		return( err );
 	}

	// Check the dongle version
	// It is stored in Binary-Coded-Decimal format
	float deviceVersion = 0;
	libusb_device_descriptor ddDescriptor;
	libusb_get_device_descriptor(usb_device, &ddDescriptor);
	deviceVersion += (float) ((ddDescriptor.bcdDevice >> 12) & 0x000f) * 10;
	deviceVersion += (float) ((ddDescriptor.bcdDevice >>  8) & 0x000f);
	deviceVersion += (float) ((ddDescriptor.bcdDevice >>  4) & 0x000f) / 10;
	deviceVersion += (float) ((ddDescriptor.bcdDevice >>  0) & 0x000f) / 100;
	
	ROS_INFO_STREAM(ros::this_node::getName() << ": Dongle version: " << deviceVersion);
	if (deviceVersion < 0.3) {
		ROS_ERROR_STREAM(ros::this_node::getName() << ": Dongle version too old. Must be >= 0.3");
		return USB_DONGLE_OLD;
	}

	// Set the USB configuration and claim the interface
	libusb_set_configuration(usb_device_hnd, 1);
	err = libusb_claim_interface(usb_device_hnd, 0);
	if (err != 0){
		ROS_FATAL_STREAM(ros::this_node::getName() << ": Unable to claim dongle interface");
		return( err );
	}

	return( USB_NO_ERROR );
}


/*
 * Open the USB dongle specified.
 *
 * @param dongleNumber The dongle to open
 *
 * @return libUSB error code or USB_ERROR_CODE
 */
int openUSBDongle(unsigned int dongleNumber) {
	libusb_device **deviceList;
 	unsigned int numDevices = 0;
 	unsigned int currentDongle = 0;

	// Get a complete list of all USB devices on the system
	numDevices = libusb_get_device_list(usb_libContext, &deviceList);
	if (numDevices < 0) {
		// An error occured while getting the device list
		ROS_FATAL_STREAM(ros::this_node::getName() << ": Error getting device list: " << std::string( libusb_strerror((libusb_error) numDevices) ) );
		return( USB_DONGLE_CLAIM_FAILED );
	}

	unsigned int dongleClaimed = 0;
	// Iterate over every device
	for (unsigned int i = 0; i < numDevices; i++) {
		libusb_device *devCurrent = deviceList[i];
		libusb_device_descriptor ddDescriptor;

		libusb_get_device_descriptor(devCurrent, &ddDescriptor);

		// Check if the device is a dongle
		if ( (ddDescriptor.idVendor == USB_VENDOR_ID) &&
			 (ddDescriptor.idProduct == USB_PRODUCT_ID) ) {
			
			// Check for the desired dongle
			if (currentDongle == dongleNumber) {
				int err = libusb_open(devCurrent, &usb_device_hnd);
				if (err == 0) {
					ROS_INFO_STREAM(ros::this_node::getName() << ": Claimed dongle " << currentDongle);
					usb_device = devCurrent;
					dongleClaimed = 1;
				} else {;
					ROS_FATAL_STREAM(ros::this_node::getName() << ": Error claiming dongle: " << std::string( libusb_strerror((libusb_error) err) ) );
					dongleClaimed = 2;
				}
			}
			currentDongle++;
		}
	}
	libusb_free_device_list(deviceList, 1);

	// Do error processing
	ROS_DEBUG_STREAM(ros::this_node::getName() << ": Found " << currentDongle << " dongles");
	if (dongleClaimed == 1) {
		return( USB_NO_ERROR );
	} else if (currentDongle == 0) {
	    ROS_FATAL_STREAM(ros::this_node::getName() << ": No USB Dongles Present");
	    return( USB_DONGLE_CLAIM_FAILED );
	} else if (dongleClaimed == 0) {
		ROS_FATAL_STREAM(ros::this_node::getName() << ": Dongle " << dongleNumber << " does not exist");
		return( USB_DONGLE_CLAIM_FAILED );
	} else if (dongleClaimed = 2) {
		return( USB_DONGLE_CLAIM_FAILED );
	}
}

/**
 * Write control data to the radio to change settings.
 *
 * @param data Pointer to the data to send to the radio
 * @param len Length of the data buffer (in bytes)
 * @param requestType The request type
 * @param request The request field for the packet
 * @param val The value field of the control packet
 * @param ind The index field of the control packet
 *
 * @return libUSB error code for transaction
 */
int writeControlData(unsigned char *data, int len,
					 int requestType, int request,
					 int val, int ind) {

	// Hardcoded a timeout of 1000ms for the transaction
	int retVal = libusb_control_transfer(usb_device_hnd,
			requestType, request, val, ind,
			data, len, 1000);

	return( retVal );
}

/*
 * Write data chunks to the radio
 *
 * @param data Pointer to the data to send
 * @param len Length of the data buffer (in bytes)
 * @param sentLen Pointer to a variable to store the number of bytes actually sent
 * @param timeout The timeout for the operation (in milliseconds)
 *
 * @return libUSB error code for transaction
 */
int writeTransmissionData(unsigned char *data, int len, int *sentLen, unsigned int timeout) {
    // Variable to hold the amount of data actuall sent

	// Do the transaction
	int err = libusb_bulk_transfer(usb_device_hnd,
			(0x01 | LIBUSB_ENDPOINT_OUT), data, len,
			sentLen, timeout);

	return( err );
}

/*
 * Read a data buffer from the radio
 *
 * @param data Pointer to buffer to put received data
 * @param readLength Pointer to data length to read, passes out amount of data read
 * @param timeout The timeout for the operation (in milliseconds)
 *
 * @return USB_ERROR_CODE or libUSB error code
 */
int readTransmissionData(unsigned char *data, int *readLength, unsigned int timeout) {
	int actuallyRead;
	int err = libusb_bulk_transfer(usb_device_hnd,
			(0x01 | LIBUSB_ENDPOINT_IN), data, *readLength,
			&actuallyRead, timeout);

	*readLength = actuallyRead;

	return( err );
}


/**
 * close the dongle session
 */
void closeUSBsession() {
	ROS_INFO_STREAM(ros::this_node::getName() << ": Closing dongle");
	libusb_close(usb_device_hnd);
	libusb_exit(usb_libContext);
}