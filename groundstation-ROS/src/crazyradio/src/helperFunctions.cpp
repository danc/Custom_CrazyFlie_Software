// Project includes
#include "radioInterface.h"
#include "crtpDefs.h"

// System includes
#include "ros/ros.h"
#include <string>

int pingQuadcopter() {
	crtpPacket packet;

	packet.port = CRTP_Port_Link;
	packet.channel = CRTP_LinkChan_Echo;
	memset(packet.data, 0, CRTP_PAYLOAD_LENGTH);
	packet.data[0] = 'A';
	packet.dataLength = 1;

	// Wait for a response from the quadcopter
	unsigned char dataBuffer[64];
	crtpPacket receivedPacket;
	std::string receivedData;
/*
	int amountRead = 0;
	sendData( (unsigned char*) &packet.fullPacket, CRTP_PacketLength(packet), 1000);
	int retVal = receiveData( dataBuffer, 64, &amountRead, 1000);
*/

	for (int i = 0; i < 100; i++) {
		int amountRead = 0;
		sendData( (unsigned char*) &packet.fullPacket, CRTP_PacketLength(packet), 1000);
		int retVal = receiveData( dataBuffer, 64, &amountRead, 1000);
		ROS_DEBUG_STREAM("Read " << amountRead<< " bytes");
		memcpy(&receivedPacket.fullPacket, dataBuffer, amountRead);
		memset(dataBuffer, 0, 64);

		receivedData.append( (char*) receivedPacket.data );
		//ROS_DEBUG_STREAM("Read " << amountRead<< " bytes: " << dataBuffer);
	}
//	ROS_DEBUG_STREAM("Read " << amountRead<< " bytes");
//	memcpy(&receivedPacket.fullPacket, dataBuffer, amountRead);

	ROS_DEBUG_STREAM("Data: " << std::hex <<  receivedData.c_str());
}