#ifndef CRTPDEFS_H_
#define CRTPDEFS_H_

#define CRTP_PAYLOAD_LENGTH 31

// Crazyflie port structure
// From wiki.bitcraze.io/doc:crazyflie:crtp:index
typedef enum CRTP_Ports {
	CRTP_Port_Console = 0,
	CRTP_Port_Param = 2,
	CRTP_Port_Command = 3,
	CRTP_Port_Memory = 4,
	CRTP_Port_Logging = 5,
	CRTP_Port_Platform = 13,
	CRTP_Port_Link = 15,
} CRTP_Ports;

// Channels used by the link layer port
typedef enum CRTP_LinkChan {
  CRTP_LinkChan_Echo   = 0x00,		// Packet is returned
  CRTP_LinkChan_Source = 0x01,		// Packet sent containing string "Bitcraze Crazyflie"
  CRTP_LinkChan_Sink   = 0x02,		// Packet is ignored
} CRTP_LinkChan;

// Structure containing a CRTP packet
typedef struct __attribute__((packed)) crtpPacket {
	union {
		struct __attribute__((packed)) {
			uint8_t channel : 2;
			uint8_t reserved : 2;
			uint8_t port : 4;
			uint8_t data[CRTP_PAYLOAD_LENGTH];
		};
		uint8_t fullPacket[CRTP_PAYLOAD_LENGTH+1];
	};
	uint8_t dataLength;
} crtpPacket;

// Macro to compute the length of the CRTP packet for sending
#define CRTP_PacketLength(packet)	(packet.dataLength + 1)

#endif