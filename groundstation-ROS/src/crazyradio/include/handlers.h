#ifndef HANDLERS_H_
#define HANDLERS_H_

#include "crazy_msgs/associateQuadcopter.h"
#include "crazy_msgs/crtp_stream.h"


/*
 * Callback function to handle data transmission requests
 */
void handler_crtpTX(const crazy_msgs::crtp_stream::ConstPtr &msg);

/*
 * Callback function to provide the service to associate with a quadcopter
 * 
 */
void handler_associateQuad(crazy_msgs::associateQuadcopter::Request &request,
						   crazy_msgs::associateQuadcopter::Response &response);

#endif