#ifndef RADIOINTERFACE_H_
#define RADIOINTERFACE_H_

#include <stdint.h>

typedef enum radio_ARD_waitTime {
	r_ARD_250us = 0x00,
	r_ARD_500us = 0x01,
	r_ARD_750us = 0x02,
	r_ARD_1000us = 0x03,
	r_ARD_1250us = 0x04,
	r_ARD_1500us = 0x05,
	r_ARD_1750us = 0x06,
	r_ARD_2000us = 0x07,
	r_ARD_2250us = 0x08,
	r_ARD_2500us = 0x09,
	r_ARD_2750us = 0x0A,
	r_ARD_3000us = 0x0B,
	r_ARD_3250us = 0x0C,
	r_ARD_3500us = 0x0D,
	r_ARD_3750us = 0x0E,
	r_ARD_4000us = 0x0F,
} radio_ARD_waitTime;

typedef enum radio_power {
	r_power_MINUS_18_dBm = 0,
	r_power_MINUS_12_dBm = 1,
	r_power_MINUS_6_dBm = 2,
	r_power_0_dBm = 3
} radio_power;

typedef enum radio_dataRate {
	r_dataRate_250_KBPS = 0,
	r_dataRate_1_MBPS = 1,
	r_dataRate_2_MBPS = 2
} radio_dataRate;

typedef struct radio_settings {
	unsigned int channel;
	radio_power power;
	radio_dataRate dataRate;
	bool useACK;
	bool useACKlength;
	unsigned int ARD_ACK_Value;
	uint64_t address;
	unsigned int retryCount;
} radio_settings;

/**
 * Initialize the radio to default settings
 *
 * @param dongleNumber The radio dongle to open
 * @param startupSettings Settings to configure the radio to have
 *
 * @return Error code
 */
int initializeRadio(unsigned int dongleNumber, radio_settings startupSettings);

/*
 * Set whether the radio should wait for ACK packets.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param useACK True to use ACK, False to not
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, libusb_error otherwise
 */

int setUseACK(bool useACK, bool forceWrite);

/*
 * Set the length of the ACK payload the radio expects.
 *
 * This can be done in 2 ways:
 *   1) Set an ARD time.
 *   2) Set an ACK packet length
 *
 * If an ACK packet length is set, then the radio auto-calculates the ARD time using
 * the datarate and ACK packet length information.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newValue The new ARD time or ACK length to use
 * @param useACKlength Whether to use an ACK length (true) or ARD time (false)
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, libusb_error otherwise
 */
int setACKlength_ARDtime(unsigned int newValue, bool useACKlength, bool forceWrite);

/*
 * Set the number of retries for the radio.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param retryCount The number of times to try sending the packet (between 0 and 15)
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, libusb_error otherwise
 */
int setRetryCount(unsigned int retryCount, bool forceWrite);

/*
 * Set the RF channel that the radio operates on
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newChannel A number between 0 and 125 corresponding to the radio channel to use
 * @param forceWrite Whether to force writing of the value
 *
 * @return 0 on success, libusb_error otherwise
 */
int setChannel(unsigned int newChannel, bool forceWrite);

/**
 * Set the data transmission and receive rate of the radio.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newRate The new datarate to set the radio to
 * @param forceWrite Force the datarate to be written
 *
 * @return 0 on success, otherwise libusb_error code
 */
int setDataRate(radio_dataRate newRate, bool forceWrite);

/**
 * Configure the power level the radio will use.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newPower The power level to use for transmissions
 * @param forceWrite Force the wait time to use
 *
 * @return 0 on success, otherwise libusb_error code
 */
int setPower(radio_power newPower, bool forceWrite);

/**
 * Configure the address the radio uses.
 *
 * This function will compare the existing radio setting with the desired setting,
 * and will only issue the USB transaction if they differ. To force the USB transaction
 * to be issued no matter what, set the forceWrite flag.
 *
 * @param newAddr The address to set the radio to (must be 5 bytes long)
 * @param forceWrite Force the wait time to use
 *
 * @return 0 on success, otherwise libusb_error code
 */
int setAddress(uint64_t newAddr, bool forceWrite);

/*
 * This function will send raw data over the radio.
 *
 * @param data Pointer to the data array to send
 * @param dataLength Length of the data array
 * @param timeout Amount of time to wait before stopping transaction (in milliseconds)
 *
 * @return returns USB_ERROR_CODE or libUSB error code
 */
int sendData(unsigned char *data, unsigned int dataLength, unsigned int timeout);

/**
 * Function to receive data from the radio
 *
 * @param data Pointer to an array to hold the data
 * @param lenToRead Number of bytes to read in
 * @param lenRead Number of bytes actually read
 * @param timeout Amount of time to wait before stopping transaction (in milliseconds)
 *
 * @return libUSB error code
 */
int receiveData(unsigned char* data, int lenToRead, int *lenRead, unsigned int timeout);

/**
 * Close the radio
 */
int closeRadio();


#endif //RADIOINTERFACE_H_