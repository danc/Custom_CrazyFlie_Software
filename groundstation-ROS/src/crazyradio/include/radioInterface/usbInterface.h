/*
 * This overall file was created by Ian McInerney <Ian.S.McInerneny@ieee.org>
 * using some source code that is by Jan Winkler <winkler@cs.uni-bremen.de>
 *
 * The original sections are:
 * Copyright (c) 2013, Jan Winkler <winkler@cs.uni-bremen.de>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of Universität Bremen nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/



#ifndef USBINTERFACE_H_
#define USBINTERFACE_H_

#include <stdint.h>

// USB values for the CrazyRadio PA dongle
#define USB_VENDOR_ID	0x1915
#define USB_PRODUCT_ID	0x7777

/**
 * Error codes associated with the USB system
 */
typedef enum USB_ERROR_CODES {
	USB_NO_ERROR = 0,
	USB_INIT_FAILED,
	USB_DONGLE_CLAIM_FAILED,
	USB_NO_DONGLES,
	USB_DONGLE_OLD
} USB_ERROR_CODES;

/**
 * Initialize the hardware subsystem
 *
 * @param dongleNumber The number of the dongle to claim
 *
 * @return The error code (LIBUSB_ERROR_CODES and custom codes)
 */
int initializeUSBsession(unsigned int dongleNumber);

/**
 * Write control data to the radio to change settings.
 *
 * @param data Pointer to the data to send to the radio
 * @param len Length of the data buffer (in bytes)
 * @param requestType The request type
 * @param request The request field for the packet
 * @param val The value field of the control packet
 * @param ind The index field of the control packet
 *
 * @return libUSB error code for transaction
 */
int writeControlData(unsigned char *data, int len,
					 int requestType, int request,
					 int val, int ind);

/*
 * Write data chunks to the radio
 *
 * @param data Pointer to the data to send
 * @param len Length of the data buffer (in bytes)
 * @param sentLen Pointer to a variable to store the number of bytes actually sent
 * @param timeout The timeout for the operation (in milliseconds)
 *
 * @return libUSB error code for transaction
 */
int writeTransmissionData(unsigned char *data, int len, int *sentLen, unsigned int timeout);

/*
 * Read a data buffer from the radio
 *
 * @param data Pointer to buffer to put received data
 * @param readLength Pointer to data length to read, passes out amount of data read
 * @param timeout The timeout for the operation (in milliseconds)
 *
 * @return USB_ERROR_CODE or libUSB error code
 */
int readTransmissionData(unsigned char *data, int *readLength, unsigned int timeout);

/**
 * Remove the dongle
 */
void closeUSBsession();

#endif // USBINTERFACE_H_