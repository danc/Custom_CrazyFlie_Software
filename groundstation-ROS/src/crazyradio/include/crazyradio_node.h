#ifndef CRAZYRADIO_NODE_H_
#define CRAZYRADIO_NODE_H_

#include <string>
#include <vector>
#include <stdint.h>

typedef struct quadcopterData {
	uint8_t channel;
	uint64_t address;
	uint8_t assignedNumber;
	std::string topic_RX;
} quadcopterData;

extern std::vector<quadcopterData> associatedQuads;

#endif