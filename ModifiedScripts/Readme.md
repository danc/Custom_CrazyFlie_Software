The two files in the Version Fixing directory are derived from the fix to CrazyFlie software issue #38 on their GitHub (https://github.com/bitcraze/crazyflie-firmware/issues/38)
This issue prevents the compilation from working when it is based upon the tarball.

To fix this, copy the two files into the scripts/ directory of the source tree and replace the existing files of the same name.
