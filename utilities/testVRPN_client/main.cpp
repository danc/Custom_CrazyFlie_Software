#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <quat.h>

#include <vrpn_Connection.h>
#include <vrpn_Tracker.h>
#include <sys/time.h>

using namespace std;

ofstream logFile;

vrpn_Connection *connection;
vrpn_Tracker_Remote *tracker;

void VRPN_CALLBACK trackerCallback( void*, const vrpn_TRACKERCB t);

// Function to open the log file
void openLog();

int main( int argc, char *argv[] ) {
	char server[255];
	char trackableName[255];

	if ( argc == 3 ) {
		strcpy(server, argv[1]);
		strcpy(trackableName, argv[2]);
	} else {
		std::cout << "Usage: " << argv[0] << " serverIP trackableName" << std::endl;
		return( 0 );
	}

	// Open the log file
	openLog();

	// Create a connection to the server
	connection = vrpn_get_connection_by_name(server);

	// Create the tracker listener
	tracker = new vrpn_Tracker_Remote(trackableName, connection);
	tracker->register_change_handler(0, trackerCallback);

	std::cout << "Created VRPN Connection" << std::endl;

	while(true) {
		tracker->mainloop();
		usleep(10);
	}

	return( 0 );
}


void VRPN_CALLBACK trackerCallback(void*, const vrpn_TRACKERCB t) {
	// Variable to hold the previous loop time
	static double previousCompLoop = 0;
	static double previousVRPNloop = 0;

	// Get the current computer time
	timeval compTime;
	gettimeofday(&compTime, NULL);
	double computerProgramClock = compTime.tv_sec + ( (double) compTime.tv_usec / 1000000 );

	double computerLoopTime = (computerProgramClock - previousCompLoop);
	previousCompLoop = computerProgramClock;

	// Get the VRPN message time
	struct timeval vrpnTime = t.msg_time;
	double currentVRPNclock = vrpnTime.tv_sec + ( (double) vrpnTime.tv_usec / 1000000 );
	double vrpnLoopTime = currentVRPNclock - previousVRPNloop;
	previousVRPNloop = currentVRPNclock;

	// Record the current computer time
	logFile << computerProgramClock / (double) CLOCKS_PER_SEC << "\t\t";
	logFile << computerLoopTime << "\t\t";

	// Record the VRPN server time
	logFile << currentVRPNclock << "\t\t";
	logFile << vrpnLoopTime << "\t\t";

	// Write out the X Y Z data
	logFile << t.pos[0] << "\t\t";
	logFile << t.pos[1] << "\t\t";
	logFile << t.pos[2] << "\t\t";

	// Record the Euler angles
	q_vec_type euler;
	q_to_euler( euler, t.quat);
	logFile << euler[2] << "\t\t";
	logFile << euler[1] << "\t\t";
	logFile << euler[0] << "\t\t";

	// Record the quaternion information
	logFile << t.quat[0] << "\t\t";
	logFile << t.quat[1] << "\t\t";
	logFile << t.quat[2] << "\t\t";
	logFile << t.quat[3] << "\t\t";

	logFile << endl;

}

void openLog( ) {
	char logFileName[40];
	char timeString[40];
	time_t rawtime;
	time( &rawtime );
	struct tm *timeinfo = localtime( &rawtime );
	strftime(timeString, 40, "%Y_%m_%d_%H-%M-%S", timeinfo);

	// Create the log file name (including date)
	sprintf(logFileName, "vrpnTestLog_%s.txt", timeString);
	
	printf("Opening log file %s...", logFileName);

	try {
		logFile.open(logFileName, ios_base::out);
	} catch (...) {
		cout << "Error opening logfile";
		exit( -1 );
	}

	// Place the header information into the log file
	logFile << "#VRPN_test" << endl;
	logFile << "#Time\t\t" << timeString << endl;
	logFile << "%Time\t\tcompTimeDiff\t\tvrpnTime\t\tvrpnTimeDiff\t\tX\t\tY\t\tZ\t\tRoll\t\tPitch\t\tYaw\t\tq0\t\tq1\t\tq2\t\tq3" << endl;
	logFile << "&sec\t\tsec\t\tsec\t\tsec\t\tm\t\tm\t\tm\t\trad\t\trad\t\trad\t\traw\t\traw\t\traw\t\traw" << endl;
	cout << " Complete" << endl;
}
