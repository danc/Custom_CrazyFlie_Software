figure;

subplot(2,1,1);
plot(testData.compTimeDiff.data(2:end), 'b*');
title('Receiving time delta');
xlabel('Sample');
ylabel('Time delta (s)');


subplot(2,1,2);
plot(testData.vrpnTimeDiff.data(2:end), 'r*');
title('Sampling time delta');
xlabel('Sample');
ylabel('Time delta (s)');