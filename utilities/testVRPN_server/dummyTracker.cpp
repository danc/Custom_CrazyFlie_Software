#include "dummyTracker.h"

#include <iostream>
#include <quat.h>
#include <stdint.h>
#include <vrpn_Tracker.h>
#include <vrpn_Connection.h>


/**
 * Create the tracker object.
 *
 * @param trackerName What to name the tracker object
 * @param c The VRPN connection to attach to
 *
 */
DummyTracker::DummyTracker( uint8_t sensorID,  const char *trackerName, vrpn_Connection *c) : vrpn_Tracker( trackerName, c) {
	m_sensorID = sensorID;
	m_trackerName = trackerName;

	register_server_handlers();
}

/**
 * The main tracker loop
 */
void DummyTracker::mainloop() {
	// Get the current time
	vrpn_gettimeofday(&m_timestamp, NULL);

	vrpn_Tracker::timestamp = m_timestamp;

	// Create the fake data
	// The X, Y, Z position
	pos[0] = 1;
	pos[1] = 0.5;
	pos[2] = 0.25;

	// The Roll, Pitch, Yaw
	q_vec_type euler;
	euler[Q_ROLL] = Q_DEG_TO_RAD( 10 );
	euler[Q_PITCH] = Q_DEG_TO_RAD( 15 );
	euler[Q_YAW] = Q_DEG_TO_RAD( 5 );

	q_from_euler( d_quat, euler[Q_YAW], euler[Q_PITCH], euler[Q_ROLL] );

	char msgbuf[1000];

	d_sensor = 0;

	int len = vrpn_Tracker::encode_to( msgbuf );

	if ( d_connection->pack_message( len, m_timestamp, position_m_id, d_sender_id, msgbuf,
		 							 vrpn_CONNECTION_LOW_LATENCY) ) {
		std::cout << "Error writing message for tracker " << m_trackerName << std::endl;
	}

	server_mainloop();
}
