#ifndef DUMMYTRACKER_H_
#define DUMMYTRACKER_H_

#include <stdint.h>
#include <vrpn_Tracker.h>
#include <vrpn_Connection.h>

/*
 * This class provides a dummy tracker object
 */
class DummyTracker : public vrpn_Tracker {

public:
	DummyTracker( uint8_t sensorID, const char *trackerName, vrpn_Connection *c = NULL );
	virtual ~DummyTracker() {};

	virtual void mainloop();


protected:
	const char *m_trackerName;
	uint8_t m_sensorID;
	struct timeval m_timestamp;
};


#endif
