#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include "dummyTracker.h"
#include <stdlib.h>
#include <vrpn_Connection.h>


int main( int argc, char *argv[] ) {
	char trackerName[255];
	char interface[255];
	uint32_t period;
	uint16_t port;


	// Read in the arguments for the program
	if ( argc == 5 ) {
		strcpy(trackerName, argv[1]);
		strcpy(interface, argv[2]);
		port = atoi(argv[3]);
		period = atoi(argv[4]);
	} else {
		std::cout << "Not enough arguments" << std::endl;
		std::cout << "Usage: " << argv[0] << " trackerName interface port usPeriod" << std::endl;
		return(0);
	}

	std::cout << "Creating server on interface " << interface << ":" << port;
	std::cout << " with trackable " << trackerName << " and send interval " << period << "us" << std::endl;

	// Create the VRPN server
	vrpn_Connection* connec = vrpn_create_server_connection( port, NULL, NULL, interface );

	// Create a tracker
	DummyTracker *tracker1 = new DummyTracker(0, trackerName, connec);

	std::cout << "Created VRPN Server" << std::endl;

	while(true) {
		tracker1->mainloop();
		connec->mainloop();
		usleep(period);
	}

	return( 0 );
}
